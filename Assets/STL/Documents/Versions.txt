1.5  (Sep 4, 2017)
	– Added support for SkinnedMeshRenderer bone deformation.
	– Fixed an issue with models being mirroed on the x-axis.
	– Fixed an issue with normals that were not guaranteed to be normalized.
	– Fixed an issue with ASCII files that was allowed to have negative vertices.
	– Deprecated the ExportBinary and ExportText methods.
	– Added new method Export functions for better binary/ascii consistency.
	– Added a success status flag that is returned by new Export method.
	– Added a code reference pdf.
	– Moved the versions text to the text file you are reading.
	
1.4  (Dec 9, 2015)
	– Fixed warning introduced in Unity 5.3

1.3  (Sep 18, 2015)
	– BEWARE that this update will break previous code!
	– Added a runtime example.
	– Added support for SkinnedMeshRenderers.
	– Changed the naming of methods.
	– Removed script reference in the top of the script.
	– Added inline documentation.

1.2  (Jul 09, 2014)
	– Fixed an out of memory issue with ExportText() when exporting extremely large meshes.
	– Removed a warning message that was displayed when cancelling an export in the editor.
	
1.1 
	– Fixed a float formating bug in exported text based STL files.
	– Fixed a missing end statement in exported text based STL files.
	
1.0 (May 17, 2012)
	– initial version
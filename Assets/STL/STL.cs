/*
	AUTHOR
	=============
	Created by Carl Emil Carlsen.
	Copyright 2015-2017 Sixth Sensor.
	All rights reserved.
	http://sixthsensor.dk


	LICENSE
	=======
	This is a Unity Asset Store product.
	https://www.assetstore.unity3d.com/en/#!/content/3397
	

	VERSION
	=============
	1.5
	

	DESCRIPTION
	=============
	STL is an extension for Unity that enables export of meshes to the STL file format.
	STL files are widely used for rapid prototyping and computer-aided manufacturing (3D printing).
*/	

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;


public class STL
{
	const string logPrepend = "<b>[STL]</b> ";


	/// <summary>
	/// Exports all meshes found in MeshFilter and SkinnedMeshRenderer components attached to the supplied game object (and it's children) to a binary stl file at specified file path formated as binary (default) or ASCII. Returns success status.
	/// </summary>
	public static bool Export( GameObject gameObject, string filePath, bool asASCII = false )
	{
		return Export( new GameObject[]{ gameObject }, filePath, asASCII );
	}


	/// <summary>
	/// Exports all meshes found in MeshFilter and SkinnedMeshRenderer components attached to the supplied game objects (and their children) to a binary stl file at specified file path as binary (default) or ASCII. Returns success status.
	/// </summary>
	public static bool Export( GameObject[] gameObjects, string filePath, bool asASCII = false )
	{
		Mesh[] meshes;
		Matrix4x4[] matrices;
		GetMeshesAndMatrixes( gameObjects, out meshes, out matrices );
		return Export( meshes, matrices, filePath, asASCII );
	}


	/// <summary>
	/// Exports mesh found in supplied MeshFilter to a binary stl file at specified file path formated as binary (default) or ASCII. Returns success status.
	/// </summary>
	public static bool Export( MeshFilter filter, string filePath, bool asASCII = false )
	{
		if( !filter.sharedMesh ){
			Debug.LogError( logPrepend + "Export failed. Meshfilter has no mesh.\n" );
			return false;
		}
		return Export(new MeshFilter[]{ filter }, filePath, asASCII );
	}


	/// <summary>
	/// Exports all meshes found in supplied MeshFilters to a binary stl file at specified file path formated as binary (default) or ASCII. Returns success status.
	/// </summary>
	public static bool Export( MeshFilter[] filters, string filePath, bool asASCII = false )
	{
		Mesh[] meshes;
		Matrix4x4[] matrices;
		GetMeshesAndMatrixes( filters, out meshes, out matrices );
		return Export( meshes, matrices, filePath, asASCII );
	}


	/// <summary>
	/// Exports mesh found in supplied SkinnedMeshRenderer component to a binary stl file at specified file path formated as binary (default) or ASCII. Returns success status.
	/// </summary>
	public static bool Export( SkinnedMeshRenderer skin, string filePath, bool asASCII = false )
	{
		if( !skin.sharedMesh ){
			Debug.LogError( logPrepend + "Export failed. SkinnedMeshRenderer has no mesh.\n" );
			return false;
		}
		return Export( new SkinnedMeshRenderer[]{ skin }, filePath, asASCII );
	}


	/// <summary>
	/// Exports all meshes found in supplied SkinnedMeshRenderer components to a binary stl file at specified file path formated as binary (default) or ASCII. Returns success status.
	/// </summary>
	public static bool Export( SkinnedMeshRenderer[] skins, string filePath, bool asASCII = false )
	{
		Mesh[] meshes;
		Matrix4x4[] matrices;
		GetMeshesAndMatrixes( skins, out meshes, out matrices );
		return Export( meshes, matrices, filePath, asASCII );
	}


	/// <summary>
	/// Exports a mesh to a binary stl file at specified file path formated as binary (default) or ASCII. Returns success status.
	/// </summary>
	public static bool Export( Mesh mesh, string filePath, bool asASCII = false )
	{
		return Export( new Mesh[]{ mesh }, new Matrix4x4[]{ Matrix4x4.identity }, filePath, asASCII );
	}


	/// <summary>
	/// Exports a mesh to a binary stl file at specified file path formated as binary (default) or ASCII. Returns success status.
	/// </summary>
	public static bool Export( Mesh[] meshes, string filePath, bool asASCII = false )
	{
		Matrix4x4[] matrices = new Matrix4x4[meshes.Length];
		for( int m = 0; m < matrices.Length; m++ ) matrices[m] = Matrix4x4.identity;
		return Export( meshes, matrices, filePath, asASCII );
	}


	/// <summary>
	/// Exports a mesh with matrix transformation to a binary stl file at specified file path formated as binary (default) or ASCII. Returns success status.
	/// </summary>
	public static bool Export( Mesh mesh, Matrix4x4 matrix, string filePath, bool asASCII = false )
	{
		return Export( new Mesh[]{ mesh }, new Matrix4x4[]{ matrix }, filePath, asASCII );
	}


	/// <summary>
	/// Exports meshes with matrix transformations to a stl file at specified file path formated as binary (default) or ASCII. Returns success status.
	/// </summary>
	public static bool Export( Mesh[] meshes, Matrix4x4[] matrices, string filePath, bool asASCII = false )
	{
		if( asASCII ) return ExportSTLAsASCII( meshes, matrices, filePath );
		else return ExportSTLAsBinary( meshes, matrices, filePath );
	}


	/// <summary>
	/// Gets shared meshes and model matrixes from all MeshFilter and SkinnedMeshRenderer components found in provided GameObjects and their children.
	/// </summary>
	public static void GetMeshesAndMatrixes( GameObject[] objects, out Mesh[] meshes, out Matrix4x4[] matrices )
	{
		List<Mesh> meshList = new List<Mesh>();
		List<Matrix4x4> matrixList = new List<Matrix4x4>();

		for( int g = 0; g < objects.Length; g++ )
		{
			MeshFilter[] filters = objects[g].GetComponentsInChildren<MeshFilter>();
			GetMeshesAndMatrixes( filters, out meshes, out matrices );
			meshList.AddRange( meshes );
			matrixList.AddRange( matrices );

			SkinnedMeshRenderer[] skins = objects[g].GetComponentsInChildren<SkinnedMeshRenderer>();
			GetMeshesAndMatrixes( skins, out meshes, out matrices );
			meshList.AddRange( meshes );
			matrixList.AddRange( matrices );
		}

		meshes = meshList.ToArray();
		matrices = matrixList.ToArray();
	}


	/// <summary>
	/// Gets shared meshes and model matrixes from all provided MeshFilter components.
	/// </summary>
	public static void GetMeshesAndMatrixes( MeshFilter[] filters, out Mesh[] meshes, out Matrix4x4[] matrices )
	{
		List<Mesh> meshList = new List<Mesh>();
		List<Matrix4x4> matrixList = new List<Matrix4x4>();
		for( int f=0; f<filters.Length; f++ ){
			if( filters[f] && filters[f].sharedMesh ){
				meshList.Add( filters[f].sharedMesh );
				matrixList.Add( filters[f].transform.localToWorldMatrix );
			}
		}
		meshes = meshList.ToArray();
		matrices = matrixList.ToArray();
	}


	/// <summary>
	/// Gets shared meshes and model matrixes from all provided SkinnedMeshRenderer components.
	/// </summary>
	public static void GetMeshesAndMatrixes( SkinnedMeshRenderer[] skins, out Mesh[] meshes, out Matrix4x4[] matrices )
	{
		List<Mesh> meshList = new List<Mesh>();
		List<Matrix4x4> matrixList = new List<Matrix4x4>();
		for( int s=0; s<skins.Length; s++ ){
			if( skins[s] && skins[s].sharedMesh ){
				Mesh mesh = new Mesh();
				mesh.name = skins[s].sharedMesh.name;
				skins[s].BakeMesh( mesh );
				meshList.Add( mesh );
				matrixList.Add( skins[s].transform.localToWorldMatrix );
			}
		}
		meshes = meshList.ToArray();
		matrices = matrixList.ToArray();
	}
		

	static bool ExportSTLAsBinary( Mesh[] meshes, Matrix4x4[] matrices, string filePath )
	{
		// Check array lengths.
		if( meshes.Length != matrices.Length ){
			Debug.LogError( logPrepend + "Mesh array length and matrix array length must match.\n" );
			return false;
		}

		try
		{
			using( BinaryWriter writer = new BinaryWriter( File.Open( filePath, FileMode.Create ) ) )
			{
				// Write header.
				writer.Write( new char[ 80 ] );
				
				// Count all triangles and write.
				int triangleIndexCount = 0;
				foreach( Mesh mesh in meshes ) {
					for( int s = 0; s < mesh.subMeshCount; s++ ) triangleIndexCount += mesh.GetTriangles( s ).Length;
				}
				uint triangleCount = (uint) ( triangleIndexCount / 3 );
				writer.Write( triangleCount );
				
				// For each mesh ...
				int i;
				short attribute = 0;
				Vector3 u, v;
				Vector3 normal = Vector3.zero;
				int[] triangles;
				Vector3[] vertices;
				for( int m=0; m<meshes.Length; m++ )
				{
					// Get matrix and correct mirrored x-axis.
					Matrix4x4 matrix = Matrix4x4.Scale( new Vector3( -1, 1, 1 ) ) * matrices[m];

					// Get vertices and tranform them.
					vertices = meshes[m].vertices;
					for( int vx = 0; vx < vertices.Length; vx++ ) vertices[vx] = matrix.MultiplyPoint( vertices[ vx ] );
					
					// For each sub mesh ...
					for( int s = 0; s < meshes[m].subMeshCount; s++ )
					{
						// Get trianlges.
						triangles = meshes[m].GetTriangles( s );
						
						// For each triangle ...
						for( int t = 0; t < triangles.Length; t += 3 )
						{
							// Calculate and write normal.
							u = vertices[ triangles[t+1] ] - vertices[ triangles[t] ];
							v = vertices[ triangles[t+2] ] - vertices[ triangles[t] ];
							normal.Set( u.y * v.z - u.z * v.y, u.z * v.x - u.x * v.z, u.x * v.y - u.y * v.x );
							normal.Normalize();
							for( i = 0; i < 3; i++ ) writer.Write( normal[i] );
							
							// Write vertices.
							for( i = 0; i < 3; i++ ) writer.Write( vertices[ triangles[ t+2 ] ][i] );
							for( i = 0; i < 3; i++ ) writer.Write( vertices[ triangles[ t+1 ] ][i] );
							for( i = 0; i < 3; i++ ) writer.Write( vertices[ triangles[ t ] ][i] );
							
							// Write attribute byte count.
							writer.Write( attribute );
						}
					}
				}
				
				// End of file.
				writer.Close();
			}
		}
		catch( System.Exception e ){
			Debug.LogWarning( logPrepend + "Failed exporting binary STL file at: " + filePath + "\n" + e );
			return false;
		}

		// Success!
		return true;
	}


	static bool ExportSTLAsASCII( Mesh[] meshes, Matrix4x4[] matrices, string filePath )
	{
		// Check array lengths.
		if( meshes.Length != matrices.Length ){
			Debug.LogError( logPrepend + "Mesh array length and matrix array length must match.\n" );
			return false;
		}

		// Transform all vertices while checking bounds.
		Bounds bounds = new Bounds();
		List<Vector3[]> transformedVertices = new List<Vector3[]>();
		for( int m=0; m<meshes.Length; m++ )
		{
			// Get matrix and correct mirrored x-axis.
			Matrix4x4 matrix = Matrix4x4.Scale( new Vector3( -1, 1, 1 ) ) * matrices[m];

			// Get vertices and tranform them while computing bounds.
			Vector3[] vertices = meshes[m].vertices;
			for( int vx = 0; vx < vertices.Length; vx++ ){
				vertices[vx] = matrix.MultiplyPoint( vertices[ vx ] );
				if( m == 0 && vx == 0 ) bounds.SetMinMax( vertices[vx], vertices[vx] );
				else bounds.Encapsulate( vertices[vx] );
			}
			transformedVertices.Add( vertices );
		}

		// ASCII STL files may not contain vertices in negative space because of sign-mantissa-"e"-sign-exponent formating.
		// Translate if needed.
		// https://en.wikipedia.org/wiki/STL_(file_format)
		if( bounds.min.x < 0 || bounds.min.y < 0 || bounds.min.z < 0 ){
			Vector3 safeOfffset = - new Vector3( Mathf.Min( bounds.min.x, 0 ), Mathf.Min( bounds.min.y, 0 ), Mathf.Min( bounds.min.z, 0 ) );
			for( int m=0; m<meshes.Length; m++ ){
				Vector3[] vertices = transformedVertices[m];
				for( int vx = 0; vx < vertices.Length; vx++ ) vertices[vx] += safeOfffset;
			}
		}
		
		// Begin export ...
		try
		{
			bool append = false;
			using( StreamWriter sw = new StreamWriter( filePath, append ) ) 
			{
				string name = "Unity Mesh";

				// Write header to disk.
				sw.WriteLine( "solid " + name );
				
				// For each mesh filter ...
				Vector3 u, v;
				Vector3 normal = Vector3.zero;
				int[] triangles;
				Vector3[] vertices;
				System.Globalization.CultureInfo ci = System.Globalization.CultureInfo.CreateSpecificCulture( "en-US" );
				for( int m=0; m<meshes.Length; m++ )
				{
					// Create a new string builder for each mesh to avoid out of memory errors.
					StringBuilder sb = new StringBuilder();

					// Get transformed vertices.
					vertices = transformedVertices[m];
					
					// For each sub mesh ...
					for( int s = 0; s < meshes[m].subMeshCount; s++ )
					{
						// Get trianlges.
						triangles = meshes[m].GetTriangles( s );
						
						// For each triangle ...
						for( int t = 0; t < triangles.Length; t += 3 )
						{
							// Calculate and write normal.
							u = vertices[ triangles[ t+1 ] ] - vertices[ triangles[t] ];
							v = vertices[ triangles[ t+2 ] ] - vertices[ triangles[t] ];
							normal.Set( u.y * v.z - u.z * v.y, u.z * v.x - u.x * v.z, u.x * v.y - u.y * v.x );
							normal.Normalize();
							sb.AppendLine( "facet normal " + normal.x.ToString("e",ci) + " " + normal.y.ToString("e",ci) + " " + normal.z.ToString("e",ci) );

							// Begin triangle.
							sb.AppendLine( "outer loop" );
							
							// Write vertices.
							sb.AppendLine( "vertex " + vertices[ triangles[ t+2 ] ].x.ToString("e",ci) + " " + vertices[ triangles[ t+2 ] ].y.ToString("e",ci) + " " + vertices[ triangles[ t+2 ] ].z.ToString("e",ci) );
							sb.AppendLine( "vertex " + vertices[ triangles[ t+1 ] ].x.ToString("e",ci) + " " + vertices[ triangles[ t+1 ] ].y.ToString("e",ci) + " " + vertices[ triangles[ t+1 ] ].z.ToString("e",ci) );
							sb.AppendLine( "vertex " + vertices[ triangles[ t ] ].x.ToString("e",ci) + " " + vertices[ triangles[ t ] ].y.ToString("e",ci) + " " + vertices[ triangles[ t ] ].z.ToString("e",ci) );

							// End triangle.
							sb.AppendLine( "endloop" );
							sb.AppendLine( "endfacet" );
						}
					}
					
					// Write string builder memory to the disk.
					sw.Write( sb.ToString() );
				}
				
				// Write ending to disk and close writer.
				sw.WriteLine( "endsolid " + name );
				sw.Close();
			}
		}
		catch( System.Exception e ){
			Debug.LogWarning( logPrepend + "Failed exporting ASCII STL file at: " + filePath + "\n" + e );
			return false;
		}

		// Success!
		return true;
	}
	
	
	static string DateTimeCode()
	{
		return System.DateTime.Now.ToString("yy") + System.DateTime.Now.ToString("MM") + System.DateTime.Now.ToString("dd") + "_" + System.DateTime.Now.ToString("hh") + System.DateTime.Now.ToString("mm") + System.DateTime.Now.ToString("ss");
	}


	/// <summary>
	/// Deprecated. Use the Export method instead.
	/// </summary>
	[System.Obsolete( "Deprecated. Use the Export method instead." )]
	public static void ExportBinary( GameObject[] gameObjects, string filePath )
	{
		Export( gameObjects, filePath );
	}


	/// <summary>
	/// Deprecated. Use the Export method instead.
	/// </summary>
	[System.Obsolete( "Deprecated. Use the Export method instead." )]
	public static void ExportBinary( MeshFilter[] filters, string filePath )
	{
		Export( filters, filePath );
	}


	/// <summary>
	/// Deprecated. Use the Export method instead.
	/// </summary>
	[System.Obsolete( "Deprecated. Use the Export method instead." )]
	public static void ExportBinary( SkinnedMeshRenderer[] skins, string filePath )
	{
		Export( skins, filePath );
	}


	/// <summary>
	/// Deprecated. Use the Export method instead.
	/// </summary>
	[System.Obsolete( "Deprecated. Use the Export method instead." )]
	public static void ExportBinary( Mesh mesh, Matrix4x4 matrix, string filePath )
	{
		Export( mesh, filePath );
	}


	/// <summary>
	/// Deprecated. Use the Export method instead.
	/// </summary>
	[System.Obsolete( "Deprecated. Use the Export method instead." )]
	public static void ExportBinary( Mesh[] meshes, Matrix4x4[] matrices, string filePath )
	{
		Export( meshes, matrices, filePath );
	}


	/// <summary>
	/// Deprecated. Use the Export method instead.
	/// </summary>
	[System.Obsolete( "Deprecated. Use the Export method instead." )]
	public static void ExportText( GameObject[] gameObjects, string filePath )
	{
		bool asASCII = true;
		Export( gameObjects, filePath, asASCII );
	}


	/// <summary>
	/// Deprecated. Use the Export method instead.
	/// </summary>
	[System.Obsolete( "Deprecated. Use the Export method instead." )]
	public static void ExportText( Mesh mesh, Matrix4x4 matrix, string filePath )
	{
		bool asASCII = true;
		Export( mesh, filePath, asASCII );
	}


	/// <summary>
	/// Deprecated. Use the Export method instead.
	/// </summary>
	[System.Obsolete( "Deprecated. Use the Export method instead." )]
	public static void ExportText( Mesh[] meshes, Matrix4x4[] matrices, string filePath )
	{
		bool asASCII = true;
		Export( meshes, filePath, asASCII );
	}
}
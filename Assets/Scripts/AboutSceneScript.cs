﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class AboutSceneScript : MonoBehaviour {

    public static string versionNumber = "1.0";
    public static System.DateTime releaseDate = new System.DateTime(2017, 6, 4);

    public void OnButtonWeb()
    {
        string URL = "http://www.ilovefractals.cz";
        Application.OpenURL(URL);
    }

    public void OnButtonBack()
    {
        MySceneManager.Instance.PopScene();
    }

	// Use this for initialization
	void Start () {
        Text versionText = GameObject.Find("Version Text").GetComponent<Text>();
        versionText.text = string.Format("Fractals - Version {0}\nCreated by Martin Holla @ {1}", versionNumber, GetReleaseDateString());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    string GetReleaseDateString()
    {
        return releaseDate.ToString("D", CultureInfo.CreateSpecificCulture("en-US"));// string.Format("{0}", releaseDate);
    }
}

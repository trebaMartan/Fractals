﻿using UnityEngine;
using System.Collections;
using System;

public class ColorPaletteDiscretizer : ICloneable
{
    public ColorPaletteDiscretizer(GetColorSetting colorComputation, int colorIndexCount)
    {
        ColorDelegate = colorComputation;
        ColorIndexCount = colorIndexCount;
    }

    ColorPaletteDiscretizer(ColorPaletteDiscretizer old)
    {
        ColorIndexCount = old.ColorIndexCount;
        ColorDelegate = old.ColorDelegate;
    }

    public object Clone()
    {
        ColorPaletteDiscretizer obj = new ColorPaletteDiscretizer(this);
        return obj;
    }

    private int m_ColorIndexCount = 1;
    public int ColorIndexCount
    {
        get { return m_ColorIndexCount; }
        set
        {
            if (value < 1)
            {
                Debug.Assert(false, "Wrong color count");
                return;
            }

            m_ColorIndexCount = value;
            m_colors = new Color[m_ColorIndexCount];
            m_dirty = true;
        }
    }


    public delegate Color GetColorSetting(float ratio);
    private GetColorSetting m_ColorDelegate = null;
    public GetColorSetting ColorDelegate
    {
        get { return m_ColorDelegate; }
        set { m_ColorDelegate = value; }
    }

    public Color GetDiscretizedColor(float ratio)
    {
        if (m_ColorIndexCount < 1)
        {
            return Color.black;
        }
        //if (m_ColorIndexCount == 1)
        //{
        //    return GetColor(0);
        //}
        int index = Mathf.FloorToInt(ratio * m_ColorIndexCount);//zxc optimize
        return GetColor(index);
    }

    public Color GetColor(byte colorIndex)
    {
        return GetColor((int)colorIndex);
    }

    public Color GetColor(int colorIndex)
    {
        if (m_dirty)
        {
            RecomputeColors();
        }
        if (colorIndex < 0)
        {
            throw new System.ArgumentException("index is out of range");
        }
        if (colorIndex >= m_ColorIndexCount)
        {
            return m_colors[m_ColorIndexCount - 1];
        }
        return m_colors[colorIndex];
    }

    void RecomputeColors()
    {
        m_dirty = false;
        if (m_ColorIndexCount < 1)
            return;
        m_colors = new Color[m_ColorIndexCount];
        if (m_ColorIndexCount == 1)
        {
            m_colors[0] = m_ColorDelegate(0);
            return;
        }
        for (int i = 0; i < m_ColorIndexCount; i++)
        {
            float f = i / (float)(m_ColorIndexCount - 1);
            Color c = m_ColorDelegate(f);
            m_colors[i] = c;
        }
    }

    private Color[] m_colors = null;
    private bool m_dirty = true;
}

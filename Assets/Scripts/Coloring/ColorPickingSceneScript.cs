﻿using Assets.Scripts.Utils;
using UnityEngine;
using UnityEngine.UI;

public class ColorPickingSceneScript : MonoBehaviour
{
    public void SliderRChanged()
    {
        // Debug.Log(mainSlider.value);
        GameObject go = GameObject.Find("Slider R");
        if (!go)
            return;
        Slider s = go.GetComponent<Slider>();

        if (m_color.r != s.value)
        {
            m_color.r = s.value;
            OnColorChanged();
        }
    }
    public void SliderGChanged()
    {
        GameObject go = GameObject.Find("Slider G");
        if (!go)
            return;
        Slider s = go.GetComponent<Slider>();

        if (m_color.g != s.value)
        {
            m_color.g = s.value;
            OnColorChanged();
        }
    }
    public void SliderBChanged()
    {
        GameObject go = GameObject.Find("Slider B");
        if (!go)
            return;
        Slider s = go.GetComponent<Slider>();

        if (m_color.b != s.value)
        {
            m_color.b = s.value;
            OnColorChanged();
        }
    }
    public void SliderACHanged()
    {
        GameObject go = GameObject.Find("Slider A");
        if (!go)
            return;
        Slider s = go.GetComponent<Slider>();

        if (m_color.a != s.value)
        {
            m_color.a = s.value;
            OnColorChanged();
        }
    }

    public void OnOkClicked()
    {
        // setting of result color
        Persistent.Instance.SelectedColor = m_color;

        CloseScene();
        if (Persistent.Instance.SelColorDelegate != null)
        {
            Persistent.Instance.SelColorDelegate(m_color);
            Persistent.Instance.SelColorDelegate = null;
        }
    }

    public void OnCancelClicked()
    {
        // clearing of color delegate
        Persistent.Instance.SelColorDelegate = null;

        CloseScene();
    }

    // Use this for initialization
    void Start()
    {
        Camera camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        camera.clearFlags = CameraClearFlags.SolidColor;

        //m_color = new Color();
        //m_color = Color.white;
        m_color = Persistent.Instance.SelectedColor;

        // setting of interface
        OnColorChanged();
    }

    // Update is called once per frame
    void Update()
    {

    }

    int ConvertColorToInt(float color)
    {
        return (int)(color * 255 + 0.5f);
    }

    float ConvertColorToFloat(int intColor)
    {
        return intColor / (float)255;
    }

    Color GetColor()
    {
        return m_color;
    }

    void OnColorChanged()
    {
        // setting of inteface according to current color
        new IntWidget(new Ref<int>(() => ConvertColorToInt(Persistent.Instance.SelectedColor.r),
            v => { m_color.r = ConvertColorToFloat(v); }, UpdateInterface),
            GameObject.Find("InputField R").GetComponent<InputField>());
        new IntWidget(new Ref<int>(() => ConvertColorToInt(GetColor().g),
            v => { m_color.g = ConvertColorToFloat(v); }, UpdateInterface),
            GameObject.Find("InputField G").GetComponent<InputField>());
        new IntWidget(new Ref<int>(() => ConvertColorToInt(GetColor().b),
            v => { m_color.b = ConvertColorToFloat(v); }, UpdateInterface),
            GameObject.Find("InputField B").GetComponent<InputField>());
        new IntWidget(new Ref<int>(() => ConvertColorToInt(GetColor().a),
            v => { m_color.a = ConvertColorToFloat(v); }, UpdateInterface),
            GameObject.Find("InputField A").GetComponent<InputField>());

        UpdateInterface();
    }

    void SetImages()
    {
        // nastaveni barvy obrazku
        GameObject go = GameObject.Find("Result Image");
        RawImage im = go.GetComponent<RawImage>();
        Texture t1 = im.texture;
        if (t1 == null)
        {
            t1 = new Texture2D(20, 20);
            im.texture = t1;
        }
        TextureUtils.FillWithColor(t1 as Texture2D, m_color);

        RawImage ri;
        Texture2D t;
        int textureSize = 256;
        Color c1;
        Color c2;

        ri = GameObject.Find("Image R").GetComponent<RawImage>();
        t = ri.texture as Texture2D;
        if (t == null)
        {
            ri.texture = t = new Texture2D(textureSize, textureSize);
        }
        c1 = c2 = m_color;
        c1.r = 0;
        c2.r = 1;
        TextureUtils.WritePaletteIntoTexture(t, new SinglePalette(c1, c2), 
            new Rect(0, 0, textureSize, textureSize));

        ri = GameObject.Find("Image G").GetComponent<RawImage>();
        t = ri.texture as Texture2D;
        if (t == null)
            ri.texture = t = new Texture2D(textureSize, textureSize);
        c1 = c2 = m_color;
        c1.g = 0;
        c2.g = 1;
        TextureUtils.WritePaletteIntoTexture(t, new SinglePalette(c1, c2), 
            new Rect(0, 0, textureSize, textureSize));

        ri = GameObject.Find("Image B").GetComponent<RawImage>();
        t = ri.texture as Texture2D;
        if (t == null)
            ri.texture = t = new Texture2D(textureSize, textureSize);
        c1 = c2 = m_color;
        c1.b = 0;
        c2.b = 1;
        TextureUtils.WritePaletteIntoTexture(t, new SinglePalette(c1, c2), 
            new Rect(0, 0, textureSize, textureSize));

        ri = GameObject.Find("Image A").GetComponent<RawImage>();
        t = ri.texture as Texture2D;
        if (t == null)
            ri.texture = t = new Texture2D(textureSize, textureSize);
        c1 = c2 = m_color;
        c1.a = 0;
        c2.a = 1;
        TextureUtils.WritePaletteIntoTexture(t, new SinglePalette(c1, c2), 
            new Rect(0, 0, textureSize, textureSize));
    }

    void UpdateInterface()
    {
        SetImages();
        SetInterface(m_color.r, GameObject.Find("Slider R").GetComponent<Slider>(),
            GameObject.Find("InputField R").GetComponent<InputField>());
        SetInterface(m_color.g, GameObject.Find("Slider G").GetComponent<Slider>(),
            GameObject.Find("InputField G").GetComponent<InputField>());
        SetInterface(m_color.b, GameObject.Find("Slider B").GetComponent<Slider>(),
            GameObject.Find("InputField B").GetComponent<InputField>());
        SetInterface(m_color.a, GameObject.Find("Slider A").GetComponent<Slider>(),
            GameObject.Find("InputField A").GetComponent<InputField>());
    }

    void SetInterface(float colorValue, Slider slider, InputField input)
    {
        if (slider != null)
        {
            float val = slider.value;
            if (colorValue != val)
                slider.value = colorValue;
        }
        if (input != null)
        {
            string text = ((int)(colorValue * 255)).ToString();
            if (text != input.text)
                input.text = text;
        }
    }

    void CloseScene()
    {
        MySceneManager.GoToPreviousScene();
    }

    Color m_color;
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    public enum RepeatType
    {
        Saw,
        Triangle
    }

    public class ColoringAlgorithm
    {
        const int DefaultTransparentColorIndex = -1;
        const bool DefaultTransparentOnlyBegin = true;
        public ColoringAlgorithm()
        {
            Palette = new SinglePalette(Color.red, Color.yellow);
            RepeatType = RepeatType.Triangle;
            ColorCount = 10;
            BackgroundColor = Color.yellow;
            TransparentColorIndex = DefaultTransparentColorIndex;
            TransparentOnlyBegin = DefaultTransparentOnlyBegin;
        }
        public ColoringAlgorithm(ColoringAlgorithm old)
        {
            Palette = (IColorPalette)old.Palette.Clone();
            RepeatType = old.RepeatType;
            ColorCount = old.ColorCount;
            BackgroundColor = old.BackgroundColor;
            TransparentColorIndex = old.TransparentColorIndex;
            TransparentOnlyBegin = old.TransparentOnlyBegin;
        }

        public static bool operator ==(ColoringAlgorithm first, ColoringAlgorithm second)
        {
            return first.RepeatType == second.RepeatType &&
                first.ColorCount == second.ColorCount &&
                first.BackgroundColor == second.BackgroundColor &&
                first.TransparentColorIndex == second.TransparentColorIndex &&
                first.TransparentOnlyBegin == second.TransparentOnlyBegin &&
                first.Palette.Equals(second.Palette);
        }
        public static bool operator !=(ColoringAlgorithm first, ColoringAlgorithm second)
        {
            return first.RepeatType != second.RepeatType ||
                first.ColorCount != second.ColorCount ||
                first.BackgroundColor != second.BackgroundColor ||
                first.TransparentColorIndex != second.TransparentColorIndex ||
                first.TransparentOnlyBegin != second.TransparentOnlyBegin ||
                (first.Palette != null && !first.Palette.Equals(second.Palette));
        }

        const int OriginalColorIndexEmpty = -1;
        public Color GetColor(int colorIndex, int originalColorIndex = OriginalColorIndexEmpty)
        {
            if (colorIndex < 0)
            {
                return BackgroundColor;
            }

            if (TransparentOnlyBegin)
            {
                if (originalColorIndex == OriginalColorIndexEmpty && colorIndex <= TransparentColorIndex ||
                    originalColorIndex != OriginalColorIndexEmpty && originalColorIndex <= TransparentColorIndex)
                {
                    return m_colorMap[colorIndex] = new Color(0, 0, 0, 0);
                }
            }

            Color color;
            if (!TransparentOnlyBegin && m_colorMap.TryGetValue(colorIndex, out color))
            {
                return color;
            }

            if (!TransparentOnlyBegin)
            {
                // repeated transparent colors
                if (colorIndex <= TransparentColorIndex)
                {
                    return m_colorMap[colorIndex] = new Color(0, 0, 0, 0);
                }
            }

            if (colorIndex < ColorCount)
            {
                float ratio = colorIndex / (float)(ColorCount - 1);
                return m_colorMap[colorIndex] = GetColorInBasicInterval(ratio);
            }
            switch (RepeatType)
            {
            case RepeatType.Saw:
                return m_colorMap[colorIndex] = GetColor(colorIndex % ColorCount, colorIndex);
            default:
            case RepeatType.Triangle:
                {
                    int index = colorIndex % m_triangleRepeatCount;
                    if (index < ColorCount)
                    {
                        return m_colorMap[colorIndex] = GetColor(index, colorIndex);
                    }
                    index = m_triangleRepeatCount - index;
                    return m_colorMap[colorIndex] = GetColor(index, colorIndex);
                }
            }
        }

        public Color GetColorInBasicInterval(float ratio)
        {
            // returns color value in basic interval - <0, 1>
            return Palette.GetColor(ratio);
        }
        public Color GetDiscretizedColorInBasicInterval(float ratio)
        {
            // returns color value in basic interval - <0, 1>
            if (ratio < 0)
            {
                return GetColorInBasicInterval(0);
            }
            if (ratio > 1)
            {
                return GetColorInBasicInterval(1);
            }
            int index = Mathf.FloorToInt(ratio * ColorCount);
            return GetColor(index);
        }

        public XmlColor Background
        {
            get { return new XmlColor(BackgroundColor); }
            set { BackgroundColor = value; }
        }

        [XmlIgnore]
        public Color BackgroundColor
        {
            get { return m_backgroundColor; }
            set { m_backgroundColor = value; }
        }

        [XmlIgnore]
        public IColorPalette Palette
        {
            get { return m_palette; }
            set { m_palette = value;
                InvalidateColorMap();
            }
        }

        public RepeatType RepeatType
        {
            get { return m_repeatType; }
            set
            {
                m_repeatType = value;
                InvalidateColorMap();
            }
        }

        public int ColorCount
        {
            get { return m_colorCount; }
            set
            {
                m_colorCount = value;
                InvalidateColorMap();
                m_triangleRepeatCount = 2 * ColorCount - 2;
            }
        }
        public SinglePalette SinglePalette
        {
            get { return Palette as SinglePalette; }
            set
            {
                Palette = value;
                InvalidateColorMap();
            }
        }

        [DefaultValue(DefaultTransparentColorIndex)]
        public int TransparentColorIndex
        {
            get { return m_transparentColorIndex; }
            set { m_transparentColorIndex = value;
                InvalidateColorMap();
            }
        }

        [DefaultValue(DefaultTransparentOnlyBegin)]
        public bool TransparentOnlyBegin
        {
            get { return m_transparentOnlyBegin; }
            set { m_transparentOnlyBegin = value;
                InvalidateColorMap();
            }
        }

        private void InvalidateColorMap()
        {
            m_colorMap.Clear();
        }

        private Color m_backgroundColor;
        private IColorPalette m_palette;
        private RepeatType m_repeatType;
        private int m_colorCount;
        private int m_triangleRepeatCount;
        private Dictionary<int, Color> m_colorMap = new Dictionary<int, Color>();
        private int m_transparentColorIndex;
        private bool m_transparentOnlyBegin;
    }
}

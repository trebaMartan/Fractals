﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CompositePalette : IColorPalette
{
    public CompositePalette()
    {
        m_palettes = new List<Pair<SinglePalette, float>>(4);
    }

    public CompositePalette(CompositePalette old)
    {
        foreach (Pair<SinglePalette, float> pair in old.Palettes)
        {
            SinglePalette pal = pair.first;
            Palettes.Add(new Pair<SinglePalette, float>((SinglePalette)pal.Clone(), pair.second) );
        }
        ReadOnly = old.ReadOnly;
    }

    // ============== properties ================
    public bool ReadOnly
    {
        get { return m_isReadOnly; }
        set { m_isReadOnly = value; }
    }

    List<Pair<SinglePalette, float>> Palettes
    {
        get { return m_palettes; }
        set { m_palettes = value; }
    }

    // =============== public functions ================
    public object Clone()
    {
        CompositePalette palette = new CompositePalette(this);
        return palette;
    }

    public bool Equals(IColorPalette other)
    {
        if (other is CompositePalette)
        {
            return true;// zxc
        }
        return false;
    }

    public Color GetColor(float ratio)
    {
        if (m_totalLength == 0)
            return Color.white;
        if (ratio <= 0)
            return m_palettes[0].first.GetColor(0);
        int lastIndex = m_palettes.Count - 1;
        if (ratio >= 1)
            return m_palettes[lastIndex].first.GetColor(1);

        float actualLengthBegin = 0;
        float actualLengthEnd = 0;
        float actualLengthDesired = ratio * m_totalLength;
        int index = 0;
        foreach (var v in m_palettes)
        {
            actualLengthEnd = actualLengthBegin + v.second;
            if (actualLengthDesired <= actualLengthEnd || index == lastIndex)
            {
                // we are in right component
                float localRatio = (actualLengthDesired - actualLengthBegin) / v.second;

                return v.first.GetColor(localRatio);
            }
            // moving of index
            index++;
        }

        // just because of compiler
        Debug.Assert(false, "we shouldn't get here");
        return Color.white;
    }

    public bool AddPalette(SinglePalette palette, float relLength)
    {
        if (relLength <= 0)
            return false;
        m_palettes.Add(new Pair<SinglePalette, float>(palette, relLength));
        RecomputeLength();
        return true;
    }

    public bool IsReadOnly()
    {
        return m_isReadOnly;
    }

    // ============ private =============
    private void RecomputeLength()
    {
        m_totalLength = 0;
        foreach (var v in m_palettes)
        {
            m_totalLength += v.second;
        }
    }

    private float m_totalLength = 0;
    private bool m_isReadOnly = false;
    private List<Pair<SinglePalette, float>> m_palettes;    // in float is relative length of single palette - it have to be positive number  
}

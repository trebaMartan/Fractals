﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using Assets.Scripts.Utils;

public class CreateEditSinglePaletteScript : MonoBehaviour
{
    public void OnOkClicked()
    {
        // copy parameters into edited palette
        List<SinglePalette> list = Saver.LoadSinglePalettes();
        if (Persistent.Instance.EditPaletteMode == Persistent.EEditPaletteMode.EditPaletteList)
        {
            list[Persistent.Instance.SelectedSinglePaletteIndex] = new SinglePalette(m_palette);
        }
        else if (Persistent.Instance.EditPaletteMode == Persistent.EEditPaletteMode.CreatePalette)
        {
            list.Insert(0, new SinglePalette(m_palette));
            Persistent.Instance.SelectedSinglePaletteIndex = 0;
        }
        Saver.SaveSinglePalettes(list);
        MySceneManager.GoToPreviousScene();

        // setting of results / notification delegates
        if (Persistent.Instance.EditPaletteMode == Persistent.EEditPaletteMode.EditPalette &&
            Persistent.Instance.EditedPaletteDelegate != null)
        {
            Persistent.Instance.EditedPaletteDelegate(m_palette);
            Persistent.Instance.EditedPaletteDelegate = null;
        }
        if (Persistent.Instance.EditPaletteDelegate != null)
        {
            Persistent.Instance.EditPaletteDelegate();
            Persistent.Instance.EditPaletteDelegate = null;
        }
    }

    public void OnCancelClicked()
    {
        MySceneManager.GoToPreviousScene();
    }

    public void OnStartColorClicked()
    {
        if (m_palette.IsReadOnly())
        {
            m_messagePanel.ShowMessage(s_readOnlyError);
            return;
        }

        Persistent.Instance.SelColorDelegate = (Color color) =>
        {
            Persistent.Instance.SinglePalette.StartColor = color;
            UpdatePaletteImage();
        };
        Persistent.Instance.SelectedColor = m_palette.StartColor;
        MySceneManager.GoToScene("ColorPicking", UnityEngine.SceneManagement.LoadSceneMode.Additive);
    }

    public void OnEndColorClicked()
    {
        if (m_palette.IsReadOnly())
        {
            m_messagePanel.ShowMessage(s_readOnlyError);
            return;
        }

        Persistent.Instance.SelColorDelegate = (Color color) =>
        {
            Persistent.Instance.SinglePalette.EndColor = color;
            UpdatePaletteImage();
        };
        Persistent.Instance.SelectedColor = m_palette.EndColor;
        MySceneManager.GoToScene("ColorPicking", UnityEngine.SceneManagement.LoadSceneMode.Additive);
    }

    public void OnButtonSwap()
    {
        if (m_palette.IsReadOnly())
        {
            m_messagePanel.ShowMessage(s_readOnlyError);
            return;
        }

        Color pom = m_palette.EndColor;
        m_palette.EndColor = m_palette.StartColor;
        m_palette.StartColor = pom;
        UpdatePaletteImage();
    }


    // Use this for initialization
    void Start()
    {
        Camera camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        camera.clearFlags = CameraClearFlags.SolidColor;

        Debug.Assert(Persistent.Instance.SinglePalette != null);
        m_palette = Persistent.Instance.SinglePalette;

        new BoolWidget(new Ref<bool>(() => m_palette.ReadOnly, v => { m_palette.ReadOnly = v; }), GameObject.Find("Toggle Read Only").GetComponent<Toggle>());

        UpdatePaletteImage();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void UpdatePalette()
    {
        UpdatePaletteImage();
    }

    private void UpdatePaletteImage()
    {
        Texture2D t1 = new Texture2D(30, 30);
        TextureUtils.FillWithColor(t1, m_palette.StartColor);
        GameObject.Find("Image Start Color").GetComponent<RawImage>().texture = t1;

        Texture2D t2 = new Texture2D(30, 30);
        TextureUtils.FillWithColor(t2, m_palette.EndColor);
        GameObject.Find("Image End Color").GetComponent<RawImage>().texture = t2;

        Texture2D texture = new Texture2D(300, 30);
        Rect rect = new Rect(0, 0, 300, 30);
        TextureUtils.WritePaletteIntoTexture(texture, m_palette, rect);

        GameObject.Find("Image Palette").GetComponent<RawImage>().texture = (Texture)texture;
    }

    void Awake()
    {
        m_messagePanel = MessagePanel.GetLastInstance();
    }

    SinglePalette m_palette;

    static string s_readOnlyError = "Sorry. This palette is read only.";
    MessagePanel m_messagePanel;
}

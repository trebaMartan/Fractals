﻿using UnityEngine;
using System.Collections;
using System;

public interface IColorPalette : ICloneable
{
    Color GetColor(float ratio);
    bool Equals(IColorPalette other);
    bool IsReadOnly();
}

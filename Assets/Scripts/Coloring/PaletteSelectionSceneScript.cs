﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Timers;

public class PaletteSelectionSceneScript : MonoBehaviour
{

    const int TextLeftOffset = 10;
    const float TextRightLimitRel = 0.47f;
    const float TextureBeginRel = 0.05f;
    const float TextureEndRel = 0.95f;

    public enum PaletteType
    {
        __Begin,
        Single = __Begin,
        //Composite,
        __End,
    };

    public void OnButtonOk()
    {
        SavePalettes();
        MySceneManager.Instance.PopScene();
        IColorPalette selectedPalette = GetSelectedPalette();
        if (selectedPalette != null)
        {
            Persistent.Instance.SelectedPalette = selectedPalette;
            if (Persistent.Instance.SelectedPaletteDelegate != null)
            {
                Persistent.Instance.SelectedPaletteDelegate(selectedPalette);
                Persistent.Instance.SelectedPaletteDelegate = null;
            }
        }
    }

    public void OnButtonCancel()
    {
        Persistent.Instance.SelectedPaletteDelegate = null;
        MySceneManager.Instance.PopScene();
    }

    public void OnCreatePaletteClicked()
    {
        Persistent.Instance.EditPaletteMode = Persistent.EEditPaletteMode.CreatePalette;
        Persistent.Instance.EditPaletteDelegate = (() =>
        {
            InitListBox();
        });
        MySceneManager.GoToScene("CreateEditSimplePalette", UnityEngine.SceneManagement.LoadSceneMode.Additive);
    }

    public void OnButtonClonePalette()
    {
        IColorPalette selectedPalette = GetSelectedPalette();
        if (selectedPalette == null)
        {
            return;
        }
        IColorPalette clone = (IColorPalette)selectedPalette.Clone();
        int selectedIndex = GetSelectedPaletteIndex();
        InsertPalette(clone, selectedIndex);
        SavePalettes();
        InitListBox();
    }

    public void OnEditPalette()
    {
        EditItem(m_listbox.GetCurSel());
    }

    public void OnButtonDelete()
    {
        // deleting of a fractal
        m_modalPanel.SetButtonText(1, "Yes");
        m_modalPanel.SetButtonText(2, "No");
        m_modalPanel.Choice("Do you really want to delete selected palette?", DeleteSelectedPalette, DoNothing);
        m_modalPanel.gameObject.SetActive(true);
    }

    void DoNothing()
    {
    }

    void DeleteSelectedPalette()
    {
        int selectedPaletteIndex = m_listbox.GetCurSel();
        if (selectedPaletteIndex == ListBox.NoSelection)
        {
            return;
        }

        if (m_listbox.Count == 1)
        {
            // forbide to delete last item
            return;
        }

        // todo: some dialog with confirmation?
        IColorPalette selectedPalette = GetSelectedPalette();
        if (selectedPalette.IsReadOnly())
        {
            MessagePanel.Message(s_readOnlyError);
            return;
        }


        m_listbox.RemoveItem(selectedPaletteIndex);
        if (m_listbox.SelectedIndex == ListBox.NoSelection)
        {
            m_listbox.SelectedIndex = selectedPaletteIndex;
            if (m_listbox.SelectedIndex == ListBox.NoSelection)
            {
                m_listbox.SelectedIndex = selectedPaletteIndex-1;
                if (m_listbox.SelectedIndex == ListBox.NoSelection)
                {
                    m_listbox.SelectedIndex = 0;
                }
            }
        }
        OnSelectionChanged(m_listbox.SelectedIndex);

        SavePalettes();
        InitListBox();
    }

    public void OnSelectedTypeChanged()
    {
        int selected = m_paletteTypeDropdown.value;
        Persistent.Instance.SelectedPaletteType = (PaletteType)selected;// items in graphical inteface have to copy positions in enum

        InitListBox();
    }

    void InitDropdownPaletteType()
    {
        m_paletteTypeDropdown.ClearOptions();
        Dictionary<int, string> paletteTypeToString = new Dictionary<int, string>();
        paletteTypeToString[(int)PaletteType.Single] = "Single";
        //paletteTypeToString[(int)PaletteType.Composite] = "Composite";
        List<string> newOptions = new List<string>();
        for (int i = (int)PaletteType.__Begin; i < (int)PaletteType.__End; i++)
        {
            newOptions.Add(paletteTypeToString[i]);
        }
        m_paletteTypeDropdown.AddOptions(newOptions);

        // setting of value
        m_paletteTypeDropdown.value = (int)Persistent.Instance.SelectedPaletteType;
    }

    public void OnButtonUp()
    {
        m_listbox.MoveItemUp(m_listbox.GetCurSel());
        SavePalettes();
        InitListBox();
    }

    public void OnButtonDown()
    {
        m_listbox.MoveItemDown(m_listbox.GetCurSel());
        SavePalettes();
        InitListBox();
    }

    void Awake()
    {
        m_modalPanel = ModalPanel.Instance();
        m_modalPanel.gameObject.SetActive(false);
    }

    // Use this for initialization
    void Start()
    {
        Camera camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        camera.clearFlags = CameraClearFlags.SolidColor;

        m_modalPanel.gameObject.SetActive(false);

        m_paletteTypeDropdown = GameObject.Find("Dropdown Type").GetComponent<Dropdown>();

        // add some predefined palettes

        //GameObject go = GameObject.Find("Palette Scroll View");
        GameObject scrollView = GameObject.Find("Scroll View Palette");
        Scrollbar scrollbar = scrollView.GetComponent<Scrollbar>();

        RectTransform transform = scrollbar.GetComponent<RectTransform>();

        Rect area = transform.rect;
        area.x = transform.position.x - transform.pivot.x * area.width;
        area.y = transform.position.y - transform.pivot.y * area.height;


        int listBoxWidth = (int)area.width;
        m_listbox = new ListBox(scrollView, false, true);
        m_listbox.TextOffset = TextLeftOffset;
        m_listbox.TextRightLimit = (int)(listBoxWidth * TextRightLimitRel);

        m_textureWidth = listBoxWidth;
        m_textureHeight = 20;

        m_listbox.onDoubleClickEvent.AddListener(itemIndex =>
        {
            EditItem(itemIndex);
        });
        m_listbox.onSelectionChangedEvent.AddListener(itemIndex =>
        {
            OnSelectionChanged(itemIndex);
        });

        InitListBox();

        // initialization of selected palette
        Persistent.Instance.SelectedPalette = GetSelectedPalette();


        // init of dropbox
        InitDropdownPaletteType();

        SetSelectedTexture();
    }

    void SetSelectedTexture()
    {
        RawImage paletteImage = GameObject.Find("RawImage Palette").GetComponent<RawImage>();
        int textureSizeX = Mathf.FloorToInt(paletteImage.rectTransform.sizeDelta.x);
        int textureSizeY = Mathf.FloorToInt(paletteImage.rectTransform.sizeDelta.y);
        Texture2D t2 = new Texture2D(textureSizeX, textureSizeY);
        TextureUtils.WritePaletteIntoTexture(t2, GetSelectedPalette(), 
            new Rect(0, 0, textureSizeX, textureSizeY));
        paletteImage.texture = t2;
    }


    // Update is called once per frame
    void Update()
    {
        if (m_listbox != null)
        {
            m_listbox.Update();
        }

        if (m_savePalettes)
        {
            SavePalettes();
        }
    }

    void InitListBox()
    {
        m_listbox.Clear();
        Persistent p = Persistent.Instance;
        switch (p.SelectedPaletteType)
        {
            case PaletteType.Single:
                InitSinglePalettes();
                break;
            //case PaletteType.Composite:
            //    InitCompositeTypes();
            //    break;
        }
        m_listbox.SelectedIndex = p.GetSelectedPaletteIndex(p.SelectedPaletteType);
        m_listbox.Update();
    }

    void OnTimer(object source, ElapsedEventArgs e)
    {
        //do things
        m_savePalettes = true;
    }

    void OnSelectionChanged(int itemIndex)
    {
        // saving of current selection into persistent data
        Persistent.Instance.SetSelectedPaletteIndex(Persistent.Instance.SelectedPaletteType, itemIndex);
        SetSelectedTexture();
    }

    void SavePalettes()
    {

        switch (Persistent.Instance.SelectedPaletteType)
        {
            case PaletteType.Single:
                Saver.SaveSinglePalettes(GetSinglePalettes());
                break;
            //case PaletteType.Composite:
            //    Saver.SaveCompositePalettes(GetCompositePalettes());
            //    break;
        }
    }

    void InitSinglePalettes()
    {
        List<SinglePalette> palettes = Saver.LoadSinglePalettes();
        if (palettes.Count == 0)
        {
            InitPredefinedSinglePalettes();
        }
        else
        {
            // some palettes are saved already
            BuildListBoxFromList<SinglePalette>(palettes);
        }
    }

    void InitCompositeTypes()
    {
        List<CompositePalette> palettes = Saver.LoadCompositePalettes();
        if (palettes.Count == 0)
        {
            InitPredefinedCompositePalettes();
        }
        else
        {
            // some palettes are saved already
            BuildListBoxFromList<CompositePalette>(palettes);
        }
    }

    void BuildListBoxFromList<T>(List<T> list)
    {
        m_listbox.Clear();
        foreach (IColorPalette pal in list)
        {
            AddPalette(pal);
        }
    }

    void InitPredefinedSinglePalettes()
    {
        SinglePalette palette = new SinglePalette(Color.blue, Color.red);
        m_predefinedSinglePalettes.Add(palette);

        palette = new SinglePalette(Color.red, Color.blue);
        m_predefinedSinglePalettes.Add(palette);

        palette = new SinglePalette(Color.red, Color.green);
        m_predefinedSinglePalettes.Add(palette);

        palette = new SinglePalette(Color.green, Color.red);
        m_predefinedSinglePalettes.Add(palette);

        palette = new SinglePalette(Color.blue, Color.green);
        m_predefinedSinglePalettes.Add(palette);

        palette = new SinglePalette(Color.green, Color.blue);
        m_predefinedSinglePalettes.Add(palette);

        palette = new SinglePalette(Color.yellow, Color.cyan);
        m_predefinedSinglePalettes.Add(palette);

        palette = new SinglePalette(Color.yellow, Color.red);
        m_predefinedSinglePalettes.Add(palette);

        palette = new SinglePalette(Color.yellow, Color.magenta);
        m_predefinedSinglePalettes.Add(palette);

        palette = new SinglePalette(Color.black, Color.white);
        m_predefinedSinglePalettes.Add(palette);

        BuildListBoxFromList<SinglePalette>(m_predefinedSinglePalettes);
    }

    void InitPredefinedCompositePalettes()
    {

    }

    Texture2D PrepareTexture(IColorPalette palette, GUIStyle textureStyle)
    {
        Color gray = new Color(180 / 255.0f, 180 / 255.0f, 180 / 255.0f);
        Color lightGray = new Color(220 / 255.0f, 220 / 255.0f, 220 / 255.0f);
        if (palette.IsReadOnly())
        {
            textureStyle.hover = NewGUIStyle(palette, m_textureWidth, m_textureHeight, gray, Color.black);
            textureStyle.active = NewGUIStyle(palette, m_textureWidth, m_textureHeight, gray, Color.cyan);
        }
        else
        {
            textureStyle.hover = NewGUIStyle(palette, m_textureWidth, m_textureHeight, lightGray, Color.cyan);
            textureStyle.active = NewGUIStyle(palette, m_textureWidth, m_textureHeight, lightGray, Color.cyan);
        }
        textureStyle.normal = NewGUIStyle(palette, m_textureWidth, m_textureHeight, Color.black, Color.white);

        Texture2D texture = new Texture2D(m_textureWidth, m_textureHeight);
        int borderY = 2;
        TextureUtils.WritePaletteIntoTexture(texture, palette, 
            new Rect(m_textureWidth * TextureBeginRel, borderY, m_textureWidth * (TextureEndRel - TextureBeginRel), m_textureHeight - 2 * borderY));

        return texture;
    }

    static GUIStyleState NewGUIStyle(IColorPalette palette, int width, int height, Color textColor, Color backgroundColor)
    {
        GUIStyleState result = new GUIStyleState();
        result.background = new Texture2D(width, height);
        TextureUtils.FillWithColor(result.background, backgroundColor);

        int borderY = 2;

        TextureUtils.MakeTransparentBorders(result.background, borderY, 3);

        float paletteStart = TextureBeginRel;
        float paletteEnd = TextureEndRel;

        TextureUtils.WritePaletteIntoTexture(result.background, palette, 
            new Rect(width * paletteStart, borderY, width * (paletteEnd - paletteStart), height - 2 * borderY));

        result.textColor = textColor;
        return result;
    }


    void AddPalette(IColorPalette palette)
    {
        GUIStyle textureStyle = new GUIStyle();
        int itemIndex = m_listbox.AddItem(PrepareTexture(palette, textureStyle), "", textureStyle);
        m_listbox.SetItemData(itemIndex, palette);
    }

    void InsertPalette(IColorPalette palette, int index)
    {
        GUIStyle textureStyle = new GUIStyle();
        int itemIndex = m_listbox.InsertItem(index, PrepareTexture(palette, textureStyle), m_textureHeight, "", textureStyle);
        m_listbox.SetItemData(itemIndex, palette);
    }

    SinglePalette GetSinglePalette(int index)
    {
        if (!IsIndexValid(index))
        {
            return null;
        }

        return m_listbox.GetItemData(index) as SinglePalette;
    }

    int GetSelectedPaletteIndex()
    {
        return m_listbox.GetCurSel();
    }

    IColorPalette GetSelectedPalette()
    {
        int selectedIndex = GetSelectedPaletteIndex();
        if (!IsIndexValid(selectedIndex))
        {
            return null;
        }
        return (IColorPalette)m_listbox.GetItemData(selectedIndex);
    }

    bool IsIndexValid(int itemIndex)
    {
        return itemIndex >= 0 && itemIndex < m_listbox.Count;
    }

    void EditItem(int itemIndex)
    {
        if (!IsIndexValid(itemIndex))
        {
            return;
        }

        switch (Persistent.Instance.SelectedPaletteType)
        {
            case PaletteType.Single:
                Persistent.Instance.SinglePalette = GetSinglePalette(itemIndex);
                break;
            //case PaletteType.Composite:
            //    //zxc make composite palletes
            //    break;
        }
        Persistent.Instance.EditPaletteMode = Persistent.EEditPaletteMode.EditPaletteList;
        Persistent.Instance.SelectedSinglePaletteIndex = itemIndex;
        Persistent.Instance.EditPaletteDelegate = (() => 
        {
            InitListBox();
        });
        MySceneManager.GoToScene("CreateEditSimplePalette", UnityEngine.SceneManagement.LoadSceneMode.Additive);
    }

    List<SinglePalette> GetSinglePalettes()
    {
        List<SinglePalette> list = new List<SinglePalette>();
        for (int i = 0; i < m_listbox.Count; i++)
        {
            SinglePalette palette = m_listbox.GetItemData(i) as SinglePalette;
            if (palette != null)
            {
                list.Add(palette);
            }
        }
        return list;
    }

    List<CompositePalette> GetCompositePalettes()
    {
        List<CompositePalette> list = new List<CompositePalette>();
        for (int i = 0; i < m_listbox.Count; i++)
        {
            CompositePalette palette = m_listbox.GetItemData(i) as CompositePalette;
            if (palette != null)
            {
                list.Add(palette);
            }
        }
        return list;
    }


    List<SinglePalette> m_predefinedSinglePalettes = new List<SinglePalette>();
    //List<IColorPalette> m_predefinedCompositePalettes = new List<IColorPalette>();

    ListBox m_listbox;

    int m_textureWidth;
    int m_textureHeight;
    Dropdown m_paletteTypeDropdown;

    bool m_savePalettes = false;
    static string s_readOnlyError = "Sorry. This palette is read only.";
    private ModalPanel m_modalPanel;
}

﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;
using System;

public class SinglePalette : IColorPalette
{
    public SinglePalette()
    {
        m_startColor = Color.black;
        m_endColor = Color.black;
    }

    public SinglePalette(Color begin, Color end)
    {
        m_startColor = begin;
        m_endColor = end;
    }

    public SinglePalette(SinglePalette old)
    {
        m_startColor = old.StartColor;
        m_endColor = old.EndColor;
        ReadOnly = old.ReadOnly;
    }

    // ============= properties ==============
    [XmlIgnore]
    public Color StartColor
    {
        get { return m_startColor; }
        set { m_startColor = value; }
    }

    [XmlIgnore]
    public Color EndColor
    {
        get { return m_endColor; }
        set { m_endColor = value; }
    }

    public XmlColor StartC
    {
        get { return new XmlColor(StartColor); }
        set { StartColor = value; }
    }
    public XmlColor EndC
    {
        get { return new XmlColor(EndColor); }
        set { EndColor = value; }
    }

    public bool ReadOnly
    {
        get { return m_isReadOnly; }
        set { m_isReadOnly = value; }
    }
    
    // ============= public functions ==============
    public object Clone()
    {
        SinglePalette palette = new SinglePalette(this);
        return palette;
    }

    public void CopyFrom(SinglePalette palette)
    {
        m_startColor = palette.m_startColor;
        m_endColor = palette.m_endColor;
    }

    public bool Equals(IColorPalette other)
    {
        if (other is SinglePalette)
        {
            return m_startColor == (other as SinglePalette).m_startColor &&
                m_endColor == (other as SinglePalette).m_endColor;
        }
        return false;
    }

    public Color GetColor(float ratio)
    {
        if (ratio <= 0)
            return m_startColor;
        if (ratio >= 1)
            return m_endColor;
        float beginWeight = 1 - ratio;
        float endWeight = 1 - beginWeight;
        Color newColor = new Color(beginWeight * m_startColor.r + endWeight * m_endColor.r, beginWeight * m_startColor.g + endWeight * m_endColor.g,
            beginWeight * m_startColor.b + endWeight * m_endColor.b, beginWeight * m_startColor.a + endWeight * m_endColor.a);
        return newColor;
    }

    public bool IsReadOnly()
    {
        return m_isReadOnly;
    }

    // =============== private  ================
    private Color m_startColor;
    private Color m_endColor;
    private bool m_isReadOnly = true;
}

public class XmlColor
{

    public XmlColor() { }
    public XmlColor(Color c) { m_color = c; }

    public Color ToColor()
    {
        return m_color;
    }

    public void FromColor(Color c)
    {
        m_color = c;
    }

    public static implicit operator Color(XmlColor x)
    {
        return x.ToColor();
    }

    public static implicit operator XmlColor(Color c)
    {
        return new XmlColor(c);
    }

    [XmlAttribute("C")]
    public string Color1
    {
        get
        {
            string str = string.Format("{0},{1},{2},{3}", m_color.r, m_color.g, m_color.b, m_color.a);
            return str;
        }
        set
        {
            string[] subStrings = value.Split(',');
            m_color.r = float.Parse(subStrings[0]);
            m_color.g = float.Parse(subStrings[1]);
            m_color.b = float.Parse(subStrings[2]);
            m_color.a = float.Parse(subStrings[3]);
        }
    }

    private Color m_color = Color.black;
}
﻿using UnityEngine;
using UnityEngine.UI;
using Exocortex.DSP;
using System.IO;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.UI_Adapters;
using OxOD;
using UnityEngine.Events;
using System.Linq;
using Assets.Scripts.Computation;

public class Complex2DSceneScript : MonoBehaviour
{
    [Header("OxOD Reference")]
    public FileDialog fileDialog;

    public struct ComputationTimeParams
    {
        public float timeOfRecord;
        public string computationParametersHash;
    }

    enum PanelType
    {
        __Begin,
        Computation = __Begin,
        Other,
        __End
    }

    enum MainPanelType
    {
        PanelMain,
        PanelExport
    }

    public enum ExportSizeType
    {
        UserDefined,
        SameAsScreen,
        SameRatioAsFractal,
    }

    public void OnUndo()
    {
        string changedProperty = GetActiveFractal().GetUndoRedoList().Undo();
        // setting of UI
        SetPanelAccordingToChangedProperty(changedProperty);
        UpdateUndoRedoButtons();
    }

    public void OnRedo()
    {
        string changedProperty = GetActiveFractal().GetUndoRedoList().Redo();
        // setting of UI
        SetPanelAccordingToChangedProperty(changedProperty);
        UpdateUndoRedoButtons();
    }

    public void OnButtonCompute()
    {
        if (!CheckParameters())
        {
            return;
        }

        // writing values into undo-redo listUpdateUndoList
        AddNewValueToUndoList();

        GameObject selectedGameObject = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject;

        // get parameters are received from ui (for case that we were in some inputfield)
        UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(null);
        int width = m_fractalImage.mainTexture.width;
        int height = m_fractalImage.mainTexture.height;
        Texture2D texture = new Texture2D(width, height);

        if (m_computationStruct == null)
        {
            m_computationStruct = new Complex2DComputationStruct(GetActiveFractal());
        }
        else
        {
            m_computationStruct.SetFractal(GetActiveFractal());
        }
        if (m_computationStruct.IsJustComputed)
        {
            // something else is computed now
            return;
        }

        // starting of computation
        m_computationStruct.IsJustComputed = true;
        m_computationStruct.IsComputationFinished = false;
        m_computationStruct.PerformAfterComputation = () =>
        {
            //m_computationStruct.IsFinished = false;

            m_computationStruct.Fractal.ComputeTextureFromIterationIndex(texture, width, height);
            m_fractalImage.texture = texture;
            m_computationStruct.IsJustComputed = false;

            // restore selection
            UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(selectedGameObject);

            OnHideUI();
            SetEnabledComputation(true);
        };

        SetEnabledComputation(false);
        m_computationStruct.ComputeIterationIndexAsynchronous(width, height);
    }

    void SetEnabledComputation(bool enabled)
    {
        m_buttonCompute.interactable = enabled;
    }

    void SetEnabledExport(bool enabled)
    {
        GameObject.Find("Button Export Png").GetComponent<Button>().interactable = enabled;
        GameObject.Find("Button Export Back").GetComponent<Button>().interactable = enabled;
        m_exportProgressSlider.gameObject.SetActive(!enabled);
    }

    public void OnButtonSelectPalette()
    {
        Persistent.Instance.SelectedPaletteDelegate = (IColorPalette palette) =>
        {
            GetActiveFractal().Palette = palette;
            UpdatePaletteImages();
            RefreshFractalImage();
            AddNewValueToUndoList();
        };

        MySceneManager.GoToScene("PalettePicking", LoadSceneMode.Additive);
    }

    public void OnButtonEditPalette()
    {
        Persistent.Instance.EditPaletteMode = Persistent.EEditPaletteMode.EditPalette;
        Persistent.Instance.SinglePalette = GetActiveFractal().Palette as SinglePalette;//zxc osetrit i jine typy
        Persistent.Instance.EditedPaletteDelegate = (IColorPalette palette) =>
        {
            GetActiveFractal().Palette = palette;
            UpdatePaletteImages();
            RefreshFractalImage();
            AddNewValueToUndoList();
        };

        MySceneManager.GoToScene("CreateEditSimplePalette", LoadSceneMode.Additive);
    }

    void RefreshFractalImage()
    {
        int textureWidth = m_fractalImage.mainTexture.width;
        int textureHeight = m_fractalImage.mainTexture.height;
        string computationHash = GetActiveFractal().GetComputationParametersHash();
        IterationIndex iterationIndex = GetActiveFractal().GetIterationIndex(textureWidth, textureHeight, computationHash);
        if (iterationIndex.IsComputed())
        {
            Texture2D texture = new Texture2D(textureWidth, textureHeight);
            GetActiveFractal().ComputeTextureFromIterationIndex(texture, textureWidth, textureHeight);
            m_fractalImage.texture = texture;
        }
    }

    void SetBackgroundColor()
    {
        Camera camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        camera.clearFlags = CameraClearFlags.SolidColor;
        camera.backgroundColor = GetActiveFractal().BackgroundColor;
    }


    void UpdatePaletteImages()
    {
        SetPaletteImage();
        SetDiscretizedPaletteImage();
        SetPaletteImageWithRepeat();
    }

    public void OnNumberOfColorChanged()
    {
        SetDiscretizedPaletteImage();
        SetPaletteImageWithRepeat();
    }

    public void OnChangeBackgroundColorClicked()
    {
        Persistent.Instance.SelColorDelegate = (Color color) =>
        {
            GetActiveFractal().BackgroundColor = color;
            UpdateInterfaceFractalBackgroundColor();
            SetBackgroundColor();
            AddNewValueToUndoList();
        };
        Persistent.Instance.SelectedColor = GetActiveFractal().BackgroundColor;
        //SaveFractal();
        MySceneManager.GoToScene("ColorPicking", LoadSceneMode.Additive);
    }

    public void OnButtonBack()
    {
        if (MySceneManager.GoToPreviousScene() == SceneManager.GetActiveScene())
        {
            // no previous scene
            MySceneManager.GoToScene("2D Fractals");
        }
    }

    public void OnSelectedPowerChanged()
    {
        int row = m_dropdownSelectedPowerInput.value;
        OnSelectedPowerChanged(row);
    }

    public void OnSelectedPowerChanged(int selectedPower)
    {
        int row = selectedPower;
        GameObject panel = GameObject.Find("Panel Computation");

        DoubleWidget cWidget = new DoubleWidget(new Ref<double>(() =>
        Persistent.Instance.FormOfComplexNumbers == Persistent.ComplexNumberForm.RealImaginary ?
            GetActiveFractal().GetC(row).Re :
            GetActiveFractal().GetC(row).GetModulus(),
            v => {
                if (Persistent.Instance.FormOfComplexNumbers == Persistent.ComplexNumberForm.RealImaginary)
                {
                    GetActiveFractal().SetC(row, new Complex(v, GetActiveFractal().GetC(row).Im));
                }
                else
                {
                    GetActiveFractal().SetC(row, new Complex(Complex.FromModulusArgument(v, GetActiveFractal().GetC(row).GetArgument())));
                }
            }),
            m_inputRe);
        cWidget.OnChangeDelegate += OnUsedPowersChanged;

        if (m_sliderWidgetRe != null)
        {
            m_sliderWidgetRe.RemoveListeners();
        }
        if (Persistent.Instance.FormOfComplexNumbers == Persistent.ComplexNumberForm.RealImaginary)
        {
            m_sliderWidgetRe = new SliderWidget(m_sliderRe, cWidget, -Persistent.Instance.MaxSliderValue, Persistent.Instance.MaxSliderValue);
        }
        else
        {
            m_sliderWidgetRe = new SliderWidget(m_sliderRe, cWidget, 0, Persistent.Instance.MaxSliderValue);
        }

        cWidget = new DoubleWidget(new Ref<double>(() =>
        Persistent.Instance.FormOfComplexNumbers == Persistent.ComplexNumberForm.RealImaginary ? 
            GetActiveFractal().GetC(row).Im :
            GetActiveFractal().GetC(row).GetArgument(),
            v => {
                if (Persistent.Instance.FormOfComplexNumbers == Persistent.ComplexNumberForm.RealImaginary)
                {
                    GetActiveFractal().SetC(row, new Complex(GetActiveFractal().GetC(row).Re, v));
                }
                else
                {
                    GetActiveFractal().SetC(row, new Complex(Complex.FromModulusArgument(GetActiveFractal().GetC(row).GetModulus(), v)));
                }
            }),
            m_inputIm);
        cWidget.OnChangeDelegate += OnUsedPowersChanged;

        const float PI = 3.1415926f;
        if (m_sliderWidgetIm != null)
        {
            m_sliderWidgetIm.RemoveListeners();
        }
        if (Persistent.Instance.FormOfComplexNumbers == Persistent.ComplexNumberForm.RealImaginary)
        {
            m_sliderWidgetIm = new SliderWidget(m_sliderIm, cWidget, -Persistent.Instance.MaxSliderValue, Persistent.Instance.MaxSliderValue);
        }
        else
        {
            m_sliderWidgetIm = new SliderWidget(m_sliderIm, cWidget, -PI, PI );
        }

        cWidget = new DoubleWidget(new Ref<double>(() =>
        Persistent.Instance.FormOfComplexNumbers == Persistent.ComplexNumberForm.RealImaginary ?
            GetActiveFractal().GetC0(row).Re :
            GetActiveFractal().GetC0(row).GetModulus(),
            v => {
                if (Persistent.Instance.FormOfComplexNumbers == Persistent.ComplexNumberForm.RealImaginary)
                {
                    GetActiveFractal().SetC0(row, new Complex(v, GetActiveFractal().GetC0(row).Im));
                }
                else
                {
                    GetActiveFractal().SetC0(row, new Complex(Complex.FromModulusArgument(v, GetActiveFractal().GetC0(row).GetArgument())));
                }
            }),
            m_inputC0Re);
        //cWidget = new DoubleWidget(new Ref<double>(() => GetActiveFractal().GetC0(row).Re,
        //    v => { GetActiveFractal().SetC0(row, new Complex(v, GetActiveFractal().GetC0(row).Im)); }),
        //    m_inputC0Re);
        cWidget.OnChangeDelegate += OnUsedPowersChanged;

        if (m_sliderWidgetC0Re != null)
        {
            m_sliderWidgetC0Re.RemoveListeners();
        }
        if (Persistent.Instance.FormOfComplexNumbers == Persistent.ComplexNumberForm.RealImaginary)
        {
            m_sliderWidgetC0Re = new SliderWidget(m_sliderC0Re, cWidget, -Persistent.Instance.MaxSliderValue, Persistent.Instance.MaxSliderValue);
        }
        else
        {
            m_sliderWidgetC0Re = new SliderWidget(m_sliderC0Re, cWidget, 0, Persistent.Instance.MaxSliderValue);
        }

        //        new SliderWidget(m_sliderC0Re, cWidget, -Persistent.Instance.MaxSliderValue, Persistent.Instance.MaxSliderValue);

        cWidget = new DoubleWidget(new Ref<double>(() =>
        Persistent.Instance.FormOfComplexNumbers == Persistent.ComplexNumberForm.RealImaginary ?
            GetActiveFractal().GetC0(row).Im :
            GetActiveFractal().GetC0(row).GetArgument(),
            v => {
                if (Persistent.Instance.FormOfComplexNumbers == Persistent.ComplexNumberForm.RealImaginary)
                {
                    GetActiveFractal().SetC0(row, new Complex(GetActiveFractal().GetC0(row).Re, v));
                }
                else
                {
                    GetActiveFractal().SetC0(row, new Complex(Complex.FromModulusArgument(GetActiveFractal().GetC0(row).GetModulus(), v)));
                }
            }),
            m_inputC0Im);

        //cWidget = new DoubleWidget(new Ref<double>(() => GetActiveFractal().GetC0(row).Im,
        //    v => { GetActiveFractal().SetC0(row, new Complex(GetActiveFractal().GetC0(row).Re, v)); }),
        //    m_inputC0Im);
        cWidget.OnChangeDelegate += OnUsedPowersChanged;

        if (m_sliderWidgetC0Im != null)
        {
            m_sliderWidgetC0Im.RemoveListeners();
        }
        if (Persistent.Instance.FormOfComplexNumbers == Persistent.ComplexNumberForm.RealImaginary)
        {
            m_sliderWidgetC0Im = new SliderWidget(m_sliderC0Im, cWidget, -Persistent.Instance.MaxSliderValue, Persistent.Instance.MaxSliderValue);
        }
        else
        {
            m_sliderWidgetC0Im = new SliderWidget(m_sliderC0Im, cWidget, -PI, PI);
        }

        //      new SliderWidget(m_sliderC0Im, cWidget, -Persistent.Instance.MaxSliderValue, Persistent.Instance.MaxSliderValue);

        //widget.OnChangeDelegate += OnUsedPowersChanged;
        //widget.OnChangeDelegate += UpdateUndoList;
        //widgetC0.OnChangeDelegate += OnUsedC0PowersChanged;
        //widgetC0.OnChangeDelegate += UpdateUndoList;
    }

    public void OnButtonSwitchComplexForm()
    {
        if (Persistent.Instance.FormOfComplexNumbers == Persistent.ComplexNumberForm.RealImaginary)
        {
            Persistent.Instance.FormOfComplexNumbers = Persistent.ComplexNumberForm.Polar;
        }
        else
        {
            Persistent.Instance.FormOfComplexNumbers = Persistent.ComplexNumberForm.RealImaginary;
        }

        InitCLabelTexts();
        InitSwitchButtonText();
        OnSelectedPowerChanged();
    }

    void InitSwitchButtonText()
    {
        Button switchButton = GameObject.Find("Button Switch Complex Form").GetComponent<Button>();
        switch (Persistent.Instance.FormOfComplexNumbers)
        {
        case Persistent.ComplexNumberForm.RealImaginary:
            {
                (switchButton.GetComponentInChildren(typeof(Text), true) as Text).text = "To Polar";
                break;
            }
        case Persistent.ComplexNumberForm.Polar:
            {
                (switchButton.GetComponentInChildren(typeof(Text), true) as Text).text = "To Component";
                break;
            }
        default:
            Debug.Assert(false);
            break;
        }
    }

    void InitCLabelTexts()
    {
        Text textRe = GameObject.Find("Text Re").GetComponent<Text>();
        Text textIm = GameObject.Find("Text Im").GetComponent<Text>();

        if (Persistent.Instance.FormOfComplexNumbers == Persistent.ComplexNumberForm.RealImaginary)
        {
            textRe.text = "C: Re";
            textIm.text = "C: Im";
        }
        else
        {
            textRe.text = "C: Radius";
            textIm.text = "C: Angle";
        }
        if (GameObject.Find("Text C0 Re") == null)
        {
            return;
        }
        Text textC0Re = GameObject.Find("Text C0 Re").GetComponent<Text>();
        Text textC0Im = GameObject.Find("Text C0 Im").GetComponent<Text>();
        if (Persistent.Instance.FormOfComplexNumbers == Persistent.ComplexNumberForm.RealImaginary)
        {
            textC0Re.text = "C0: Re";
            textC0Im.text = "C0: Im";
        }
        else
        {
            textC0Re.text = "C0:Radius";
            textC0Im.text = "C0: Angle";
        }
    }

    ComplexFractal2D GetActiveFractal()
    {
        return m_activeFractal;
    }

    void SetActiveFractal(ComplexFractal2D fractal)
    {
        m_activeFractal = fractal;
    }

    ComplexFractal2D GetPreviewFractal()
    {
        return m_previewFractal;
    }

    public void OnHideUI()
    {
        m_isPanelHidden = true;
        m_mainPanel.SetActive(false);
        m_hideTime = UnityEngine.Time.realtimeSinceStartup;
    }

    public void OnButtonToComputation()
    {
        m_panelToggles[PanelType.Computation].isOn = true;
    }

    public void OnButtonToOther()
    {
        m_panelToggles[PanelType.Other].isOn = true;
    }

    public void OnButtonRandomize()
    {
        ComplexFractal2D fractal = RandomFunc.GetRandom2DFractalDefinition(GetNewSaveName(Persistent.GetFractalsDir()));
        SetActiveFractal(fractal);
        SetUI();
        OnButtonCompute();
    }

    public void OnButtonRandomSettings()
    {
        Persistent.Instance.Random2DSettingsWithoutGenerate = true;
        MySceneManager.GoToScene("2D Random Fractals", LoadSceneMode.Additive);
    }

    public void OnPanelSelectionChanged()
    {
        for (int i = (int)PanelType.__Begin; i < (int)PanelType.__End; i++)
        {
            if (m_panelToggles[(PanelType)i].isOn)
            {
                ActivatePanel(i);
                return;
            }
        }
    }

    public void OnButtonExportToPng()
    {
        if (!CheckParameters())
        {
            return;
        }
        if (Persistent.Instance.ExportedPictureWidth <= 0 || Persistent.Instance.ExportedPictureHeight <= 0)
        {
            MessagePanel.Message("Picture width and height have to be positive");
            return;
        }
        string saveDir = Persistent.Instance.SavePicturesDirectory;
        string extension = ".png";
        fileDialog.onResultDelegate = (string path) =>
        {
            UnityAction writeAction = () =>
            {
                int width = Persistent.Instance.ExportedPictureWidth;
                int height = Persistent.Instance.ExportedPictureHeight;
                Texture2D texture = new Texture2D(width, height);
                if (m_exportComputationStruct == null)
                {
                    m_exportComputationStruct = new Complex2DComputationStruct(GetActiveFractal());
                }
                else
                {
                    m_exportComputationStruct.SetFractal(GetActiveFractal());
                }
                m_exportComputationStruct.IsJustComputed = true;
                m_exportComputationStruct.PerformAfterComputation = () =>
                {
                    m_exportComputationStruct.Fractal.ComputeTextureFromIterationIndex(texture, width, height);
                    TextureUtils.SaveTextureToPng(path, texture);
                    m_exportComputationStruct.IsJustComputed = false;
                    SetEnabledExport(true);
                    System.GC.Collect();
                };

                SetEnabledExport(false);
                m_exportComputationStruct.ComputeIterationIndexAsynchronous(width, height);
                // record saving path                
                Persistent.Instance.SavePicturesDirectory = Path.GetDirectoryName(path);
            };
            if (File.Exists(path))
            {
                OverwriteConfirmation(writeAction);
            }
            else
            {
                writeAction();
            }
        };
        string saveName = "";
        if (GetActiveFractal().FilePath.Length > 0)
        {
            saveName = Path.GetFileNameWithoutExtension(GetActiveFractal().FilePath) 
                + System.String.Format("_{0}_{1}", Persistent.Instance.ExportedPictureWidth, Persistent.Instance.ExportedPictureHeight) 
                + extension;
        }
        else
        {
            saveName = GetNewImageName(saveDir, extension);
        }
        StartCoroutine(fileDialog.Save(Path.Combine(saveDir, saveName), extension, "Save fractal"));
    }

    string GetNewImageName(string directory, string extension)
    {
        return extension;
    }    

    public void OnAlgorithmTypeChanged()
    {
    }

    public void OnButtonLoad()
    {
        string extensions = Persistent.GetExtensionWithDot(Persistent.SaveExtension.fractal2DDefinitionExtension);

        fileDialog.onResultDelegate = (string path) =>
        {
            m_loadPath = path;
            ComplexFractal2D fractal;
            if (ComplexFractal2D.Load(path, out fractal))
            {
                ClearScene();
                SetActiveFractal(fractal);
                GetActiveFractal().GetUndoRedoList().AddNewValue();
                ActivateParameterPanel(PanelType.Computation);
            }
        };
        StartCoroutine(fileDialog.Open(m_loadPath, extensions, "Load 3D Fractal"));
    }

    bool CheckParameters()
    {
        string error = ValidateEditedParameters();
        if (error.Length > 0)
        {
            MessagePanel.Message(error);
            return false;
        }
        return true;
    }

    public void OnButtonSave()
    {
        if (!CheckParameters())
        {
            return;
        }
        SaveFractal();
        SavePrevies(GetActiveFractal(), GetActiveFractal().FilePath);//Persistent.Instance.SelectedFractalFile);//zxc jine jmeno?
    }

    public void OnButtonSaveNew()
    {
        if (!CheckParameters())
        {
            return;
        }
        string saveDir = Persistent.GetFractalsDir();
        string extension = Persistent.GetExtensionWithDot(Persistent.SaveExtension.fractal2DDefinitionExtension);

        fileDialog.onResultDelegate = (string path) =>
        {
            GetActiveFractal().Save(path);
            SavePrevies(GetActiveFractal(), GetActiveFractal().FilePath);
            Persistent.Instance.SelectedComplex2DFile = GetActiveFractal().FilePath;
        };
        string saveName = GetNewSaveName(Persistent.GetFractalsDir());
        StartCoroutine(fileDialog.Save(Path.Combine(saveDir, saveName), extension, "Save fractal"));
    }

    public void OnButtonSaveAs()
    {
        if (!CheckParameters())
        {
            return;
        }
        string saveDir = Persistent.GetFractalsDir();
        string extension = Persistent.GetExtensionWithDot(Persistent.SaveExtension.fractal2DDefinitionExtension);

        fileDialog.onResultDelegate = (string path) =>
        {
            UnityAction writeAction = () =>
            {
                GetActiveFractal().Save(path);
                SavePrevies(GetActiveFractal(), GetActiveFractal().FilePath);
                Persistent.Instance.SelectedComplex2DFile = GetActiveFractal().FilePath;
            };
            if (File.Exists(path) && path != GetActiveFractal().FilePath)
            {
                OverwriteConfirmation(writeAction);
            }
            else
            {
                writeAction();
            }
        };
        string saveName = GetActiveFractal().FilePath.Length > 0 ? Path.GetFileName(GetActiveFractal().FilePath) : GetNewSaveName(saveDir);
        StartCoroutine(fileDialog.Save(Path.Combine(saveDir, saveName), extension, "Save fractal"));
    }

    public void OverwriteConfirmation(UnityAction overwriteAction)
    {
        m_questionPanel.SetButtonText(1, "Yes");
        m_questionPanel.SetButtonText(2, "No");
        m_questionPanel.Choice("Do you want to overwrite existing file?", overwriteAction);
        m_questionPanel.gameObject.SetActive(true);
    }

    public void OnButtonConvert()
    {
        OtherUtils.Convert2DSaves(Persistent.GetFractalsDir());
    }

    public void OnButtonExport()
    {
        ActivatePanel(MainPanelType.PanelExport);
    }

    void SavePredefinedExportImageSizes()
    {
        // save of predefined sizes
        UIUtils.SaveInputField("InputField Predefined 2d Size 1");
        UIUtils.SaveInputField("InputField Predefined 2d Size 2");
    }

    public void OnButtonExportBack()
    {
        SavePredefinedExportImageSizes();
        ActivatePanel(MainPanelType.PanelMain);
    }

    void ActivatePanel(MainPanelType panelType)
    {
        m_mainPanel.SetActive(panelType == MainPanelType.PanelMain);
        m_exportPanel.SetActive(panelType == MainPanelType.PanelExport);
        switch (panelType)
        {
        case MainPanelType.PanelExport:
            InitExportPanel();
            break;       
        }
    }

    void OnChangeExportSizeType()
    {
        switch (Persistent.Instance.ExportSizeType)
        {
        case ExportSizeType.UserDefined:
            {
                m_inputFieldExportedImageWidth.interactable = true;
                m_inputFieldExportedImageHeight.interactable = true;
                // user will set both dimensions
                m_toggleSetWidth.SetActive(false);
                m_toggleSetHeight.SetActive(false);
                break;
            }
        case ExportSizeType.SameAsScreen:
            {
                m_toggleSetWidth.SetActive(false);
                m_toggleSetHeight.SetActive(false);
                m_inputFieldExportedImageWidth.interactable = false;
                m_inputFieldExportedImageHeight.interactable = false;
                Persistent.Instance.ExportedPictureWidth = Screen.width;
                Persistent.Instance.ExportedPictureHeight = Screen.height;
                break;
            }
        case ExportSizeType.SameRatioAsFractal:
            {
                m_toggleSetWidth.SetActive(true);
                m_toggleSetHeight.SetActive(true);
                new BoolWidget(new Ref<bool>(() => Persistent.Instance.ExportedPictureSetWidth, v => { Persistent.Instance.ExportedPictureSetWidth = v; }), m_toggleSetWidth.GetComponent<Toggle>()).OnChangeDelegate += OnChangeExportSizeType;
                new BoolWidget(new Ref<bool>(() => !Persistent.Instance.ExportedPictureSetWidth, v => { Persistent.Instance.ExportedPictureSetWidth = !v; }), m_toggleSetHeight.GetComponent<Toggle>()).OnChangeDelegate += OnChangeExportSizeType;
                m_inputFieldExportedImageWidth.interactable = Persistent.Instance.ExportedPictureSetWidth;
                m_inputFieldExportedImageHeight.interactable = !Persistent.Instance.ExportedPictureSetWidth;
                if (Persistent.Instance.ExportedPictureSetWidth)
                {
                    OnChangeExportedImageWidth();
                }
                else
                {
                    OnChangeExportedImageHeight();
                }
                break;
            }
        }

        new IntWidget(new Ref<int>(() => Persistent.Instance.ExportedPictureWidth,
            v => { Persistent.Instance.ExportedPictureWidth = v; }, OnChangeExportedImageWidth),
            m_inputFieldExportedImageWidth);
        new IntWidget(new Ref<int>(() => Persistent.Instance.ExportedPictureHeight,
            v => { Persistent.Instance.ExportedPictureHeight = v; }, OnChangeExportedImageHeight),
            m_inputFieldExportedImageHeight);
    }

    void OnChangeExportedImageWidth()
    {
        if (Persistent.Instance.ExportSizeType == ExportSizeType.SameRatioAsFractal)
        {
            int newHeight = (int)(Persistent.Instance.ExportedPictureWidth * GetActiveFractal().ComputationArea.HalfSizeY / GetActiveFractal().ComputationArea.HalfSizeX + 0.5f);
            if (newHeight != Persistent.Instance.ExportedPictureHeight)
            {
                Persistent.Instance.ExportedPictureHeight = newHeight;
                OnChangeExportSizeType();
            }
        }
    }

    void OnChangeExportedImageHeight()
    {
        if (Persistent.Instance.ExportSizeType == ExportSizeType.SameRatioAsFractal)
        {
            int newWidth = (int)(Persistent.Instance.ExportedPictureHeight * GetActiveFractal().ComputationArea.HalfSizeX / GetActiveFractal().ComputationArea.HalfSizeY + 0.5f);
            if (newWidth != Persistent.Instance.ExportedPictureWidth)
            {
                Persistent.Instance.ExportedPictureWidth = newWidth;
                OnChangeExportSizeType();
            }
        }
    }

    public void OnUsedPowersChanged()
    {
        m_inputUsedCPowers.text = GetUsedCPowers();
    }
    public void OnUsedC0PowersChanged()
    {
        m_inputUsedC0Powers.text = GetUsedC0Powers();
    }
    void Awake()
    {
        m_questionPanel = ModalPanel.Instance();
        m_questionPanel.gameObject.SetActive(false);
    }

    void OnFractalParametersChanged()
    {
        if (m_previewComputationStruct == null)
        {
            m_previewComputationStruct = new Complex2DComputationStruct(m_previewFractal);
        }
        if (m_previewComputationStruct.IsJustComputed)
        {
            // some computation is in progress
            return;
        }
        m_previewComputationStruct.IsJustComputed = true;
        // copy of actual parameters
        if (m_previewFractal == null)
        {
            m_previewFractal = new ComplexFractal2D(GetActiveFractal());
        }
        else
        {
            m_previewFractal.CopyFrom(GetActiveFractal());
        }
        // recomputing of fractal preview
        Vector2 currentPreviewSize = m_fractalPreviewImage.GetComponent<RectTransform>().sizeDelta;
        int width = (int)currentPreviewSize.x;
        int height = (int)currentPreviewSize.y;
        if (m_previewTexture == null)
        {
            m_previewTexture = new Texture2D(width, height);
        }

        m_previewComputationStruct.SetFractal(m_previewFractal);
        m_previewComputationStruct.PerformAfterComputation = () =>
        {
            m_previewComputationStruct.Fractal.ComputeTextureFromIterationIndex(m_previewTexture, width, height);
            m_fractalPreviewImage.texture = m_previewTexture;
            m_previewComputationStruct.IsJustComputed = false;
            System.GC.Collect();
        };
        m_previewComputationStruct.ComputeIterationIndexAsynchronous(width, height);
    }

    // Use this for initialization
    void Start()
    {
        m_inputName = GameObject.Find("InputField Name").GetComponent<InputField>();

        m_inputRe = GameObject.Find("InputField Re").GetComponent<InputField>();
        m_inputIm = GameObject.Find("InputField Im").GetComponent<InputField>();
        m_gameObjectInputC0Re = GameObject.Find("InputField C0 Re");
        m_inputC0Re = m_gameObjectInputC0Re.GetComponent<InputField>();
        m_gameObjectInputC0Im = GameObject.Find("InputField C0 Im");
        m_inputC0Im = m_gameObjectInputC0Im.GetComponent<InputField>();

        m_sliderRe = GameObject.Find("Slider C Re").GetComponent<Slider>();
        m_sliderIm = GameObject.Find("Slider C Im").GetComponent<Slider>();
        m_sliderC0Re = GameObject.Find("Slider C0 Re").GetComponent<Slider>();
        m_sliderC0Im = GameObject.Find("Slider C0 Im").GetComponent<Slider>();
        m_centerLimitX = GameObject.Find("Slider Limit Center X").GetComponent<Slider>();
        m_centerLimitY = GameObject.Find("Slider Limit Center Y").GetComponent<Slider>();
        m_progressSlider = GameObject.Find("Slider Progress").GetComponent<Slider>();
        m_inputUsedCPowers = GameObject.Find("InputField Used C Powers").GetComponent<InputField>();
        m_inputUsedCPowers.enabled = false;
        m_gameObjectInputUsedC0Powers = GameObject.Find("InputField Used C0 Powers");
        m_inputUsedC0Powers = m_gameObjectInputUsedC0Powers.GetComponent<InputField>();
        m_inputUsedC0Powers.enabled = false;

        InitCLabelTexts();

        m_paletteImage = GameObject.Find("RawImage Palette").GetComponent<RawImage>();
        m_paletteImageDiscretized = GameObject.Find("RawImage Palette Discretized").GetComponent<RawImage>();
        m_paletteImageWithRepeat = GameObject.Find("RawImage Palette With Repeat").GetComponent<RawImage>();
        m_backgroundColorImage = GameObject.Find("Image Background Color").GetComponent<RawImage>();
        m_fractalImage = GameObject.Find("RawImage Fractal").GetComponent<RawImage>();

        m_fractalPreviewImage = GameObject.Find("RawImage Fractal Preview").GetComponent<RawImage>();
        Vector2 currentPreviewSize = m_fractalPreviewImage.GetComponent<RectTransform>().sizeDelta;
        // setting of similar xy size ratio as have Screen
        int newWidth = (int)(Screen.width * currentPreviewSize.y / Screen.height + 0.5f);
        currentPreviewSize.x = newWidth;
        m_fractalPreviewImage.GetComponent<RectTransform>().sizeDelta = currentPreviewSize;

        m_toggleSetWidth = GameObject.Find("Toggle Set Width");
        m_toggleSetHeight = GameObject.Find("Toggle Set Height");
        m_toggleTransparentOnlyBegin = GameObject.Find("Toggle Transparent Only First Period");
        m_inputFieldExportedImageWidth = GameObject.Find("InputField Width").GetComponent<InputField>();
        m_inputFieldExportedImageHeight = GameObject.Find("InputField Height").GetComponent<InputField>();

        m_mainPanel = GameObject.Find("Panel Main");
        m_exportPanel = GameObject.Find("Panel Export");
        m_panels[PanelType.Computation] = GameObject.Find("Panel Computation");
        m_panels[PanelType.Other] = GameObject.Find("Panel Other");

        m_buttonUndo = GameObject.Find("Button Undo").GetComponent<Button>();
        m_buttonRedo = GameObject.Find("Button Redo").GetComponent<Button>();
        m_buttonRandomize = GameObject.Find("Button Randomize").GetComponent<Button>();
        m_buttonCompute = GameObject.Find("Button Compute").GetComponent<Button>();

        m_panelToggles[PanelType.Computation] = GameObject.Find("Toggle Computation").GetComponent<Toggle>();
        m_panelToggles[PanelType.Other] = GameObject.Find("Toggle Other").GetComponent<Toggle>();

        m_dropdownAlgorithmType = GameObject.Find("Dropdown Algorithm Type").GetComponent<Dropdown>();
        m_inputFormula = GameObject.Find("InputField Formula").GetComponent<InputField>();

        m_inputAreaCenterX = GameObject.Find("InputField CenterX").GetComponent<InputField>();
        m_inputAreaCenterY = GameObject.Find("InputField CenterY").GetComponent<InputField>();
        m_inputAreaSizeX = GameObject.Find("InputField Area Size X").GetComponent<InputField>();
        m_inputAreaSizeY = GameObject.Find("InputField Area Size Y").GetComponent<InputField>();

        m_inputLimitCenterX = GameObject.Find("InputField LimitCenterX").GetComponent<InputField>();
        m_inputLimitCenterY = GameObject.Find("InputField LimitCenterY").GetComponent<InputField>();
        m_inputLimitRadius = GameObject.Find("InputField LimitRadius").GetComponent<InputField>();

        m_inputNumberOfColors = GameObject.Find("InputField Color Count").GetComponent<InputField>();
        m_inputfieldMaxComputationStepCount = GameObject.Find("InputField Max Computation Step Count").GetComponent<InputField>();
        m_inputfieldTransparentColorIndex = GameObject.Find("InputField Transparent Color Index").GetComponent<InputField>();

        m_dropdownSelectedPowerInput = GameObject.Find("Dropdown Power").GetComponent<Dropdown>();
        m_dropdownProportional = GameObject.Find("Dropdown Proportional - Stretch").GetComponent<Dropdown>(); 
        m_dropdownExportSizeType = GameObject.Find("Dropdown Size Type").GetComponent<Dropdown>();

        m_loadPath = Persistent.GetFractalsDir();

        InitFractal();
        m_previewFractal = new ComplexFractal2D();

        SetUI();

        if (Persistent.Instance.ComputeImmediatelyRandom2D)
        {
            ColorBlock colorBlock = new ColorBlock();
            colorBlock = m_buttonRandomize.colors;
            colorBlock.normalColor = Color.red;
            m_buttonRandomize.colors = colorBlock;
            OnButtonRandomize();
        }
    }

    void SetUI()
    {
        UpdateUndoRedoButtons();

        InitFractalImage();

        ActivatePanel(MainPanelType.PanelMain);
        ActivateParameterPanel(PanelType.Computation);
    }

    void InitFractal()
    {
        SetActiveFractal(null);
        if (Persistent.Instance.SelectedComplex2DFile.Length > 0 && File.Exists(Persistent.Instance.SelectedComplex2DFile))
        {
            if (!ComplexFractal2D.Load(Persistent.Instance.SelectedComplex2DFile, out m_activeFractal))
            {
                SetActiveFractal(null);
            }
        }

        if (m_activeFractal == null)
        {
            // default fractal
            m_activeFractal = new ComplexFractal2D();
        }
        m_activeFractal.GetUndoRedoList().AddNewValue();
    }

    void InitFractalImage()
    {
        Texture2D texture = new Texture2D((int)m_fractalImage.rectTransform.rect.width, (int)m_fractalImage.rectTransform.rect.height);
        TextureUtils.FillWithColor(texture, new Color(0, 0, 0, 0));
        m_fractalImage.texture = texture;
    }

    void InitComputationPanel()
    {
        InitSwitchButtonText();
        new EnumWidget(new Ref<int>(() => GetActiveFractal().PowerCindex, 
            v => {
                GetActiveFractal().PowerCindex = v;
                OnSelectedPowerChanged(GetActiveFractal().PowerCindex);
            }), 
            m_dropdownSelectedPowerInput).OnChangeDelegate += AddNewValueToUndoList;

        new EnumWidget(new Ref<int>(() => (int)GetActiveFractal().ComputationType,
            v => { GetActiveFractal().ComputationType = (ComplexFractal2D.EComputationType)v;
                InitAlgorithmDependentWidgets();
            }),
            m_dropdownAlgorithmType).OnChangeDelegate += AddNewValueToUndoList;

        // init of c
        OnSelectedPowerChanged(GetActiveFractal().PowerCindex);

        GameObject panel = GameObject.Find("Panel Computation");
        
        new IntWidget(new Ref<int>(() => GetActiveFractal().MaxComputationStepCount,
            v => { GetActiveFractal().MaxComputationStepCount = v; }),
            m_inputfieldMaxComputationStepCount).OnChangeDelegate += AddNewValueToUndoList;

        DoubleWidget widget = new DoubleWidget(new Ref<double>(() => GetActiveFractal().ComputationArea.CenterX,
            v => { GetActiveFractal().ComputationArea.CenterX = v; }),
            m_inputAreaCenterX);
        widget.OnChangeDelegate += AddNewValueToUndoList;
        GetActiveFractal().ComputationArea.AddMethodObserver(widget, "CenterX");
        widget = new DoubleWidget(new Ref<double>(() => GetActiveFractal().ComputationArea.CenterY,
            v => { GetActiveFractal().ComputationArea.CenterY = v; }),
            m_inputAreaCenterY);
        widget.OnChangeDelegate += AddNewValueToUndoList;
        GetActiveFractal().ComputationArea.AddMethodObserver(widget, "CenterY");
        widget = new DoubleWidget(new Ref<double>(() => GetActiveFractal().ComputationArea.SizeX,
            v => { GetActiveFractal().ComputationArea.SizeX = v; }),
            m_inputAreaSizeX);
        widget.OnChangeDelegate += AddNewValueToUndoList;
        GetActiveFractal().ComputationArea.AddMethodObserver(widget, "SizeX");
        widget = new DoubleWidget(new Ref<double>(() => GetActiveFractal().ComputationArea.SizeY,
            v => { GetActiveFractal().ComputationArea.SizeY = v; }),
            m_inputAreaSizeY);
        widget.OnChangeDelegate += AddNewValueToUndoList;
        GetActiveFractal().ComputationArea.AddMethodObserver(widget, "SizeY");

        if (GetActiveFractal().LimitArea is CircularArea2D)
        {
            DoubleWidget dWidget = new DoubleWidget(new Ref<double>(() => (GetActiveFractal().LimitArea as CircularArea2D).CenterX,
                v => { (GetActiveFractal().LimitArea as CircularArea2D).CenterX = v; }),
                m_inputLimitCenterX);
            dWidget.OnChangeDelegate += AddNewValueToUndoList;
            new SliderWidget(m_centerLimitX, dWidget, -Persistent.Instance.MaxSliderValue, Persistent.Instance.MaxSliderValue);

            dWidget = new DoubleWidget(new Ref<double>(() => (GetActiveFractal().LimitArea as CircularArea2D).CenterY,
                v => { (GetActiveFractal().LimitArea as CircularArea2D).CenterY = v; }),
                m_inputLimitCenterY);
            dWidget.OnChangeDelegate += AddNewValueToUndoList;
            new SliderWidget(m_centerLimitY, dWidget, -Persistent.Instance.MaxSliderValue, Persistent.Instance.MaxSliderValue);

            new DoubleWidget(new Ref<double>(() => (GetActiveFractal().LimitArea as CircularArea2D).Radius,
                v => { (GetActiveFractal().LimitArea as CircularArea2D).Radius = v; }),
                m_inputLimitRadius).OnChangeDelegate += AddNewValueToUndoList;
        }

        new EnumWidget(new Ref<int>(() => (int)GetActiveFractal().DisplayMode,
            v => { GetActiveFractal().DisplayMode = (ComplexFractal2D.EDisplayMode)v; }),
            m_dropdownProportional).OnChangeDelegate += AddNewValueToUndoList;

        InitAlgorithmDependentWidgets();

        // setting of used powers
        OnUsedPowersChanged();
        OnUsedC0PowersChanged();
    }

    void AddNewValueToUndoList()
    {
        GetActiveFractal().GetUndoRedoList().AddNewValue();
        UpdateUndoRedoButtons();
    }

    void InitAlgorithmDependentWidgets()
    {
        bool activeMixedWidgets = GetActiveFractal().ComputationType == ComplexFractal2D.EComputationType.MixedMJ;
        m_gameObjectInputC0Re.SetActive(activeMixedWidgets);
        m_gameObjectInputC0Im.SetActive(activeMixedWidgets);
        m_sliderC0Re.gameObject.SetActive(activeMixedWidgets);
        m_sliderC0Im.gameObject.SetActive(activeMixedWidgets);
        m_gameObjectInputUsedC0Powers.SetActive(activeMixedWidgets);
        m_inputFormula.text = GetActiveFractal().GetFormula();
    }

    void InitOtherPanel()
    {
        GameObject panel = GameObject.Find("Panel Other");

        // computing of palette image
        SetPaletteImage();
        SetDiscretizedPaletteImage();
        SetPaletteImageWithRepeat();

        UpdateInterfaceFractalBackgroundColor();
        UpdatetoggleTransparentOnlyBeginVisibility();

        new IntWidget(new Ref<int>(() => GetActiveFractal().ColoringAlgorithm.ColorCount,
            v => {
                if (v < 2)
                {
                    MessagePanel.Message("Color count have to be bigger than 1");
                    m_inputNumberOfColors.text = "2";
                    v = 2;
                }
                GetActiveFractal().ColoringAlgorithm.ColorCount = v;
                UpdatePaletteImages();
                RefreshFractalImage();
            }),
            m_inputNumberOfColors).OnChangeDelegate += AddNewValueToUndoList;

        new IntWidget(new Ref<int>(() => GetActiveFractal().ColoringAlgorithm.TransparentColorIndex,
            v => { GetActiveFractal().ColoringAlgorithm.TransparentColorIndex = v;
                UpdatePaletteImages();
                RefreshFractalImage();
                UpdatetoggleTransparentOnlyBeginVisibility();
            }),
            m_inputfieldTransparentColorIndex).OnChangeDelegate += AddNewValueToUndoList;

        new BoolWidget(new Ref<bool>(() => GetActiveFractal().ColoringAlgorithm.TransparentOnlyBegin,
            v => { GetActiveFractal().ColoringAlgorithm.TransparentOnlyBegin = v;
                UpdatePaletteImages();
                RefreshFractalImage();
            }),
            m_toggleTransparentOnlyBegin.GetComponent<Toggle>()).OnChangeDelegate += AddNewValueToUndoList;

        EnumWidget repetitionTypeWidget = new EnumWidget(new Ref<int>(() => (int)GetActiveFractal().ColoringAlgorithm.RepeatType,
            v => {
                GetActiveFractal().ColoringAlgorithm.RepeatType = (RepeatType)v;
                UpdatePaletteImages();
                RefreshFractalImage();
            }),
            UIUtils.FindComponentInChildren<Dropdown>(panel, "Dropdown Repeat Type"));
        repetitionTypeWidget.OnChangeDelegate += AddNewValueToUndoList;

        new StringWidget(new Ref<string>(() => GetActiveFractal().Name, v => { GetActiveFractal().Name = v; }), m_inputName).OnChangeDelegate += AddNewValueToUndoList;

        List<string> options = new List<string>();
        options.Add("Saw"); options.Add("Triangle");
        repetitionTypeWidget.SetOptions(options);
    }

    void UpdatetoggleTransparentOnlyBeginVisibility()
    {
        m_toggleTransparentOnlyBegin.SetActive(GetActiveFractal().ColoringAlgorithm.TransparentColorIndex >= 0);
    }

    // Update is called once per frame
    void Update()
    {
        m_progressSlider.value = Persistent.Instance.Progress;
        GameObject textProgress = GameObject.Find("Text Progress");
        if (textProgress)
        {
            textProgress.GetComponent<Text>().text = Persistent.Instance.Progress.ToString();
        }
        if (m_exportProgressSlider != null)
        {
            m_exportProgressSlider.value = Persistent.Instance.Progress;
        }

        // after computation step for preview fractal
        if (m_previewComputationStruct != null)
        {
            if (m_previewComputationStruct.IsComputationFinished)
            {
                m_previewComputationStruct.IsComputationFinished = false;
                m_previewComputationStruct.PerformAfterComputation();
            }
        }
        // check computation result
        if (m_computationStruct != null)
        {
            if (m_computationStruct.IsComputationFinished)
            {
                m_computationStruct.IsComputationFinished = false;
                m_computationStruct.PerformAfterComputation();
            }
        }
        if (m_exportComputationStruct != null)
        {
            if (m_exportComputationStruct.IsComputationFinished)
            {
                m_exportComputationStruct.IsComputationFinished = false;
                m_exportComputationStruct.PerformAfterComputation();
            }
        }

        // check of change of fractal arameters for undo/redo list
        string computationParametersHash = GetActiveFractal().GetComputationParametersHash();
        if (m_undoRedoParams.computationParametersHash == computationParametersHash)
        {
            if (UnityEngine.Time.realtimeSinceStartup > m_undoRedoParams.timeOfRecord + PlatformConstants.ComplexRecordChangeDelay)
            {
                AddNewValueToUndoList();
                m_undoRedoParams.timeOfRecord = 1e15f; // some big time
            }
        }
        else
        {
            m_undoRedoParams.computationParametersHash = computationParametersHash;
            m_undoRedoParams.timeOfRecord = UnityEngine.Time.realtimeSinceStartup;
        }

        //if (m_bigFractalParams.computationParametersHash == computationParametersHash)
        //{
        //    if (UnityEngine.Time.realtimeSinceStartup > m_bigFractalParams.timeOfRecord + PlatformConstants.ComplexRecordChangeDelay)
        //    {
        //        m_bigFractalParams.timeOfRecord = 1e15f; // some big time
        //    }
        //}

        // check of change of fractal for preview
        if (ArePreviewFractalParametersChanged(computationParametersHash))
        {
            OnFractalParametersChanged();
        }

        bool isStringInputFieldFocused = UIUtils.IsStringInputFieldFocused();
        if (!isStringInputFieldFocused)
        {
            if (!m_isPanelHidden && Input.GetKeyDown(KeyCode.H))
            {
                OnHideUI();
                return;
            }

            if (Input.GetKeyDown(KeyCode.C))
            {
                OnButtonCompute();
                return;
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                OnButtonRandomize();
                return;
            }

            if (Input.GetKeyDown(KeyCode.Z) && (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)))
            {
                // undo
                OnUndo();
                return;
            }
            if (Input.GetKeyDown(KeyCode.Y) && (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)))
            {
                // redo
                OnRedo();
                return;
            }
        }

        /////////////////// zooming //////////////////////
        double zoomBase = 2;
        double zoomSensitivity = 2;
        if (Input.GetKey(KeyCode.LeftShift))
        {
            zoomSensitivity = 0.2;
        }
        float wheelValue = Input.GetAxis("Mouse ScrollWheel");
        float zoomCoefficient = (float)System.Math.Pow(zoomBase, wheelValue * zoomSensitivity);
        //zoomCoefficient += 1;
        if (wheelValue > 0 || wheelValue < 0)
        {
            OnZoom(zoomCoefficient);
            return;
        }

        if (Input.GetKey(KeyCode.LeftAlt) && Input.GetKeyDown(KeyCode.UpArrow))
        {
            float zoomCoef = (float)System.Math.Pow(zoomBase, 0.2 * zoomSensitivity);
            OnZoom(zoomCoef);
            return;
        }
        if (Input.GetKey(KeyCode.LeftAlt) && Input.GetKeyDown(KeyCode.DownArrow))
        {
            float zoomCoef = (float)System.Math.Pow(zoomBase, -0.2 * zoomSensitivity);
            OnZoom(zoomCoef);
            return;
        }

        //////////////// unhiding and init of drag //////////////////////////
        if (m_isPanelHidden && (Input.anyKeyDown && !Input.GetKeyDown(KeyCode.LeftAlt)) 
            && UnityEngine.Time.realtimeSinceStartup > m_hideTime + MinHideDelay)
        {
#if UNITY_STANDALONE
            // on windows etc. isn't gui unhided by mouse click - key is required (for possibility of drag fractal)
            if (!(Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2)))
#endif
            {
                m_mainPanel.SetActive(true);
                m_isPanelHidden = false;
                return;
            }

            if (Input.GetMouseButtonDown(0))
            {
                m_mouseDownPoint = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
                m_mouseDragInited = true;
            }
        }

        /////////// drag //////////////
        if (m_isPanelHidden && m_mouseDragInited && Input.GetKey(KeyCode.Mouse0))
        {
            OnMouseDragByUpdate();
        }
        if (!Input.GetKey(KeyCode.Mouse0))
        {
            {
            if (m_mouseDragInited)
                OnMouseDradByUpdateFinished();
            }
            m_mouseDragInited = false;
        }

        //////////// recompute of fractal on idle ////////////
        if (m_recomputeOnIdle && 
            UnityEngine.Time.realtimeSinceStartup > m_lastRecomputeRequestTime + 0.2f)
        {
            m_recomputeOnIdle = false;
            OnButtonCompute();
        }
    }

    void OnMouseDragByUpdate()
    {

    }

    void OnMouseDradByUpdateFinished()
    {
        Vector2 m_mouseDifference = new Vector2(Input.mousePosition.x, Input.mousePosition.y) - m_mouseDownPoint;
        // determination of diffecence of center position
        if (GetActiveFractal().DisplayMode == ComplexFractal2D.EDisplayMode.Stretch)
        {
            Vector2 relativeScreenDifference = new Vector2(m_mouseDifference.x / Screen.width, m_mouseDifference.y / Screen.height);
            // transform to computation size
            GetActiveFractal().ComputationArea.CenterX -= GetActiveFractal().ComputationArea.SizeX * relativeScreenDifference.x;
            GetActiveFractal().ComputationArea.CenterY -= GetActiveFractal().ComputationArea.SizeY * relativeScreenDifference.y;
        }
        else
        {
            // proportional mode
            double pixelSizeX = (double)GetActiveFractal().ComputationArea.SizeX / Screen.width;
            double pixelSizeY = (double)GetActiveFractal().ComputationArea.SizeY / Screen.height;
            bool sizeAccordingToX = pixelSizeX > pixelSizeY;

            Vector2 relativeScreenDifference = sizeAccordingToX ? new Vector2(m_mouseDifference.x / Screen.width, m_mouseDifference.y / Screen.width) :
                new Vector2(m_mouseDifference.x / Screen.height, m_mouseDifference.y / Screen.height);
            //Vector2 shift = new Vector2(computationSize.x * relativeScreenDifference.x, computationSize.y * relativeScreenDifference.y);
            //GetActiveFractal().ComputationArea.SetCenter(GetActiveFractal().ComputationArea.GetCenter() - shift);
            GetActiveFractal().ComputationArea.CenterX -= GetActiveFractal().ComputationArea.SizeX * relativeScreenDifference.x;
            GetActiveFractal().ComputationArea.CenterY -= GetActiveFractal().ComputationArea.SizeY * relativeScreenDifference.y;
        }
        RecomputeOnIdle();
    }

    void OnZoom(float zoomCoefficient)
    {
        GetActiveFractal().ComputationArea.SizeX /= zoomCoefficient;
        GetActiveFractal().ComputationArea.SizeY /= zoomCoefficient;
        // update of interface

        RecomputeOnIdle();
    }

    void RecomputeOnIdle()
    {
        m_recomputeOnIdle = true;
        m_lastRecomputeRequestTime = UnityEngine.Time.realtimeSinceStartup;
    }

    void UpdateUndoRedoButtons()
    {
        m_buttonUndo.interactable = GetActiveFractal().GetUndoRedoList().IsUndoAvailable();
        m_buttonRedo.interactable = GetActiveFractal().GetUndoRedoList().IsRedoAvailable();
    }

    void SetPanelAccordingToChangedProperty(string propertyName)
    {
        if (propertyName == "")
        {
            return;
        }
        Dictionary<string, PanelType> propertyOnPanel = new Dictionary<string, PanelType>();
        propertyOnPanel["ComputationArea"] = PanelType.Computation;
        propertyOnPanel["DisplayMode"] = PanelType.Computation;
        propertyOnPanel["ComputationType"] = PanelType.Computation;
        propertyOnPanel["LimitArea"] = PanelType.Computation;
        propertyOnPanel["C"] = PanelType.Computation;
        propertyOnPanel["C0"] = PanelType.Computation;
        propertyOnPanel["MaxComputationStepCount"] = PanelType.Computation;
        propertyOnPanel["PowerCindex"] = PanelType.Computation;
        propertyOnPanel["Name"] = PanelType.Other;
        propertyOnPanel["ColoringAlgorithm"] = PanelType.Other;

        if (!propertyOnPanel.ContainsKey(propertyName))
        {
            Debug.Assert(false, string.Format("not handled property name {0}", propertyName));
            return;
        }

        ActivateParameterPanel(propertyOnPanel[propertyName]);
    }


    bool ArePreviewFractalParametersChanged(string currentFractalComputationHash)
    {
        if (GetActiveFractal() == null || GetPreviewFractal() == null)
        {
            return false;
        }
        //bool computationTypeDifferent = GetEditedFractal().ComputationType != GetPreviewFractal().ComputationType;
        //bool cDifferent = !GetEditedFractal().C.SequenceEqual(GetPreviewFractal().C);
        //bool c0Differet = !GetEditedFractal().C0.SequenceEqual(GetPreviewFractal().C0);
        //bool computationAreaDifferent = !GetEditedFractal().ComputationArea.Equals(GetPreviewFractal().ComputationArea);
        //bool displayModeDifferent = GetEditedFractal().DisplayMode != GetPreviewFractal().DisplayMode;
        //bool maxComputationStepDifferent = GetEditedFractal().MaxComputationStepCount != GetPreviewFractal().MaxComputationStepCount;
        //bool limitAreaDifferent = !GetEditedFractal().LimitArea.Equals(GetPreviewFractal().LimitArea);
        //bool coloringAlgorithmDifferent = GetEditedFractal().ColoringAlgorithm != GetPreviewFractal().ColoringAlgorithm;
        //bool different = computationTypeDifferent || cDifferent || c0Differet ||
        //    computationAreaDifferent || displayModeDifferent || maxComputationStepDifferent ||
        //    limitAreaDifferent || coloringAlgorithmDifferent;
        if (GetPreviewFractal().GetComputationParametersHash() != currentFractalComputationHash)
        {
            return true;
        }
        return GetActiveFractal().ColoringAlgorithm != GetPreviewFractal().ColoringAlgorithm;
        //return GetActiveFractal().ComputationType != GetPreviewFractal().ComputationType ||
        //    !GetActiveFractal().C.SequenceEqual(GetPreviewFractal().C) ||
        //    !GetActiveFractal().C0.SequenceEqual(GetPreviewFractal().C0) ||
        //    !GetActiveFractal().ComputationArea.Equals(GetPreviewFractal().ComputationArea) ||
        //    GetActiveFractal().DisplayMode != GetPreviewFractal().DisplayMode ||
        //    GetActiveFractal().MaxComputationStepCount != GetPreviewFractal().MaxComputationStepCount ||
        //    !GetActiveFractal().LimitArea.Equals(GetPreviewFractal().LimitArea) ||
        //    GetActiveFractal().ColoringAlgorithm != GetPreviewFractal().ColoringAlgorithm;
    }

    string ValidateEditedParameters()
    {
        if (GetActiveFractal().ComputationArea.HalfSizeX < 0)
        {
            ActivateParameterPanel(PanelType.Computation);
            return "Size X - Computation Area size has to be positive";
        }
        if (GetActiveFractal().ComputationArea.HalfSizeY < 0)
        {
            ActivateParameterPanel(PanelType.Computation);
            return "Size Y - Computation Area size has to be positive";
        }
        if (GetActiveFractal().MaxComputationStepCount <= 1)
        {
            ActivateParameterPanel(PanelType.Computation);
            return "Max Computation Step should be bigger than 1";
        }
        if (GetActiveFractal().ColoringAlgorithm.ColorCount <= 1)
        {
            ActivateParameterPanel(PanelType.Other);
            return "Color count should be bigger than 1";
        }
        if ((GetActiveFractal().LimitArea as CircularArea2D).Radius <= 0)
        {
            ActivateParameterPanel(PanelType.Computation);
            return "Radius should be positive.";
        }

        // no error
        return "";
    }

    void ActivatePanel(int i)
    {
        ActivateParameterPanel((PanelType)i);
    }

    void ActivateParameterPanel(PanelType panelType)
    {
        for (int i = (int)PanelType.__Begin; i < (int)PanelType.__End; i++)
        {
            m_panels[(PanelType)i].SetActive((PanelType)i == panelType);
            bool isOn = (PanelType)i == panelType;
            if (m_panelToggles[(PanelType)i].isOn != isOn)
            {
                m_panelToggles[(PanelType)i].isOn = isOn;
            }
        }

        // some initialization
        SetBackgroundColor();

        switch (panelType)
        {
        case PanelType.Computation:
            InitComputationPanel();
            break;
        case PanelType.Other:
            InitOtherPanel();
            break;
        }
    }

    PanelType GetActiveParameterPanelType()
    {
        for (int i = (int)PanelType.__Begin; i < (int)PanelType.__End; i++)
        {
            if (m_panelToggles[(PanelType)i].isOn)
            {
                return (PanelType)i;
            }
        }
        return PanelType.Computation;
    }

    void OnGUI()
    {
        if (m_isPanelHidden && m_hideTime != UnityEngine.Time.realtimeSinceStartup)
        {
            Rect rect = new Rect(Vector2.zero, new Vector2(Screen.width, Screen.height));
            Color temp = GUI.color;
            GUI.color = new Color(1, 1, 1, 0.0f);
            if (GUI.Button(rect, ""))
            {
                UnhideUI();
            }
            GUI.color = temp;
        }
    }

    System.Collections.IEnumerator UnhideUI()
    {
        m_mainPanel.SetActive(true);
        m_isPanelHidden = false;
        yield return new WaitForSeconds(0.0f);
    }

    void SetPaletteImage()
    {
        bool discretized = false;
        int textureSizeX = Mathf.FloorToInt(m_paletteImage.rectTransform.sizeDelta.x);
        int textureSizeY = Mathf.FloorToInt(m_paletteImage.rectTransform.sizeDelta.y);
        Texture2D t1 = new Texture2D(textureSizeX, textureSizeY);
        TextureUtils.WriteColoringAlgorithmIntoTexture(t1, GetActiveFractal().ColoringAlgorithm, new Rect(0, 0, textureSizeX, textureSizeY), discretized);
        m_paletteImage.texture = t1;
    }

    void SetDiscretizedPaletteImage()
    {
        bool discretized = true;
        int textureSizeX = Mathf.FloorToInt(m_paletteImageDiscretized.rectTransform.sizeDelta.x);
        int textureSizeY = Mathf.FloorToInt(m_paletteImageDiscretized.rectTransform.sizeDelta.y);
        Texture2D t2 = new Texture2D(textureSizeX, textureSizeY);
        //GetActiveFractal().Palette.SetColorCount(GetActiveFractal().ColoringAlgorithm.ColorCount);
        //TextureUtils.WriteColoringAlgorithmIntoTexture(t2, GetActiveFractal().ColoringAlgorithm, new Rect(0, 0, textureSizeX, textureSizeY), discretized);
        TextureUtils.WriteColorsHorizontal(t2, 0, 1,
            v => { return GetActiveFractal().ColoringAlgorithm.GetDiscretizedColorInBasicInterval(v); });
        m_paletteImageDiscretized.texture = t2;
    }

    void SetPaletteImageWithRepeat()
    {
        int textureSizeX = Mathf.FloorToInt(m_paletteImageWithRepeat.rectTransform.sizeDelta.x);
        int textureSizeY = Mathf.FloorToInt(m_paletteImageWithRepeat.rectTransform.sizeDelta.y);
        Texture2D t2 = new Texture2D(textureSizeX, textureSizeY);
        TextureUtils.WriteColorsHorizontal(t2, 0, GetActiveFractal().MaxComputationStepCount,
            v => { return GetActiveFractal().ColoringAlgorithm.GetColor((int)v); });
        m_paletteImageWithRepeat.texture = t2;
    }

    void UpdateInterfaceFractalBackgroundColor()
    {
        int textureSize = 2;
        Texture2D t = new Texture2D(textureSize, textureSize);
        TextureUtils.FillWithColor(t, GetActiveFractal().BackgroundColor);
        m_backgroundColorImage.texture = t;
    }

    void SaveFractal()
    {
        if (Persistent.Instance.CreateFractal)
        {
            // creation of new fractal name
            string newFilename = GetNewSaveName(Persistent.GetFractalsDir());
            GetActiveFractal().Save(newFilename);

            // selecting of newly created fractal
            Persistent.Instance.SelectedComplex2DFile = newFilename;
        }
        else
        {
            GetActiveFractal().Save(GetActiveFractal().FilePath);
        }
    }

    public static void SavePrevies(ComplexFractal2D fractal, string fractalFilename)
    {
        int smallSize = FractalSelectionSceneScript.GetListItemFractalSize();
        string smallPreviewFilename = PreviewSaver.GetPreviewFileName(fractalFilename, smallSize);
        TextureUtils.SaveTextureToPng(smallPreviewFilename, fractal.GetTexture(smallSize));
    }

    string GetNewSaveName(string saveDir)
    {
        int i = 0;
        while (File.Exists(GetFilename(saveDir, i)))
        {
            i++;
        }
        return GetFilename(saveDir, i);
    }

    string GetFilename(string saveDir, int i)
    {
        return saveDir + Persistent.ComplexFractalFilenamePrefix + i.ToString() + Persistent.ComplexFractalFilenameAnnex;
    }

    string GetUsedCPowers()
    {
        string result = "";
        for (int i = 0; i < GetActiveFractal().C.Count; i++)
        {
            if (!GetActiveFractal().GetC(i).IsZero())
            {
                result = result + string.Format("{0},", i);
            }
        }
        if (result.Length > 0)
        {
            // remove last comma
            result = result.Remove(result.Length - 1);
        }
        return result;
    }
    string GetUsedC0Powers()
    {
        string result = "";
        for (int i = 0; i < GetActiveFractal().C0.Count; i++)
        {
            if (!GetActiveFractal().GetC0(i).IsZero())
            {
                result = result + string.Format("{0},", i);
            }
        }
        if (result.Length > 0)
        {
            // remove last comma
            result = result.Remove(result.Length - 1);
        }
        return result;
    }

    void InitPredefinedImageResolutions()
    {
        UIUtils.FillInputField("InputField Predefined 2d Size 1", "1366,768");
        UIUtils.FillInputField("InputField Predefined 2d Size 2", "1920,1080");
    }

    public void OnButtonSetPredefinedSize1()
    {
        SavePredefinedExportImageSizes();
        string value = UIUtils.GetInputFieldValue("InputField Predefined 2d Size 1");
        if (!UIUtils.ValidateInputTwoInts(value))
        {
            MessagePanel.Message("Expected input should be: width,height - like 1000,800");
        }
        string[] values = value.Split(",".ToCharArray(), System.StringSplitOptions.RemoveEmptyEntries);
        Persistent.Instance.ExportedPictureWidth = int.Parse(values[0]);
        Persistent.Instance.ExportedPictureHeight = int.Parse(values[1]);
        InitExportPanel();
    }

    public void OnButtonSetPredefinedSize2()
    {
        SavePredefinedExportImageSizes();
        string value = UIUtils.GetInputFieldValue("InputField Predefined 2d Size 2");
        if (!UIUtils.ValidateInputTwoInts(value))
        {
            MessagePanel.Message("Expected input should be: width,height - like 1000,800");
        }
        string[] values = value.Split(",".ToCharArray(), System.StringSplitOptions.RemoveEmptyEntries);
        Persistent.Instance.ExportedPictureWidth = int.Parse(values[0]);
        Persistent.Instance.ExportedPictureHeight = int.Parse(values[1]);
        InitExportPanel();
    }

    void InitExportPanel()
    {
        if (m_exportProgressSlider == null)
        {
            m_exportProgressSlider = GameObject.Find("Slider Progress").GetComponent<Slider>();
            m_exportProgressSlider.gameObject.SetActive(false);
        }
        new EnumWidget(new Ref<int>(() => (int)Persistent.Instance.ExportSizeType,
            v =>
            {
                Persistent.Instance.ExportSizeType = (ExportSizeType)v;
            }),
            m_dropdownExportSizeType).OnChangeDelegate += OnChangeExportSizeType;

        OnChangeExportSizeType();
        InitPredefinedImageResolutions();
    }

    void ClearScene()
    {
        //zxc
    }

    InputField m_inputName;

    InputField m_inputRe;
    InputField m_inputIm;
    InputField m_inputC0Re;
    InputField m_inputC0Im;
    GameObject m_gameObjectInputC0Re;
    GameObject m_gameObjectInputC0Im;

    Slider m_sliderRe;
    Slider m_sliderIm;
    Slider m_sliderC0Re;
    Slider m_sliderC0Im;
    SliderWidget m_sliderWidgetRe;
    SliderWidget m_sliderWidgetIm;
    SliderWidget m_sliderWidgetC0Re;
    SliderWidget m_sliderWidgetC0Im;
    Slider m_centerLimitX;
    Slider m_centerLimitY;
    Slider m_progressSlider;
    Slider m_exportProgressSlider;

    InputField m_inputAreaCenterX;
    InputField m_inputAreaCenterY;
    InputField m_inputAreaSizeX;
    InputField m_inputAreaSizeY;
    InputField m_inputLimitCenterX;
    InputField m_inputLimitCenterY;
    InputField m_inputLimitRadius;
    InputField m_inputNumberOfColors;

    InputField m_inputfieldMaxComputationStepCount;
    InputField m_inputfieldTransparentColorIndex;
    InputField m_inputUsedCPowers;
    InputField m_inputUsedC0Powers;
    GameObject m_gameObjectInputUsedC0Powers;

    GameObject m_toggleSetWidth;
    GameObject m_toggleSetHeight;
    GameObject m_toggleTransparentOnlyBegin;
    InputField m_inputFieldExportedImageWidth;
    InputField m_inputFieldExportedImageHeight;

    GameObject m_mainPanel;
    GameObject m_exportPanel;
    Dictionary<PanelType, GameObject> m_panels = new Dictionary<PanelType, GameObject>();
    Dictionary<PanelType, Toggle> m_panelToggles = new Dictionary<PanelType, Toggle>();
    RawImage m_paletteImage;
    RawImage m_paletteImageDiscretized;
    RawImage m_paletteImageWithRepeat;
    RawImage m_backgroundColorImage;
    RawImage m_fractalImage;
    RawImage m_fractalPreviewImage;
    Dropdown m_dropdownAlgorithmType;
    Dropdown m_dropdownSelectedPowerInput;
    Dropdown m_dropdownProportional;
    Dropdown m_dropdownExportSizeType;

    InputField m_inputFormula;
    Button m_buttonUndo;
    Button m_buttonRedo;
    Button m_buttonRandomize;
    Button m_buttonCompute;
    bool m_isPanelHidden;
    float m_hideTime = 0;
    const float MinHideDelay = 0.2f;

    string m_loadPath;

    ComplexFractal2D m_activeFractal;
    private ModalPanel m_questionPanel;

    ComplexFractal2D m_previewFractal;
    bool m_recomputeOnIdle;
    float m_lastRecomputeRequestTime;

    bool m_mouseDragInited;
    private Vector2 m_mouseDownPoint;
    Complex2DComputationStruct m_computationStruct;
    Complex2DComputationStruct m_previewComputationStruct;
    Complex2DComputationStruct m_exportComputationStruct;
    Texture2D m_previewTexture;
    ComputationTimeParams m_undoRedoParams;
    ComputationTimeParams m_bigFractalParams;
}
﻿using Assets.Scripts.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Assets.Scripts.Computation
{
    class _3DComputationStruct
    {
        public _3DComputationStruct(I3DFractal fractal)
        {
            m_isComputationFinished = false;
            m_isJustComputed = false;
            m_fractal = fractal;
        }

        public I3DFractal Fractal
        {
            get { return m_fractal; }
        }

        public void SetFractal(I3DFractal fractal)
        {
            m_fractal = fractal;
            m_isComputationFinished = false;
            m_isJustComputed = false;
        }

        //event 
        public delegate void PerformAfterComputationDelegate();

        public PerformAfterComputationDelegate PerformAfterComputation
        {
            get { return m_performAfterComputation; }
            set { m_performAfterComputation = value; }
        }

        public bool IsJustComputed
        {
            get { return m_isJustComputed; }
            set { m_isJustComputed = value; }
        }

        public bool IsComputationFinished
        {
            get { return m_isComputationFinished; }
            set { m_isComputationFinished = value; }
        }

        public List<Box> BoxesCpp
        {
            get { return m_boxesCpp; }
        }

        public string GraphicSaveFile
        {
            get { return m_graphicSaveFile; }
        }

        public void ComputeAsynchronous()
        {
            IsComputationFinished = false;

            m_boxesCpp.Clear();

            // clearing of actual graphic
            QuaternionFractal3D fractal = (Fractal as QuaternionFractal3D);
            fractal.GetGraphic().Clear(true, false);
            fractal.CreateNewGraphic();
            fractal.GetGraphic().SetColor(fractal.FractalColor);

            m_graphicSaveFile = fractal.GetGraphicSaveFile(true);

            if (m_workingThread == null)
            {
                m_workingThread =
                new System.Threading.Thread(() =>
                {
                    while (true)
                    {
                        fractal.ComputeFractalBoxes(m_graphicSaveFile, m_boxesCpp);
                        IsComputationFinished = true;
                        m_waitHandle.WaitOne();
                    }
                });
                m_workingThread.Start();
            }
            else
            {
                m_waitHandle.Set();
            }
        }

        I3DFractal m_fractal;
        private PerformAfterComputationDelegate m_performAfterComputation = null;
        bool m_isComputationFinished;
        bool m_isJustComputed;
        Thread m_workingThread;
        EventWaitHandle m_waitHandle = new AutoResetEvent(false);
        List<Box> m_boxesCpp = new List<Box>();
        string m_graphicSaveFile;
    }
}

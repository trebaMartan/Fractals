﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using UnityEngine;

namespace Assets.Scripts.Computation
{
    public class Complex2DComputationStruct
    {
        public Complex2DComputationStruct(ComplexFractal2D fractal)
        {
            m_isComputationFinished = false;
            m_isJustComputed = false;
            m_fractal = fractal;
        }

        public ComplexFractal2D Fractal
        {
            get { return m_fractal; }
        }

        public void SetFractal(ComplexFractal2D fractal)
        {
            m_fractal = fractal;
        }

        //event 
        public delegate void PerformAfterComputationDelegate();

        public PerformAfterComputationDelegate PerformAfterComputation
        {
            get { return m_performAfterComputation; }
            set { m_performAfterComputation = value; }
        }

        public IterationIndex IterationIndex
        {
            get { return m_iterationIndex; }
            set { m_iterationIndex = value; }
        }

        // nejak dat zpravu hlavnimu threadu, ze vypocet je dokoncen a ma provest perform after computation

        public bool IsJustComputed
        {
            get { return m_isJustComputed; }
            set { m_isJustComputed = value; }
        }

        public bool IsComputationFinished
        {
            get { return m_isComputationFinished; }
            set { m_isComputationFinished = value; }
        }

        public void ComputeIterationIndexAsynchronous(int width, int height)
        {
            IsComputationFinished = false;

            if (m_workingThread == null)
            {
                m_workingThread =
                new System.Threading.Thread(() =>
                {
                    while (true)
                    {
                        IterationIndex = Fractal.GetOrComputeIterationIndex(width, height);
                        IsComputationFinished = true;
                        m_waitHandle.WaitOne();
                    }
                });
                m_workingThread.Start();
            }
            else
            {
                m_waitHandle.Set();
            }
        }

        public Texture2D ComputeTextureFromIterationIndex(Texture2D texture)
        {
            // make image from colorIndexes
            Fractal.CreateTextureFromIterationIndex(texture, IterationIndex);
            bool applyBackgroundColor = true;
            if (applyBackgroundColor)
            {
                TextureUtils.MixTextureWithBackgroundColor(texture, Fractal.BackgroundColor);
            }
            return texture;
        }

        private PerformAfterComputationDelegate m_performAfterComputation = null;
        bool m_isComputationFinished;
        bool m_isJustComputed;
        ComplexFractal2D m_fractal;
        IterationIndex m_iterationIndex;
        Thread m_workingThread;
        EventWaitHandle m_waitHandle = new AutoResetEvent(false);
    }
}

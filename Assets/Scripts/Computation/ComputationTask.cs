﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Assets.Scripts.Utils;
using UnityEngine;
using Assets.Scripts.Math;

namespace Assets.Scripts.Computation
{
    class ComputationTask
    {
        static public QuaternionFractal3D ComputeRandomFractal(string fractalPath)
        {
            // computes random fractal
           
            QuaternionFractal3D fractal;
            List<Box> boxes = new List<Box>();
            UnityEngine.Bounds bounds;
            do
            {
                fractal = RandomFunc.GetRandomFractalDefinition(fractalPath);
                fractal.CubeFineness = QuaternionFractal3D.Fineness.Low;
                fractal.GetGraphic().BoxesCount = 0;
                fractal.ComputeFractal(false, true, boxes);
                // check if boxes are rather like sphere - not interesting random fractal
            }
            while (fractal.GetGraphic().BoxesCount < 3 || BoxesAreAlmostSpherical(boxes, fractal.MinCubeSize, out bounds));

            // setting of adjusted bounds
            bounds.size = bounds.size * 1.3f;
            fractal.ComputationArea.SetFromGraphicBounds(bounds, true);

            fractal.CubeFineness = Persistent.Instance.RandomComputationFineness;
            fractal.ComputeFractal(true);
            fractal.Save(fractalPath);
            return fractal;
        }

        static public void ComputeFractalDouble(QuaternionFractal3D fractal, QuaternionFractal3D.Fineness fineness)
        {
            List<Box> boxes = new List<Box>();
            UnityEngine.Bounds bounds;
            fractal.GetGraphic().BoxesCount = 0;
            fractal.MaximizeComputationArea();
            fractal.CubeFineness = QuaternionFractal3D.Fineness.Normal;// precombutation
            fractal.ComputeFractal(false, true, boxes);
            bounds = Fractal3DGraphic.GetBounds(boxes, (float)fractal.MinCubeSize);

            bounds.size = bounds.size * 1.2f;
            fractal.ComputationArea.SetFromGraphicBounds(bounds, true);

            fractal.CubeFineness = fineness;// QuaternionFractal3D.Fineness.High;// better fineness than 1st computation
            fractal.ComputeFractal(true);
        }

        static bool IsInTolerance(float a, float b, float tolerance)
        {
            if (a == 0 && b == 0)
            {
                return true;
            }
            if (a == 0 || b == 0)
            {
                return false;
            }
            float dif = (a - b) / b;
            return System.Math.Abs(dif) <= tolerance; 
        }

        static bool BoxesAreAlmostSpherical(List<Box> boxes, double minCubeSize, out UnityEngine.Bounds bounds)
        {
            bounds = Fractal3DGraphic.GetBounds(boxes, (float)minCubeSize);
            // checking of sizes 
            float sizeTolerance = 0.3f;
            if (!IsInTolerance(bounds.size.x, bounds.size.y, sizeTolerance) ||
                !IsInTolerance(bounds.size.x, bounds.size.z, sizeTolerance) ||
                !IsInTolerance(bounds.size.y, bounds.size.z, sizeTolerance))
            {
                return false;
            }

            // check all lines - points on end of lines
            int allPointsCount = 0;
            foreach (Box box in boxes)
            {
                if (box.size.z == minCubeSize)
                {
                    allPointsCount++;
                }
                else
                {
                    allPointsCount += 2;
                }
            }
            float radius = (bounds.size.x + bounds.size.y + bounds.size.z) / 6;
            float maxAllowedPercentOnSphere = 80;
            float sphereTolerance = 0.1f;
            int pointsOnSphereCount = 0;
            int pointsNotOnSphereCount = 0;
            int liminPointsNotOnSphere = (int)(allPointsCount * (100 - maxAllowedPercentOnSphere) / 100);
            int maxPointsOnSphere = (int)(allPointsCount * maxAllowedPercentOnSphere / 100);
            UnityEngine.Vector3 center = bounds.center;
            UnityEngine.Vector3 point;
            foreach (Box box in boxes)
            {
                if (pointsOnSphereCount > maxPointsOnSphere)
                {
                    return true;
                }
                if (pointsNotOnSphereCount > liminPointsNotOnSphere)
                {
                    return false;
                }
                if (box.size.z == minCubeSize)
                {
                    // one point
                    if (IsInTolerance((box.position - center).magnitude, radius, sphereTolerance))
                    {
                        pointsOnSphereCount++;
                    }
                    else
                    {
                        pointsNotOnSphereCount++;
                    }
                }
                else
                {
                    // line => check end points
                    point = box.position;
                    point.z -= box.size.z / 2;
                    if (IsInTolerance((point - center).magnitude, radius, sphereTolerance))
                    {
                        pointsOnSphereCount++;
                    }
                    else
                    {
                        pointsNotOnSphereCount++;
                    }
                    // second point
                    point.z += box.size.z;
                    if (IsInTolerance((point - center).magnitude, radius, sphereTolerance))
                    {
                        pointsOnSphereCount++;
                    }
                    else
                    {
                        pointsNotOnSphereCount++;
                    }
                }
            }
            return pointsOnSphereCount >= maxPointsOnSphere;
        }

        static double ChangeDouble(double d, double change, int significantDigits = 4)
        {
            if (significantDigits <= 0)
            {
                return d + change;
            }
            return (d + change).RoundToSignificantDigits(significantDigits);
        }

        public static System.Collections.Generic.IEnumerable<Pair<string, QuaternionFractal3D>> GetNeigbourhood(QuaternionFractal3D baseFractal, string resultDir)
        {
            // fractals in vicinity keep in a direrectory
            QuaternionFractal3D copy;
            double change = 0.1;
            bool cloneGraphic = false;
            if (baseFractal.CoefficientBeforePower)
            {
                for (int i = 0; i < baseFractal.C.Count; i++)
                {
                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.C[i].X0 = ChangeDouble(copy.C[i].X0, change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("C-{0}-PlusX0", i), copy);

                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.C[i].X0 = ChangeDouble(copy.C[i].X0, -change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("C-{0}-MinusX0", i), copy);

                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.C[i].I = ChangeDouble(copy.C[i].I, change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("C-{0}-PlusI", i), copy);

                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.C[i].I = ChangeDouble(copy.C[i].I, -change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("C-{0}-MinusI", i), copy);

                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.C[i].J = ChangeDouble(copy.C[i].J, change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("C-{0}-PlusJ", i), copy);

                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.C[i].J = ChangeDouble(copy.C[i].J, -change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("C-{0}-MinusJ", i), copy);

                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.C[i].K = ChangeDouble(copy.C[i].K, change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("C-{0}-PlusK", i), copy);

                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.C[i].K = ChangeDouble(copy.C[i].K, -change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("C-{0}-MinusK", i), copy);
                }
            }
            
            if (baseFractal.CoefficientAfterPower)
            {
                for (int i = 0; i < baseFractal.CA.Count; i++)
                {
                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.CA[i].X0 = ChangeDouble(copy.CA[i].X0, change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("CA-{0}-PlusX0", i), copy);

                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.CA[i].X0 = ChangeDouble(copy.CA[i].X0, -change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("CA-{0}-MinusX0", i), copy);

                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.CA[i].I = ChangeDouble(copy.CA[i].I, change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("CA-{0}-PlusI", i), copy);

                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.CA[i].I = ChangeDouble(copy.CA[i].I, -change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("CA-{0}-MinusI", i), copy);

                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.CA[i].J = ChangeDouble(copy.CA[i].J, change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("CA-{0}-PlusJ", i), copy);

                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.CA[i].J = ChangeDouble(copy.CA[i].J, -change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("CA-{0}-MinusJ", i), copy);

                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.CA[i].K = ChangeDouble(copy.CA[i].K, change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("CA-{0}-PlusK", i), copy);

                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.CA[i].K = ChangeDouble(copy.CA[i].K, -change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("CA-{0}-MinusK", i), copy);
                }
            }

            if (baseFractal.ComputeDelegateType == QuaternionDelegateType.MixedMJ &&
                baseFractal.CoefficientC0BeforePower)
            {
                for (int i = 0; i < baseFractal.C0.Count; i++)
                {
                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.C0[i].X0 = ChangeDouble(copy.C0[i].X0, change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("C0-{0}-PlusX0", i), copy);

                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.C0[i].X0 = ChangeDouble(copy.C0[i].X0, -change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("C0-{0}-MinusX0", i), copy);

                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.C0[i].I = ChangeDouble(copy.C0[i].I, change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("C0-{0}-PlusI", i), copy);

                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.C0[i].I = ChangeDouble(copy.C0[i].I, -change);

                    yield return new Pair<string, QuaternionFractal3D>(String.Format("C0-{0}-MinusI", i), copy);
                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.C0[i].J = ChangeDouble(copy.C0[i].J, change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("C0-{0}-PlusJ", i), copy);

                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.C0[i].J = ChangeDouble(copy.C0[i].J, -change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("C0-{0}-MinusJ", i), copy);

                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.C0[i].K = ChangeDouble(copy.C0[i].K, change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("C0-{0}-MinusK", i), copy);

                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.C0[i].K = ChangeDouble(copy.C0[i].K, -change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("C0-{0}-PlusK", i), copy);
                }
            }

            if (baseFractal.ComputeDelegateType == QuaternionDelegateType.MixedMJ &&
                baseFractal.CoefficientC0AfterPower)
            {
                for (int i = 0; i < baseFractal.C0A.Count; i++)
                {
                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.C0A[i].X0 = ChangeDouble(copy.C0A[i].X0, change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("C0A-{0}-PlusX0", i), copy);

                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.C0A[i].X0 = ChangeDouble(copy.C0A[i].X0, -change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("C0A-{0}-MinusX0", i), copy);

                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.C0A[i].I = ChangeDouble(copy.C0A[i].I, change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("C0A-{0}-PlusI", i), copy);

                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.C0A[i].I = ChangeDouble(copy.C0A[i].I, -change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("C0A-{0}-MinusI", i), copy);

                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.C0A[i].J = ChangeDouble(copy.C0A[i].J, change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("C0A-{0}-PlusJ", i), copy);

                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.C0A[i].J = ChangeDouble(copy.C0A[i].J, -change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("C0A-{0}-MinusJ", i), copy);

                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.C0A[i].K = ChangeDouble(copy.C0A[i].K, change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("C0A-{0}-PlusK", i), copy);

                    copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
                    copy.C0A[i].K = ChangeDouble(copy.C0A[i].K, -change);
                    yield return new Pair<string, QuaternionFractal3D>(String.Format("C0A-{0}-MinusK", i), copy);
                }
            }

            copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
            copy.MaxComputationStepCount = (int)(0.75f * copy.MaxComputationStepCount);
            yield return new Pair<string, QuaternionFractal3D>("Max075", copy);

            copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
            copy.MaxComputationStepCount = (int)(0.5f * copy.MaxComputationStepCount);
            yield return new Pair<string, QuaternionFractal3D>("Max050", copy);

            copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
            copy.MaxComputationStepCount = (int)(1.5f * copy.MaxComputationStepCount);
            yield return new Pair<string, QuaternionFractal3D>("Max150", copy);

            copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
            copy.MaxComputationStepCount = (int)(2.0f * copy.MaxComputationStepCount);
            yield return new Pair<string, QuaternionFractal3D>("Max200", copy);

            copy = new QuaternionFractal3D(baseFractal, cloneGraphic);
            if (baseFractal.LimitArea is RectangularQuaternionArea)
            {
                RectangularQuaternionArea rectArea = baseFractal.LimitArea as RectangularQuaternionArea;
                copy.LimitArea = new CircularQuaternionArea(rectArea.Center, (float)rectArea.HalfSize.Max());
                yield return new Pair<string, QuaternionFractal3D>("Circular", copy);
            }
            else
            {
                CircularQuaternionArea circularArea = baseFractal.LimitArea as CircularQuaternionArea;
                float r = (float)circularArea.Radius;
                copy.LimitArea = new RectangularQuaternionArea(circularArea.Center, new SmartMathLibrary.Quaternion(r,r,r,r));
                yield return new Pair<string, QuaternionFractal3D>("Rectangular", copy);
            }
        }

        public static string GetUniqueFileName(string mask, string directory)
        {
            string fractalMask = mask + "." + Persistent.GetExtension(Persistent.SaveExtension.fractal3DZipExtension);
            string fractalPath = FileUtils.GetMaxNumberFilename(directory, fractalMask, Persistent.GetExtension(Persistent.SaveExtension.fractal3DZipExtension));
            return fractalPath;
        }

        public static string GetUniqueFileName(string directory)
        {
            string fractalMask = "*." + Persistent.GetExtension(Persistent.SaveExtension.fractal3DZipExtension);
            string fractalPath = FileUtils.GetMaxNumberFilename(directory, fractalMask, Persistent.GetExtension(Persistent.SaveExtension.fractal3DZipExtension));
            return fractalPath;
        }

        static public ComplexFractal2D Compute2DRandomFractal(string fractalPath)
        {
            // computes random fractal
            ComplexFractal2D fractal;
            bool moreColorsThanMin = false;
            do
            {
                fractal = RandomFunc.GetRandom2DFractalDefinition(fractalPath);
                Texture2D texture = fractal.GetTexture(30);
                moreColorsThanMin = TextureUtils.HasColorCountBiggerThan(texture, 3);
            } while (!moreColorsThanMin); // texture with little color are skipped
            
            fractal.Save(fractalPath);
            Complex2DSceneScript.SavePrevies(fractal, fractalPath);
            return fractal;
        }
    }
}

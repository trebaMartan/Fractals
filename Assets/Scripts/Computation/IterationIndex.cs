﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Computation
{
    public class IterationIndex
    {
        public IterationIndex()
        {
            Iterations = new List<int>();
        }
        public IterationIndex(int width, int height)
        {
            Iterations = new List<int>();
            m_width = width;
            m_height = height;
            RecountSize();
        }

        private int m_width;
        public int Width
        {
            get { return m_width; }
            set { m_width = value;
                RecountSize();
            }
        }
        private int m_height;
        public int Height
        {
            get { return m_height; }
            set { m_height = value;
                RecountSize();
            }
        }
        public List<int> Iterations;
        public string ComputationParametersHash;

        public bool IsComputed()
        {
            return ComputationParametersHash != null && ComputationParametersHash.Length > 0;
        }

        public void SetNotComputed()
        {
            ComputationParametersHash = null;
        }

        public void SetComputed(string computationParametersHash)
        {
            ComputationParametersHash = computationParametersHash;
        }

        private void RecountSize()
        {
            Iterations = Enumerable.Repeat(-1, Width * Height).ToList();
        }
    }
}

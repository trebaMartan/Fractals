﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Computation
{
    public class PlatformConstants
    {
        public const int CachedNumberOfComputationIndexes = 10;
        public const float RecomputeBig2DFractalTime = 1.0f;
        public const float ComplexRecordChangeDelay = 0.5f;
        public const int UndoRedoListSize = 50;
    }
}

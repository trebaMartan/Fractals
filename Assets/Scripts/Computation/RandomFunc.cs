﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Utils;
using SmartMathLibrary;
using Exocortex.DSP;
using System.Diagnostics;

namespace Assets.Scripts.Computation
{
    static class RandomFunc
    {
        static public QuaternionFractal3D GetRandomFractalDefinition(string fractalPath)
        {
            QuaternionFractal3D fractal = new QuaternionFractal3D();

            bool antisymetric = Persistent.Instance.RandomCoefficientsDependenceType == Persistent.CoefficientsDependencyType.Transposed;
            bool inverse = Persistent.Instance.RandomCoefficientsDependenceType == Persistent.CoefficientsDependencyType.Inverse;

            switch(Persistent.Instance.RandomComputationUsedAlgorithmType)
            {
            case Fractals3DSceneScript.UsedAlgorithmType.Julia:
                fractal.ComputeDelegateType = QuaternionDelegateType.Julia;
                break;
            case Fractals3DSceneScript.UsedAlgorithmType.Mixed:
                fractal.ComputeDelegateType = QuaternionDelegateType.MixedMJ;
                break;
            case Fractals3DSceneScript.UsedAlgorithmType.Random:
                fractal.ComputeDelegateType = GetTrueWithProbability(0.5) ? QuaternionDelegateType.Julia : QuaternionDelegateType.MixedMJ;
                break;
            }

            if (antisymetric || inverse)
            {
                fractal.CoefficientBeforePower = fractal.CoefficientAfterPower = true;
            }
            else
            do
            {
                fractal.CoefficientBeforePower = GetTrueWithProbability(0.5);
                fractal.CoefficientAfterPower = GetTrueWithProbability(0.5);
            }
            while (!fractal.CoefficientBeforePower && !fractal.CoefficientAfterPower);

            List<bool> positions = GetRandomPositions();
            for (int i = 0; i < positions.Count; i++)
            {
                if (positions[i])
                {
                    if (fractal.CoefficientBeforePower)
                    {
                        fractal.SetC(i, GetRandomCoefficientQuaternion());
                    }
                    if (inverse)
                    {
                        fractal.SetCA(i, fractal.GetC(i).Inverse);
                    }
                    else
                    if (antisymetric)
                    {
                        fractal.SetCA(i, fractal.GetC(i).Conjugation);
                    }
                    else
                    if (fractal.CoefficientAfterPower)
                    {
                        fractal.SetCA(i, GetRandomCoefficientQuaternion());
                    }
                }
            }

            if (fractal.ComputeDelegateType == QuaternionDelegateType.MixedMJ)
            {
                // setting of C0 values
                if (antisymetric || inverse)
                {
                    fractal.CoefficientC0BeforePower = fractal.CoefficientC0AfterPower = true;
                }
                else
                    do
                    {
                    fractal.CoefficientC0BeforePower = GetTrueWithProbability(0.5);
                    fractal.CoefficientC0AfterPower = GetTrueWithProbability(0.5);
                }
                while (!fractal.CoefficientC0BeforePower && !fractal.CoefficientC0AfterPower);

                positions = GetRandomPositions();
                for (int i = 0; i < positions.Count; i++)
                {
                    if (positions[i])
                    {
                        if (fractal.CoefficientC0BeforePower)
                        {
                            fractal.SetC0(i, GetRandomCoefficientQuaternion());
                        }
                        if (inverse)
                        {
                            fractal.SetC0A(i, fractal.GetC0(i).Inverse);
                        }
                        else
                        if (antisymetric)
                        {
                            fractal.SetC0A(i, fractal.GetC0(i).Conjugation);
                        }
                        else
                        if (fractal.CoefficientC0AfterPower)
                        {
                            fractal.SetC0A(i, GetRandomCoefficientQuaternion());
                        }
                    }
                }
            }
            SetRandomMapping(fractal);
            fractal.CubeFineness = QuaternionFractal3D.Fineness.Low;
            fractal.MaxComputationStepCount = 25;
            fractal.FilePath = fractalPath;
            return fractal;
        }

        static void SetRandomMapping(QuaternionFractal3D fractal)
        {
            Fractals3DSceneScript.QuaternionMappingType mappingType = Persistent.Instance.RandomQuaternionMappingType;
            switch (mappingType)
            {
            case Fractals3DSceneScript.QuaternionMappingType.X0_I_J:
                break;
            case Fractals3DSceneScript.QuaternionMappingType.I_J_K:
                {
                    fractal.Vec2QuatMapping = new Vector2QuaternionMapping();
                    fractal.Vec2QuatMapping.SetMappingQuaternion(0, new Quaternion(0, 1, 0, 0));
                    fractal.Vec2QuatMapping.SetMappingQuaternion(1, new Quaternion(0, 0, 1, 0));
                    fractal.Vec2QuatMapping.SetMappingQuaternion(2, new Quaternion(0, 0, 0, 1));
                    fractal.Vec2QuatMapping.CountDependentValues();
                }
                break;
            case Fractals3DSceneScript.QuaternionMappingType.RandomBasicDirections:
                {
                    fractal.Vec2QuatMapping = new Vector2QuaternionMapping();
                    Quaternion quat = GetRandomBasicQuaternion();
                    Quaternion quat1;
                    Quaternion quat2;
                    GetTwoPrependicularQuaternions(quat, out quat1, out quat2);
                    fractal.Vec2QuatMapping.SetMappingQuaternion(0, quat);
                    fractal.Vec2QuatMapping.SetMappingQuaternion(1, quat1);
                    fractal.Vec2QuatMapping.SetMappingQuaternion(2, quat2);
                    fractal.Vec2QuatMapping.CountDependentValues();
                }
                break;
            case Fractals3DSceneScript.QuaternionMappingType.RandomDirections:
                {
                    fractal.Vec2QuatMapping = new Vector2QuaternionMapping();
                    Quaternion quat = GetRandomUnitQuaternion();
                    Quaternion quat1;
                    Quaternion quat2;
                    GetTwoPrependicularQuaternions(quat, out quat1, out quat2);
                    fractal.Vec2QuatMapping.SetMappingQuaternion(0, quat);
                    fractal.Vec2QuatMapping.SetMappingQuaternion(1, quat1);
                    fractal.Vec2QuatMapping.SetMappingQuaternion(2, quat2);
                    fractal.Vec2QuatMapping.CountDependentValues();
                }
                break;
            default:
                {
                    Debug.Assert(false, "Unhandled type");
                }
                break;
            }
        }

        static Quaternion GetRandomUnitQuaternion()
        {
            Quaternion quat = new Quaternion(GetRandom(-1.0, 1.0), GetRandom(-1.0, 1.0), GetRandom(-1.0, 1.0), GetRandom(-1.0, 1.0));
            return quat.Normalize();
        }

        static Quaternion GetRandomBasicQuaternion()
        {
            Quaternion quat;
            switch (GetRandom(0, 8))
            {
            case 0:
                quat = new Quaternion(1, 0, 0, 0);
                break;
            case 1:
                quat = new Quaternion(-1, 0, 0, 0);
                break;
            case 2:
                quat = new Quaternion(0, 1, 0, 0);
                break;
            case 3:
                quat = new Quaternion(0, -1, 0, 0);
                break;
            case 4:
                quat = new Quaternion(0, 0, 1, 0);
                break;
            case 5:
                quat = new Quaternion(0, 0, -1, 0);
                break;
            case 6:
                quat = new Quaternion(0, 0, 0, 1);
                break;
            case 7:
            default:
                quat = new Quaternion(0, 0, 0, -1);
                break;
            }
            return quat;
        }

        static void GetTwoPrependicularQuaternions(Quaternion quat, out Quaternion quat1, out Quaternion quat2)
        {
            List<bool> positions = GetRandomPositions(3, 2);
            List<Quaternion> quaternions = new List<Quaternion>();
            quaternions.Add(new Quaternion(0, 1, 0, 0)); // I
            quaternions.Add(new Quaternion(0, 0, 1, 0)); // J
            quaternions.Add(new Quaternion(0, 0, 0, 1)); // K
            Quaternion quatCoefficient1 = new Quaternion();
            Quaternion quatCoefficient2 = new Quaternion();
            int i = 0;
            for (; i < 3; i++)
            {
                if (positions[i])
                {
                    quatCoefficient1 = quaternions[i++];
                    break;
                }
            }
            for (; i < 3; i++)
            {
                if (positions[i])
                {
                    quatCoefficient2 = quaternions[i++];
                    break;
                }
            }
            if (GetRandom(0,1) == 1)
            {
                quatCoefficient1 *= -1;
            }
            if (GetRandom(0, 1) == 1)
            {
                quatCoefficient2 *= -1;
            }
            quat1 = quat * quatCoefficient1;
            quat2 = quat * quatCoefficient2;
        }

        static public ComplexFractal2D GetRandom2DFractalDefinition(string fractalPath)
        {
            ComplexFractal2D fractal = new ComplexFractal2D();

            switch (Persistent.Instance.RandomComputation2DUsedAlgorithmType)
            {
            case Fractals2DRandomSceneScript.UsedAlgorithmType.Julia:
                fractal.ComputationType = ComplexFractal2D.EComputationType.Julia;
                break;
            case Fractals2DRandomSceneScript.UsedAlgorithmType.Mixed:
                fractal.ComputationType = ComplexFractal2D.EComputationType.MixedMJ;
                break;
            case Fractals2DRandomSceneScript.UsedAlgorithmType.Random:
                fractal.ComputationType = GetTrueWithProbability(0.5) ? ComplexFractal2D.EComputationType.Julia : ComplexFractal2D.EComputationType.MixedMJ;
                break;
            }

            List<bool> positions = GetRandom2DPositions();
            for (int i = 0; i < positions.Count; i++)
            {
                if (positions[i])
                {
                    fractal.SetC(i, GetRandomCoefficientComplex());
                }
            }

            if (fractal.ComputationType == ComplexFractal2D.EComputationType.MixedMJ)
            {
                // setting of C0 values
                positions = GetRandom2DPositions();
                for (int i = 0; i < positions.Count; i++)
                {
                    if (positions[i])
                    {
                        fractal.SetC0(i, GetRandomCoefficientComplex());
                    }
                }
            }
            fractal.MaxComputationStepCount = 100;

            // random count of colors 
            List<int> colorNumbers = new List<int>();
            colorNumbers.Add(5);
            colorNumbers.Add(10);
            colorNumbers.Add(25);
            colorNumbers.Add(50);
            colorNumbers.Add(100);
            fractal.ColoringAlgorithm.ColorCount = colorNumbers[GetRandom(0, colorNumbers.Count - 1)];

            fractal.FilePath = fractalPath;
            if (Persistent.Instance.RandomComputation2DUsedPaletteType == Fractals2DRandomSceneScript.UsedPaletteType.Selected)
            {
                fractal.Palette = Persistent.Instance.SelectedPalette;
            }
            else
            {
                fractal.Palette = GetRandomPalette();
            }
            return fractal;
        }

        static IColorPalette GetRandomPalette()
        {
            // zxc use here also other palette types
            List<SinglePalette> palettes = Saver.LoadSinglePalettes();
            if (palettes.Count == 0)
            {
                return new SinglePalette(UnityEngine.Color.red, UnityEngine.Color.yellow);
            }
            int index = GetRandom(0, palettes.Count - 1);
            return palettes[index];
        }

        static List<int> Parse(string input)
        {
            var ints = new List<int>();
            foreach (var x in input.Split(','))
            {
                int number = 0;
                int.TryParse(x, out number);
                ints.Add(number);
            }
            return ints;
        }

        static int Max(List<int> numbers)
        {
            if (numbers.Count == 0)
            {
                return -1;
            }
            int max = numbers[0];
            foreach(int i in numbers)
            {
                if (i > max)
                {
                    max = i;
                }
            }
            return max;
        }

        static bool Contains(List<int> numbers, int number)
        {
            foreach (int i in numbers)
            {
                if (i == number)
                {
                    return true;
                }
            }
            return false;
        }

        static private List<bool> GetRandomPositions()
        {
            int size = 0;
            List<bool> positions = new List<bool>(size);

            switch(Persistent.Instance.RandomComputationUsedPowersType)
            {
            case Fractals3DSceneScript.UsedPowersType.UserDefined:
                {
                    var ints = new List<int>();
                    string usedPowers = Persistent.Instance.RandomComputationUsedPowers;
                    ints = Parse(usedPowers);
                    int max = Max(ints);
                    for (int i = 0; i <= max; i++)
                    {
                        positions.Add(Contains(ints, i));
                    }
                }
                break;
            case Fractals3DSceneScript.UsedPowersType.MaxIsMaximum:
                {
                    int powersCount = Persistent.Instance.RandomComputation3DMaxPower + 1;
                    int minSetPositions = 2;
                    // generation of powers
                    positions = GetRandomPositions(powersCount, GetRandom(minSetPositions, powersCount));
                }
                break;
            case Fractals3DSceneScript.UsedPowersType.MaxPlusOthers:
                {
                    int powersCount = Persistent.Instance.RandomComputation3DMaxPower;
                    int minSetPositions = 1;
                    // generation of powers
                    positions = GetRandomPositions(powersCount, GetRandom(minSetPositions, powersCount));
                    positions.Add(true);
                }
                break;
            }

            return positions;
        }

        static private List<bool> GetRandom2DPositions()
        {
            int size = 0;
            List<bool> positions = new List<bool>(size);

            switch (Persistent.Instance.RandomComputationUsed2DPowersType)
            {
            case Fractals2DRandomSceneScript.UsedPowersType.UserDefined:
                {
                    var ints = new List<int>();
                    string usedPowers = Persistent.Instance.RandomComputationUsed2DPowers;
                    ints = Parse(usedPowers);
                    int max = Max(ints);
                    for (int i = 0; i <= max; i++)
                    {
                        positions.Add(Contains(ints, i));
                    }
                }
                break;
            case Fractals2DRandomSceneScript.UsedPowersType.MaxIsMaximum:
                {
                    int powersCount = Persistent.Instance.RandomComputation2DMaxPower + 1;
                    int minSetPositions = 2;
                    // generation of powers
                    positions = GetRandomPositions(powersCount, GetRandom(minSetPositions, powersCount));
                }
                break;
            case Fractals2DRandomSceneScript.UsedPowersType.MaxPlusOthers:
                {
                    int powersCount = Persistent.Instance.RandomComputation2DMaxPower;
                    int minSetPositions = 1;
                    // generation of powers
                    positions = GetRandomPositions(powersCount, GetRandom(minSetPositions, powersCount));
                    positions.Add(true);
                }
                break;
            }

            return positions;
        }

        static private List<bool> GetRandomPositions(int size, int numberOfFilledPositions)
        {
            List<bool> positions = new List<bool>(size);
            for (int i = 0; i < size; i++)
            {
                positions.Add(false);
            }
            FillRandomPositions(positions, numberOfFilledPositions);
            return positions;
        }

        static private void FillRandomPositions(List<bool> positions, int numberOfFilledPositions)
        {
            UnityEngine.Debug.Assert(positions.Count >= numberOfFilledPositions);
            if (numberOfFilledPositions == positions.Count)
            {
                for (int i = 0; i < positions.Count; i++)
                {
                    positions[i] = true;
                }
                return;
            }
            for (int i = 0; i < positions.Count; i++)
            {
                positions[i] = false;
            }
            // selection of random indexes to set
            for (int i = 0; i < numberOfFilledPositions; i++)
            {
                bool isSet = true;
                int index = 0;
                while (isSet)
                {
                    index = GetRandom(0, positions.Count - 1);
                    isSet = positions[index];
                }
                positions[index] = true;
            }
        }

        static public double GetRandom(double minValue, double maxValue)
        {
            return s_random.NextDouble() * (maxValue - minValue) + minValue;
        }
        static bool GetTrueWithProbability(double probability)
        {
            return s_random.NextDouble() < probability;
        }
        static public int GetRandom(int min, int max)
        {
            //double rand = s_random.NextDouble();
            int r = s_random.Next(max - min + 1);
            int ret = r + min;
            UnityEngine.Debug.Assert(ret >= min && ret <= max);
            return ret;
        }

        static void FillRandomValues(List<bool> fillValue, List<double> values, double minValue, double maxValue)
        {
            UnityEngine.Debug.Assert(values.Count == fillValue.Count);
            for (int i = 0; i < fillValue.Count; i++)
            {
                if (fillValue[i])
                {
                    values[i] = GetRandom(minValue, maxValue);
                }
                else
                {
                    values[i] = 0;
                }
            }
        }

        static private Quaternion GetRandomCoefficientQuaternion()
        {
            const int Dims = 4;
            int setDimensions;
            if (Persistent.Instance.RandomComputationUsedQuaternionPartsType == Fractals3DSceneScript.UsedQuaternionPartsType.Random)
            {
                setDimensions = GetRandom(1, Dims);
            }
            else
            {
                int minParts = Persistent.Instance.RandomComputationMinParts;
                int maxParts = Persistent.Instance.RandomComputationMaxParts;
                setDimensions = GetRandom(minParts, maxParts);
            }
            List<bool> positions = GetRandomPositions(Dims, setDimensions);
            List<double> values = new List<double>(Dims);
            for (int i = 0; i < Dims; i++)
            {
                values.Add(0);
            }
            double limitValue = 1.2;
            FillRandomValues(positions, values, -limitValue, limitValue);
            Quaternion result = new Quaternion(values);
            return result;
        }

        static private Complex GetRandomCoefficientComplex()
        {
            const int Dims = 2;
            int setDimensions;
            if (Persistent.Instance.RandomComputationUsedComplexPartsType == Fractals2DRandomSceneScript.UsedComplexPartsType.Random)
            {
                setDimensions = GetRandom(1, Dims);
            }
            else
            if (Persistent.Instance.RandomComputationUsedComplexPartsType == Fractals2DRandomSceneScript.UsedComplexPartsType.RealOrImaginary)
            {
                setDimensions = 1;
            }
            else
            {
                setDimensions = 2;
            }
            List<bool> positions = GetRandomPositions(Dims, setDimensions);
            List<double> values = new List<double>(Dims);
            for (int i = 0; i < Dims; i++)
            {
                values.Add(0);
            }
            double limitValue = 1.2;
            FillRandomValues(positions, values, -limitValue, limitValue);
            Complex result = new Complex(values);
            return result;
        }

        private static Random s_random = new Random(Environment.TickCount);
    }
}

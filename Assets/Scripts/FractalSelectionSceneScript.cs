﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.IO;
using System;
using Assets.Scripts.Utils;
using Assets.Scripts.UI;

public class FractalSelectionSceneScript : MonoBehaviour {

    public const int SmallTextureSize = 38;
    public const int SmallTextureBorder = 3;

    const int TextLeftOffset = 10;
    const float TextRightLimitRel = 0.47f;
    const float TextureBeginRel = 0;//0.5f;
    const float TextureEndRel = 1;//0.95f;
    const int ListItemHeight = SmallTextureSize + 2 * SmallTextureBorder;
    const int ListItemVerticalBorder = SmallTextureBorder;
    const int MaxDebugStep = 99;

    public enum FractalType
    {
        Complex2D,
        //Backup,
        Quaternion3D
    };

    public void OnMainScene()
    {
        MySceneManager.GoToScene("MainMenu");
    }

    public void OnButton2D()
    {
        MySceneManager.GoToScene("2D Fractals");
    }

    public void OnButton3D()
    {
        MySceneManager.GoToScene("3D Fractals");
    }

    public void OnButtonGenerateRandom()
    {
        switch (Persistent.Instance.SelectedFractalType)
        {
        case FractalType.Complex2D:
            {
                Fractals2DRandomSceneScript.GenerateRandomFractals();
            }
            break;
        case FractalType.Quaternion3D:
            {
                Fractals3DSceneScript.GenerateRandomFractals();
            }
            break;
        }
        InitListBox();
    }

    public void OnButtonClearRandom()
    {
        m_modalPanel.SetButtonText(1, "Yes");
        m_modalPanel.SetButtonText(2, "No");
        m_modalPanel.Choice("Do you really want to delete all random fractals (in Random directory)?", DeleteAllRandomFractals, DoNothing);
        m_modalPanel.gameObject.SetActive(true);
    }

    void DeleteAllRandomFractals()
    {
        switch (Persistent.Instance.SelectedFractalType)
        {
        case FractalType.Complex2D:
            {
                FileUtils.CreateOrClearDirectory(Persistent.GetRandom2DFractalDir());
            }
            break;
        case FractalType.Quaternion3D:
            {
                FileUtils.CreateOrClearDirectory(Persistent.GetRandom3DFractalDir());
            }
            break;
        }
        InitListBox();
    }

    public void OnNew()
    {
        Persistent.Instance.CreateFractal = true;
        switch (Persistent.Instance.SelectedFractalType)
        {
        case FractalType.Complex2D:
            {
                Persistent.Instance.ComputeImmediatelyRandom2D = false;
                MySceneManager.GoToScene("Complex2D");
            }
            break;
        case FractalType.Quaternion3D:
            {
                MySceneManager.GoToScene("Quaternion3D");
            }
            break;
        }

    }

    public void OnEdit()
    {
        Persistent.Instance.CreateFractal = false;
        int index = m_listbox.GetCurSel();
        string fractalPath = m_listbox.GetItemData(index) as string;
        Persistent.Instance.SelectedFractalFile = fractalPath;
        switch (Persistent.Instance.SelectedFractalType)
        {
        case FractalType.Complex2D:
            {
                Persistent.Instance.ComputeImmediatelyRandom2D = false;
                MySceneManager.GoToScene("Complex2D");
            }
            break;
        case FractalType.Quaternion3D:
            {
                MySceneManager.GoToScene("Quaternion3D");
            }
            break;
        }
    }

    public void OnView()
    {

    }

    public void OnDelete()
    {
        // deleting of a fractal
        m_modalPanel.SetButtonText(1, "Yes");
        m_modalPanel.SetButtonText(2, "No");
        m_modalPanel.Choice("Do you really want to delete selected fractal?", DeleteSelectedFractal, DoNothing);
        m_modalPanel.gameObject.SetActive(true);
    }

    public void OnMove()
    {

    }

    public void OnExport()
    {
        // reading of all fractals and saving them - reexport
        //SaveFractals();
    }

    public void OnSelectedTypeChanged()
    {
        Persistent.Instance.SelectedFractalType = (FractalType)m_selectedFractalDropdown.value;// za predpokladu, ze je stejne usporadan enum i dropdown

        // reinicializave list boxu
        UpdateInterfaceChangedType();
        Update2D3DButtons();
    }

    void SetTextOnMoveButton(bool toBackup)
    {
        Button buttonMove = GameObject.Find("Button Move to").GetComponent<Button>();
        Debug.Assert(buttonMove != null);
        if (toBackup)
        {
            buttonMove.GetComponentInChildren<Text>().text = "Move to Backup";
        }
        else
        {
            buttonMove.GetComponentInChildren<Text>().text = "Move from Backup";
        }
    }

    void Awake()
    {
        m_modalPanel = ModalPanel.Instance();
        m_modalPanel.gameObject.SetActive(false);
    }

    // Use this for initialization
    void Start ()
    {
        Camera camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        camera.clearFlags = CameraClearFlags.SolidColor;

        m_button2D = GameObject.Find("Button 2D");
        m_button3D = GameObject.Find("Button 3D");

        m_buttonGenerateRandom = GameObject.Find("Button Generate Random");
        m_buttonClearRandom = GameObject.Find("Button Clear Random");

        GameObject textObject = GameObject.Find("Text Debug");
        if (textObject)
        {
            m_debugText = textObject.GetComponent<Text>();
        }

        new BoolWidget(new Ref<bool>(() => Persistent.Instance.ShowOnlyRandomFractals, v => { Persistent.Instance.ShowOnlyRandomFractals = v; }), GameObject.Find("Toggle Only Random").GetComponent<Toggle>()).OnChangeDelegate += OnFileFilterChanged;

        GameObject scrollView = GameObject.Find("Scroll View Fractals");
        Scrollbar scrollbar = scrollView.GetComponent<Scrollbar>();

        RectTransform transform = scrollbar.GetComponent<RectTransform>();

        Rect area = transform.rect;
        area.x = transform.position.x - transform.pivot.x * area.width;
        area.y = transform.position.y - transform.pivot.y * area.height;

        int listBoxWidth = (int)area.width;
        m_listbox = new GridBox(scrollView, SmallTextureSize, SmallTextureBorder, true, null);

        m_textureWidth = ListItemHeight;
        m_textureHeight = ListItemHeight;

        m_listbox.onDoubleClickEvent.AddListener(EditItem);
        m_listbox.onSelectionChangedEvent.AddListener(OnSelectedItemChanged);

        //InitListBox();

        // init of dropbox
        GameObject dropDown = GameObject.Find("Dropdown Type");
        if (dropDown)
        {
            m_selectedFractalDropdown = dropDown.GetComponent<Dropdown>();
            if (m_selectedFractalDropdown.value != (int)Persistent.Instance.SelectedFractalType)
            {
                m_selectedFractalDropdown.value = (int)Persistent.Instance.SelectedFractalType;
            }
            else
            {
                InitListBox();  // in upper branch initialization is done by Update function 
            }
        }

        // setting of text on move button
        //bool currentIsBackup = Persistent.Instance.SelectedFractalType == FractalType.Backup;
        //SetTextOnMoveButton(!currentIsBackup);

        //UpdateInterfaceChangedType();
        Update2D3DButtons();
        UpdateRandomButtons();
    }

    void Update2D3DButtons()
    {
        bool selected2D = Persistent.Instance.SelectedFractalType == FractalType.Complex2D;
        m_button2D.SetActive(selected2D);
        m_button3D.SetActive(!selected2D);
    }

    void UpdateRandomButtons()
    {
        m_buttonClearRandom.SetActive(Persistent.Instance.ShowOnlyRandomFractals);
        m_buttonGenerateRandom.SetActive(Persistent.Instance.ShowOnlyRandomFractals);
    }

    void OnFileFilterChanged()
    {
        UpdateRandomButtons();
        InitListBox();
    }

    int GetListboxIndex(string itemFile)
    {
        for ( int i=0; i<m_listbox.Count; i++ )
        {
            if (String.Equals(m_listbox.GetItemData(i) as string, itemFile))
                return i;
        }
        return -1;
    }

    void UpdateInterfaceChangedType()
    {
        // init of dropbox
        GameObject dropDown = GameObject.Find("Dropdown Type");
        if (dropDown)
        {
            m_selectedFractalDropdown = dropDown.GetComponent<Dropdown>();
            if (m_selectedFractalDropdown.value != (int)Persistent.Instance.SelectedFractalType)
            {
                m_selectedFractalDropdown.value = (int)Persistent.Instance.SelectedFractalType;
            }
            else
            {
                InitListBox();
            }
        }
    }

    void InitListBox()
    {
        m_listbox.Clear();
        m_listbox.SelectedIndex = GridBox.NoSelection;
        Persistent p = Persistent.Instance;
        string[] files;

        switch (Persistent.Instance.SelectedFractalType)
        {
        case FractalType.Complex2D:
            {
                if (Persistent.Instance.ShowOnlyRandomFractals)
                {
                    files = Directory.GetFiles(Persistent.GetRandom2DFractalDir(), GetTypeMask(p.SelectedFractalType), SearchOption.AllDirectories);
                }
                else
                {
                    files = Directory.GetFiles(Persistent.GetFractalsDir(), GetTypeMask(p.SelectedFractalType), SearchOption.AllDirectories);
                }
                m_tempFractals = new List<IFractal>();
                foreach (string file in files)
                {
                    ComplexFractal2D fr = Saver.Load<ComplexFractal2D>(file);
                    fr.FilePath = file; // not loaded property
                    m_tempFractals.Add(fr);
                }
                BuildListBoxFromList(m_tempFractals);
            }
            break;
        case FractalType.Quaternion3D:
            {
                if (Persistent.Instance.ShowOnlyRandomFractals)
                {
                    files = Directory.GetFiles(Persistent.GetRandom3DFractalDir(), GetTypeMask(p.SelectedFractalType), SearchOption.TopDirectoryOnly);
                }
                else
                {
                    files = Directory.GetFiles(Persistent.Get3DFractalsDir(), GetTypeMask(p.SelectedFractalType), SearchOption.TopDirectoryOnly);
                }
                BuildListboxFromFileList(files);
            }
            break;
        }
        int selectedIndex = GetListboxIndex(p.SelectedFractalFile);
        m_listbox.SelectedIndex = selectedIndex;
        if (m_listbox.SelectedIndex != selectedIndex)
        {
            m_listbox.SelectedIndex = selectedIndex - 1;
            if (m_listbox.SelectedIndex != selectedIndex - 1)
            {
                m_listbox.SelectedIndex = 0;
            }
        }
    }

    void LoadFractals(FractalType type)
    {
        var files = Directory.GetFiles(Persistent.GetFractalsDir(), GetTypeMask(type), SearchOption.AllDirectories);
        List<IFractal> fractals = new List<IFractal>();
        foreach(string file in files)
        {
            ComplexFractal2D fr = Saver.Load<ComplexFractal2D>(file);
            fr.FilePath = file; // not loaded property
            fractals.Add(fr);
        }

        BuildListBoxFromList(fractals);
    }

    string GetTypeMask(FractalType type)
    {
        switch(type)
        {
        case FractalType.Complex2D:
            return "*." + Persistent.GetExtension(Persistent.SaveExtension.fractal2DDefinitionExtension);
        case FractalType.Quaternion3D:
            return "*." + Persistent.GetExtension(Persistent.SaveExtension.fractal3DZipExtension);
        default:
            Debug.Assert(false);
            return "";
        }
    }

    void BuildListBoxFromList<T>(List<T> list)
    {
        m_listbox.Clear();
        foreach (IFractal fr in list)
        {
            float zac = RecordTime("add fr -zac");
            AddFractal(fr);
            RecordTime("add fr - kon", zac);
        }
    }

    void BuildListboxFromFileList(string[] filelist)
    {
        m_listbox.Clear();
        foreach(string file in filelist)
        {
            AddFractal(file);
        }
    }

    void AddFractal(IFractal fractal)
    {
        GUIStyle textureStyle = new GUIStyle();
        int itemIndex = m_listbox.AddItem(PrepareTexture(fractal, textureStyle), textureStyle);
        m_listbox.SetItemData(itemIndex, fractal.GetFilename());
    }

    void AddFractal(string path)
    {
        GUIStyle textureStyle = new GUIStyle();
        int itemIndex = m_listbox.AddItem(PrepareTexture(path, textureStyle), textureStyle);
        m_listbox.SetItemData(itemIndex, path);
    }

    Texture2D PrepareTexture(IFractal fractal, GUIStyle textureStyle)
    {
        //RecordTime("PrepareTexture -zac");
        //textureStyle.hover = NewGUIStyle(fractal, m_textureWidth, m_textureHeight, Color.black, Color.cyan);//pouzivalo se pro gui buttony
        //textureStyle.active = NewGUIStyle(fractal, m_textureWidth, m_textureHeight, Color.black, Color.cyan);
        //textureStyle.normal = NewGUIStyle(fractal, m_textureWidth, m_textureHeight, Color.black, Color.white);
        Color gray = new Color(180 / 255.0f, 180 / 255.0f, 180 / 255.0f);
        textureStyle.hover.textColor = gray;
        textureStyle.active.textColor = gray;

        Texture2D listItemTexture = new Texture2D(m_textureWidth, m_textureHeight);
        TextureUtils.FillWithColor(listItemTexture, new Color(0, 0, 0, 0));

        Texture2D fractalTexture = null;
        int fractalPictureSize = GetListItemFractalSize();
        //RecordTime("PrepareTexture - 1");
        if (!GetListItemFractalTexture(fractal.GetFilename(), out fractalTexture))
        {
            fractalTexture = fractal.GetTexture(fractalPictureSize);

            // saving of preview texture
            string smallPreviewFilename = PreviewSaver.GetPreviewFileName(fractal.GetFilename(), fractalPictureSize);
            TextureUtils.SaveTextureToPng(smallPreviewFilename, fractalTexture);//zxc is this working on Android? (according to comment something isn't working)
        }
        //RecordTime("PrepareTexture - 4");

        int borderY = ListItemVerticalBorder;
        TextureUtils.WriteTextureIntoTexture(listItemTexture, fractalTexture, new Rect(SmallTextureBorder, borderY, m_textureWidth * (TextureEndRel - TextureBeginRel), m_textureHeight - 2 * borderY));
        //RecordTime("PrepareTexture - 5");

        return listItemTexture;
    }

    Texture2D PrepareTexture(string fractalPath, GUIStyle textureStyle)
    {
        //RecordTime("PrepareTexture -zac");
        //textureStyle.hover = NewGUIStyle(fractal, m_textureWidth, m_textureHeight, Color.black, Color.cyan);//pouzivalo se pro gui buttony
        //textureStyle.active = NewGUIStyle(fractal, m_textureWidth, m_textureHeight, Color.black, Color.cyan);
        //textureStyle.normal = NewGUIStyle(fractal, m_textureWidth, m_textureHeight, Color.black, Color.white);
        Color gray = new Color(180 / 255.0f, 180 / 255.0f, 180 / 255.0f);
        textureStyle.hover.textColor = gray;
        textureStyle.active.textColor = gray;

        Texture2D listItemTexture = new Texture2D(m_textureWidth, m_textureHeight);
        TextureUtils.FillWithColor(listItemTexture, new Color(0, 0, 0, 0));

        Texture2D fractalTexture = null;
        int fractalPictureSize = GetListItemFractalSize();
        //RecordTime("PrepareTexture - 1");
        if (!GetListItemFractalTexture(fractalPath, out fractalTexture))
        {
            TextureUtils.GetTextureFromPng(Persistent.GetNotComputedTextureSmallPath(), out fractalTexture);
        }
        //RecordTime("PrepareTexture - 4");

        int borderY = ListItemVerticalBorder;
        TextureUtils.WriteTextureIntoTexture(listItemTexture, fractalTexture, new Rect(SmallTextureBorder, borderY, m_textureWidth * (TextureEndRel - TextureBeginRel), m_textureHeight - 2 * borderY));

        //RecordTime("PrepareTexture - 5");

        return listItemTexture;
    }

    public static int GetListItemFractalSize()
    {
        return ListItemHeight - ListItemVerticalBorder * 2;
    }

    bool GetListItemFractalTexture(string fractalFilename, out Texture2D texture)
    {
        int smallSize = FractalSelectionSceneScript.GetListItemFractalSize();
        string smallPreviewFilename = PreviewSaver.GetPreviewFileName(fractalFilename, smallSize);
        return TextureUtils.GetTextureFromPng(smallPreviewFilename, out texture);
    }

    static GUIStyleState NewGUIStyle(IFractal fractal, int width, int height, Color textColor, Color backgroundColor)
    {
        GUIStyleState result = new GUIStyleState();
        result.background = new Texture2D(width, height);
        TextureUtils.FillWithColor(result.background, backgroundColor);

        int borderY = 2;

        TextureUtils.MakeTransparentBorders(result.background, borderY, 3);

        float paletteStart = TextureBeginRel;
        float paletteEnd = TextureEndRel;

        Texture2D texture = fractal.GetTexture(height);
        TextureUtils.WriteTextureIntoTexture(result.background, texture, new Rect(width * paletteStart, borderY, width * (paletteEnd - paletteStart), height - 2 * borderY));

        result.textColor = textColor;
        return result;
    }

    void SetSelected2DFractal(IFractal fractal)
    {
        RawImage fractalImage = GameObject.Find("RawImage Fractal").GetComponent<RawImage>();
        int textureSizeX = Mathf.FloorToInt(fractalImage.rectTransform.sizeDelta.x);
        int textureSizeY = Mathf.FloorToInt(fractalImage.rectTransform.sizeDelta.y);
        Texture2D t2 = new Texture2D(textureSizeX, textureSizeY);
        TextureUtils.FillWithColor(t2, new Color(0, 0, 0, 0));
        float xLeft = (textureSizeX - textureSizeY) / 2;
        TextureUtils.WriteTextureIntoTexture(t2, fractal.GetTexture(textureSizeY), new Rect(xLeft, 0, textureSizeY, textureSizeY));//zxc TODO: set size according to style - stretching/fix ratio of axes

        fractalImage.texture = t2;
    }

    void SetSelected3DFractal(string path)
    {
        RawImage fractalImage = GameObject.Find("RawImage Fractal").GetComponent<RawImage>();
        int textureSizeX = Mathf.FloorToInt(fractalImage.rectTransform.sizeDelta.x);
        int textureSizeY = Mathf.FloorToInt(fractalImage.rectTransform.sizeDelta.y);
        Texture2D t2 = new Texture2D(textureSizeX, textureSizeY);

        string texturePath = Persistent.GetBigTexturePathFromFractalPath(path);
        Texture2D fractalTexture;
        if (!File.Exists(texturePath))
        {
            TextureUtils.GetTextureFromPng(Persistent.GetNotComputedTextureBigPath(), out fractalTexture);
        }
        else
        {
            TextureUtils.GetTextureFromPng(texturePath, out fractalTexture);
        }

        Texture2D resized = TextureUtils.ScaleTexture(fractalTexture, textureSizeX, textureSizeY);
        fractalImage.texture = resized;
    }

    void DeleteSelectedFractal()
    {
        int selectedIndex = m_listbox.SelectedIndex;

        string fractalFilename = Persistent.Instance.SelectedFractalFile;
        File.Delete(fractalFilename);
        switch (Persistent.Instance.SelectedFractalType)
        {
        case FractalType.Complex2D:
            {
                // delete small preview
                int smallSize = FractalSelectionSceneScript.GetListItemFractalSize();
                string smallPreviewFilename = PreviewSaver.GetPreviewFileName(fractalFilename, smallSize);
                File.Delete(smallPreviewFilename);
            }
            break;
        case FractalType.Quaternion3D:
            {
                // delete also screenshots
                Persistent.DeleteQuaternionFractalScreenshots(fractalFilename);
            }
            break;
        }
        
        InitListBox();

        string fractalPath = m_listbox.GetItemData(selectedIndex) as string;
        if (fractalPath == null)
        {
            fractalPath = m_listbox.GetItemData(0) as string;
        }
        Persistent.Instance.SelectedFractalFile = fractalPath;
    }

    void DoNothing()
    {
    }

    float RecordTime(string message=null, float lastTime=-1)
    {
        float currentTime = Time.realtimeSinceStartup;
        if (m_debugStep >= 0 && m_debugStep < MaxDebugStep)
        {
            //m_times[m_debugStep] = currentTime;//Time.fixedTime;
        }
        if (lastTime == -1 && m_debugStep < MaxDebugStep)
        {
            //lastTime = m_debugStep == 0 ? 0 : m_times[m_debugStep - 1];
        }
        //string str = string.Format("{0}. {1} delta:{2}", m_debugStep, Time.fixedTime, Time.fixedTime-lastTime);
        string str = string.Format("{0}. {1} delta:{2}", m_debugStep, currentTime, currentTime - lastTime);
        if (message != null)
        {
            str += " " + message;
        }
        WriteDebugLine(str);
        m_debugStep++;

        return currentTime;
    }

    // Update is called once per frame
    void Update()
    {
        //UpdateInterfaceChangedType();
        //m_debugStep++;
        if (m_listbox != null)
        {
            m_listbox.Update();
        }

        int selectedIndex = GetListboxIndex(Persistent.Instance.SelectedFractalFile);

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (selectedIndex > 0)
            {
                selectedIndex--;
                m_listbox.SelectedIndex = selectedIndex;
            }
            return;
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (selectedIndex < m_listbox.Count - 1)
            {
                selectedIndex++;
                m_listbox.SelectedIndex = selectedIndex;
            }
            return;
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (selectedIndex >= m_listbox.GetMaxHorizontalCount())
            {
                selectedIndex -= m_listbox.GetMaxHorizontalCount();
                m_listbox.SelectedIndex = selectedIndex;
            }
            return;
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (selectedIndex < m_listbox.Count - m_listbox.GetMaxHorizontalCount())
            {
                selectedIndex += m_listbox.GetMaxHorizontalCount();
                m_listbox.SelectedIndex = selectedIndex;
            }
            return;
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            OnEdit();
        }

            m_listbox.SelectedIndex = selectedIndex;
    }

    bool IsIndexValid(int itemIndex)
    {
        return itemIndex >= 0 && itemIndex < m_listbox.Count;
    }

    void EditItem(int itemIndex)
    {
        if (!IsIndexValid(itemIndex))
        {
            return;
        }
        OnEdit();

        //Persistent.Instance.Create = false;
        //Persistent.Instance.SelectedFractalFile = m_listbox.GetItemData(itemIndex) as string;
        //MySceneManager.GoToScene("Complex2D");
    }

    void OnGUI()
    {
        GUI.color = new Color(1.0f, 1.0f, 1.0f);

        if (m_listbox != null)
        {
            //m_listbox.ReDraw();
        }
    }

    void OnSelectedItemChanged(int itemIndex)
    {
        string fractalPath = m_listbox.GetItemData(itemIndex) as string;
        Persistent.Instance.SelectedFractalFile = fractalPath;
        Text filenameText = GameObject.Find("Text Filename").GetComponent<Text>();
        if (filenameText.text != fractalPath)
        {
            filenameText.text = fractalPath;
        }
        switch (Persistent.Instance.SelectedFractalType)
        {
        case FractalType.Complex2D:
            {
                ComplexFractal2D fractal;
                if (ComplexFractal2D.Load(fractalPath, out fractal))
                {
                    SetSelected2DFractal(fractal);
                }
            }
            break;
        case FractalType.Quaternion3D:
            {
                SetSelected3DFractal(fractalPath);
            }
            break;
        }                    
    }

    void WriteDebugLine(string str)
    {
        if (!m_debugText)
            return;
        m_debugText.text += str + "\n";
    }

    Text m_debugText;
    //ListBox m_listbox;
    GridBox m_listbox;
    Dropdown m_selectedFractalDropdown;
    GameObject m_button2D;
    GameObject m_button3D;

    int m_textureWidth;
    int m_textureHeight;

    //float[] m_times;
    int m_debugStep = 0;

    List<IFractal> m_tempFractals;//temp

    private ModalPanel m_modalPanel;
    GameObject m_buttonGenerateRandom;
    GameObject m_buttonClearRandom;
}
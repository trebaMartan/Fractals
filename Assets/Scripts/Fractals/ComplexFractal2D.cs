﻿//#define CHECK_2D_COMPUTATION

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Exocortex.DSP;
using System.Xml.Serialization;
using System;
using Assets.Scripts.Utils;
using System.Runtime.InteropServices;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;
using Assets.Scripts.Computation;
using Assets.Scripts.Math;
using System.Reflection;

[XmlInclude(typeof(Area2DBase))]
[XmlInclude(typeof(CircularArea2D))]
[XmlInclude(typeof(RectangularArea2D))]
public class ComplexFractal2D : IFractal
{
    // recurrent formula for counting is:
    // z(n+1) = z(n)*z(n) + c; where c is complex number (or general formula is z(n) ^ m * C[m] + z(n) ^ (m-1) * C[m-1] + ... + C[0])

    public enum EComputationType
    {
        Julia,
        MixedMJ
    };

    public enum EDisplayMode
    {
        Proportional,
        Stretch
    }

    public delegate int ComputeIterationsDelegate(double x, double y, int maxIterations);

    public ComplexFractal2D()
    {
        ComputationType = EComputationType.Julia;
        C = new List<Complex>(2);
        C0 = new List<Complex>(2);

        m_computationArea = new RectangularArea2D();
        m_name = "N";

        MaxComputationStepCount = 25;

        LimitArea = new CircularArea2D(0, 0, 2);
        ColoringAlgorithm = new ColoringAlgorithm();
        ColoringAlgorithm.BackgroundColor = Persistent.Instance.DefaultBackgroundColor;
        ColoringAlgorithm.SinglePalette = new SinglePalette(UnityEngine.Color.red, UnityEngine.Color.yellow);
        ColoringAlgorithm.ColorCount = MaxComputationStepCount;
        ColoringAlgorithm.RepeatType = RepeatType.Triangle;
        m_undoRedoList = new UndoRedoList();
        m_undoRedoList.SetObject(this);
    }

    public ComplexFractal2D(ComplexFractal2D old)
    {
        ComputationArea = new RectangularArea2D(old.ComputationArea);
        ComputationType = old.ComputationType;
        C = new List<Complex>(old.C.Count);
        foreach (var c in old.C)
        {
            C.Add(c);
        }
        C0 = new List<Complex>(old.C0.Count);
        foreach (var c0 in old.C0)
        {
            C0.Add(c0);
        }

        if (old.LimitArea != null)
        {
            LimitArea = old.LimitArea.Clone() as Area2DBase;
        }

        MaxComputationStepCount = old.MaxComputationStepCount;

        ColoringAlgorithm = new ColoringAlgorithm(old.ColoringAlgorithm);
        FilePath = (string)old.FilePath.Clone();
        Name = old.Name;
        m_undoRedoList = old.m_undoRedoList;
    }

    /////////////// public properties /////////////////
    [ComputationAttribute]
    public RectangularArea2D ComputationArea
    {
        get { return m_computationArea; }
        set { m_computationArea = value; }
    }

    public string Version
    {
        get { return AboutSceneScript.versionNumber; }
        set { }
    }

    [ComputationAttribute]
    public EDisplayMode DisplayMode
    {
        get { return m_displayMode; }
        set { m_displayMode = value; }
    }

    //public ComputeIterationsDelegate ComputeDelegate
    //{
    //    get { return m_computeDelegate; }
    //}

    [ComputationAttribute]
    public EComputationType ComputationType
    {
        get { return m_computationType; }
        set
        {
            switch (value)
            {
            case EComputationType.Julia:
                m_computeDelegate = new ComputeIterationsDelegate(ComputeIterationsJulia);
                break;
            case EComputationType.MixedMJ:
                m_computeDelegate = new ComputeIterationsDelegate(ComputeIterationsMixed);
                break;
            default:
                UnityEngine.Debug.Assert(false, "not handled delegate type");
                break;
            }
            m_computationType = value;
        }
    }

    [XmlIgnore]
    public IColorPalette Palette
    {
        get
        {
            return ColoringAlgorithm.SinglePalette;
        }
        set
        {
            ColoringAlgorithm.Palette = value;
        }
    }

    [XmlIgnore]
    public SinglePalette SinglePalette
    {
        get { return Palette as SinglePalette; }
        set
        {
            ColoringAlgorithm.SinglePalette = value;
        }
    }

    public ColoringAlgorithm ColoringAlgorithm
    {
        get { return m_coloringAlgrorithm; }
        set { m_coloringAlgrorithm = value; }
    }

    [ComputationAttribute]
    public Area2DBase LimitArea
    {
        get { return m_limitArea; }
        set { m_limitArea = value; }
    }

    [ComputationAttribute]
    public List<Complex> C // coefficients for powers of ci
    {
        get { return m_c; }
        set { m_c = value; }
    }

    [ComputationAttribute]
    public List<Complex> C0 // coefficients for powers of c0 - initial position
    {
        get { return m_c0; }
        set { m_c0 = value; }
    }

    [ComputationAttribute]
    public int MaxComputationStepCount
    {
        get { return m_maxComputationStepCount; }
        set { m_maxComputationStepCount = value; }
    }

    [XmlIgnore]
    public UnityEngine.Color BackgroundColor
    {
        get { return ColoringAlgorithm.BackgroundColor; }
        set { ColoringAlgorithm.BackgroundColor = value; }
    }

    [XmlIgnore]
    public string FilePath
    {
        get { return m_filePath; }
        set { m_filePath = value; }
    }

    public string Name
    {
        get { return m_name; }
        set { m_name = value; }
    }

    [UndoRedoAttribute]
    [XmlIgnore]
    public int PowerCindex
    {
        get { return m_powerCindex; }
        set { m_powerCindex = value; }
    }


    // ============ public functions ===================
    public void CopyFrom(ComplexFractal2D old)
    {
        if (!ComputationArea.Equals(old.ComputationArea))
        {
            ComputationArea = new RectangularArea2D(old.ComputationArea);
        }
        ComputationType = old.ComputationType;
        if (C.Count != old.C.Count)
        {
            C.Clear();
            foreach (var c in old.C)
            {
                C.Add(c);
            }
        }
        else
        {
            // copy of C
            for (int i = 0; i < C.Count; i++)
            {
                C[i] = old.C[i];
            }
        }
        if (C0.Count != old.C0.Count)
        {
            C0.Clear();
            foreach (var c0 in old.C0)
            {
                C0.Add(c0);
            }
        }
        else
        {
            for (int i = 0; i < C.Count; i++)
            {
                C0[i] = old.C0[i];
            }
        }

        if (old.LimitArea != null)
        {
            if (!old.LimitArea.Equals(LimitArea))
            {
                LimitArea = old.LimitArea.Clone() as Area2DBase;
            }
        }

        MaxComputationStepCount = old.MaxComputationStepCount;

        if (ColoringAlgorithm != old.ColoringAlgorithm)
        {
            ColoringAlgorithm = new ColoringAlgorithm(old.ColoringAlgorithm);
        }
        if (old.FilePath != null && !old.FilePath.Equals(FilePath))
        {
            //FilePath = (string)old.FilePath.Clone();
            FilePath = old.FilePath;
        }
        if (old.Name != null && !old.Name.Equals(Name))
        {
            Name = old.Name;
        }
        m_undoRedoList = old.m_undoRedoList;
    }

    public void SetC(int n, Complex c)
    {
        if (C == null)
        {
            C = new List<Complex>();
        }
        while (C.Count < n + 1)
        {
            C.Add(Complex.Zero);
        }
        C[n] = c;
    }

    public Complex GetC(int n)
    {
        if (n < 0 || n > C.Count - 1)
        {
            return Complex.Zero;
        }
        //if (n < 0)
        //{
        //    return Complex.Zero;
        //}
        //if (C.Count < n + 1)
        //{
        //    SetC(n, new Complex(0, 0));
        //}
        return C[n];
    }

    public void SetC0(int n, Complex c)
    {
        if (C0 == null)
        {
            C0 = new List<Complex>();
        }
        while (C0.Count < n + 1)
        {
            C0.Add(Complex.Zero);
        }
        C0[n] = c;
    }

    public Complex GetC0(int n)
    {
        if (n < 0 || n > C0.Count - 1)
        {
            return Complex.Zero;
        }

        //if (n < 0)
        //{
        //    return Complex.Zero;
        //}
        //if (C0.Count < n + 1)
        //{
        //    SetC0(n, new Complex(0, 0));
        //}
        return C0[n];
    }

    public object Clone()
    {
        ComplexFractal2D fr = new ComplexFractal2D(this);
        return fr;
    }

    public string GetFormula()
    {
        string formula = "";
        switch (ComputationType)
        {
        case EComputationType.Julia:
            {
                formula = "Z(i+1) = " + GetFormulaJulia();
            }
            break;
        case EComputationType.MixedMJ:
            {
                formula = "Z(i+1) = SUM(j)(C0(j) * (Z(0) ^ j)) + " + GetFormulaJulia();
            }
            break;
        }
        return formula;
    }

    // from interface
    public string GetName()
    {
        return Name;
    }

    public string GetFilename()
    {
        return m_filePath;
    }

    public Color GetBackgroundColor() { return BackgroundColor; }

    public Texture2D GetTexture(int textureHeight)
    {
        Texture2D texture = new Texture2D(textureHeight, textureHeight);
        GetTexture(texture, textureHeight, textureHeight);
        return texture;
    }

    public void GetTexture(Texture2D texture, int textureWidth, int textureHeight)
    {
#if CHECK_2D_COMPUTATION
        Texture2D texture = new Texture2D(textureWidth, textureHeight);

        double leftX = ComputationArea.CenterX - ComputationArea.HalfSizeX;
        double downY = ComputationArea.CenterY - ComputationArea.HalfSizeY;
        double rightX = ComputationArea.CenterX + ComputationArea.HalfSizeX;
        double upY = ComputationArea.CenterY + ComputationArea.HalfSizeY;
        double realWidth = ComputationArea.SizeX;
        double realHeight = ComputationArea.SizeY;

        double pixelSizeX = realWidth / textureWidth;
        double pixelSizeY = realHeight / textureHeight;
        int columnOffset = 0;
        int rowOffset = 0;
        int pixelCountX = textureWidth;
        int pixelCountY = textureHeight;
        if (DisplayMode == EDisplayMode.Proportional)
        {
            TextureUtils.FillWithColor(texture, new Color(0,0,0,0));
            pixelSizeX = pixelSizeY = Math.Max(pixelSizeX, pixelSizeY);
            pixelCountX = (int)(realWidth / pixelSizeX + 0.5f);
            pixelCountY = (int)(realHeight / pixelSizeY + 0.5f);
            if (pixelCountX < textureWidth)
            {
                columnOffset = (textureWidth - pixelCountX) / 2;
            }
            if (pixelCountY < textureHeight)
            {
                rowOffset = (textureHeight - pixelCountY) / 2;
            }
        }

        int maxIterations = MaxComputationStepCount - 1;

        EliminateExtraZeroPowers();//some optimization

        m_iterationIndexCSharp.Width = textureWidth;
        m_iterationIndexCSharp.Height = textureHeight;
        m_iterationIndexCSharp.ComputationParametersHash = GetComputationParametersHash();

        //string CstrRe = DoubleConverter.ToExactString(C[0].Re);
        //string CstrIm = DoubleConverter.ToExactString(C[0].Im);

        for (int column = 0; column < pixelCountX; column++ )
        {
            double xReal = leftX + column * pixelSizeX;
            for (int row = 0; row < pixelCountY; row++ )
            {
                double yReal = downY + row * pixelSizeY;
                int iterations = m_computeDelegate(xReal, yReal, maxIterations);
                int index = column + columnOffset + (row + rowOffset) * textureWidth;
                m_iterationIndexCSharp.Iterations[index] = iterations;
                texture.SetPixel(column + columnOffset, row + rowOffset, ColoringAlgorithm.GetColor(iterations));
            }
        }

        texture.Apply();

        bool applyBackgroundColor = true;
        if (applyBackgroundColor)
        {
            TextureUtils.MixTextureWithBackgroundColor(texture, BackgroundColor);
        }

        Texture2D textureCpp = GetTextureCpp(textureWidth, textureHeight);

        // saving
        PreviewSaver.SaveTextureToPng("C:/joy/textureCpp.png", textureCpp);
        PreviewSaver.SaveTextureToPng("C:/joy/textureCSharp.png", texture);

        Texture2D textureFromIndex = CreateTextureFromIterationIndex(m_iterationIndexCSharp);
        PreviewSaver.SaveTextureToPng("C:/joy/textureIterationsCSharp.png", textureFromIndex);

        int n = textureWidth * textureHeight;
        for (int i = 0; i < n; i++)
        {
            int column = i % textureWidth;
            int row = i / textureWidth;
            int iterationsCSherp = m_iterationIndexCSharp.Iterations[i];
            int iterationsCpp = m_iterationIndexCpp.Iterations[i];
            if (iterationsCSherp != iterationsCpp)
            {
                int j = 0;
            }
        }

        return texture;
#else
        GetTextureCpp(texture, textureWidth, textureHeight, true);
        //float timeCpp1 = UnityEngine.Time.realtimeSinceStartup;
        //Texture2D texture = GetTextureCpp(textureWidth, textureHeight);
        //float difCpp1 = UnityEngine.Time.realtimeSinceStartup - timeCpp1;

        //if (textureHeight == 450)
        //{
        //    using (StreamWriter outputFile = new StreamWriter("C:/joy/TimeSharp2D.txt", true))
        //    {
        //        outputFile.WriteLine(String.Format("C++ 2D time:{0}", difCpp1));
        //    };
        //}
        //return texture;
#endif
    }

    public void Save(string path)
    {
        if (path == null)
            return;
        EliminateExtraZeroPowers();
        RoundDoubles();
        Saver.Save(this, path);
        FilePath = path;
    }

    public static bool Load(string path, out ComplexFractal2D loadedFractal)
    {
        bool ok = Saver.Load(path, out loadedFractal);
        if (ok)
        {
            loadedFractal.RoundDoubles();
            loadedFractal.FilePath = path; // not loaded property
        }
        return ok;
    }

    public void ComputeTextureFromIterationIndex(Texture2D texture, int width, int height)
    {
        // make image from colorIndexes
        IterationIndex iterationIndex = GetIterationIndex(width, height, GetComputationParametersHash());
        ComputeTextureFromIterationIndex(texture, iterationIndex);
    }

    public void ComputeTextureFromIterationIndex(Texture2D texture, IterationIndex iterationIndex)
    {
        // make image from colorIndexes
        CreateTextureFromIterationIndex(texture, iterationIndex);
        bool applyBackgroundColor = true;
        if (applyBackgroundColor)
        {
            TextureUtils.MixTextureWithBackgroundColor(texture, BackgroundColor);
        }
    }

    public UndoRedoList GetUndoRedoList()
    {
        return m_undoRedoList;
    }

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    delegate void ProgressCallback(int value);

    // =================== private functions ==========================
    [DllImport("FractalsDll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
    static extern void ComputeFractalTexture(int processorCount, int textureWidth, int textureHeight,
        string fractalDefinition, // path in debug or xml string with definition
        [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 5)] out int[] iterations, out int count, ProgressCallback progressCallback);

    public void ComputeIterationIndexCached(int textureWidth, int textureHeight)
    {
        string computationParametersHash = GetComputationParametersHash();
        IterationIndex iterationIndex = GetIterationIndex(textureWidth, textureHeight, computationParametersHash);
        ComputeIterationIndex(iterationIndex);
    }

    public IterationIndex GetOrComputeIterationIndex(int textureWidth, int textureHeight)
    {
        string computationHash = GetComputationParametersHash();
        IterationIndex iterationIndex = GetIterationIndex(textureWidth, textureHeight, computationHash);
        ComputeIterationIndex(iterationIndex);
        return iterationIndex;
    }

    public void ComputeIterationIndex(IterationIndex iterationIndex)
    {
        if (iterationIndex.IsComputed())
        {
            return;
        }
        string fractalDefinition = Saver.Serialize<ComplexFractal2D>(this);
        int textureWidth = iterationIndex.Width;
        int textureHeight = iterationIndex.Height;
        int len = textureWidth * textureHeight;
        int[] iterations = new int[len];
        string computationHash = GetComputationParametersHash();
        ProgressCallback callback = (value) => {
            Persistent.Instance.Progress = value;
        };

        ComputeFractalTexture(Persistent.Instance.ProcessorCount, textureWidth, textureHeight, fractalDefinition, out iterations, out len, callback);
        iterationIndex.SetComputed(computationHash);
        iterationIndex.Iterations = iterations.ToList<int>();
    }

    public string GetComputationParametersHash()
    {
        string parameters = GetComputationParameters();
        return Saver.GetHashString(parameters);
    }

    string GetComputationParameters()
    {
        string str = "";
        PropertyInfo[] properties = typeof(ComplexFractal2D).GetProperties();
        foreach (PropertyInfo property in properties)
        {
            if (Attribute.IsDefined(property, typeof(ComputationAttribute)))
            {
                string line = property.Name + ":";
                object obj = OtherUtils.GetPropValue(this, property.Name);
                if (obj is List<Complex>)
                {
                    line += obj.ToString();
                    List<Complex> list = (obj as List<Complex>);
                    foreach (var v in list)
                    {
                        line += v.ToString() + ",";
                    }
                    line += "\n";
                }
                else
                {
                    line += obj.ToString() + "\n";
                }
                str += line;
            }
        }
        return str;
    }

    public void GetTextureCpp(Texture2D texture, int textureWidth, int textureHeight, bool cacheIterationIndex)
    {
        if (cacheIterationIndex)
        {
            ComputeIterationIndexCached(textureWidth, textureHeight);
            ComputeTextureFromIterationIndex(texture, textureWidth, textureHeight);
        }
        else
        {
            IterationIndex iterationIndex = new IterationIndex(textureWidth, textureHeight);
            ComputeIterationIndex(iterationIndex);
            ComputeTextureFromIterationIndex(texture, iterationIndex);
        }
    }

    public void CreateTextureFromIterationIndex(Texture2D texture, IterationIndex iterationIndex)
    {
        if (iterationIndex == null)
        {
            return;
        }
        int textureWidth = iterationIndex.Width;
        int textureHeight = iterationIndex.Height;
        List<int> iterationsIndex = iterationIndex.Iterations;

        if (DisplayMode == EDisplayMode.Proportional)
        {
            TextureUtils.FillWithColor(texture, new Color(0, 0, 0, 0));
        }

        Debug.Assert(iterationIndex.Iterations.Count == textureWidth * textureHeight);
        if (iterationsIndex.Count != textureWidth * textureHeight)
        {
            return;
        }
        int n = textureWidth * textureHeight;
        for (int i = 0; i < n; i++)
        {
            int column = i % textureWidth;
            int row = i / textureWidth;
            int currentColorIndex = iterationsIndex[i];
            if (currentColorIndex >= 0)
            {
                // pixels that are outside of computed are are eventually filled with FillColor function
                Color color = ColoringAlgorithm.GetColor(currentColorIndex);
                texture.SetPixel(column, row, color);// ColoringAlgorithm.GetColor(currentColorIndex));
            }
        }

        texture.Apply();
    }

    //int ComputeIterationsMandelbrot(float x, float y, int maxIterations)
    //{
    //    Area2DBase areaLimit = new CircularArea2D(0, 0, 2);
    //    if (areaLimit.IsOut(x, y, 0))
    //        // is out already
    //        return 0;
    //    Complex pos = new Complex(x, y);
    //    Complex newPos = new Complex();
    //    Complex pos0 = pos;

    //    for (int i = 0; i < maxIterations;)
    //    {
    //        newPos = pos * pos + pos0;
    //        pos = newPos;
    //        i++;
    //        if (areaLimit.IsOut((float)pos.Re, (float)pos.Im, i))
    //            return i;
    //    }

    //    // number wasn't out from area for all iterations
    //    return maxIterations;
    //}

    private int ComputeIterationsJulia(double x, double y, int maxIterations)
    {
        // returns 0 .. maxIterations
        // return number of iteration when number is outside of desired area (or maxIterations if number wasn't out for all iterations)
        Complex pos = new Complex(x, y);
        Complex newPos = new Complex();
        if (LimitArea.IsOut((float)pos.Re, (float)pos.Im, 0))
            // is out already
            return 0;
        int maxN = C.Count;
        for (int i = 0; i < maxIterations;)
        {
            newPos.Clear();
            for (int j = 0; j < maxN; j++)
            {
                if (!C[j].IsZero()) // skipping of zero coefficients
                {
                    newPos += (pos ^ j) * C[j];
                }
            }
            //string newPosStrRe = DoubleConverter.ToExactString(newPos.Re);
            //string newPosStrIm = DoubleConverter.ToExactString(newPos.Im);
            pos = newPos;
            // zxc debug if is copied object or its content
            i++;
            if (LimitArea.IsOut((float)pos.Re, (float)pos.Im, i))
                return i;
        }

        // number wasn't out from area for all iterations
        return maxIterations;
    }

    private int ComputeIterationsMixed(double x, double y, int maxIterations)
    {
        // returns 0 .. maxIterations
        // return number of iteration when number is outside of desired area (or maxIterations if number wasn't out for all iterations)
        Complex pos0 = new Complex(x, y);
        Complex pos = new Complex(x, y);
        Complex newPos = new Complex();
        if (LimitArea.IsOut((float)pos.Re, (float)pos.Im, 0))
            // is out already
            return 0;
        int maxN = C.Count;
        int maxNC0 = C0.Count;
        Complex addC0 = new Complex(0, 0);
        for (int j = 0; j < maxNC0; j++)
        {
            if (!C0[j].IsZero()) // skipping of zero coefficients
            {
                addC0 += (pos0 ^ j) * C0[j];
            }
        }
        for (int i = 0; i < maxIterations;)
        {
            newPos.Clear();
            for (int j = 0; j < maxN; j++)
            {
                if (!C[j].IsZero()) // skipping of zero coefficients
                {
                    newPos += (pos ^ j) * C[j];
                }
            }
            newPos += addC0;
            pos = newPos;
            // zxc debug if is copied object or its content
            i++;
            if (LimitArea.IsOut((float)pos.Re, (float)pos.Im, i))
                return i;
        }

        // number wasn't out from area for all iterations
        return maxIterations;
    }

    private string GetFormulaJulia()
    {
        return "SUM(j)(C(j) * (Z(i) ^ j))";
    }

    void EliminateExtraZeroPowers()
    {
        while (C.Count > 0 && C[C.Count - 1] == Complex.Zero)
        {
            C.RemoveAt(C.Count - 1);
        }
        if (C0 != null)
        {
            while (C0.Count > 0 && C0[C0.Count - 1] == Complex.Zero)
            {
                C0.RemoveAt(C0.Count - 1);
            }
        }
    }

    public IterationIndex GetIterationIndex(int width, int height, string hash)
    {
        foreach(IterationIndexRecords records in s_iterationIndexRecords)
        {
            if (records.width == width && records.height == height)
            {
                return records.GetIterationIndex(hash);
            }
        }
        // such record doesn't exist yet
        IterationIndexRecords newRecords = new IterationIndexRecords(width, height);
        s_iterationIndexRecords.Add(newRecords);
        return newRecords.GetIterationIndex(hash);
    }

    void ClearIterationIndexes()
    {
        s_iterationIndexRecords.Clear();
    }

    void RoundDoubles()
    {
        foreach(Complex c in C)
        {
            c.RoundDoubles();
        }
        foreach (Complex c in C0)
        {
            c.RoundDoubles();
        }
    }



    // ========================== members =========================
    private List<Complex> m_c;
    private List<Complex> m_c0;
    private RectangularArea2D m_computationArea;
    private EDisplayMode m_displayMode = EDisplayMode.Proportional;
    private ComputeIterationsDelegate m_computeDelegate;
    private EComputationType m_computationType;
    private ColoringAlgorithm m_coloringAlgrorithm;
    private Area2DBase m_limitArea = new CircularArea2D(0, 0, 4);
    private int m_maxComputationStepCount;
    private string m_filePath;  // file with fractal (complete path)
    private string m_name;
#if CHECK_2D_COMPUTATION
    private IterationIndex m_iterationIndexCSharp = new IterationIndex();
#endif
    public struct IterationIndexRecord
    {
        public IterationIndexRecord(int w, int h, string hashString)
        {
            width = w;
            height = h;
            hash = hashString;
            iterationIndex = new IterationIndex(w, h);
        }
        public int width;
        public int height;
        public string hash;
        public IterationIndex iterationIndex;
    }
    public class IterationIndexRecords
    {
        public int width;
        public int height;
        public IterationIndexRecords(int w, int h)
        {
            width = w;
            height = h;
            m_roundIndex = 0;
            m_iterationRecords = new List<IterationIndexRecord>();
        }
        public IterationIndex GetIterationIndex(string hash)
        {
            foreach (IterationIndexRecord record in m_iterationRecords)
            {
                if (String.Compare(record.hash, hash, false) == 0)
                {
                    return record.iterationIndex;
                }
            }

            // iteration index doesn't exist
            if (m_iterationRecords.Count >= PlatformConstants.CachedNumberOfComputationIndexes)
            {
                // recycling of iteration index record
                int currentRoundIndex = m_roundIndex;
                m_roundIndex++;
                m_roundIndex = m_roundIndex % PlatformConstants.CachedNumberOfComputationIndexes;
                IterationIndexRecord record = m_iterationRecords[currentRoundIndex];
                record.hash = hash;
                record.iterationIndex.SetNotComputed();
                m_iterationRecords[currentRoundIndex] = record;
                return record.iterationIndex;
            }
            else
            {
                IterationIndexRecord newIterationIndexRecord = new IterationIndexRecord(width, height, hash);
                m_iterationRecords.Add(newIterationIndexRecord);
                return newIterationIndexRecord.iterationIndex;
            }
        }

        List<IterationIndexRecord> m_iterationRecords;
        int m_roundIndex;
    }

    private static List<IterationIndexRecords> s_iterationIndexRecords = new List<IterationIndexRecords>();
    private UndoRedoList m_undoRedoList;
    int m_powerCindex;
}
﻿//#define CHECK_BOXES

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Assets.Scripts.Utils;
using System.IO;
using Assets.Scripts.Math;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Xml.Serialization;
using Assets.Scripts.Graphic;


public class Fractal3DGraphic
{
    private enum AxisType
    {
        X,
        Y,
        Z
    };

    public class VectorXComparer : IComparer<Vector3Int>
    {
        public int Compare(Vector3Int x, Vector3Int y)
        {
            if (x.y < y.y)
            {
                return -1;
            }
            else if (x.y > y.y)
            {
                return 1;
            }
            if (x.z < y.z)
            {
                return -1;
            }
            else if (x.z > y.z)
            {
                return 1;
            }
            if (x.x < y.x)
            {
                return -1;
            }
            else if (x.x > y.x)
            {
                return 1;
            }
            return 0;
        }
    }

    public class VectorYComparer : IComparer<Vector3Int>
    {
        public int Compare(Vector3Int x, Vector3Int y)
        {
            if (x.x < y.x)
            {
                return -1;
            }
            else if (x.x > y.x)
            {
                return 1;
            }
            if (x.z < y.z)
            {
                return -1;
            }
            else if (x.z > y.z)
            {
                return 1;
            }
            if (x.y < y.y)
            {
                return -1;
            }
            else if (x.y > y.y)
            {
                return 1;
            }
            return 0;
        }
    }

    public class VectorZComparer : IComparer<Vector3Int>
    {
        public int Compare(Vector3Int x, Vector3Int y)
        {
            if (x.x < y.x)
            {
                return -1;
            }
            else if (x.x > y.x)
            {
                return 1;
            }
 
            if (x.y < y.y)
            {
                return -1;
            }
            else if (x.y > y.y)
            {
                return 1;
            }

            if (x.z < y.z)
            {
                return -1;
            }
            else if (x.z > y.z)
            {
                return 1;
            }
            return 0;
        }
    }

    const float SIDE_CONNECTION_COEFFICIENT = 1 / 3.0f;
    const float CORNER_CONNECTION_COEFFICIENT = 0.4f;

    public Fractal3DGraphic()
    {
        Init();
    }

    public Fractal3DGraphic(Fractal3DGraphic old)
    {
        Init();
        MinCubeSize = old.MinCubeSize;
        SavePath = old.SavePath;
        CubesCount = old.CubesCount;
        BoxesCount = old.BoxesCount;
        InsideCubesCount = old.InsideCubesCount;
        LinesCount = old.LinesCount;

        GetBaseObject().transform.localPosition = old.GetBaseObject().transform.localPosition;
        GetBaseObject().transform.localRotation = old.GetBaseObject().transform.localRotation;
        GetBaseObject().transform.localScale = old.GetBaseObject().transform.localScale;

        for (int i = 0; i < old.GetBaseObject().transform.childCount; i++)
        {
            UnityEngine.Transform transform = old.GetBaseObject().transform.GetChild(i);
            AddCube(transform);
        }
        ComputationHash = old.ComputationHash;
        GraphicsHash = old.GraphicsHash;
    }

    public float MinCubeSize
    {
        get { return m_minCubeSize; }
        set { m_minCubeSize = value; }
    }
    public string SavePath
    {
        get { return m_savePath; }
        set { m_savePath = value; }
    }

    [XmlIgnore]
    public int CubesCount
    {
        get { return m_cubesCount; }
        set { m_cubesCount = value; }
    }
    [XmlIgnore]
    public int BoxesCount
    {
        get { return m_boxesCount; }
        set { m_boxesCount = value; }
    }
    [XmlIgnore]
    public int InsideCubesCount
    {
        get { return m_insideCubesCount; }
        set { m_insideCubesCount = value; }
    }
    [XmlIgnore]
    public int LinesCount
    {
        get { return m_linesCount; }
        set { m_linesCount = value; }
    }

    [XmlIgnore]
    public string ComputationHash
    {
        get { return m_computationHash; }
        set { m_computationHash = value; }
    }

    public string ComputationParameters;

    [XmlIgnore]
    public string GraphicsHash
    {
        get { return m_graphicsHash; }
        set { m_graphicsHash = value; }
    }

    /////////////////// public functions ///////////////////
    //public void AddCube(Vector3 position, float size)
    //{
    //    GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
    //    cube.transform.localPosition = position;
    //    cube.transform.localScale = new Vector3(size, size, size);

    //    AddCuboid(cube);
    //}

    public void AddCube(Transform transform)
    {
        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.transform.localPosition = transform.localPosition;
        cube.transform.localRotation = transform.localRotation;
        cube.transform.localScale = transform.localScale;

        AddCuboid(cube);
    }

    public void AddZSideConnections(List<B> connections)
    {
        Vector3 beginVector = new Vector3();
        float connectionSize = MinCubeSize * SIDE_CONNECTION_COEFFICIENT;
        foreach (B box in connections)
        {
            TransformToGraphicConnectionCoordinate(box, ref beginVector, AxisType.Z);

            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.transform.localPosition = beginVector;
            cube.transform.localScale = new Vector3(connectionSize, connectionSize, MinCubeSize * box.c);
            cube.name = "diagonalZ";

            AddCuboid(cube, UnityEngine.Quaternion.Euler(0, 0, 45));
        }
    }

    public void AddXSideConnections(List<B> connections)
    {
        Vector3 beginVector = new Vector3();
        Vector3 endVector = new Vector3();
        float connectionSize = MinCubeSize * SIDE_CONNECTION_COEFFICIENT;
        foreach (B box in connections)
        {
            TransformToGraphicConnectionCoordinate(box, ref beginVector, AxisType.X);

            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.transform.localPosition = beginVector;
            cube.transform.localScale = new Vector3(MinCubeSize * box.c, connectionSize, connectionSize);
            cube.name = "diagonalX";

            AddCuboid(cube, UnityEngine.Quaternion.Euler(45, 0, 0));
        }
    }

    public void RemoveDiagonalConnections()
    {
        int childs = m_baseObject.transform.childCount;
        for (int i = childs - 1; i >= 0; i--)
        {
            //m_baseObject.transform.GetChild(i).gameObject.SetActive(false);
            GameObject go = m_baseObject.transform.GetChild(i).gameObject;
            if (go.name == "diagonalX" ||
                go.name == "diagonalY" ||
                go.name == "diagonalZ" ||
                go.name == "corner111" ||
                go.name == "corner1m11" ||
                go.name == "corner11m1" ||
                go.name == "corner1m1m1"
                )
            GameObject.DestroyImmediate(go);
        }
    }

    public void AddYSideConnections(List<B> connections)
    {
        Vector3 beginVector = new Vector3();
        float connectionSize = MinCubeSize * SIDE_CONNECTION_COEFFICIENT;
        foreach (B box in connections)
        {
            TransformToGraphicConnectionCoordinate(box, ref beginVector, AxisType.Y);

            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.transform.localPosition = beginVector;
            cube.transform.localScale = new Vector3(connectionSize, MinCubeSize * box.c, connectionSize);
            cube.name = "diagonalY";

            AddCuboid(cube, UnityEngine.Quaternion.Euler(0, 45, 0));
        }
    }

    public void AddCorner111Connections(List<Vector3Int> connections)
    {
        Vector3 beginVector = new Vector3();
        float connectionSize = MinCubeSize * CORNER_CONNECTION_COEFFICIENT;
        foreach (Vector3Int box in connections)
        {
            TransformToGraphicCornerConnectionCoordinate(box, ref beginVector);

            GameObject cylinder = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
            cylinder.transform.localPosition = beginVector;
            cylinder.transform.localScale = new Vector3(connectionSize, connectionSize, connectionSize);
            cylinder.name = "corner111";

            AddCyliner(cylinder, UnityEngine.Quaternion.Euler(45, 0, -45));
        }
    }

    public void AddCorner11m1Connections(List<Vector3Int> connections)
    {
        Vector3 beginVector = new Vector3();
        float connectionSize = MinCubeSize * CORNER_CONNECTION_COEFFICIENT;
        foreach (Vector3Int box in connections)
        {
            TransformToGraphicCornerConnectionCoordinate(box, ref beginVector);

            GameObject cylinder = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
            cylinder.transform.localPosition = beginVector;
            cylinder.transform.localScale = new Vector3(connectionSize, connectionSize, connectionSize);
            cylinder.name = "corner11m1";

            AddCyliner(cylinder, UnityEngine.Quaternion.Euler(-45, 0, -45));
        }
    }

    public void AddCorner1m11Connections(List<Vector3Int> connections)
    {
        Vector3 beginVector = new Vector3();
        float connectionSize = MinCubeSize * CORNER_CONNECTION_COEFFICIENT;
        foreach (Vector3Int box in connections)
        {
            TransformToGraphicCornerConnectionCoordinate(box, ref beginVector);

            GameObject cylinder = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
            cylinder.transform.localPosition = beginVector;
            cylinder.transform.localScale = new Vector3(connectionSize, connectionSize, connectionSize);
            cylinder.name = "corner1m11";

            AddCyliner(cylinder, UnityEngine.Quaternion.Euler(-45, 0, 45));
        }
    }

    public void AddCorner1m1m1Connections(List<Vector3Int> connections)
    {
        Vector3 beginVector = new Vector3();
        float connectionSize = MinCubeSize * CORNER_CONNECTION_COEFFICIENT;
        foreach (Vector3Int box in connections)
        {
            TransformToGraphicCornerConnectionCoordinate(box, ref beginVector);

            GameObject cylinder = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
            cylinder.transform.localPosition = beginVector;
            cylinder.transform.localScale = new Vector3(connectionSize, connectionSize, connectionSize);
            cylinder.name = "corner1m1m1";

            AddCyliner(cylinder, UnityEngine.Quaternion.Euler(45, 0, 45));
        }
    }

    //public void DeleteCube(Vector3 position)
    //{
    //    GameObject cube = m_cuboids[position];
    //    if (cube != null)
    //    {
    //        m_cuboids.Remove(position);
    //        GameObject.Destroy(cube);            
    //    }
    //}

    public void AddCuboidWithCentralAlignment(Box box)
    {
        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.transform.localPosition = Coord.Transform(box.position);
        cube.transform.localScale = new Vector3(box.size.x, box.size.y, box.size.z);

        AddCuboid(cube);
    }

    //public void AddSphere(Vector3 position, float radius)
    //{
    //    GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Sphere);
    //    cube.transform.position = position;
    //    cube.transform.localScale = new Vector3(radius, radius, radius);

    //    Renderer rend = cube.GetComponent<Renderer>();
    //    rend.material = m_material;
    //    cube.transform.parent = m_baseObject.transform;
    //}

    public void Clear(bool destroyBaseObject, bool createNewBaseObject)
    {
        //m_cuboids.Clear();
        //m_baseObject.transform.Clear(); // clearing of all children
        int childs = m_baseObject.transform.childCount;
        for (int i = childs - 1; i >= 0; i--)
        {
            //m_baseObject.transform.GetChild(i).gameObject.SetActive(false);
            GameObject.DestroyImmediate(m_baseObject.transform.GetChild(i).gameObject);
        }
        if (destroyBaseObject)
        {
            //m_baseObject.SetActive(false);
            GameObject.DestroyImmediate(m_baseObject);

            if (createNewBaseObject)
            {
                CreateNewBaseObject();
                InitBaseObject();
            }
        }
        ComputationHash = "";
        GraphicsHash = "";
    }

    public void SetColor(UnityEngine.Color color)
    {
        m_material.color = color;
    }

    public void Save(string path, float minCubeSize, List<Box> boxes)
    {
        List<B> intBoxes = new List<B>(boxes.Count);
        foreach (Box box in boxes)
        {
            B b = new B();
            Debug.Assert(Round(box.size.x / minCubeSize) == 1, "size x is bigger that basic cube size");
            Debug.Assert(Round(box.size.y / minCubeSize) == 1, "size y is bigger that basic cube size");
            b.x = Round(box.position.x / minCubeSize);
            b.y = Round(box.position.y / minCubeSize);
            b.z = Round(box.position.z / minCubeSize + 0.1f);
            b.c = Round(box.size.z / minCubeSize);
            //b.i = intBoxes.Count;
            intBoxes.Add(b);
        }
        Fractal3DGraphicSaver graphicSaver = new Fractal3DGraphicSaver(minCubeSize, intBoxes);
        graphicSaver.Save(path);
    }


    static public UnityEngine.Bounds GetBounds(List<Box> boxes, float minCubeSize)
    {
        float big = 10000000000;
        Vector3 minGlobal = new Vector3(big, big, big);
        Vector3 maxGlobal = new Vector3(-big, -big, -big);
        Vector3 addMin = new Vector3(-minCubeSize / 2, -minCubeSize / 2, 0);
        Vector3 addMax = new Vector3(minCubeSize / 2, minCubeSize / 2, 0);
        foreach (Box box in boxes)
        {
            addMin.z = -box.size.z / 2;
            Vector3 min = box.position + addMin;
            addMax.z = box.size.z / 2;
            Vector3 max = box.position + addMax;
            minGlobal.x = Functions.Min(minGlobal.x, min.x);
            minGlobal.y = Functions.Min(minGlobal.y, min.y);
            minGlobal.z = Functions.Min(minGlobal.z, min.z);
            maxGlobal.x = Functions.Max(maxGlobal.x, max.x);
            maxGlobal.y = Functions.Max(maxGlobal.y, max.y);
            maxGlobal.z = Functions.Max(maxGlobal.z, max.z);
        }
        Bounds computedBounds = new Bounds((minGlobal + maxGlobal) / 2, maxGlobal - minGlobal);
        return computedBounds;
    }

    public UnityEngine.Bounds GetBounds()
    {
        GameObject baseObject = GetBaseObject();
        float big = 10000000000;
        Vector3 minGlobal = new Vector3(big, big, big);
        Vector3 maxGlobal = new Vector3(-big, -big, -big);
        foreach (Transform box in baseObject.transform)
        {
            Vector3 min = box.localPosition - box.localScale * 0.5f;
            Vector3 max = box.localPosition + box.localScale * 0.5f;
            minGlobal.x = Functions.Min(minGlobal.x, min.x);
            minGlobal.y = Functions.Min(minGlobal.y, min.y);
            minGlobal.z = Functions.Min(minGlobal.z, min.z);
            maxGlobal.x = Functions.Max(maxGlobal.x, max.x);
            maxGlobal.y = Functions.Max(maxGlobal.y, max.y);
            maxGlobal.z = Functions.Max(maxGlobal.z, max.z);
        }
        Bounds bounds = new Bounds((minGlobal + maxGlobal) / 2, maxGlobal - minGlobal);
        return bounds;
    }



    public UnityEngine.Bounds GetBounds(float expandCoefficient, Bounds defaultBounds)
    {
        // expandCoefficient - if expandCoefficient == 1.2 ... expanding by 20 percent
        GameObject baseObject = GetBaseObject();
        float big = 10000000000;
        Vector3 minGlobal = new Vector3(big, big, big);
        Vector3 maxGlobal = new Vector3(-big, -big, -big);
        foreach (Transform box in baseObject.transform)
        {
            Vector3 min = box.localPosition - box.localScale * 0.5f;
            Vector3 max = box.localPosition + box.localScale * 0.5f;
            minGlobal.x = Functions.Min(minGlobal.x, min.x);
            minGlobal.y = Functions.Min(minGlobal.y, min.y);
            minGlobal.z = Functions.Min(minGlobal.z, min.z);
            maxGlobal.x = Functions.Max(maxGlobal.x, max.x);
            maxGlobal.y = Functions.Max(maxGlobal.y, max.y);
            maxGlobal.z = Functions.Max(maxGlobal.z, max.z);
        }

        if (minGlobal.x == big)
        {
            // graphic isn't computed yet - no objects in graphic
            return defaultBounds;
        }

        Bounds computedBounds = new Bounds((minGlobal + maxGlobal) / 2, maxGlobal - minGlobal);

        // expansion of bounds
        computedBounds.size = computedBounds.size * expandCoefficient;
        return computedBounds;
    }

    public List<B> GetGraphicIntBoxes()
    {
        List<B> intBoxes = new List<B>();
        GameObject baseObject = GetBaseObject();
        float minCubeSize = MinCubeSize;
        foreach (Transform box in baseObject.transform)
        {
            //child.position = Vector3.zero;
            B b = new B();
            Debug.Assert(Round(box.transform.localScale.x / minCubeSize) == 1, "size x is bigger that basic cube size");
            Debug.Assert(Round(box.transform.localScale.y / minCubeSize) == 1, "size y is bigger that basic cube size");
            Vector3 graphicCoordinate = box.localPosition;
            b.x = Round(graphicCoordinate.x / minCubeSize + 0.1f);
            b.y = Round(graphicCoordinate.y / minCubeSize + 0.1f);
            b.z = Round((graphicCoordinate.z - box.transform.localScale.z / 2) / minCubeSize + 0.1f);
            b.c = Round(box.transform.localScale.z / minCubeSize);
            //b.i = intBoxes.Count;      
            intBoxes.Add(b);
        }
        return intBoxes;
    }

    public void TransformToIntCoordinates(Vector3 graphicCoordinate, ref Vector3Int b)
    {
        b.x = Round(graphicCoordinate.x / MinCubeSize + 0.1f);
        b.y = Round(graphicCoordinate.y / MinCubeSize + 0.1f);
        b.z = Round(graphicCoordinate.z / MinCubeSize + 0.1f);
    }


    public void TransformToGraphicCornerConnectionCoordinate(Vector3Int b, ref Vector3 graphicCoordinate)
    {
        graphicCoordinate.x = MinCubeSize * b.x + MinCubeSize / 2;
        graphicCoordinate.y = MinCubeSize * b.y + MinCubeSize / 2;
        graphicCoordinate.z = MinCubeSize * b.z + MinCubeSize / 2;

        graphicCoordinate += GetBaseObject().transform.position;
        //graphicCoordinate = Coord.Transform(graphicCoordinate);
    }

    private void TransformToGraphicConnectionCoordinate(B b, ref Vector3 graphicCoordinate, AxisType axisType)
    {
        graphicCoordinate.x = MinCubeSize * b.x + MinCubeSize / 2;
        graphicCoordinate.y = MinCubeSize * b.y + MinCubeSize / 2;
        graphicCoordinate.z = MinCubeSize * b.z + MinCubeSize / 2;
        switch (axisType)
        {
            case AxisType.X:
                graphicCoordinate.x = MinCubeSize * (b.x + (b.c - 1) / 2.0f);
                break;
            case AxisType.Y:
                graphicCoordinate.y = MinCubeSize * (b.y + (b.c - 1) / 2.0f);
                break;
            case AxisType.Z:
                graphicCoordinate.z = MinCubeSize * (b.z + (b.c - 1) / 2.0f);
                break;
        }
        graphicCoordinate += GetBaseObject().transform.position;
    }

    public void Save(string path)
    {
        if (SavePath != null && SavePath.Length > 0 && File.Exists(SavePath))
        {
            // copy of already saved graphic
            File.Copy(SavePath, path);
            return;
        }
        List<B> intBoxes = new List<B>();
        GameObject baseObject = GetBaseObject();
        float minCubeSize = MinCubeSize;
        foreach (Transform box in baseObject.transform)
        {
            //child.position = Vector3.zero;
            B b = new B();
            Debug.Assert(Round(box.transform.localScale.x / minCubeSize) == 1, "size x is bigger that basic cube size");
            Debug.Assert(Round(box.transform.localScale.y / minCubeSize) == 1, "size y is bigger that basic cube size");
            Vector3 computationCoordinate = Coord.Transform(box.localPosition); // transforming coordinate from dipslay coordinate to computation (basic) coordinates
            b.x = Round(computationCoordinate.x / minCubeSize);
            b.y = Round(computationCoordinate.y / minCubeSize);
            b.z = Round(computationCoordinate.z / minCubeSize + 0.1f);
            b.c = Round(box.transform.localScale.z / minCubeSize);
            //b.i = intBoxes.Count;      
            intBoxes.Add(b);
        }
        Fractal3DGraphicSaver graphicSaver = new Fractal3DGraphicSaver(minCubeSize, intBoxes);
        graphicSaver.Save(path);
    }

    public bool Load(string path)
    {
        //        Fractal3DGraphicSaver saver;
        //        bool res = Fractal3DGraphicSaver.Load(path, out saver);
        //        if (res)
        //        {
        //            // converting of int box coordinates into graphic boxes
        //            float minCubeSize = saver.BasicCubeSize;
        //            List<Box> boxes = new List<Box>();
        //            foreach (B intBox in saver.Boxes)
        //            {
        //                Box box = new Box();
        //                box.position.x = intBox.x * minCubeSize;
        //                box.position.y = intBox.y * minCubeSize;
        //                box.position.z = intBox.z * minCubeSize - ((intBox.c % 2 == 0) ? minCubeSize / 2 : 0);
        //                box.size.x = minCubeSize;
        //                box.size.y = minCubeSize;
        //                box.size.z = intBox.c * minCubeSize;
        //                //box.i = boxes.Count;
        //                boxes.Add(box);
        //            }

        //#if CHECK_BOXES
        //                    boxes.Sort(Box.CompareBoxes);
        //#endif
        //            InitFromList(boxes, minCubeSize);
        //            //Saver.SaveBoxes("C:/joy/boxesAfterLoad.txt", boxes);
        //        }// loading through cpp function is quicker
        //Save("C:/joy/saveAfterLoad.sav");

        // loading through cpp function
        bool res = File.Exists(path);
        if (!res)
        {
            return res;
        }
        string boxesLoaded;
        float outMinCubeSize;
        GetGraphicBoxesCpp(path, out boxesLoaded, out outMinCubeSize);
        MinCubeSize = outMinCubeSize;
        {
            List<Box> boxesCpp2 = new List<Box>();
            {
                Box box = new Box();
                box.size.x = box.size.y = outMinCubeSize;
                foreach (var myString in boxesLoaded.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    float[] temp = myString.Split().Select(x => float.Parse(x, CultureInfo.InvariantCulture)).ToArray();
                    box.position.x = temp[0];
                    box.position.y = temp[1];
                    box.position.z = temp[2];
                    box.size.z = temp[3];
                    boxesCpp2.Add(box);
                }
            }
            InitFromList(boxesCpp2, outMinCubeSize);
        }
        return res;
    }

    [DllImport("FractalsDll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
    [return: MarshalAs(UnmanagedType.LPStr)]
    public static extern string GetGraphicBoxes(string graphicPath, ref float minCubeSize);


    static void GetGraphicBoxesCpp(string graphicPath, out string boxes, out float minCubeSize)
    {
        minCubeSize = 0;
        boxes = GetGraphicBoxes(graphicPath, ref minCubeSize);
    }

    private void CheckGraphic(Fractal3DGraphic graphic, Fractal3DGraphic graphic1)
    {
        if (graphic.GetBaseObject().transform.childCount != graphic1.GetBaseObject().transform.childCount)
        {
            Debug.Assert(false, "not equal children size");
            return;
        }
        for (int i = 0; i < graphic.GetBaseObject().transform.childCount; i++)
        {
            UnityEngine.Transform box = graphic.GetBaseObject().transform.GetChild(i);
            UnityEngine.Transform box1 = graphic1.GetBaseObject().transform.GetChild(i);
            if (box.position != box1.position || box.localScale != box1.localScale)
            {
                Debug.Assert(false, "not equal boxes");
                return;
            }
        }
    }


    int Round(float value)
    {
        return Math.Sign(value) * (int)(Math.Abs(value) + 0.5);
    }

    public void InitFromList(List<Box> boxes, float minCubeSize)
    {
        MinCubeSize = minCubeSize;
        foreach (Box box in boxes)
        {
            AddCuboidWithCentralAlignment(box);
        }

        InitBaseObject();
    }

    void Init()
    {
        CreateNewBaseObject();
        InitBaseObject();

        //m_cuboids = new Dictionary<Vector3, GameObject>();
        //Shader shader = Shader.Find("Transparent/Diffuse");
        //Shader shader = Shader.Find("Mobile/Diffuse");
        Shader shader = Shader.Find("Legacy Shaders/Diffuse");
        m_material = new Material(shader);
    }

    public GameObject GetBaseObject() { return m_baseObject; }

    //////////////////// private functions //////////////////
    private void AddCuboid(GameObject cube)
    {
        Renderer rend = cube.GetComponent<Renderer>();
        rend.material = m_material;
        cube.transform.parent = m_baseObject.transform;
        cube.transform.localRotation = UnityEngine.Quaternion.Euler(0, 0, 0);

        //m_cuboids[cube.transform.position] = cube;
    }

    private void AddCuboid(GameObject cube, Quaternion localRotation)
    {
        Renderer rend = cube.GetComponent<Renderer>();
        rend.material = m_material;
        cube.transform.parent = m_baseObject.transform;
        cube.transform.localRotation = localRotation;

        //m_cuboids[cube.transform.position] = cube;
    }

    private void AddCyliner(GameObject cube, Quaternion localRotation)
    {
        Renderer rend = cube.GetComponent<Renderer>();
        if (m_warningMaterial == null)
        {
            Shader shader = Shader.Find("Legacy Shaders/Diffuse");
            m_warningMaterial = new Material(shader);
            m_warningMaterial.color = Color.red;
        }
        rend.material = m_warningMaterial;
        cube.transform.parent = m_baseObject.transform;
        cube.transform.localRotation = localRotation;
    }

    private void CreateNewBaseObject()
    {
        if (m_baseObject)
        {
            m_baseObject.SetActive(false);
            GameObject.DestroyObject(m_baseObject);
        }
        m_baseObject = new UnityEngine.GameObject();
        m_baseObject.name = "3D fractal base object";
    }

    private void InitBaseObject()
    {
        GameObject parentObject = UnityEngine.GameObject.Find(Fractal3DGraphic.BaseSceneObjectName);
        if (parentObject != null)
        {
            GetBaseObject().transform.parent = parentObject.transform;
        }
        GetBaseObject().transform.localPosition = new UnityEngine.Vector3(0, 0, 0);
        GetBaseObject().transform.localRotation = UnityEngine.Quaternion.Euler(0, 0, 0);
    }

    public const string BaseSceneObjectName = "3D Scene Base Object";                                                 
    private GameObject m_baseObject;
    //private Dictionary<Vector3, GameObject> m_cuboids;
    private Material m_material;
    private Material m_warningMaterial;
    private float m_minCubeSize;
    private string m_savePath;
    int m_cubesCount = 0;
    int m_boxesCount = 0;
    int m_insideCubesCount = 0;
    int m_linesCount = 0;
    string m_computationHash;
    string m_graphicsHash;
}

﻿using UnityEngine;
using System.Collections;
using System;

public interface IFractal : ICloneable
{
    string GetName();
    string GetFilename();
    Texture2D GetTexture(int textureHeight);
    Color GetBackgroundColor();
}

public interface I3DFractal : IFractal
{
}

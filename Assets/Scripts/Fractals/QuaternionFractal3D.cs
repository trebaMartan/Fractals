﻿//#define CHECK_CUBES // requires special single thread build of dll
//#define CHECK_BOXES
//#define CHECK_GRAPHIC // requires special single thread build of dll

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using SmartMathLibrary;
using Assets.Scripts.Math;
using Assets.Scripts.Utils;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Globalization;
using System.Diagnostics;
using Assets.Scripts.Computation;

public enum QuaternionDelegateType
{
    Julia,
    MixedMJ
};

public class ComputationAttribute : Attribute
{
}
public class GraphicAttribute : Attribute
{
}

[XmlInclude(typeof(RectangularArea2D))]
[XmlInclude(typeof(Vector3D))]
public class SliceParameters
{
    public Vector3D AxisX
    {
        get { return m_axisX; }
        set { m_axisX = value; }
    }
    public Vector3D AxisY
    {
        get { return m_axisY; }
        set { m_axisY = value; }
    }
    public Vector3D PlaneZero
    {
        get { return m_planeZero; }
        set { m_planeZero = value; }
    }
    public RectangularArea2D SliceComputationArea
    {
        get { return m_computationArea; }
        set { m_computationArea = value; }
    }
    Vector3D m_axisX;
    Vector3D m_axisY;
    Vector3D m_planeZero;
    RectangularArea2D m_computationArea;
}

[XmlInclude(typeof(QuaternionAreaBase))]
[XmlInclude(typeof(CircularQuaternionArea))]
[XmlInclude(typeof(RectangularQuaternionArea))]
[XmlInclude(typeof(RectangularArea3D))]
public class QuaternionFractal3D : Observable, I3DFractal, IObserver 
{

    public QuaternionFractal3D()
    {
        CreateNewGraphic();
        AddObserver(this);
        ComputationArea = new RectangularArea3D(0, 0, 0, 2, 2, 2);
        m_undoRedoList = new UndoRedoList();
        FractalColor = Persistent.Instance.DefaultForegroundColor;
        BackgroundColor = Persistent.Instance.DefaultBackgroundColor;
        m_undoRedoList.SetObject(this);
        m_sliceColoringAlgorithm = new ColoringAlgorithm();
    }

    public QuaternionFractal3D(QuaternionFractal3D old, bool cloneGraphic)
    {
        if (cloneGraphic)
        {
            m_graphic = new Fractal3DGraphic(old.m_graphic);
        }
        else
        {
            m_graphic = old.m_graphic;
        }
        if (old.Palette != null)
        {
            Palette = old.Palette.Clone() as IColorPalette;
        }
        else
        {
            Palette = null;
        }
        FractalColor = old.FractalColor;
        FractalBackgroundColor = old.FractalBackgroundColor;
        Name = old.Name.Clone() as string;
        FilePath = old.FilePath.Clone() as string;
        MaxComputationStepCount = old.MaxComputationStepCount;
        MinCubeSize = old.MinCubeSize;
        LimitArea = old.LimitArea.Clone();
        for (int i = 0; i < old.C.Count; i++)
        {
            SetC(i, old.GetC(i).Copy());
        }
        for (int i = 0; i < old.CA.Count; i++)
        {
            SetCA(i, old.GetCA(i).Copy());
        }
        for (int i = 0; i < old.C0.Count; i++)
        {
            SetC0(i, old.GetC0(i).Copy());
        }
        for (int i = 0; i < old.C0A.Count; i++)
        {
            SetC0A(i, old.GetC0A(i).Copy());
        }
        Vec2QuatMapping = old.Vec2QuatMapping.Clone();
        ComputeDelegateType = old.ComputeDelegateType;
        CubeFineness = old.CubeFineness;
        ComputationArea = old.ComputationArea.Clone() as RectangularArea3D;
        CoefficientBeforePower = old.CoefficientBeforePower;
        CoefficientAfterPower = old.CoefficientAfterPower;
        CoefficientC0BeforePower = old.CoefficientC0BeforePower;
        CoefficientC0AfterPower = old.CoefficientC0AfterPower;
        m_undoRedoList = old.GetUndoRedoList().Clone() as UndoRedoList;
        m_undoRedoList.SetObject(this);
        SliceColoringAlgorithm = new ColoringAlgorithm(old.SliceColoringAlgorithm);
    }

    /////////////// public properties /////////////////
    public string Version
    {
        get { return AboutSceneScript.versionNumber; }
        set { }
    }

    [ComputationAttribute]
    public string ComputationVersion
    {
        get { return "0.00"; }
    }

    [XmlIgnore]
    public IColorPalette Palette
    {
        get { return m_colorPalette; }
        set
        {
            if (m_colorPalette == value)
                return;
            m_colorPalette = value;
        }
    }

    public SinglePalette SinglePalette
    {
        get { return Palette as SinglePalette; }
        set { Palette = value; }
    }

    public CompositePalette CompositePalette
    {
        get { return Palette as CompositePalette; }
        set { Palette = value; }
    }

    [XmlIgnore]
    [GraphicAttribute]
    public UnityEngine.Color FractalColor
    {
        get { return m_fractalColor; }
        set { m_fractalColor = value;
            if (GetGraphic() != null)
            {
                GetGraphic().SetColor(m_fractalColor);
            }
        }
    }
    public XmlColor Color
    {
        get { return new XmlColor(FractalColor); }
        set { FractalColor = value; }
    }

    [XmlIgnore]
    [GraphicAttribute]
    public UnityEngine.Color FractalBackgroundColor
    {
        get { return m_fractalBackgroundColor; }
        set
        {
            m_fractalBackgroundColor = value;
        }
    }
    public XmlColor BackgroundColor
    {
        get { return new XmlColor(FractalBackgroundColor); }
        set { FractalBackgroundColor = value; }
    }

    public string Name
    {
        get { return m_name; }
        set { m_name = value; }
    }

    [XmlIgnore]
    public string FilePath
    {
        get { return m_fileName; }
        set { m_fileName = value; }
    }

    public delegate int ComputeIterationsDelegate(double x, double y, double z, int maxIterations);

    [ComputationAttribute]
    public QuaternionDelegateType ComputeDelegateType
    {
        get { return m_computeDelegateType; }
        set
        {
            switch (value)
            {
            case QuaternionDelegateType.Julia:
                m_computeDelegate = new ComputeIterationsDelegate(ComputeJuliaMapped);
                break;
            case QuaternionDelegateType.MixedMJ:
                m_computeDelegate = new ComputeIterationsDelegate(ComputeMixedMapped);
                break;
            default:
                UnityEngine.Debug.Assert(false, "not handled delegate type");
                break;
            }
            m_computeDelegateType = value;
        }
    }

    [ComputationAttribute]
    public int MaxComputationStepCount
    {
        get { return m_maxComputationStep; }
        set { m_maxComputationStep = value; }
    }
    [ComputationAttribute]
    public float MinCubeSize
    {
        get { return m_minCubeSize; }
        set { m_minCubeSize = value; }
    }
    [ComputationAttribute]
    public bool CoefficientBeforePower
    {
        get { return m_coefficientBeforePower; }
        set { m_coefficientBeforePower = value; }
    }
    [ComputationAttribute]
    public bool CoefficientAfterPower
    {
        get { return m_coefficientAfterPower; }
        set { m_coefficientAfterPower = value; }
    }
    [ComputationAttribute]
    public bool CoefficientC0BeforePower
    {
        get { return m_coefficientC0BeforePower; }
        set { m_coefficientC0BeforePower = value; }
    }
    [ComputationAttribute]
    public bool CoefficientC0AfterPower
    {
        get { return m_coefficientC0AfterPower; }
        set { m_coefficientC0AfterPower = value; }
    }

    public enum Fineness
    {
        __Begin,
        Low = __Begin,
        Normal,
        High,
        ExtraHigh,
        UltraHigh,
        UserDefined,
        __End,
    }

    [ComputationAttribute]
    public Fineness CubeFineness
    {
        get { return m_fineness; }
        set {
            Fineness oldValue = m_fineness;
            m_fineness = value;
            if (oldValue != value)
            {
                Notify();
            }
        }
    }

    [UndoRedoAttribute]
    [XmlIgnore]
    public int LimitAreaType
    {
        get { return m_limitAreaType; }
        set { m_limitAreaType = value; }
    }

    [ComputationAttribute]
    public QuaternionAreaBase LimitArea
    {
        get { return m_limitArea; }
        set { m_limitArea = value; }
    }
    [ComputationAttribute]
    public RectangularArea3D ComputationArea
    {
        get { return m_computationArea; }
        set {
            RectangularArea3D oldValue = m_computationArea;
            if (oldValue != null)
            {
                oldValue.RemoveObserver(this);
            }
            m_computationArea = value;
            if (oldValue != value)
            {
                Notify();
                m_computationArea.AddObserver(this);
            }
        }
    }
    [ComputationAttribute]
    public List<Quaternion> C
    {
        get { return m_c; }
        set { m_c = value; }
    }
    [ComputationAttribute]
    public List<Quaternion> CA
    {
        get { return m_cA; }
        set { m_cA = value; }
    }
    [ComputationAttribute]
    public List<Quaternion> C0
    {
        get { return m_c0; }
        set { m_c0 = value; }
    }
    [ComputationAttribute]
    public List<Quaternion> C0A
    {
        get { return m_c0A; }
        set { m_c0A = value; }
    }
    [ComputationAttribute]
    public Vector2QuaternionMapping Vec2QuatMapping
    {
        get { return m_vec2QuatMapping; }
        set { m_vec2QuatMapping = value; }
    }

    [UndoRedoAttribute]
    [XmlIgnore]
    public int PowerCindex
    {
        get { return m_powerCindex; }
        set { m_powerCindex = value; }
    }

    [UndoRedoAttribute]
    [XmlIgnore]
    public int PowerC0index
    {
        get { return m_powerC0index; }
        set { m_powerC0index = value; }
    }

    public ColoringAlgorithm SliceColoringAlgorithm
    {
        get { return m_sliceColoringAlgorithm; }
        set { m_sliceColoringAlgorithm = value; }
    }

    ////////////// public functions ///////////////////
    public object Clone()
    {
        QuaternionFractal3D fr = new QuaternionFractal3D(this, false);
        return fr;
    }

    public object Clone(bool cloneGraphic)
    {
        QuaternionFractal3D fr = new QuaternionFractal3D(this, cloneGraphic);
        return fr;
    }

    public bool GraphicIsComputed()
    {
        return GetGraphic() != null && GetGraphic().ComputationHash == GetComputationParametersHash();
    }

    /// function set fractal color (and not graphic color)
    public void SetFractalColor(UnityEngine.Color color)
    {
        m_fractalColor = color;
    }

    /// function set graphic color (and not fractal color)
    public void SetGraphicColor(UnityEngine.Color color)
    {
        GetGraphic().SetColor(color);
    }

    public void OnChange(Object observable)
    {
        if (m_fineness != Fineness.UserDefined)
        {
            RecomuteMinCubeSize();
        }
    }

    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public delegate void ProgressCallback(int value);

    [DllImport("FractalsDll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
    [return: MarshalAs(UnmanagedType.LPStr)]
    public static extern string ComputeFractalGraphic(int processorCount,
        string fractalDefinition, string computedGraphicPath,
        bool returnBoxes, ref int boxesCount, ProgressCallback progressCallback);

    static void ComputeFractalGraphicCpp(int processorCount, string fractalDefinition,
        string computedGraphicPath, bool returnBoxes,
        out string strReturn, out int boxesCount)
    {
        boxesCount = 0;
        ProgressCallback callback = (value) => {
            Persistent.Instance.Progress = value;
        };

        strReturn = ComputeFractalGraphic(processorCount, fractalDefinition, computedGraphicPath, returnBoxes, ref boxesCount, callback);
    }

    public void CreateNewGraphic()
    {
        m_graphic = new Fractal3DGraphic();
    }

    [DllImport("FractalsDll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
    static extern void ComputeFractalSliceTexture(int processorCount, int textureWidth, int textureHeight,
    string fractalDefinition, // path in debug or xml string with definition
    string sliceParameters, // serialization of parameters (axes x, y, point zero on plane, area of computation) - path to file or directly serialized params
    [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 5)] out int[] iterations, out int count);

    public UnityEngine.Texture2D GetSliceTextureCpp(int textureWidth, int textureHeight, Vector3D axisX, Vector3D axisY, Vector3D pointZeroOnPlane)
    {
        int processorCount = Persistent.Instance.ProcessorCount;

        int len = textureWidth * textureHeight;
        int[] iterations = new int[len];
        string computationHash = GetComputationParametersHash();
        IterationIndex iterationIndex = GetIterationIndex(textureWidth, textureHeight);

        string fractalDefinition = Saver.Serialize<QuaternionFractal3D>(this);
        
        //string fractalDefinition = Persistent.GetTempDir();
        //fractalDefinition = Path.Combine(fractalDefinition, "definition3D." + Persistent.GetExtension(Persistent.SaveExtension.fractal3DDefinitionExtension));
        //SaveDefinition(fractalDefinition);

        SliceParameters sliceParameters = new SliceParameters();
        sliceParameters.AxisX = axisX;
        sliceParameters.AxisY = axisY;
        sliceParameters.PlaneZero = pointZeroOnPlane;
        sliceParameters.SliceComputationArea = new RectangularArea2D(0, 0, ComputationArea.GetMaxSize(), ComputationArea.GetMaxSize());
        string sliceParametersString = Saver.Serialize<SliceParameters>(sliceParameters);

        string sliceParametersPath = Persistent.GetTempDir();
        sliceParametersPath += "sliceParams.xml";
        Saver.Save(sliceParameters, sliceParametersPath);

        ComputeFractalSliceTexture(processorCount, textureWidth, textureHeight, fractalDefinition,
            sliceParametersString, 
            //sliceParametersPath,
            out iterations, out len);
        iterationIndex.SetComputed(computationHash);
        iterationIndex.Iterations = iterations.ToList<int>();

        SliceColoringAlgorithm.BackgroundColor = UnityEngine.Color.yellow;
        SliceColoringAlgorithm.SinglePalette = new SinglePalette(SliceColoringAlgorithm.BackgroundColor, FractalColor);
        SliceColoringAlgorithm.ColorCount = MaxComputationStepCount;
        SliceColoringAlgorithm.RepeatType = RepeatType.Triangle;

        UnityEngine.Texture2D texture = CreateTextureFromIterationIndex(iterationIndex);

        return texture;
    }

    IterationIndex GetIterationIndex(int width, int height)
    {
        m_sliceIterationIndex.Width = width;
        m_sliceIterationIndex.Height = height;
        return m_sliceIterationIndex;
    }

    UnityEngine.Texture2D CreateTextureFromIterationIndex(IterationIndex iterationIndex)
    {
        int textureWidth = iterationIndex.Width;
        int textureHeight = iterationIndex.Height;
        List<int> iterationsIndex = iterationIndex.Iterations;
        UnityEngine.Texture2D texture = new UnityEngine.Texture2D(textureWidth, textureHeight);

        TextureUtils.FillWithColor(texture, new UnityEngine.Color(0, 0, 0, 0));

        Debug.Assert(iterationIndex.Iterations.Count == textureWidth * textureHeight);
        if (iterationsIndex.Count != textureWidth * textureHeight)
        {
            return texture;
        }
        int n = textureWidth * textureHeight;
        for (int i = 0; i < n; i++)
        {
            int column = i % textureWidth;
            int row = i / textureWidth;
            int currentColorIndex = iterationsIndex[i];
            if (currentColorIndex >= 0)
            {
                // pixels that are outside of computed are are eventually filled with FillColor function
                texture.SetPixel(column, row, SliceColoringAlgorithm.GetColor(currentColorIndex));
            }
        }

        texture.Apply();
        return texture;
    }

    public void MaximizeComputationArea()
    {
        double xMin = GetMinMax(Axis3D.X, false);
        double yMin = GetMinMax(Axis3D.Y, false);
        double zMin = GetMinMax(Axis3D.Z, false);
        double xMax = GetMinMax(Axis3D.X, true);
        double yMax = GetMinMax(Axis3D.Y, true);
        double zMax = GetMinMax(Axis3D.Z, true);

        double centerX = (xMin + xMax) / 2;
        double centerY = (yMin + yMax) / 2;
        double centerZ = (zMin + zMax) / 2;

        ComputationArea = new RectangularArea3D((float)centerX, (float)centerY, (float)centerZ, (float)(xMax - centerX), (float)(yMax - centerY), (float)(zMax - centerZ));
    }

    public UnityEngine.Color GetBackgroundColor() { return UnityEngine.Color.black; }

    // from interface
    public string GetName()
    {
        return Name;
    }

    public string GetFilename()
    {
        return m_fileName;
    }

    public Fractal3DGraphic GetGraphic()
    {
        return m_graphic;
    }

    public UnityEngine.Texture2D GetTexture(int textureHeight)
    {
        int textureWidth = textureHeight;
        UnityEngine.Texture2D texture = new UnityEngine.Texture2D(textureWidth, textureHeight);

        // zxc TODO make some screenshot of something

        texture.Apply();
        return texture;
    }

    public void SetC(int n, Quaternion c)
    {
        if (C == null)
        {
            C = new List<Quaternion>();
        }
        while (C.Count < n + 1)
        {
            C.Add(new Quaternion());
        }
        C[n] = c;
    }
    public Quaternion GetC(int n)
    {
        if (n < 0 || n > C.Count - 1)
        {
            return Quaternion.Zero;
        }
        return C[n];
    }

    public void SetCA(int n, Quaternion c)
    {
        if (CA == null)
        {
            CA = new List<Quaternion>();
        }
        while (CA.Count < n + 1)
        {
            CA.Add(new Quaternion());
        }
        CA[n] = c;
    }
    public Quaternion GetCA(int n)
    {
        if (n < 0 || n > CA.Count - 1)
        {
            return Quaternion.Zero;
        }
        return CA[n];
    }

    public void SetC0(int n, Quaternion c)
    {
        if (C0 == null)
        {
            C0 = new List<Quaternion>();
        }
        while (C0.Count < n + 1)
        {
            C0.Add(Quaternion.Zero);
        }
        C0[n] = c;
    }
    public Quaternion GetC0(int n)
    {
        if (n < 0 || n > C0.Count - 1)
        {
            return Quaternion.Zero;
        }
        return C0[n];
    }
    public void SetC0A(int n, Quaternion c)
    {
        if (C0A == null)
        {
            C0A = new List<Quaternion>();
        }
        while (C0A.Count < n + 1)
        {
            C0A.Add(Quaternion.Zero);
        }
        C0A[n] = c;
    }
    public Quaternion GetC0A(int n)
    {
        if (n < 0 || n > C0A.Count - 1)
        {
            return Quaternion.Zero;
        }
        return C0A[n];
    }

    public void ComputeFractal(bool initGraphic)
    {
        ComputeFractal(initGraphic, true);
    }


    public void ComputeFractalBoxes(string saveFileName, List<Box> boxesCpp)
    {
        // counting over cpp dll
        EliminateExtraZeroPowers();
        RoundDoubles();
        string fractalDefinition = Saver.Serialize<QuaternionFractal3D>(this);
        //string fractalDefinitionPath = Path.Combine(Persistent.Get3DFractalsDir(), "fractalDefinition.txt");
        //SaveDefinition(fractalDefinitionPath);

        int processorCount = Persistent.Instance.ProcessorCount;
        string returnedStr;
        string computedGraphicPath = saveFileName;
        int graphicBoxesCount;
        bool returnBoxes = true;

        ComputeFractalGraphicCpp(processorCount, fractalDefinition, computedGraphicPath,
            returnBoxes, out returnedStr, out graphicBoxesCount);
        GetGraphic().BoxesCount = graphicBoxesCount;

        if (boxesCpp == null)
        {
            boxesCpp = new List<Box>();
        }
        if (returnBoxes)
        {
            boxesCpp.Clear();
            Box box = new Box();
            box.size.x = box.size.y = MinCubeSize;
            foreach (var myString in returnedStr.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries))
            {
                float[] temp = myString.Split().Select(x => float.Parse(x, CultureInfo.InvariantCulture)).ToArray();
                box.position.x = temp[0];
                box.position.y = temp[1];
                box.position.z = temp[2];
                box.size.z = temp[3];
                boxesCpp.Add(box);
            }
        }
    }

    public string GetGraphicSaveFile(bool initGraphic)
    {
        string graphicExt = Persistent.GetExtension(Persistent.SaveExtension.fractal3DGraphicExtension);
        string computedGraphicPath;
        if (initGraphic)
        {
            computedGraphicPath = Path.Combine(Persistent.Get3DFractalsTempGraphicsDir(), GetComputationParametersHash() + "." + graphicExt);
        }
        else
        {
            // we don't want to store these temporary graphic files - next will overwrite previous
            computedGraphicPath = Path.Combine(Persistent.Get3DFractalsTempGraphicsDir(), "graphic." + graphicExt);
        }
        return computedGraphicPath;
    }

    public void ComputeFractal(bool initGraphic, bool getBoxes = true, List<Box> boxesCpp = null)
    {
        //float currentTime = UnityEngine.Time.realtimeSinceStartup;
        RoundDoubles();

#if CHECK_CUBES
        BoxContainer boxContainer = new BoxContainer(MinCubeSize);
        List<UnityEngine.Vector3> cubeCoordinates = new List<UnityEngine.Vector3>();
        ComputeFractal(cubeCoordinates, boxContainer);
#else
#if CHECK_BOXES
        BoxContainer boxContainer = new BoxContainer(MinCubeSize);
        ComputeFractal(boxContainer);
#endif
#endif
        //float time1 = UnityEngine.Time.realtimeSinceStartup - currentTime;
        //using (StreamWriter outputFile = new StreamWriter("C:/joy/TimeSharp.txt", true))
        //{
        //    outputFile.WriteLine(String.Format("C# time1:{0}", time1));
        //};

#if CHECK_CUBES
        CheckBoxesAndLines(cubeCoordinates, boxContainer);

        Saver.SaveCubeCoordinates("C:/joy/cubesSharp.txt", cubeCoordinates);
#endif

        GetGraphic().Clear(true, false);
        CreateNewGraphic();
        GetGraphic().SetColor(FractalColor);

        // saving of graphic
        //SaveGraphic(Saver.GetActual3DFractalsDir() + "graphic" + GetComputationParametersHash() + "." + Saver.GetExtension(Saver.SaveExtension.fractal3DGraphicExtension), boxes);

#if CHECK_BOXES
        //Saver.SaveBoxes("C:/joy/boxesSharp.txt", boxContainer.GetBoxes());
#endif
        //foreach (UnityEngine.Vector3 coordinate in cubeCoordinates)
        //{
        //    m_graphic.AddCube(coordinate, MinCubeSize);
        //}

        //GetGraphic().InitFromList(boxContainer.GetBoxes(), MinCubeSize);
        //GetGraphic().Save("C:/joy/graphic1.sav");


        // counting over cpp dll
        string fractalDefinitionPath = Path.Combine(Persistent.Get3DFractalsDir(), "fractalDefinition.txt");
        SaveDefinition(fractalDefinitionPath);

        //currentTime = UnityEngine.Time.realtimeSinceStartup;

        int processorCount = Persistent.Instance.ProcessorCount;
        string returnedStr;
        string computedGraphicPath = GetGraphicSaveFile(initGraphic);
        int graphicBoxesCount;
        bool returnBoxes = getBoxes;

        ComputeFractalGraphicCpp(processorCount, fractalDefinitionPath, computedGraphicPath, 
            returnBoxes, out returnedStr, out graphicBoxesCount);
        GetGraphic().BoxesCount = graphicBoxesCount;

        //float timeCpp = UnityEngine.Time.realtimeSinceStartup - currentTime;


#if CHECK_CUBES
        List<UnityEngine.Vector3> cubeCoordinatesCpp = new List<UnityEngine.Vector3>();
        cubeCoordinatesCpp = Saver.LoadCubeCoordinates("C:/joy/cubesCpp.txt");

        float time2 = UnityEngine.Time.realtimeSinceStartup - currentTime;

        using (StreamWriter outputFile = new StreamWriter("C:/joy/TimeSharp.txt", true))
        {
            outputFile.WriteLine(String.Format("C++ time:{0} after load time:{1}", timeCpp, time2));
        };

        CheckUtils.CheckCubeCoordinates(cubeCoordinates, cubeCoordinatesCpp);
#endif // #if CHECK_CUBES

        // parsing of returned string
        float timeParsing = UnityEngine.Time.realtimeSinceStartup;

        if (boxesCpp == null)
        {
            boxesCpp = new List<Box>();
        }
        if (returnBoxes)
        {
            boxesCpp.Clear();
            Box box = new Box();
            box.size.x = box.size.y = MinCubeSize;
            foreach (var myString in returnedStr.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries))
            {
                float[] temp = myString.Split().Select(x => float.Parse(x, CultureInfo.InvariantCulture)).ToArray();
                box.position.x = temp[0];
                box.position.y = temp[1];
                box.position.z = temp[2];
                box.size.z = temp[3];
                boxesCpp.Add(box);
            }
        }
        timeParsing = UnityEngine.Time.realtimeSinceStartup - timeParsing;

#if CHECK_BOXES
        // checking of boxes from C# and Cpp computation
        // sorting of Cpp boxes
        boxesCpp.Sort(Box.CompareBoxes);
        CheckUtils.CheckBoxes(boxContainer.GetBoxes(), boxesCpp);
#endif

        float timeInitGraphic = UnityEngine.Time.realtimeSinceStartup;
        if (initGraphic)
        {
            GetGraphic().InitFromList(boxesCpp, MinCubeSize);
            GetGraphic().ComputationHash = GetComputationParametersHash();
            GetGraphic().GraphicsHash = GetGraphicParametersHash();
#if CHECK_GRAPHIC
            Fractal3DGraphic graphicCpp = new Fractal3DGraphic();
            graphicCpp.Load(computedGraphicPath);

            CheckUtils.CheckGraphic(GetGraphic(), graphicCpp, boxesCpp);
            graphicCpp.Clear(true, false);
#endif
        }
        else
        {
            GetGraphic().Clear(true, true);
        }
        GetGraphic().SavePath = computedGraphicPath;
        
        timeInitGraphic = UnityEngine.Time.realtimeSinceStartup - timeInitGraphic;

        //float timeGraphicSave = UnityEngine.Time.realtimeSinceStartup;
        //GetGraphic().Save("C:/joy/graphicSharp.txt");
        //timeGraphicSave = UnityEngine.Time.realtimeSinceStartup - timeGraphicSave;

        //float timeGraphicLoad = UnityEngine.Time.realtimeSinceStartup;
        //Fractal3DGraphic otherGraphic = new Fractal3DGraphic();
        //otherGraphic.Load("C:/joy/graphicSharp.txt");
        //timeGraphicLoad = UnityEngine.Time.realtimeSinceStartup - timeGraphicLoad;
        //using (StreamWriter outputFile = new StreamWriter("C:/joy/TimeSharp.txt", true))
        //{
        //    //outputFile.WriteLine(String.Format("C++ counting:{0} C# parsing:{1} init graphic:{2} save graphic:{3} loadGraphic:{4}", timeCpp, timeParsing, timeInitGraphic, timeGraphicSave, timeGraphicLoad));
        //    outputFile.WriteLine(String.Format("C++ counting:{0} C# parsing:{1} init graphic:{2}", timeCpp, timeParsing, timeInitGraphic));
        //};

        float VBoxes = GetVolume(boxesCpp);
        float boxVolume = (float)Math.Pow(MinCubeSize, 3);
        GetGraphic().CubesCount = (int)(VBoxes / boxVolume);
        GetGraphic().LinesCount = boxesCpp.Count;
        //InsideCubesCount = GetInsideCoxesCount(boxesCpp);
    }

    public string GetUsedCPowers()
    {
        string result = "";
        for (int i=0; i < m_c.Count; i++)
        {
            if (!GetC(i).IsZero())
            {
                result = result + string.Format("{0},", i);
            }
        }
        if (result.Length > 0)
        {
            // remove last comma
            result = result.Remove(result.Length - 1);
        }
        return result;
    }
    public string GetUsedCAPowers()
    {
        string result = "";
        for (int i = 0; i < m_cA.Count; i++)
        {
            if (!GetCA(i).IsZero())
            {
                result = result + string.Format("{0},", i);
            }
        }
        if (result.Length > 0)
        {
            // remove last comma
            result = result.Remove(result.Length - 1);
        }
        return result;
    }
    public string GetUsedC0Powers()
    {
        string result = "";
        for (int i = 0; i < m_c0.Count; i++)
        {
            if (!GetC0(i).IsZero())
            {
                result = result + string.Format("{0},", i);
            }
        }
        if (result.Length > 0)
        {
            // remove last comma
            result = result.Remove(result.Length - 1);
        }
        return result;
    }

    public string GetUsedC0APowers()
    {
        string result = "";
        for (int i = 0; i < m_c0A.Count; i++)
        {
            if (!GetC0A(i).IsZero())
            {
                result = result + string.Format("{0},", i);
            }
        }
        if (result.Length > 0)
        {
            // remove last comma
            result = result.Remove(result.Length - 1);
        }
        return result;
    }

    public void RecomuteMinCubeSize()
    {
        ComputeMinCubeSize(CubeFineness);
    }

#if CHECK_CUBES
    public void ComputeFractal(List<UnityEngine.Vector3> result, BoxContainer boxContainer)
#else
    public void ComputeFractal(BoxContainer boxContainer)
#endif
    {
        RoundDoubles();
        RecomuteMinCubeSize();
        boxContainer.SetMinCubeSize(MinCubeSize);
        float step = MinCubeSize;
        int maxIterations = MaxComputationStepCount;

        double xMin = ComputationArea.GetMin(Axis3D.X);
        double yMin = ComputationArea.GetMin(Axis3D.Y);
        double zMin = ComputationArea.GetMin(Axis3D.Z);
        double xMax = ComputationArea.GetMax(Axis3D.X);
        double yMax = ComputationArea.GetMax(Axis3D.Y);
        double zMax = ComputationArea.GetMax(Axis3D.Z);

        EliminateExtraZeroPowers();

#if CHECK_CUBES
        UnityEngine.Vector3 coordinate = new UnityEngine.Vector3();
#endif

        int xMinCoef = (int)(xMin / step);
        int xMaxCoef = (int)(xMax / step);
        int yMinCoef = (int)(yMin / step);
        int yMaxCoef = (int)(yMax / step);
        int zMinCoef = (int)(zMin / step);
        int zMaxCoef = (int)(zMax / step);

        for (int xCoef = xMinCoef; xCoef <= xMaxCoef; xCoef++)
        {
            double x = xCoef * step;
            for (int yCoef = yMinCoef; yCoef<=yMaxCoef; yCoef++)
            {
                double y = yCoef * step;
                for (int zCoef = zMinCoef; zCoef <= zMaxCoef; zCoef++)
                {
                    double z = zCoef * step;
                    int iterations = m_computeDelegate(x, y, z, maxIterations);
                    if (iterations == maxIterations)
                    {
#if CHECK_CUBES
                        coordinate.x = (float)x;
                        coordinate.y = (float)y;
                        coordinate.z = (float)z;
                        result.Add(coordinate);
#endif
                        boxContainer.AddCube((float)x, (float)y, (float)z);
                    }
                }
            }
        }
    }

    public void SaveDefinition(string path)
    {
        if (path == null)
            return;
        EliminateExtraZeroPowers();
        RoundDoubles();
        Saver.Save(this, path);
    }

    public static bool Load(string path, out QuaternionFractal3D loadedFractal, bool loadGraphic)
    {
        bool ok = false;
        loadedFractal = null;
        if (path.Contains(Persistent.GetExtension(Persistent.SaveExtension.fractal3DZipExtension)))
        {
            // loaded from zip file
            // creation / clearing of extraction directory
            string extractionDir = Persistent.GetExtractDir();
            FileUtils.CreateOrClearDirectory(extractionDir);

            Compress.Unzip(path, extractionDir);

            string definitionFile = extractionDir + Persistent.GetFractalDefinitionFilename();

            ok = Saver.Load(definitionFile, out loadedFractal);
            if (ok)
            {
                // rounding of doubles
                loadedFractal.RoundDoubles();
                loadedFractal.EliminateExtraZeroPowers();
                // load of graphic
                if (loadGraphic)
                {
                    string graphicPath = extractionDir + Persistent.GetGraphicFilename();
                    if (File.Exists(graphicPath))
                    {
                        if (loadedFractal.GetGraphic().Load(graphicPath))
                        {
                            loadedFractal.GetGraphic().ComputationHash = loadedFractal.GetComputationParametersHash();
                            loadedFractal.GetGraphic().GraphicsHash = loadedFractal.GetGraphicParametersHash();
                            // copy of loaded graphic into temp graphics (for case of saving)
                            string tempGraphicPath = loadedFractal.GetTempGraphicPath(loadedFractal.GetGraphic().ComputationHash);
                            FileUtils.CopyIfNotExists(graphicPath, tempGraphicPath);
                        }
                    }
                }
            }
        }
        else
        {
            ok = Saver.Load(path, out loadedFractal);
        }
        if (ok)
        {
            loadedFractal.FilePath = path; // not loaded property
        }
        return ok;
    }

    public void SaveGraphic(string path, List<Box> boxes)
    {
        GetGraphic().Save(path, MinCubeSize, boxes);        
    }

    public void Save(string path, string graphicPath)
    {
        string packingDir = Persistent.GetPackingDir();
        FileUtils.CreateOrClearDirectory(packingDir);

        if (!File.Exists(graphicPath))
        {
            String message = String.Format("mising graphic file {0}", graphicPath);
            Debug.Assert(false, message);
        }
        else
        {
            // graphic was found - copy it into zip dir
            string zipGraphicPath = packingDir + Persistent.GetGraphicFilename();
            File.Copy(graphicPath, zipGraphicPath);
        }

        SaveDefinitionAndZipAll(path);
    }

    public void Save(string path)
    {
        // creation / clearing of pack directory
        string packingDir = Persistent.GetPackingDir();
        FileUtils.CreateOrClearDirectory(packingDir);

        EliminateExtraZeroPowers();

        // looking for appropriate graphic
        // check if exist graphic with the same hash as are current parametres
        string computationParametersHash = GetComputationParametersHash();
        string tempGraphicPath = GetTempGraphicPath(computationParametersHash);
        string zipGraphicPath = packingDir + Persistent.GetGraphicFilename();
        string computationParameters = GetComputationParameters();
        if (File.Exists(tempGraphicPath))
        {
            // appropriate saved graphic was found - copy it into zip dir
            File.Copy(tempGraphicPath, zipGraphicPath);
        }
        else
        if (GetGraphic().ComputationHash == computationParametersHash)
        {
            // saving of current graphic
            GetGraphic().Save(zipGraphicPath);
        }
        else
        {
            // graphic isn't computed - delete screenshots of previous graphics
            Persistent.DeleteQuaternionFractalScreenshots(path);
        }

        SaveDefinitionAndZipAll(path);
    }

    public string GetComputationParameters()
    {
        string str = "";
        PropertyInfo[] properties = typeof(QuaternionFractal3D).GetProperties();
        foreach (PropertyInfo property in properties)
        {
            if (Attribute.IsDefined(property, typeof(ComputationAttribute)))
            {
                string line = property.Name + ":";
                object obj = OtherUtils.GetPropValue(this, property.Name);
                if (obj is List<SmartMathLibrary.Quaternion>)
                {
                    line = obj.ToString();
                    List<SmartMathLibrary.Quaternion> list = (obj as List<SmartMathLibrary.Quaternion>);
                    foreach (var v in list)
                    {
                        line += v.ToString() + ",";
                    }
                    line += "\n";
                }
                else
                {
                    line += obj.ToString() + "\n";
                }
                str += line;
            }
        }
        return str;
    }

    public string GetGraphicParameters()
    {
        string str = "";
        PropertyInfo[] properties = typeof(QuaternionFractal3D).GetProperties();
        foreach (PropertyInfo property in properties)
        {
            if (Attribute.IsDefined(property, typeof(GraphicAttribute)))
            {
                string line = property.Name + ":";
                object obj = OtherUtils.GetPropValue(this, property.Name);
                line += obj.ToString() + "\n";
                str += line;
            }
        }
        return str;
    }

    public string GetFormula()
    {
        string result = "";
        result += "Formula:\n";
        switch (m_computeDelegateType)
        {
        case QuaternionDelegateType.Julia:
            result += GetFormulaForJulia();
            break;
        case QuaternionDelegateType.MixedMJ:
            if (m_coefficientBeforePower || m_coefficientAfterPower)
            {
                result += GetFormulaForJulia();
                if (m_coefficientC0BeforePower || m_coefficientC0BeforePower)
                {
                    result += GetFormulaForC0Combined();
                }
            }
            else
            {
                result += GetFormulaForC0();
            }
            break;
        }
        result += "\n";
        return result;
    }

    public string GetAllParameters()
    {
        string result = "";
        if (Name.Length > 0)
        {
            result += String.Format("Name: {0}\n", Name);
        }
        result += GetFormula();
        result += "\n";
        result += GetCoefficients();
        result += "\n";
        result += String.Format("Max. Computation Step: {0}\n", MaxComputationStepCount);
        result += String.Format("Min. Cube Size: {0}\n", MinCubeSize);
        result += String.Format("Computation Area: {0}\n", ComputationArea.ToString());
        result += String.Format("Limit Area: {0}\n", LimitArea.ToString());
        result += String.Format("Mapping Quat->3D: {0}\n", Vec2QuatMapping);
        return result;
    }

    public void ComputeMinCubeSize(Fineness fineness)
    {
        if ( fineness == Fineness.UserDefined)
        {
            // in that case this is defined by user - not computed
            return;
        }
        double volume = ComputationArea.GetVolume();
        float maxCubes = 10000000;
        double cubeVolume = volume / maxCubes;
        float cubeSize = (float)Math.Pow(cubeVolume, 1 / 3.0);
        // transform to number 1 - 2
        double log = Math.Log(cubeSize, 2);
        double power = Math.Truncate(log);
        if (log < 0)
        {
            power--;
        }
        //double rest = cubeSize / Math.Pow(2, power);
        power += (int)Fineness.ExtraHigh-(int)fineness;
        double finalSize = Math.Pow(2, power);
        MinCubeSize = (float)finalSize;
        return;
    }

    public int ComputeIterations(double x0, double iq, double jq, double kq, int maxIterations)
    {
        // returns 0 .. maxIterations
        switch(ComputeDelegateType)
        {
        case QuaternionDelegateType.Julia:
            return ComputeIterationsJulia(x0, iq, jq, kq, maxIterations);
        case QuaternionDelegateType.MixedMJ:
            return ComputeIterationsMixed(x0, iq, jq, kq, maxIterations);
        default:
            Debug.Assert(false, "Unhandled computation type");
            return 1;
        }
    }

    public int ComputeIterationsJulia(double x0, double iq, double jq, double kq, int maxIterations)
    {
        // returns 0 .. maxIterations
        // return number of iteration when number is outside of desired area (or maxIterations if number wasn't out for all iterations)
        //Quaternion oldPos = new Quaternion(x0, iq, jq, kq);
        Quaternion pos = new Quaternion(x0, iq, jq, kq);
        Quaternion newPos = new Quaternion();
        if (LimitArea.IsOut(pos, 0))
        {
            // is out already
            return 0;
        }
        int maxN = C.Count;
        int maxNA = CA.Count;
        if (CoefficientBeforePower && !CoefficientAfterPower)
        {
            for (int i = 0; i < maxIterations;)
            {
                newPos.Clear();
                for (int j = 0; j < maxN; j++)
                {
                    if (!C[j].IsZero()) // skipping of zero coefficients
                    {
                        //Quaternion pow = (pos ^ j);
                        //Quaternion add = (pos ^ j) * C[j];
                        //Quaternion add2 = C[j] * (pos ^ j);
                        //newPos += (pos ^ j) * C[j];
                        newPos += C[j] * (pos ^ j);
                    }
                }
                //pos = newPos;
                pos.CopyFrom(newPos);
                i++;
                if (LimitArea.IsOut(pos, i))
                {
                    return i;
                }
            }
        }
        else
        if (CoefficientAfterPower && !CoefficientBeforePower)
        {
            // coefficient after power
            for (int i = 0; i < maxIterations;)
            {
                newPos.Clear();
                for (int j = 0; j < maxNA; j++)
                {
                    if (!CA[j].IsZero()) // skipping of zero coefficients
                    {
                        newPos += (pos ^ j) * CA[j];
                    }
                }
                pos.CopyFrom(newPos);
                i++;
                if (LimitArea.IsOut(pos, i))
                {
                    return i;
                }
            }
        }
        else
        if (CoefficientAfterPower && CoefficientBeforePower)
        {
            // after and before
            Debug.Assert(CoefficientBeforePower && CoefficientAfterPower);
            int maxNX = Math.Max(maxN, maxNA);
            for (int i = 0; i < maxIterations;)
            {
                newPos.Clear();
                for (int j = 0; j < maxNX; j++)
                {
                    if (!GetC(j).IsZero()) // skipping of zero coefficients
                    {
                        if(!GetCA(j).IsZero())
                        {
                            newPos += C[j] * (pos ^ j) * CA[j];
                        }
                        else
                        { 
                            newPos += C[j] * (pos ^ j);
                        }
                    }
                    else
                    if (!GetCA(j).IsZero())
                    {
                        newPos += (pos ^ j) * CA[j];
                    }
                }
                pos.CopyFrom(newPos);
                i++;
                if (LimitArea.IsOut(pos, i))
                {
                    return i;
                }
            }
        }

        // number wasn't out from area for all iterations
        return maxIterations;
    }

    public static string GetFinenessString(Fineness fineness)
    {
        switch(fineness)
        {
        case Fineness.Low: return "Low";
        case Fineness.Normal: return "Normal";
        case Fineness.High: return "High";
        case Fineness.ExtraHigh: return "Extra High";
        case Fineness.UltraHigh: return "Ultra High";
        case Fineness.UserDefined: return "User Defined";
        default:
            Debug.Assert(false, "not handled fineness");
            break;
        }
        return "";
    }

    public static Fineness GetFineness(string finenessString)
    {
        for (int i = (int)Fineness.__Begin; i < (int)Fineness.__End; i++)
        {
            if (String.Compare(GetFinenessString((Fineness)i), finenessString) == 0)
            {
                return (Fineness)i;
            }
        }
        return Fineness.__End;
    }

    public UndoRedoList GetUndoRedoList()
    {
        return m_undoRedoList;
    }

    ////  public functions end ///////////////

    ///////////////// private functions ///////////////////

    string GetAllGraphicParameters()
    {
        return GetComputationParameters() + " " + GetGraphicParameters();
    }

    string GetFormulaForJulia()
    {
        string result = "";
        if (m_coefficientBeforePower && !m_coefficientAfterPower)
        {
            result = "Q(i+1) = SUM(j)( C(j) * (Q(i) ^ j) )";
        }
        else
        if (m_coefficientAfterPower && !m_coefficientBeforePower)
        {
            result = "Q(i+1) = SUM(j)( (Q(i) ^ j) * CA(j) )";
        }
        else
        if (m_coefficientAfterPower && m_coefficientBeforePower)
        {
            result = "Q(i+1) = SUM(j)( C(j) * (Q(i) ^ j) * CA(j)";
        }
        return result;
    }
    string GetFormulaForC0()
    {
        string result = "";
        if (m_coefficientC0BeforePower && !m_coefficientC0AfterPower)
        {
            result = "Q = SUM(j)( C0(j) * (Q(0) ^ j) )";
        }
        else
        if (m_coefficientC0AfterPower && !m_coefficientC0BeforePower)
        {
            result = "Q = SUM( (Q(0) ^ j) * C0A(j) )";
        }
        else
        if (m_coefficientC0AfterPower && m_coefficientC0BeforePower)
        {
            result = "Q = SUM( C0(j) * (Q(0) ^ j) * C0A(j)";
        }
        return result;
    }
    string GetFormulaForC0Combined()
    {
        string result = "";
        if (m_coefficientC0BeforePower && !m_coefficientC0AfterPower)
        {
            result = " + SUM(j)( C0(j) * (Q(0) ^ j) )";
        }
        else
        if (m_coefficientC0AfterPower && !m_coefficientC0BeforePower)
        {
            result = " + SUM(j)( (Q(0) ^ j) * C0A(j) )";
        }
        else
        if (m_coefficientC0AfterPower && m_coefficientC0BeforePower)
        {
            result = " + SUM(j)( C0(j) * (Q(0) ^ j) * C0A(j)";
        }
        return result;
    }

    private string GetCoefficients()
    {
        string result = "";
        if (m_coefficientBeforePower)
        {
            for (int i = 0; i < C.Count; i++)
            {
                if (!C[i].IsZero())
                {
                    result += String.Format("C[{0}]: ", i);
                    result += C[i].ToString();
                    result += "\n";
                }
            }
        }
        if (m_coefficientAfterPower)
        {
            for (int i = 0; i < CA.Count; i++)
            {
                if (!CA[i].IsZero())
                {
                    result += String.Format("CA[{0}]: ", i);
                    result += CA[i].ToString();
                    result += "\n";
                }
            }
        }
        if (m_coefficientC0BeforePower)
        {
            for (int i = 0; i < C0.Count; i++)
            {
                if (!C0[i].IsZero())
                {
                    result += String.Format("C0[{0}]: ", i);
                    result += C0[i].ToString();
                    result += "\n";
                }
            }
        }
        if (m_coefficientC0AfterPower)
        {
            for (int i = 0; i < C0A.Count; i++)
            {
                if (!C0A[i].IsZero())
                {
                    result += String.Format("C0A[{0}]: ", i);
                    result += C0A[i].ToString();
                    result += "\n";
                }
            }
        }
        return result;
    }

    struct AxisMinMax
    {
        public AxisQuat axis;
        public bool min;
    }
    Quaternion GetLimitQuaternion(AxisMinMax[] minMaxes)
    {
        Quaternion res = new Quaternion();
        for (int i = 0; i < minMaxes.Length; i++)
        {
            AxisQuat axis = minMaxes[i].axis;
            double value = minMaxes[i].min ? LimitArea.GetMin(axis) : LimitArea.GetMax(axis);
            res.SetCoordinate(axis, value);
        }
        return res;
    }

    double GetMinMax(Axis3D axis, bool max)
    {
        double extrem = max ? -1e9 : 1e9;
        AxisMinMax[] axisMinMax = new AxisMinMax[4];
        axisMinMax[0].axis = AxisQuat.X0;
        axisMinMax[1].axis = AxisQuat.I;
        axisMinMax[2].axis = AxisQuat.J;
        axisMinMax[3].axis = AxisQuat.K;
        for (int x0 = 0; x0 < 2; x0++)
        {
            axisMinMax[0].min = x0 == 0;
            for (int i = 0; i < 2; i++)
            {
                axisMinMax[1].min = i == 0;
                for (int j = 0; j < 2; j++)
                {
                    axisMinMax[2].min = j == 0;
                    for (int k = 0; k < 2; k++)
                    {
                        axisMinMax[3].min = k == 0;
                        Quaternion quat = GetLimitQuaternion(axisMinMax);
                        Vector3D vec = Vec2QuatMapping.toVector(quat);
                        switch (axis)
                        {
                        case Axis3D.X:
                            {
                                if (max)
                                {
                                    if (vec.x > extrem)
                                    {
                                        extrem = vec.x;
                                    }
                                }
                                else
                                {
                                    if (vec.x < extrem)
                                    {
                                        extrem = vec.x;
                                    }
                                }
                            }
                            break;
                        case Axis3D.Y:
                            if (max)
                            {
                                if (vec.y > extrem)
                                {
                                    extrem = vec.y;
                                }
                            }
                            else
                            {
                                if (vec.y < extrem)
                                {
                                    extrem = vec.y;
                                }
                            }
                            break;
                        case Axis3D.Z:
                            if (max)
                            {
                                if (vec.z > extrem)
                                {
                                    extrem = vec.z;
                                }
                            }
                            else
                            {
                                if (vec.z < extrem)
                                {
                                    extrem = vec.z;
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }
        return extrem;
    }

    string GetTempGraphicPath(string computationParametersHash)
    {
        return Path.Combine(Persistent.Get3DFractalsTempGraphicsDir(), computationParametersHash + "." + Persistent.GetExtension(Persistent.SaveExtension.fractal3DGraphicExtension));
    }

    void SaveDefinitionAndZipAll(string path)
    {
        string packingDir = Persistent.GetPackingDir();

        // copy of files into pack directory
        string definitionPath = packingDir + Persistent.GetFractalDefinitionFilename();
        SaveDefinition(definitionPath);

        // packing
        string[] files = Directory.GetFiles(packingDir);
        Compress.Zip(files, path);

        // saving of last file
        Persistent.Instance.Seleced3DFractalFile = path;

        FilePath = path;
    }

    void RoundDoubles()
    {
        // rounding of doubles before computation, Save and after Load
        foreach (Quaternion q in C)
        {
            q.RoundDoubles();
        }
        foreach (Quaternion q in CA)
        {
            q.RoundDoubles();
        }
        foreach (Quaternion q in C0)
        {
            q.RoundDoubles();
        }
        foreach (Quaternion q in C0A)
        {
            q.RoundDoubles();
        }
        LimitArea.RoundDoubles();
    }

    int ComputeJuliaMapped(double x, double y, double z, int maxIterations)
    {
        Vector3D vector = new Vector3D(x, y, z);
        Quaternion quat = Vec2QuatMapping.toQuaternion(vector);
        return ComputeIterationsJulia(quat.X0, quat.I, quat.J, quat.K, maxIterations);
    }

    int ComputeMixedMapped(double x, double y, double z, int maxIterations)
    {
        Vector3D vector = new Vector3D(x, y, z);
        Quaternion quat = Vec2QuatMapping.toQuaternion(vector);
        return ComputeIterationsMixed(quat.X0, quat.I, quat.J, quat.K, maxIterations);
    }

    int ComputeIterationsMixed(double x0, double iq, double jq, double kq, int maxIterations)
    {
        // returns 0 .. maxIterations
        // return number of iteration when number is outside of desired area (or maxIterations if number wasn't out for all iterations)
        Quaternion pos0 = new Quaternion(x0, iq, jq, kq);
        Quaternion pos = new Quaternion(x0, iq, jq, kq);
        Quaternion newPos = new Quaternion();
        if (LimitArea.IsOut(pos, 0))
        {
            // is out already
            return 0;
        }
        int maxN = C.Count;
        int maxNA = CA.Count;
        int maxNC0 = C0.Count;
        int maxNC0A = C0A.Count;
        Quaternion addC0 = new Quaternion();
        if (m_coefficientC0AfterPower || m_coefficientC0BeforePower)
        {
            if (m_coefficientC0BeforePower && !m_coefficientC0AfterPower)
            {
                for (int j = 0; j < maxNC0; j++)
                {
                    if (!C0[j].IsZero()) // skipping of zero coefficients
                    {
                        addC0 += C0[j] * (pos0 ^ j);
                    }
                }
            }
            else if (m_coefficientC0AfterPower && !m_coefficientC0BeforePower)
            {
                for (int j = 0; j < maxNC0A; j++)
                {
                    if (!C0A[j].IsZero()) // skipping of zero coefficients
                    {
                        addC0 += (pos0 ^ j) * C0A[j];
                    }
                }
            }
            else if (m_coefficientC0AfterPower && m_coefficientC0BeforePower)
            {
                int maxNX = (int)Math.Max(C0A.Count, C0.Count);
                for (int j = 0; j < maxNX; j++)
                {
                    if (!GetC0(j).IsZero()) // skipping of zero coefficients
                    {
                        if (!GetC0A(j).IsZero())
                        {
                            addC0 += C0[j] * (pos0 ^ j) * C0A[j];
                        }
                        else
                        {
                            addC0 += C0[j] * (pos0 ^ j);
                        }
                    }
                    else
                    {
                        if (!GetC0A(j).IsZero()) // skipping of zero coefficients
                        {
                            addC0 += (pos0 ^ j) * C0A[j];
                        }
                    }
                }
            }
        }

        if (CoefficientBeforePower && !CoefficientAfterPower)
        {
            for (int i = 0; i < maxIterations;)
            {
                newPos.Clear();
                for (int j = 0; j < maxN; j++)
                {
                    if (!C[j].IsZero()) // skipping of zero coefficients
                    {
                        //Quaternion pow = (pos ^ j);
                        //Quaternion add = (pos ^ j) * C[j];
                        //Quaternion add2 = C[j] * (pos ^ j);
                        //newPos += (pos ^ j) * C[j];
                        newPos += C[j] * (pos ^ j);
                    }
                }
                newPos += addC0;
                pos.CopyFrom(newPos);
                i++;
                if (LimitArea.IsOut(pos, i))
                {
                    return i;
                }
            }
        }
        else
        if (CoefficientAfterPower && !CoefficientBeforePower)
        {
            // coefficient after power
            for (int i = 0; i < maxIterations;)
            {
                newPos.Clear();
                for (int j = 0; j < maxNA; j++)
                {
                    if (!CA[j].IsZero()) // skipping of zero coefficients
                    {
                        newPos += (pos ^ j) * CA[j];
                    }
                }
                newPos += addC0;
                pos.CopyFrom(newPos);
                i++;
                if (LimitArea.IsOut(pos, i))
                {
                    return i;
                }
            }
        }
        else
        if (CoefficientAfterPower && CoefficientBeforePower)
        {
            // after and before
            Debug.Assert(CoefficientBeforePower && CoefficientAfterPower);
            int maxNX = Math.Max(maxN, maxNA);
            for (int i = 0; i < maxIterations;)
            {
                newPos.Clear();
                for (int j = 0; j < maxNX; j++)
                {
                    if (!GetC(j).IsZero()) // skipping of zero coefficients
                    {
                        if (!GetCA(j).IsZero())
                        {
                            newPos += C[j] * (pos ^ j) * CA[j];
                        }
                        else
                        {
                            newPos += C[j] * (pos ^ j);
                        }
                    }
                    else
                    if (!GetCA(j).IsZero())
                    {
                        newPos += (pos ^ j) * CA[j];
                    }
                }
                newPos += addC0;
                pos.CopyFrom(newPos);
                i++;
                if (LimitArea.IsOut(pos, i))
                {
                    return i;
                }
            }
        }

        // number wasn't out from area for all iterations
        return maxIterations;
    }  

    void EliminateExtraZeroPowers()
    {
        while (C.Count > 0 && C[C.Count - 1] == Quaternion.Zero)
        {
            C.RemoveAt(C.Count - 1);
        }
        while (CA.Count > 0 && CA[CA.Count - 1] == Quaternion.Zero)
        {
            CA.RemoveAt(CA.Count - 1);
        }
        while (C0.Count > 0 && C0[C0.Count - 1] == Quaternion.Zero)
        {
            C0.RemoveAt(C0.Count - 1);
        }
        while (C0A.Count > 0 && C0A[C0A.Count - 1] == Quaternion.Zero)
        {
            C0A.RemoveAt(C0A.Count - 1);
        }
    }

    public string GetComputationParametersHash()
    {
        string parameters = GetComputationParameters();
        return Saver.GetHashString(parameters);
    }

    public string GetGraphicParametersHash()
    {
        string parameters = GetGraphicParameters();
        return Saver.GetHashString(parameters);
    }

    public float GetVolume(List<Box> boxes)
    {
        float volume = 0;
        foreach(Box box in boxes)
        {
            volume += box.size.x * box.size.y * box.size.z;
        }
        return volume;
    }
    private double GetVolumeD(List<Box> boxes)
    {
        double volume = 0;
        foreach (Box box in boxes)
        {
            volume += ((double)box.size.x) * ((double)box.size.y) * ((double)box.size.z);
        }
        return volume;
    }

    private void CheckBoxesAndLines(List<UnityEngine.Vector3> cubeCoordinates, BoxContainer boxContainer)
    {
        List<Box> boxes = new List<Box>();
        CubesConnecting.MakeZLines(cubeCoordinates, MinCubeSize, boxes);
        float VCube = (float)Math.Pow(MinCubeSize, 3);
        float VCubes = cubeCoordinates.Count * VCube;
        float VBoxes = GetVolume(boxes);
        List<Box> boxesDirect = boxContainer.GetBoxes();
        float VBoxesDirect = GetVolume(boxesDirect);
        // comparison of volumes
        UnityEngine.Debug.Assert(Math.Abs(VCubes - VBoxes) < VCube / 2);
        UnityEngine.Debug.Assert(Math.Abs(VBoxes - VBoxesDirect) < VCube / 2);
        // full comparison
        for (int i = 0; i < Math.Min(boxes.Count, boxesDirect.Count); i++)
        {
            UnityEngine.Debug.Assert(boxes[i] == boxesDirect[i]);
        }
    }



    private int GetInsideCoxesCount(List<Box> boxes)
    {
        int insideBoxesCount = 0;
        Box.MinCubeSize = MinCubeSize;

        int xMin = ConvertCoordinateToInt((float)ComputationArea.GetMin(Axis3D.X));
        int yMin = ConvertCoordinateToInt((float)ComputationArea.GetMin(Axis3D.Y));
        //int zMin = ConvertCoordinateToInt(ComputationArea.GetMin(Axis3D.Z));
        int xMax = ConvertCoordinateToInt((float)ComputationArea.GetMax(Axis3D.X));
        int yMax = ConvertCoordinateToInt((float)ComputationArea.GetMax(Axis3D.Y));
        //int zMax = ConvertCoordinateToInt(ComputationArea.GetMax(Axis3D.Z));

        for (int x = xMin + 1; x < xMax - 1; x++)
        {
            for (int y = yMin + 1; y < yMax - 1; y++)
            {
                List<UnityEngine.Vector3> cubes = GetCubesForXY(boxes, x, y);
                if (cubes.Count == 0)
                {
                    continue;
                }
                List<UnityEngine.Vector3> cubes1 = GetCubesForXY(boxes, x, y - 1);
                if (cubes1.Count == 0)
                {
                    continue;
                }
                List<UnityEngine.Vector3> cubes2 = GetCubesForXY(boxes, x, y + 1);
                if (cubes2.Count == 0)
                {
                    continue;
                }
                List<UnityEngine.Vector3> cubes3 = GetCubesForXY(boxes, x - 1, y);
                if (cubes3.Count == 0)
                {
                    continue;
                }
                List<UnityEngine.Vector3> cubes4 = GetCubesForXY(boxes, x + 1, y);
                if (cubes4.Count == 0)
                {
                    continue;
                }

                foreach (UnityEngine.Vector3 cube in cubes)
                {
                    if (ExistCubeWithZ(cubes1, cube.z) &&
                        ExistCubeWithZ(cubes2, cube.z) &&
                        ExistCubeWithZ(cubes3, cube.z) &&
                        ExistCubeWithZ(cubes4, cube.z))
                    {
                        insideBoxesCount++;
                    }
                }
            }
        }
        return insideBoxesCount;
    }
    bool ExistCubeWithZ(List<UnityEngine.Vector3> cubes, float z)
    {
        foreach(var cube in cubes)
        {
            if (cube.z == z)
            {
                return true;
            }
        }
        return false;
    }
    private int ConvertCoordinateToInt(float coordinate)
    {
        return (int)Math.Round(coordinate / MinCubeSize);
    }
    private float ConvertCoordinateToFloat(int intCoordinate)
    {
        return intCoordinate * MinCubeSize;
    }
    private bool equals(float x, float x1, float tolerance)
    {
        return (Math.Abs(x - x1) < tolerance);
    }
    List<UnityEngine.Vector3> GetCubesForXY(List<Box> boxes, int x, int y)
    {
        List<UnityEngine.Vector3> cubes = new List<UnityEngine.Vector3>();
        float xFloatCoordinate = ConvertCoordinateToFloat(x);
        float yFloatCoordinate = ConvertCoordinateToFloat(y);
        float tolerance = MinCubeSize / 2;
        foreach(Box box in boxes)
        {
            if (equals(box.position.x, xFloatCoordinate, tolerance))
            {
                if (equals(box.position.y, yFloatCoordinate, tolerance))
                {
                    List<UnityEngine.Vector3> newCubes = box.GetCubes();
                    foreach (var cube in newCubes)
                        cubes.Add(cube);
                }
            }
            //if (box.position.x > xFloatCoordinate)
            //{
            //    break;
            //}
        }
        return cubes;
    }
    /////////////////// private member variables /////////////////////
    private IColorPalette m_colorPalette;
    private UnityEngine.Color m_fractalColor;
    private UnityEngine.Color m_fractalBackgroundColor;
    ColoringAlgorithm m_sliceColoringAlgorithm;
    IterationIndex m_sliceIterationIndex = new IterationIndex();
    private string m_name = string.Format("");
    private string m_fileName = string.Format("");  // file with fractal (complete path)
    private Fractal3DGraphic m_graphic;
    private ComputeIterationsDelegate m_computeDelegate;  // function used for computation of fractal
    private QuaternionDelegateType m_computeDelegateType;
    private Fineness m_fineness;  // fineness of computed cubes - high fineness = small cubes 
    private int m_maxComputationStep = 25;
    private float m_minCubeSize = 0.05f;
    private bool m_coefficientBeforePower = true;
    private bool m_coefficientAfterPower = false;
    private bool m_coefficientC0BeforePower = false;
    private bool m_coefficientC0AfterPower = false;
    QuaternionAreaBase m_limitArea = new CircularQuaternionArea(Quaternion.Zero, 2);
    RectangularArea3D m_computationArea;
    List<Quaternion> m_c = new List<Quaternion>();  // coeffifient used before power of actual position
    List<Quaternion> m_cA = new List<Quaternion>(); // coefficient used after power of actual position
    List<Quaternion> m_c0 = new List<Quaternion>();
    List<Quaternion> m_c0A = new List<Quaternion>();
    Vector2QuaternionMapping m_vec2QuatMapping = new Vector2QuaternionMapping();
    private UndoRedoList m_undoRedoList;
    int m_powerCindex;
    int m_powerC0index;
    int m_limitAreaType;
}
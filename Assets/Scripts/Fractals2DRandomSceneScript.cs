﻿using Assets.Scripts.Computation;
using Assets.Scripts.Utils;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Fractals2DRandomSceneScript : MonoBehaviour {

    public enum UsedPowersType
    {
        __Begin,
        MaxPlusOthers = __Begin,
        MaxIsMaximum,
        UserDefined,
        __End
    }

    public enum UsedComplexPartsType
    {
        __Begin,
        RealOrImaginary = __Begin,
        RealAndImaginary,
        Random,
        __End
    }

    public enum UsedAlgorithmType
    {
        __Begin,
        Julia = __Begin,
        Mixed,
        Random,
        __End
    }

    public enum UsedPaletteType
    {
        __Begin,
        Random = __Begin,
        Selected,
        __End
    }

    public void OnButtonCompute()
    {
        // validation
        string error = ValidateRandomComputationParameters();
        if (error.Length > 0)
        {
            MessagePanel.Message(error);
            return;
        }
        GenerateRandomFractals();
    }

    public static void GenerateRandomFractals()
    {
        int n = Persistent.Instance.RandomComputation2DCount;
        string resultDir = Persistent.GetRandom2DFractalDir();
        for (int i = 0; i < n; i++)
        {
            string fractalMask = "*." + Persistent.GetExtension(Persistent.SaveExtension.fractal2DDefinitionExtension);
            string fractalPath = FileUtils.GetMaxNumberFilename(resultDir, fractalMask, Persistent.GetExtension(Persistent.SaveExtension.fractal2DDefinitionExtension));
            ComplexFractal2D fractal = ComputationTask.Compute2DRandomFractal(fractalPath);
        }
    }

    public void OnButtonDeleteRandomFractals()
    {
        FileUtils.CreateOrClearDirectory(Persistent.GetRandom2DFractalDir());
    }

    string ValidateRandomComputationParameters()
    {
        string error = "";
        if (Persistent.Instance.RandomComputation2DCount <= 0)
        {
            return "Number of computed fractals has to be positive";
        }
        if (Persistent.Instance.RandomComputationUsed2DPowersType == UsedPowersType.UserDefined)
        {
            Regex regex = new Regex(@"^([0-9]([ \t\r\n\v\f]?,[ \t\r\n\v\f]?))+[0-9]+$");
            Match match = regex.Match(Persistent.Instance.RandomComputationUsedPowers);
            if (!match.Success)
            {
                return "Used power string should be in form of power numbers separated by commas - like '0,2'";
            }
        }
        else
        {
            if (Persistent.Instance.RandomComputation2DMaxPower <= 1)
            {
                return "Highest power should be higher than one";
            }
        }

        return error;
    }

    public void OnButtonBack()
    {
        // validation
        string error = ValidateRandomComputationParameters();
        if (error.Length > 0)
        {
            MessagePanel.Message(error);
            return;
        }

        if (Persistent.Instance.Random2DSettingsWithoutGenerate)
        {
            // settings mode 
            MySceneManager.Instance.PopScene();
            return;
        }

        // computation mode
        if (MySceneManager.GoToPreviousScene() == SceneManager.GetActiveScene())
        {
            // no previous scene
            MySceneManager.GoToScene("2D Fractals");
        }
    }

    // Use this for initialization
    void Start () {
        m_inputUsedPowers = GameObject.Find("InputField Used Powers");
        m_inputHighestPower = GameObject.Find("InputField Highest Power");
        m_buttonSelectPalette = GameObject.Find("Button Select Palette");
        m_rawimageSelectedPalette = GameObject.Find("RawImage Selected Palette").GetComponent<RawImage>();

        m_usedPowersType2string[UsedPowersType.MaxIsMaximum] = "Highest is max";
        m_usedPowersType2string[UsedPowersType.MaxPlusOthers] = "Highest + others";
        m_usedPowersType2string[UsedPowersType.UserDefined] = "User Defined";

        m_usedPartsType2string[UsedComplexPartsType.RealOrImaginary] = "Real or Imaginary";
        m_usedPartsType2string[UsedComplexPartsType.RealAndImaginary] = "Real and Imaginary";
        m_usedPartsType2string[UsedComplexPartsType.Random] = "Random";

        m_algoritmType2String[UsedAlgorithmType.Julia] = "Julia";
        m_algoritmType2String[UsedAlgorithmType.Mixed] = "Mixed";
        m_algoritmType2String[UsedAlgorithmType.Random] = "Random";

        m_paletteType2String[UsedPaletteType.Random] = "Random";
        m_paletteType2String[UsedPaletteType.Selected] = "Selected";

        List<string> newOptions = new List<string>();
        newOptions.Clear();
        for (int i = (int)UsedAlgorithmType.__Begin; i < (int)UsedAlgorithmType.__End; i++)
        {
            newOptions.Add(m_algoritmType2String[(UsedAlgorithmType)i]);
        }
        m_dropdownAlgorithmType = GameObject.Find("Dropdown Algorithm").GetComponent<Dropdown>();
        m_dropdownAlgorithmType.ClearOptions();
        m_dropdownAlgorithmType.AddOptions(newOptions);

        newOptions.Clear();
        for (int i = (int)UsedPaletteType.__Begin; i < (int)UsedPaletteType.__End; i++)
        {
            newOptions.Add(m_paletteType2String[(UsedPaletteType)i]);
        }
        m_dropdownPaletteType = GameObject.Find("Dropdown Palette Selection").GetComponent<Dropdown>();
        m_dropdownPaletteType.ClearOptions();
        m_dropdownPaletteType.AddOptions(newOptions);

        newOptions.Clear();
        for (int i = (int)UsedPowersType.__Begin; i < (int)UsedPowersType.__End; i++)
        {
            newOptions.Add(m_usedPowersType2string[(UsedPowersType)i]);
        }
        m_dropdownUsedPowers = GameObject.Find("Dropdown Power Selection").GetComponent<Dropdown>();
        m_dropdownUsedPowers.ClearOptions();
        m_dropdownUsedPowers.AddOptions(newOptions);

        newOptions.Clear();
        for (int i = (int)UsedComplexPartsType.__Begin; i < (int)UsedComplexPartsType.__End; i++)
        {
            newOptions.Add(m_usedPartsType2string[(UsedComplexPartsType)i]);
        }
        m_dropdownUsedParts = GameObject.Find("Dropdown Used Parts").GetComponent<Dropdown>();
        m_dropdownUsedParts.ClearOptions();
        m_dropdownUsedParts.AddOptions(newOptions);

        InitRandom();
        UpdatePaletteImage();

        if (Persistent.Instance.Random2DSettingsWithoutGenerate)
        {
            GameObject.Find("Button Compute").SetActive(false);
            GameObject.Find("InputField Number").SetActive(false);
            GameObject.Find("Button Delete Random Fractgals").SetActive(false);
        }
    }

    void InitRandom()
    {
        GameObject panel = GameObject.Find("Panel Random");
        new IntWidget(new Ref<int>(() => Persistent.Instance.RandomComputation2DCount, v => { Persistent.Instance.RandomComputation2DCount = v; }), UIUtils.FindInputFieldInChildren(panel, "InputField Number"));
        new StringWidget(new Ref<string>(() => Persistent.Instance.RandomComputationUsed2DPowers, v => { Persistent.Instance.RandomComputationUsed2DPowers = v; }), UIUtils.FindInputFieldInChildren(panel, "InputField Used Powers"));
        new IntWidget(new Ref<int>(() => Persistent.Instance.RandomComputation2DMaxPower, v => { Persistent.Instance.RandomComputation2DMaxPower = v; }), UIUtils.FindInputFieldInChildren(panel, "InputField Highest Power"));

        new EnumWidget(new Ref<int>(() => (int)Persistent.Instance.RandomComputationUsed2DPowersType, v => { Persistent.Instance.RandomComputationUsed2DPowersType = (UsedPowersType)v; }), m_dropdownUsedPowers, SetUsedPowersInput);
        new EnumWidget(new Ref<int>(() => (int)Persistent.Instance.RandomComputationUsedComplexPartsType, v => { Persistent.Instance.RandomComputationUsedComplexPartsType = (UsedComplexPartsType)v; }), m_dropdownUsedParts, SetDisplayedParts);
        new EnumWidget(new Ref<int>(() => (int)Persistent.Instance.RandomComputation2DUsedAlgorithmType, v => { Persistent.Instance.RandomComputation2DUsedAlgorithmType = (UsedAlgorithmType)v; }), m_dropdownAlgorithmType);
        new EnumWidget(new Ref<int>(() => (int)Persistent.Instance.RandomComputation2DUsedPaletteType, v => { Persistent.Instance.RandomComputation2DUsedPaletteType = (UsedPaletteType)v; }), m_dropdownPaletteType).OnChangeDelegate += UpdatePaletteImage;

        SetUsedPowersInput((int)Persistent.Instance.RandomComputationUsed2DPowersType);
    }

    public void OnButtonSelectPalette()
    {
        Persistent.Instance.SelectedPaletteDelegate = (IColorPalette palette) =>
        {
            Persistent.Instance.SelectedPalette = palette;
            UpdatePaletteImage();
        };

        MySceneManager.GoToScene("PalettePicking", LoadSceneMode.Additive);
    }

    void UpdatePaletteImage()
    {
        bool selectedPalette = Persistent.Instance.RandomComputation2DUsedPaletteType == UsedPaletteType.Selected;
        m_buttonSelectPalette.SetActive(selectedPalette);
        m_rawimageSelectedPalette.gameObject.SetActive(selectedPalette);
        if (selectedPalette)
        {
            // fill palette image
            int textureSizeX = Mathf.FloorToInt(m_rawimageSelectedPalette.rectTransform.sizeDelta.x);
            int textureSizeY = Mathf.FloorToInt(m_rawimageSelectedPalette.rectTransform.sizeDelta.y);
            Texture2D t1 = new Texture2D(textureSizeX, textureSizeY);
            TextureUtils.WritePaletteIntoTexture(t1, Persistent.Instance.SelectedPalette, 
                new Rect(0, 0, textureSizeX, textureSizeY));
            m_rawimageSelectedPalette.texture = t1;
        }
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void OnButtonSetToDefaultValues()
    {
        Persistent.Instance.RandomComputation2DCount = 5;
        Persistent.Instance.RandomComputationUsed2DPowersType = Fractals2DRandomSceneScript.UsedPowersType.MaxIsMaximum;
        Persistent.Instance.RandomComputationUsedComplexPartsType = Fractals2DRandomSceneScript.UsedComplexPartsType.Random;
        Persistent.Instance.RandomComputationUsed2DPowers = "2,0";
        Persistent.Instance.RandomComputation2DMaxPower = 4;
        InitRandom();
    }

    public void SetUsedPowersInput(int newType)
    {
        GameObject panel = GameObject.Find("Panel Random");
        if (Persistent.Instance.RandomComputationUsed2DPowersType == UsedPowersType.UserDefined)
        {
            m_inputUsedPowers.SetActive(true);
            m_inputHighestPower.SetActive(false);
            // setting of powers
        }
        else
        {
            m_inputUsedPowers.SetActive(false);
            m_inputHighestPower.SetActive(true);
        }
    }

    public void SetDisplayedParts(int number)
    {
    }

    GameObject m_inputUsedPowers;
    GameObject m_inputHighestPower;
    GameObject m_buttonSelectPalette;
    RawImage m_rawimageSelectedPalette;

    Dropdown m_dropdownAlgorithmType;
    Dropdown m_dropdownPaletteType;
    Dropdown m_dropdownUsedPowers;
    Dropdown m_dropdownUsedParts;
    Dictionary<UsedPowersType, string> m_usedPowersType2string = new Dictionary<UsedPowersType, string>();
    Dictionary<UsedComplexPartsType, string> m_usedPartsType2string = new Dictionary<UsedComplexPartsType, string>();
    Dictionary<UsedAlgorithmType, string> m_algoritmType2String = new Dictionary<UsedAlgorithmType, string>();
    Dictionary<UsedPaletteType, string> m_paletteType2String = new Dictionary<UsedPaletteType, string>();
}

﻿using UnityEngine;

public class Fractals2DSceneScript : MonoBehaviour
{

    public void OnButton2DComplexFractals()
    {
        Persistent.Instance.ComputeImmediatelyRandom2D = false;
        MySceneManager.GoToScene("Complex2D");
    }

    public void OnButtonSelection()
    {
        Persistent.Instance.SelectedFractalType = FractalSelectionSceneScript.FractalType.Complex2D;
        MySceneManager.GoToScene("FractalSelection");
    }

    public void OnButtonRandomFractals()
    {
        Persistent.Instance.Random2DSettingsWithoutGenerate = false;
        MySceneManager.GoToScene("2D Random Fractals");
    }

    public void OnButtonMainMenu()
    {
        MySceneManager.GoToScene("MainMenu");
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
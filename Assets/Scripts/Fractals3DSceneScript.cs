﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Computation;
using Assets.Scripts.Utils;
using System.IO;
using System.Collections.Generic;
using UnityEngine.UI;
using OxOD;
using System.Text.RegularExpressions;

public class Fractals3DSceneScript : MonoBehaviour {

    [Header("OxOD Reference")]
    public FileDialog fileDialog;

    enum EMenuPanelType
    {
        Main,
        Explore,
        Random
    }

    public enum UsedPowersType
    {
        __Begin,
        MaxPlusOthers = __Begin,
        MaxIsMaximum,
        UserDefined,
        __End
    }

    public enum UsedQuaternionPartsType
    {
        __Begin,
        Random = __Begin,
        DefinedMinMax,
        __End
    }

    public enum UsedAlgorithmType
    {
        __Begin,
        Julia = __Begin,
        Mixed,
        Random,
        __End
    }

    public enum QuaternionMappingType
    {
        X0_I_J,
        I_J_K,
        RandomBasicDirections,
        RandomDirections,
    }

    public void OnClicked3DQuaternionFractals()
    {
        MySceneManager.GoToScene("Quaternion3D");
    }

    public void OnButtonRandomPanel()
    {
        ActivatePanel(EMenuPanelType.Random);
    }

    public void OnButtonRandomBack()
    {
        ActivatePanel(EMenuPanelType.Main);
    }

    public void OnButtonRandomSetToDefault()
    {
        Persistent.Instance.RandomComputation3DCount = 5;
        Persistent.Instance.RandomComputationFineness = QuaternionFractal3D.Fineness.High;
        Persistent.Instance.RandomComputationUsedPowersType = Fractals3DSceneScript.UsedPowersType.MaxIsMaximum;
        Persistent.Instance.RandomComputationUsedQuaternionPartsType = Fractals3DSceneScript.UsedQuaternionPartsType.Random;
        Persistent.Instance.RandomComputationUsedPowers = "2,0";
        Persistent.Instance.RandomComputationMinParts = 1;
        Persistent.Instance.RandomComputationMaxParts = 4;
        Persistent.Instance.RandomComputation3DMaxPower = 4;
        InitRandom();
    }

    public void OnButtonRandomDeleteAllRandomFractals()
    {
        FileUtils.CreateOrClearDirectory(Persistent.GetRandom3DFractalDir());
    }

    string ValidateRandomComputationParameters()
    {
        string error = "";
        if (Persistent.Instance.RandomComputation3DCount <= 0)
        {
            return "Number of computed fractals has to be positive";
        }
        if (Persistent.Instance.RandomComputationUsedPowersType == UsedPowersType.UserDefined)
        {
            Regex regex = new Regex(@"^([0-9]([ \t\r\n\v\f]?,[ \t\r\n\v\f]?))+[0-9]+$");
            Match match = regex.Match(Persistent.Instance.RandomComputationUsedPowers);
            if (!match.Success)
            {
                return "Used power string should be in form of power numbers separated by commas - like '0,2'";
            }
        }
        else
        {
            if (Persistent.Instance.RandomComputation3DMaxPower <= 1)
            {
                return "Highest power should be higher than one";
            }
        }

        if (Persistent.Instance.RandomComputationUsedQuaternionPartsType == UsedQuaternionPartsType.DefinedMinMax)
        {
            if (Persistent.Instance.RandomComputationMinParts <= 0)
            {
                return "Min parts should be positive";
            }
            if (Persistent.Instance.RandomComputationMinParts > 4)
            {
                return "Min parts should be less that 5";
            }
            if (Persistent.Instance.RandomComputationMaxParts < Persistent.Instance.RandomComputationMinParts)
            {
                return "Max parts have to be bigger or equal as min parts";
            }
            if (Persistent.Instance.RandomComputationMaxParts > 4)
            {
                return "Max parts should be less that 5";
            }
            if (Persistent.Instance.RandomComputationMaxParts <= 0)
            {
                return "Max parts should be positive";
            }
        }
        return error;
    }

    public void OnButtonRandomCompute()
    {
        // validation
        string error = ValidateRandomComputationParameters();
        if (error.Length > 0)
        {
            MessagePanel.Message(error);
            return;
        }
        GenerateRandomFractals();
    }

    public static void GenerateRandomFractals()
    {
        int n = Persistent.Instance.RandomComputation3DCount;
        string resultDir = Persistent.GetRandom3DFractalDir();
        float startTime = UnityEngine.Time.realtimeSinceStartup;
        for (int i = 0; i < n; i++)
        {
            string fractalMask = "*." + Persistent.GetExtension(Persistent.SaveExtension.fractal3DZipExtension);
            string fractalPath = FileUtils.GetMaxNumberFilename(resultDir, fractalMask, Persistent.GetExtension(Persistent.SaveExtension.fractal3DZipExtension));
            QuaternionFractal3D fractal = ComputationTask.ComputeRandomFractal(fractalPath);
            Quaternion3DSceneScript.MakeScreenshots(fractal);
            fractal.GetGraphic().Clear(true, false);
        }
        float timeDiff = UnityEngine.Time.realtimeSinceStartup - startTime;
        StreamWriter file = File.AppendText("C:/Joy/ComputationTime.txt");
        file.Write(string.Format("time: {0}\r\n", timeDiff));
        file.Close();

    }

    public void OnButtonSelection()
    {
        Persistent.Instance.SelectedFractalType = FractalSelectionSceneScript.FractalType.Quaternion3D;
        MySceneManager.GoToScene("FractalSelection");
    }

    public void OnButtonMainMenu()
    {
        MySceneManager.GoToScene("MainMenu");
    }

    public void OnButton2DSlices()
    {
        Persistent.Instance.QuaternionFractal3D = null;
        MySceneManager.GoToScene("Quat3DSlices");
    }

    QuaternionFractal3D.Fineness GetSelectedFineness()
    {
        int value = m_dropdownFineness.value;
        string finenessString = m_dropdownFineness.options[value].text;
        return QuaternionFractal3D.GetFineness(finenessString);
    }

    public void OnButtonComputeNeighbours()
    {
        // compute neighbourhood
        string fractalPath = m_inputSelectedFractal.GetComponent<InputField>().text;//Persistent.Instance.Seleced3DFractalFile;
        if (!File.Exists(fractalPath))
        {
            MessagePanel.Message("Fractal file doesn't exist");
            return;
        }

        QuaternionFractal3D.Fineness selectedFineness = GetSelectedFineness();

        //string fractalName = Path.GetFileName(fractalPath);
        string fractalNameWithoutExtension = Path.GetFileNameWithoutExtension(fractalPath);
        string resultDir = Path.Combine(Persistent.Get3DFractalsDir(), fractalNameWithoutExtension);
        FileUtils.CreateDirectoryIfNotExist(resultDir);
        QuaternionFractal3D fractal;
        QuaternionFractal3D.Load(fractalPath, out fractal, loadGraphic: false);
        foreach (Pair<string, QuaternionFractal3D> fractalModifiedPair in ComputationTask.GetNeigbourhood(fractal, resultDir))
        {
            string descriptionOfmodification = fractalModifiedPair.first;
            QuaternionFractal3D fractalModified = fractalModifiedPair.second;

            ComputationTask.ComputeFractalDouble(fractalModified, selectedFineness);
            //fractalModified.ComputeFractal(true);
            string mask = descriptionOfmodification + "*";
            fractalModified.Save(ComputationTask.GetUniqueFileName(mask, resultDir));
            Quaternion3DSceneScript.MakeScreenshots(fractalModified);
            fractalModified.GetGraphic().Clear(true, false);
        }

        ActivatePanel(EMenuPanelType.Main);
    }

    public void OnButtonSelectSource()
    {
        string extension = Persistent.GetExtensionWithDot(Persistent.SaveExtension.fractal3DZipExtension);
        string lastSaved = Persistent.Instance.Seleced3DFractalFile;
        string lastSavedDirectory = Path.GetDirectoryName(lastSaved);

        fileDialog.onResultDelegate = (string path) =>
        {
            if (path != null)
            {
                m_inputSelectedFractal.GetComponent<InputField>().text = path;
            }
        };
        StartCoroutine(fileDialog.Open(lastSavedDirectory, extension, "Select 3D fractal"));
    }

    public void OnButtonExplore()
    {
        ActivatePanel(EMenuPanelType.Explore);
    }

    public void OnButtonExploreBack()
    {
        ActivatePanel(EMenuPanelType.Main);
    }

    // Use this for initialization
    void Start () {

        Init();

        ActivatePanel(EMenuPanelType.Main);
    }

    // Update is called once per frame
    void Update () {
        //if (Input.GetKeyDown(KeyCode.F))
        //{
        //    Screen.fullScreen = !Screen.fullScreen;
        //}
    }

    void Init()
    {
        if (m_inited)
        {
            return;
        }

        m_camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        m_camera.farClipPlane = 50;//zxc Constants.FarClippingPlane;
        m_camera.nearClipPlane = Constants.NearClippingPlane;

        m_usedPowersType2string[UsedPowersType.MaxIsMaximum] = "Highest is max";
        m_usedPowersType2string[UsedPowersType.MaxPlusOthers] = "Highest + others";
        m_usedPowersType2string[UsedPowersType.UserDefined] = "User Defined";

        m_usedPartsType2string[UsedQuaternionPartsType.Random] = "Random";
        m_usedPartsType2string[UsedQuaternionPartsType.DefinedMinMax] = "Defined Min, Max";

        m_algoritmType2String[UsedAlgorithmType.Julia] = "Julia";
        m_algoritmType2String[UsedAlgorithmType.Mixed] = "Mixed";
        m_algoritmType2String[UsedAlgorithmType.Random] = "Random";

        m_inputUsedPowers = GameObject.Find("InputField Used Powers");
        m_inputHighestPower = GameObject.Find("InputField Highest Power");
        m_minParts = GameObject.Find("InputField Min Parts");
        m_maxParts = GameObject.Find("InputField Max Parts");

        List<string> newOptions = new List<string>();
        newOptions.Clear();
        for (int i = (int)QuaternionFractal3D.Fineness.__Begin; i <= (int)QuaternionFractal3D.Fineness.UltraHigh; i++)
        {
            newOptions.Add(QuaternionFractal3D.GetFinenessString((QuaternionFractal3D.Fineness)i));
        }
        m_dropdownFinenessOnRandomPanel = GameObject.Find("Dropdown Fineness").GetComponent<Dropdown>();
        m_dropdownFinenessOnRandomPanel.ClearOptions();
        m_dropdownFinenessOnRandomPanel.AddOptions(newOptions);

        newOptions.Clear();
        for (int i = (int)UsedAlgorithmType.__Begin; i < (int)UsedAlgorithmType.__End; i++)
        {
            newOptions.Add(m_algoritmType2String[(UsedAlgorithmType)i]);
        }
        m_dropdownAlgorithmType = GameObject.Find("Dropdown Algorithm").GetComponent<Dropdown>();
        m_dropdownAlgorithmType.ClearOptions();
        m_dropdownAlgorithmType.AddOptions(newOptions);

        newOptions.Clear();
        for (int i = (int)UsedPowersType.__Begin; i < (int)UsedPowersType.__End; i++)
        {
            newOptions.Add(m_usedPowersType2string[(UsedPowersType)i]);
        }        
        m_dropdownUsedPowers = GameObject.Find("Dropdown Power Selection").GetComponent<Dropdown>();
        m_dropdownUsedPowers.ClearOptions();
        m_dropdownUsedPowers.AddOptions(newOptions);

        m_coefficientDependencyType = GameObject.Find("Dropdown Before After Relation").GetComponent<Dropdown>();
        m_dropdownQuaternionMappingType = GameObject.Find("Dropdown Quaternion Mapping Type").GetComponent<Dropdown>();

        newOptions.Clear();
        for (int i = (int)UsedQuaternionPartsType.__Begin; i < (int)UsedQuaternionPartsType.__End; i++)
        {
            newOptions.Add(m_usedPartsType2string[(UsedQuaternionPartsType)i]);
        }
        m_dropdownUsedParts = GameObject.Find("Dropdown Used Parts").GetComponent<Dropdown>();
        m_dropdownUsedParts.ClearOptions();
        m_dropdownUsedParts.AddOptions(newOptions);

        m_inputSelectedFractal = GameObject.Find("InputField Selected Fractal");
        m_dropdownFineness = GameObject.Find("Dropdown Fineness Type").GetComponent<Dropdown>();
        m_menuPanels[EMenuPanelType.Main] = GameObject.Find("Panel Main");
        m_menuPanels[EMenuPanelType.Explore] = GameObject.Find("Panel Explore");
        m_menuPanels[EMenuPanelType.Random] = GameObject.Find("Panel Random");

        m_dropdownFineness.ClearOptions();
        newOptions.Clear();
        for (int i = (int)QuaternionFractal3D.Fineness.__Begin; i <= (int)QuaternionFractal3D.Fineness.UltraHigh; i++)
        {
            newOptions.Add(QuaternionFractal3D.GetFinenessString((QuaternionFractal3D.Fineness)i));
        }
        m_dropdownFineness.AddOptions(newOptions);
        m_dropdownFineness.value = (int)QuaternionFractal3D.Fineness.High;

        m_inited = true;
    }

    void InitRandom()
    {
        GameObject panel = GameObject.Find("Panel Random");
        new IntWidget(new Ref<int>(() => Persistent.Instance.RandomComputation3DCount, v => { Persistent.Instance.RandomComputation3DCount = v; }), UIUtils.FindInputFieldInChildren(panel, "InputField Number"));
        new IntWidget(new Ref<int>(() => Persistent.Instance.RandomComputationMinParts, v => { Persistent.Instance.RandomComputationMinParts = v; }), UIUtils.FindInputFieldInChildren(panel, "InputField Min Parts"));
        new IntWidget(new Ref<int>(() => Persistent.Instance.RandomComputationMaxParts, v => { Persistent.Instance.RandomComputationMaxParts = v; }), UIUtils.FindInputFieldInChildren(panel, "InputField Max Parts"));
        new StringWidget(new Ref<string>(() => Persistent.Instance.RandomComputationUsedPowers, v => { Persistent.Instance.RandomComputationUsedPowers = v; }), UIUtils.FindInputFieldInChildren(panel, "InputField Used Powers"));
        new IntWidget(new Ref<int>(() => Persistent.Instance.RandomComputation3DMaxPower, v => { Persistent.Instance.RandomComputation3DMaxPower = v; }), UIUtils.FindInputFieldInChildren(panel, "InputField Highest Power"));

        new EnumWidget(new Ref<int>(() => (int)Persistent.Instance.RandomComputationFineness, v => { Persistent.Instance.RandomComputationFineness = (QuaternionFractal3D.Fineness)v; }), m_dropdownFinenessOnRandomPanel);
        new EnumWidget(new Ref<int>(() => (int)Persistent.Instance.RandomComputationUsedPowersType, v => { Persistent.Instance.RandomComputationUsedPowersType = (UsedPowersType)v; }), m_dropdownUsedPowers, this.SetUsedPowersInput);
        new EnumWidget(new Ref<int>(() => (int)Persistent.Instance.RandomComputationUsedQuaternionPartsType, v => { Persistent.Instance.RandomComputationUsedQuaternionPartsType = (UsedQuaternionPartsType)v; }), m_dropdownUsedParts, SetDisplayedParts);
        new EnumWidget(new Ref<int>(() => (int)Persistent.Instance.RandomComputationUsedAlgorithmType, v => { Persistent.Instance.RandomComputationUsedAlgorithmType = (UsedAlgorithmType)v; }), m_dropdownAlgorithmType);
        new EnumWidget(new Ref<int>(() => (int)Persistent.Instance.RandomCoefficientsDependenceType, v => { Persistent.Instance.RandomCoefficientsDependenceType = (Persistent.CoefficientsDependencyType)v; }), m_coefficientDependencyType);
        new EnumWidget(new Ref<int>(() => (int)Persistent.Instance.RandomQuaternionMappingType, v => { Persistent.Instance.RandomQuaternionMappingType = (QuaternionMappingType)v; }), m_dropdownQuaternionMappingType);

        SetUsedPowersInput(0);
        SetDisplayedParts(0);
    }

    public void SetUsedPowersInput(int newType)
    {
        GameObject panel = GameObject.Find("Panel Random");
        if (Persistent.Instance.RandomComputationUsedPowersType == UsedPowersType.UserDefined)
        {
            m_inputUsedPowers.SetActive(true);
            m_inputHighestPower.SetActive(false);
            // setting of powers
        }
        else
        {
            m_inputUsedPowers.SetActive(false);
            m_inputHighestPower.SetActive(true);
        }
    }

    public void SetDisplayedParts(int number)
    {
        GameObject panel = GameObject.Find("Panel Random");
        bool showMinMaxParts = Persistent.Instance.RandomComputationUsedQuaternionPartsType == UsedQuaternionPartsType.DefinedMinMax;
        m_minParts.SetActive(showMinMaxParts);
        m_maxParts.SetActive(showMinMaxParts);
        if (showMinMaxParts)
        {
        }
    }

    void ActivatePanel(EMenuPanelType panelType)
    {
        Init();
        GameObject panel = null;
        foreach (var panelIter in m_menuPanels)
        {
            bool active = panelType == panelIter.Key;
            panelIter.Value.SetActive(active);
            if (active)
            {
                panel = panelIter.Value;
            }
        }
        switch (panelType)
        {
        case EMenuPanelType.Explore:
            {
                GameObject pathEdit = GameObject.Find("InputField Selected Fractal");
                pathEdit.GetComponent<InputField>().text = Persistent.Instance.Seleced3DFractalFile;
            }
            break;
        case EMenuPanelType.Random:
            {
                InitRandom();
            }
            break;
        }
    }

    Camera m_camera;
    Dictionary<EMenuPanelType, GameObject> m_menuPanels = new Dictionary<EMenuPanelType, GameObject>();
    GameObject m_inputSelectedFractal;
    GameObject m_inputUsedPowers;
    GameObject m_inputHighestPower;
    GameObject m_minParts;
    GameObject m_maxParts;
    Dropdown m_dropdownAlgorithmType;
    Dropdown m_coefficientDependencyType;
    Dropdown m_dropdownQuaternionMappingType;
    Dropdown m_dropdownUsedPowers;
    Dropdown m_dropdownUsedParts;
    Dropdown m_dropdownFineness;
    Dropdown m_dropdownFinenessOnRandomPanel;
    bool m_inited = false;
    Dictionary<UsedPowersType, string> m_usedPowersType2string = new Dictionary<UsedPowersType, string>();
    Dictionary<UsedQuaternionPartsType, string> m_usedPartsType2string = new Dictionary<UsedQuaternionPartsType, string>();
    Dictionary<UsedAlgorithmType, string> m_algoritmType2String = new Dictionary<UsedAlgorithmType, string>();
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Graphic
{
    public class AxisArrows
    {
        public AxisArrows(Vector3 position, float size)
        {
            Init(null, position, size, size, size);
        }
        public AxisArrows(Vector3 position, Vector3 sizeVector)
        {
            Init(null, position, sizeVector.x, sizeVector.y, sizeVector.z);
        }
        public AxisArrows(GameObject parent, Vector3 localPosition, Vector3 sizeVector)
        {
            Init(parent, localPosition, sizeVector.x, sizeVector.y, sizeVector.z);
        }
        public AxisArrows(Vector3 position, float sizeX, float sizeY, float sizeZ)
        {
            Init(null, position, sizeX, sizeY, sizeZ);
        }

        public GameObject GetBaseObject()
        {
            return m_baseObject;
        }

        private void Init(GameObject parent, Vector3 localPosition, float sizeX, float sizeY, float sizeZ)
        {
            m_baseObject = new UnityEngine.GameObject();
            m_baseObject.name = "Axis Arrows";
            if (parent != null)
            {
                m_baseObject.transform.parent = parent.transform;
            }
            m_baseObject.transform.localPosition = localPosition;
            m_baseObject.transform.localRotation = UnityEngine.Quaternion.Euler(0, 0, 0);

            float slimnessCoefficient = 0.05f;
            m_axisX = GameObject.CreatePrimitive(PrimitiveType.Cube);
            m_axisX.name = "Axis X";
            m_axisX.transform.parent = m_baseObject.transform;
            m_axisX.transform.localPosition = Coord.Transform(new Vector3(sizeX / 2, 0, 0));
            m_axisX.transform.localScale = new Vector3(sizeX, slimnessCoefficient * sizeX, slimnessCoefficient * sizeX);
            m_axisX.transform.localRotation = UnityEngine.Quaternion.Euler(0, 0, 0);
            m_axisX.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            m_axisX.GetComponent<MeshRenderer>().receiveShadows = false;

            Shader shader = Shader.Find("Legacy Shaders/Diffuse");// "Transparent/Diffuse");
            Material material = new Material(shader);
            material.color = Color.red;
            m_axisX.GetComponent<Renderer>().material = material;

            m_axisY = GameObject.CreatePrimitive(PrimitiveType.Cube);
            m_axisY.name = "Axis Y";
            m_axisY.transform.parent = m_baseObject.transform;
            m_axisY.transform.localPosition = Coord.Transform(new Vector3(0, sizeY / 2, 0));
            m_axisY.transform.localScale = new Vector3(slimnessCoefficient * sizeY, sizeY, slimnessCoefficient * sizeY);
            m_axisY.transform.localRotation = UnityEngine.Quaternion.Euler(0, 0, 0);
            m_axisY.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            m_axisY.GetComponent<MeshRenderer>().receiveShadows = false;

            material = new Material(shader);
            material.color = Color.green;
            m_axisY.GetComponent<Renderer>().material = material;

            m_axisZ = GameObject.CreatePrimitive(PrimitiveType.Cube);
            m_axisZ.name = "Axis Z";
            m_axisZ.transform.parent = m_baseObject.transform;
            m_axisZ.transform.localPosition = Coord.Transform(new Vector3(0, 0, sizeZ / 2));
            m_axisZ.transform.localScale = new Vector3(slimnessCoefficient * sizeZ, slimnessCoefficient * sizeZ, sizeZ);
            m_axisZ.transform.localRotation = UnityEngine.Quaternion.Euler(0, 0, 0);
            m_axisZ.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            m_axisZ.GetComponent<MeshRenderer>().receiveShadows = false;

            material = new Material(shader);
            material.color = Color.blue;
            m_axisZ.GetComponent<Renderer>().material = material;
        }

        GameObject m_baseObject;
        GameObject m_axisX;
        GameObject m_axisY;
        GameObject m_axisZ;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Graphic
{
    public class BoundingBoxObject
    {
        public BoundingBoxObject(GameObject parent, Vector3 localPosition, Area3DBase limitArea)
        {
            m_baseObject = new UnityEngine.GameObject();
            m_baseObject.name = "Computation area";

            if (parent != null)
            {
                m_baseObject.transform.parent = parent.transform;
            }
            //m_baseObject.transform.localPosition = localPosition;
            //m_baseObject.transform.localRotation = UnityEngine.Quaternion.Euler(0, 0, 0);

            GameObject line;

            line = CreateBoundingBoxLine(m_baseObject,
                Math.Vec3.Create(limitArea.HalfSizeX, limitArea.HalfSizeY, limitArea.HalfSizeZ),
                Math.Vec3.Create(-limitArea.HalfSizeX, limitArea.HalfSizeY, limitArea.HalfSizeZ));
            line.transform.parent = m_baseObject.transform;

            line = CreateBoundingBoxLine(m_baseObject,
                Math.Vec3.Create(-limitArea.HalfSizeX, limitArea.HalfSizeY, -limitArea.HalfSizeZ),
                Math.Vec3.Create(limitArea.HalfSizeX, limitArea.HalfSizeY, -limitArea.HalfSizeZ));
            line.transform.parent = m_baseObject.transform;

            line = CreateBoundingBoxLine(m_baseObject,
                Math.Vec3.Create(limitArea.HalfSizeX, -limitArea.HalfSizeY, limitArea.HalfSizeZ),
                Math.Vec3.Create(-limitArea.HalfSizeX, -limitArea.HalfSizeY, limitArea.HalfSizeZ));
            line.transform.parent = m_baseObject.transform;

            line = CreateBoundingBoxLine(m_baseObject,
                Math.Vec3.Create(-limitArea.HalfSizeX, -limitArea.HalfSizeY, -limitArea.HalfSizeZ),
                Math.Vec3.Create(limitArea.HalfSizeX, -limitArea.HalfSizeY, -limitArea.HalfSizeZ));
            line.transform.parent = m_baseObject.transform;
            
            line = CreateBoundingBoxLine(m_baseObject,
                Math.Vec3.Create(limitArea.HalfSizeX, limitArea.HalfSizeY, limitArea.HalfSizeZ),
                Math.Vec3.Create(limitArea.HalfSizeX, -limitArea.HalfSizeY, limitArea.HalfSizeZ));
            line.transform.parent = m_baseObject.transform;

            line = CreateBoundingBoxLine(m_baseObject,
                Math.Vec3.Create(limitArea.HalfSizeX, limitArea.HalfSizeY, -limitArea.HalfSizeZ),
                Math.Vec3.Create(limitArea.HalfSizeX, -limitArea.HalfSizeY, -limitArea.HalfSizeZ));
            line.transform.parent = m_baseObject.transform;

            line = CreateBoundingBoxLine(m_baseObject,
                Math.Vec3.Create(-limitArea.HalfSizeX, limitArea.HalfSizeY, limitArea.HalfSizeZ),
                Math.Vec3.Create(-limitArea.HalfSizeX, -limitArea.HalfSizeY, limitArea.HalfSizeZ));
            line.transform.parent = m_baseObject.transform;

            line = CreateBoundingBoxLine(m_baseObject,
                Math.Vec3.Create(-limitArea.HalfSizeX, limitArea.HalfSizeY, -limitArea.HalfSizeZ),
                Math.Vec3.Create(-limitArea.HalfSizeX, -limitArea.HalfSizeY, -limitArea.HalfSizeZ));
            line.transform.parent = m_baseObject.transform;

            line = CreateBoundingBoxLine(m_baseObject,
                Math.Vec3.Create(limitArea.HalfSizeX, limitArea.HalfSizeY, limitArea.HalfSizeZ),
                Math.Vec3.Create(limitArea.HalfSizeX, limitArea.HalfSizeY, -limitArea.HalfSizeZ));
            line.transform.parent = m_baseObject.transform;

            line = CreateBoundingBoxLine(m_baseObject,
                Math.Vec3.Create(-limitArea.HalfSizeX, limitArea.HalfSizeY, limitArea.HalfSizeZ),
                Math.Vec3.Create(-limitArea.HalfSizeX, limitArea.HalfSizeY, -limitArea.HalfSizeZ));
            line.transform.parent = m_baseObject.transform;

            line = CreateBoundingBoxLine(m_baseObject,
                Math.Vec3.Create(limitArea.HalfSizeX, -limitArea.HalfSizeY, limitArea.HalfSizeZ),
                Math.Vec3.Create(limitArea.HalfSizeX, -limitArea.HalfSizeY, -limitArea.HalfSizeZ));
            line.transform.parent = m_baseObject.transform;

            line = CreateBoundingBoxLine(m_baseObject,
                Math.Vec3.Create(-limitArea.HalfSizeX, -limitArea.HalfSizeY, limitArea.HalfSizeZ),
                Math.Vec3.Create(-limitArea.HalfSizeX, -limitArea.HalfSizeY, -limitArea.HalfSizeZ));
            line.transform.parent = m_baseObject.transform;

            //Math.Vec3.Create(-limitArea.HalfSizeX, -limitArea.HalfSizeY, -limitArea.HalfSizeZ),
            new AxisArrows(m_baseObject, Coord.Transform(Math.Vec3.Create(-limitArea.HalfSizeX, -limitArea.HalfSizeY, -limitArea.HalfSizeZ)),
                Math.Vec3.Create(limitArea.HalfSizeX, limitArea.HalfSizeY, limitArea.HalfSizeZ));
            //arrows.GetBaseObject().transform.parent = m_baseObject.transform;
        }                           

        GameObject CreateBoundingBoxLine(GameObject parent, Vector3 from, Vector3 to)
        {
            float slimnessCoefficient = 0.01f;
            GameObject line = GameObject.CreatePrimitive(PrimitiveType.Cube);

            if (parent != null)
            {
                line.transform.parent = parent.transform;
            }
            //m_baseObject.transform.localPosition = localPosition;
            line.transform.localRotation = UnityEngine.Quaternion.Euler(0, 0, 0);

            if (from.x != to.x)
            {
                float sizeX = System.Math.Abs(to.x - from.x);
                line.transform.localPosition = Coord.Transform(Math.Vec3.Create((from.x+to.x)/2, from.y, from.z));
                line.transform.localScale = Math.Vec3.Create(sizeX, slimnessCoefficient * sizeX, slimnessCoefficient * sizeX);
            }
            if (from.y != to.y)
            {
                float sizeY = System.Math.Abs(to.y - from.y);
                line.transform.localPosition = Coord.Transform(Math.Vec3.Create(from.x, (from.y + to.y) / 2, from.z));
                line.transform.localScale = Math.Vec3.Create(slimnessCoefficient * sizeY, sizeY, slimnessCoefficient * sizeY);
            }
            if (from.z != to.z)
            {
                float sizeZ = System.Math.Abs(to.z - from.z);
                line.transform.localPosition = Coord.Transform(Math.Vec3.Create(from.x, from.y, (from.z + to.z) / 2));
                line.transform.localScale = Math.Vec3.Create(slimnessCoefficient * sizeZ, slimnessCoefficient * sizeZ, sizeZ);
            }

            Shader shader = Shader.Find("Legacy Shaders/Diffuse");// "Transparent/Diffuse");
            Material material = new Material(shader);
            material.color = Color.white;
            line.GetComponent<Renderer>().material = material;
            line.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            line.GetComponent<MeshRenderer>().receiveShadows = false;

            return line;
        }

        public GameObject GetBaseObject()
        {
            return m_baseObject;
        }

        GameObject m_baseObject;
    }
}

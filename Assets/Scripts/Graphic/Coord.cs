﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Graphic
{
    public static class Coord
    {
        public static Vector3 Transform(Vector3 computationCoordinate)
        {
            // transforms vector in computation coordinate into display coordinate - Unity is left handed system so
            // for righthanded system we should switch some axe
            return m_rightHanded ? new Vector3(computationCoordinate.x, computationCoordinate.y, -computationCoordinate.z) : computationCoordinate;
        }
        public static Bounds Transform(Bounds bounds)
        {
            // transform display bounds into computation bounds (or vice versa)
            return m_rightHanded ? new Bounds(Transform(bounds.center), bounds.size) : bounds;
        }
        public static Vector3 TransformedOne()
        {
            return m_rightHanded ? Transform(Vector3.one) : Vector3.one;
        }
        public static void SetRightHanded(bool rightHanded)
        {
            m_rightHanded = rightHanded;
        }
        private static bool m_rightHanded;
    }
}

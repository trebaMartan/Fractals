﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Graphic
{
    public class LightPointersObject
    {
        public LightPointersObject(GameObject parent, Vector3 localPosition, Area3DBase limitArea)
        {
            GameObject directionalLight = GameObject.Find("Directional Light");
            m_baseObject = new UnityEngine.GameObject();
            m_baseObject.name = "Light pointers";

            if (parent != null)
            {
                m_baseObject.transform.parent = parent.transform;
            }

            GameObject line;
            Vector3 size = limitArea.GetSize().Vector3;
            size *=  0.2f;
            float length = System.Math.Min(size.x, size.y);
            length = System.Math.Min(length, size.z);
            Vector3 position;
            Vector3 add = directionalLight.transform.TransformDirection(Math.Vec3.Create(0, 0, length/2));
            position = Math.Vec3.Create(-limitArea.HalfSizeX, -limitArea.HalfSizeY, -limitArea.HalfSizeZ);
            position += add;
            line = CreatePointerLine(m_baseObject, position, length, directionalLight.transform);

            position = Math.Vec3.Create(-limitArea.HalfSizeX, -limitArea.HalfSizeY, limitArea.HalfSizeZ);
            position += add;
            line = CreatePointerLine(m_baseObject, position, length, directionalLight.transform);

            position = Math.Vec3.Create(-limitArea.HalfSizeX, limitArea.HalfSizeY, -limitArea.HalfSizeZ);
            position += add;
            line = CreatePointerLine(m_baseObject, position, length, directionalLight.transform);

            position = Math.Vec3.Create(-limitArea.HalfSizeX, limitArea.HalfSizeY, limitArea.HalfSizeZ);
            position += add;
            line = CreatePointerLine(m_baseObject, position, length, directionalLight.transform);

            position = Math.Vec3.Create(limitArea.HalfSizeX, -limitArea.HalfSizeY, -limitArea.HalfSizeZ);
            position += add;
            line = CreatePointerLine(m_baseObject, position, length, directionalLight.transform);

            position = Math.Vec3.Create(limitArea.HalfSizeX, -limitArea.HalfSizeY, limitArea.HalfSizeZ);
            position += add;
            line = CreatePointerLine(m_baseObject, position, length, directionalLight.transform);

            position = Math.Vec3.Create(limitArea.HalfSizeX, limitArea.HalfSizeY, -limitArea.HalfSizeZ);
            position += add;
            line = CreatePointerLine(m_baseObject, position, length, directionalLight.transform);

            position = Math.Vec3.Create(limitArea.HalfSizeX, limitArea.HalfSizeY, limitArea.HalfSizeZ);
            position += add;
            line = CreatePointerLine(m_baseObject, position, length, directionalLight.transform);
        }

        GameObject CreatePointerLine(GameObject parent, Vector3 from,float length, Transform transform)
        {
            float slimnessCoefficient = 0.07f;
            GameObject line = GameObject.CreatePrimitive(PrimitiveType.Cube);
            if (parent != null)
            {
                line.transform.parent = parent.transform;
            }
            line.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            line.transform.localPosition = from;
            line.transform.localScale = Math.Vec3.Create(slimnessCoefficient * length, slimnessCoefficient * length, length);
            line.transform.localRotation = transform.localRotation;

            return line;
        }

        public GameObject GetBaseObject()
        {
            return m_baseObject;
        }

        GameObject m_baseObject;
    }
}

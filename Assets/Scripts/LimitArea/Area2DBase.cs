﻿using Assets.Scripts.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public abstract class Area2DBase : Observable
{
    public abstract bool IsOut(double x, double y, int iteration);
    public abstract Area2DBase Clone();
    public abstract bool Equals(Area2DBase other);
}

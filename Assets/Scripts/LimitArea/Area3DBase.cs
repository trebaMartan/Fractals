﻿using Assets.Scripts.Math;
using Assets.Scripts.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public enum Axis3D
{
    X,
    Y,
    Z
};

public abstract class Area3DBase : Observable
{
    public abstract bool IsOut(double x, double y, double z, int iteration);
    public abstract double GetMin(Axis3D axis);
    public abstract double GetMax(Axis3D axis);
    public double GetMinMax(Axis3D axis, bool min) { return min ? GetMin(axis) : GetMax(axis); }
    public Vector3D GetMinVector()
    {
        return new Vector3D(GetMin(Axis3D.X), GetMin(Axis3D.Y), GetMin(Axis3D.Z));
    }
    public Vector3D GetCenter()
    {
        return new Vector3D(CenterX, CenterY, CenterZ);
    }
    public override string ToString()
    {
        string res = base.ToString();
        res += " - x: " + m_xCenter.ToString();
        res += " y: " + m_yCenter.ToString();
        res += " z :" + m_zCenter.ToString();
        return res;
    }
    public Vector3D GetSize()
    {
        return new Vector3D(GetSize(Axis3D.X), GetSize(Axis3D.Y), GetSize(Axis3D.Z));
    }
    public double GetVolume()
    {
        return GetSize(Axis3D.X) * GetSize(Axis3D.Y) * GetSize(Axis3D.Z);
    }
    public double HalfSizeX
    {
        get { return GetSize(Axis3D.X) / 2; }
    }
    public double HalfSizeY
    {
        get { return GetSize(Axis3D.Y) / 2; }
    }
    public double HalfSizeZ
    {
        get { return GetSize(Axis3D.Z) / 2; }
    }
    public double GetSize(Axis3D axis) { return GetMax(axis) - GetMin(axis); }
    private double m_xCenter;
    public double CenterX
    {
        get { return m_xCenter; }
        set {
            if (m_xCenter != value)
            {
                m_xCenter = value;
                Notify();
                NotifyMethod("CenterX");
            }
        }
    }
    private double m_yCenter;
    public double CenterY
    {
        set {
            if (m_yCenter != value)
            {
                m_yCenter = value;
                Notify();
                NotifyMethod("CenterY");
            }
        }
        get { return m_yCenter; }
    }
    private double m_zCenter;
    public double CenterZ
    {
        set {
            if (m_zCenter != value)
            {
                m_zCenter = value;
                Notify();
                NotifyMethod("CenterZ");
            }
        }
        get { return m_zCenter; }
    }

    public abstract Area3DBase Clone();
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

[XmlInclude(typeof(CircularArea2D))]
public class CircularArea2D: Area2DBase
{
    public override bool IsOut(double x, double y, int iteration)
    {
        double difX = x - m_xCenter;
        double difY = y - m_yCenter;
        return difX * difX + difY * difY > m_radiusSquared;
    }
    public CircularArea2D()
    {
        CenterX = 0;
        CenterY = 0;
        Radius = 2;
    }
    public CircularArea2D(double x, double y, double radius)
    {
        CenterX = x;
        CenterY = y;
        Radius = radius;
    }

    public override string ToString()
    {
        string res = base.ToString();
        res += " - x: " + CenterX.ToString();
        res += " y: " + CenterY.ToString();
        res += " r :" + Radius.ToString();
        return res;
    }

    public override Area2DBase Clone()
    {
        return new CircularArea2D(CenterX, CenterY, Radius);
    }

    public override bool Equals(Area2DBase other)
    {
        if (!(other is CircularArea2D))
        {
            return false;
        }
        CircularArea2D secondCircular = other as CircularArea2D;
        return CenterX == secondCircular.CenterX && CenterY == secondCircular.CenterY &&
            Radius == secondCircular.Radius;
    }

    private double m_xCenter;
    public double CenterX
    {
        get { return m_xCenter; }
        set { m_xCenter = value; }
    }
    private double m_yCenter;
    public double CenterY
    {
        get { return m_yCenter; }
        set { m_yCenter = value; }
    }
    private double m_radius;
    private double m_radiusSquared;    // sqare of radius
    public double Radius
    {
        get { return m_radius; }
        set
        {
            m_radius = value;
            m_radiusSquared = m_radius * m_radius;
        }
    }
}


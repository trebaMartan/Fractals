﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class CircularArea3D : Area3DBase
{
    public override bool IsOut(double x, double y, double z, int iteration)
    {
        double difX = x - CenterX;
        double difY = y - CenterY;
        double difZ = z - CenterZ;
        return difX * difX + difY * difY + difZ * difZ > m_radiusSquared;
    }
    public override string ToString()
    {
        string res = base.ToString();
        res += "r:" + m_radius.ToString();
        return res;
    }
    public CircularArea3D()
    {
        CenterX = 0;
        CenterY = 0;
        CenterZ = 0;
        Radius = 2;
    }
    public CircularArea3D(double x, double y, double z, double radius)
    {
        CenterX = x;
        CenterY = y;
        CenterZ = z;
        Radius = radius;
    }
    public override Area3DBase Clone()
    {
        return new CircularArea3D(CenterX, CenterY, CenterZ, Radius);
    }

    public override double GetMin(Axis3D axis)
    {
        switch (axis)
        {
            case Axis3D.X:
                return CenterX - Radius;
            case Axis3D.Y:
                return CenterY - Radius;
            default:
            case Axis3D.Z:
                return CenterZ - Radius;
        }
    }

    override public double GetMax(Axis3D axis)
    {
        switch (axis)
        {
            case Axis3D.X:
                return CenterX + Radius;
            case Axis3D.Y:
                return CenterY + Radius;
            case Axis3D.Z:
            default:
                return CenterZ + Radius;
        }
    }

    private double m_radius;
    private double m_radiusSquared;    // sqare of radius
    public double Radius
    {
        get { return m_radius; }
        set
        {
            m_radius = value;
            m_radiusSquared = m_radius * m_radius;
        }
    }
}


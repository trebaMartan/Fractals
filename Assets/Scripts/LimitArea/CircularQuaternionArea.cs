﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartMathLibrary;
using Assets.Scripts.Math;

public class CircularQuaternionArea : QuaternionAreaBase
{
    public CircularQuaternionArea()
    {
        Center = new Quaternion();
        Radius = 2;
    }
    public CircularQuaternionArea(Quaternion center, double radius)
    {
        Center = center;
        Radius = radius;
    }
    public override bool IsOut(Quaternion q, int iteration)
    {
        return (Center - q).SquareLength > m_radiusSqared;
    }
    public override QuaternionAreaBase Clone()
    {
        return new CircularQuaternionArea(Center, Radius);
    }

    public override double GetMin(AxisQuat axis)
    {
        switch(axis)
        {
            case AxisQuat.X0:
            default:
                return Center.X0 - Radius;
            case AxisQuat.I:
                return Center.I - Radius;
            case AxisQuat.J:
                return Center.J - Radius;
            case AxisQuat.K:
                return Center.K - Radius;
        }
    }

    public override double GetMax(AxisQuat axis)
    {
        switch (axis)
        {
            case AxisQuat.X0:
            default:
                return Center.X0 + Radius;
            case AxisQuat.I:
                return Center.I + Radius;
            case AxisQuat.J:
                return Center.J + Radius;
            case AxisQuat.K:
                return Center.K + Radius;
        }
    }

    private Quaternion m_center;
    public Quaternion Center
    {
        get { return m_center; }
        set { m_center = value; }
    }
    private double m_radius;
    private double m_radiusSqared;    // sqare of radius
    public double Radius
    {
        get { return m_radius; }
        set
        {
            m_radius = value;
            m_radiusSqared = m_radius * m_radius;
        }
    }

    public override string ToString()
    {
        string res = base.ToString();
        res += " - " + m_center.ToString() + ", radius: " + m_radius.ToString();
        return res;
    }

    public override void RoundDoubles()
    {
        Center.RoundDoubles();
    }
}
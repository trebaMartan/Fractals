﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartMathLibrary;

public enum AxisQuat
{
    X0,
    I,
    J,
    K
};

public abstract class QuaternionAreaBase
{
    public abstract bool IsOut(Quaternion q, int iteration);
    public abstract double GetMin(AxisQuat axis);
    public abstract double GetMax(AxisQuat axis);
    public double GetCenter(AxisQuat axis) { return (GetMin(axis) + GetMax(axis)) / 2; }
    public abstract QuaternionAreaBase Clone();
    public abstract void RoundDoubles();
}

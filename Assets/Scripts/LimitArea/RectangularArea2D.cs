﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Xml.Serialization;
using Assets.Scripts.Utils;
using System.Runtime.InteropServices;
using System.IO;

[XmlInclude(typeof(RectangularArea2D))]
public class RectangularArea2D : Area2DBase
{
    public RectangularArea2D()
    {
        CenterX = 0;
        CenterY = 0;
        SizeX = 2;
        SizeY = 2;
    }
    public RectangularArea2D(RectangularArea2D old)
    {
        CenterX = old.CenterX;
        CenterY = old.CenterY;
        SizeX = old.SizeX;
        SizeY = old.SizeY;
    }

    public RectangularArea2D(double centerX, double centerY, double sizeX, double sizeY)
    {
        CenterX = centerX;
        CenterY = centerY;
        SizeX = sizeX;
        SizeY = sizeY;
    }

    public override string ToString()
    {
        string res = "";
        res += " - x: " + CenterX.ToString();
        res += " y: " + CenterY.ToString();
        res += " size x :" + SizeX.ToString();
        res += " size y :" + SizeY.ToString();
        return res;
    }

    public override bool IsOut(double x, double y, int iteration)
    {
        if (Math.Abs(x - CenterX) > m_halfSizeX)
            return true;
        if (Math.Abs(y - CenterY) > m_halfSizeY)
            return true;
        return false;
    }
    public override Area2DBase Clone()
    {
        return new RectangularArea2D(CenterX, CenterY, SizeX, SizeY);
    }

    public override bool Equals(Area2DBase other)
    {
        if (!(other is RectangularArea2D))
        {
            return false;
        }
        RectangularArea2D secondArea = other as RectangularArea2D;
        return CenterX == secondArea.CenterX && CenterY == secondArea.CenterY &&
            SizeX == secondArea.SizeX && SizeY == secondArea.SizeY;
    }

    public static bool operator ==(RectangularArea2D first, RectangularArea2D second)
    {
        return first.CenterX == second.CenterX && first.CenterX == second.CenterY &&
            first.SizeX == second.SizeX && first.SizeY == second.SizeY;
    }
    public static bool operator !=(RectangularArea2D first, RectangularArea2D second)
    {
        return first.CenterX != second.CenterX || first.CenterX != second.CenterY ||
            first.SizeX != second.SizeX || first.SizeY != second.SizeY;
    }
    private double m_xCenter;
    public double CenterX
    {
        get { return m_xCenter; }
        set {
            if (value != m_xCenter)
            {
                m_xCenter = value;
                NotifyMethod("CenterX");
            }
        }
    }
    private double m_yCenter;
    public double CenterY
    {
        set {
            if (value != m_yCenter)
            {
                m_yCenter = value;
                NotifyMethod("CenterY");
            }
        }
        get { return m_yCenter; }
    }

    private double m_sizeX;
    public double SizeX
    {
        get { return m_sizeX; }
        set {
            if (value != m_sizeX)
            {
                m_sizeX = value;
                m_halfSizeX = m_sizeX / 2;
                NotifyMethod("SizeX");
            }
        }
    }

    private double m_sizeY;
    public double SizeY
    {
        get { return m_sizeY; }
        set {
            if (value != m_sizeY)
            {
                m_sizeY = value;
                m_halfSizeY = m_sizeY / 2;
                NotifyMethod("SizeY");
            }
        }
    }

    //public Vector2 GetLowerLeft()
    //{
    //    return new Vector2(CenterX - m_halfSizeX, CenterY - m_halfSizeY);
    //}

    //public Vector2 GetUpperRight()
    //{
    //    return new Vector2(CenterX + m_halfSizeX, CenterY + m_halfSizeY);
    //}

    public void SetHalfSize(double halfSize)
    {
        SizeX = SizeY = 2 * halfSize;
    }

    public double HalfSizeX
    {
        get { return m_halfSizeX; }
    }
    public double HalfSizeY
    {
        get { return m_halfSizeY; }
    }

    private double m_halfSizeX;
    private double m_halfSizeY;
}

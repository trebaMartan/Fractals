﻿using Assets.Scripts.Graphic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Xml.Serialization;
using Assets.Scripts.Math;

public class RectangularArea3D : Area3DBase
{
    public RectangularArea3D()
    {
        CenterX = 0;
        CenterY = 0;
        CenterZ = 0;
        HalfSizeX = 2;
        HalfSizeY = 2;
        HalfSizeZ = 2;
    }
    public RectangularArea3D(double centerX, double centerY, double centerZ, double halfSizeX, double halfSizeY, double halfSizeZ)
    {
        CenterX = centerX;
        CenterY = centerY;
        CenterZ = centerZ;
        HalfSizeX = halfSizeX;
        HalfSizeY = halfSizeY;
        HalfSizeZ = halfSizeZ;
    }

    public override string ToString()
    {
        string res = base.ToString() + ", ";
        res += "Hx: " + HalfSizeX.ToString() + ", ";
        res += "Hy: " + HalfSizeY.ToString() + ", ";
        res += "Hz: " + HalfSizeZ.ToString();
        return res;
    }
    public override bool IsOut(double x, double y, double z, int iteration)
    {
        if (Math.Abs(x - CenterX) > HalfSizeX)
            return true;
        if (Math.Abs(y - CenterY) > HalfSizeY)
            return true;
        if (Math.Abs(z - CenterZ) > HalfSizeZ)
            return true;
        return false;
    }
    public static bool operator ==(RectangularArea3D first, RectangularArea3D second)
    {
        if ((object)first == null && (object)second == null)
        {
            return true;
        }
        if ((object)first == null || (object)second == null)
        {
            return false;
        }
        return first.GetCenter() == second.GetCenter() && first.GetSize() == second.GetSize();
    }
    public static bool operator !=(RectangularArea3D first, RectangularArea3D second)
    {
        if ((object)first == null && (object)second == null)
        {
            return false;
        }
        if ((object)first == null || (object)second == null)
        {
            return true;
        }
        return first.GetCenter() != second.GetCenter() || first.GetSize() != second.GetSize();
    }
    public override Area3DBase Clone()
    {
        return new RectangularArea3D(CenterX, CenterY, CenterZ, HalfSizeX, HalfSizeY, HalfSizeZ);
    }

    [XmlIgnore]
    public new double HalfSizeX
    {
        get { return m_sizeX / 2 ; }
        set
        {
            if (HalfSizeX != value)
            { 
                SizeX = 2 * value;
                Notify();
            }
        }
    }

    public override double GetMin(Axis3D axis)
    {
        switch (axis)
        {
            case Axis3D.X:
                return CenterX - HalfSizeX;
            case Axis3D.Y:
                return CenterY - HalfSizeY;
            default:
            case Axis3D.Z:
                return CenterZ - HalfSizeZ;
        }
    }

    public override double GetMax(Axis3D axis)
    {
        switch (axis)
        {
            case Axis3D.X:
                return CenterX + HalfSizeX;
            case Axis3D.Y:
                return CenterY + HalfSizeY;
            case Axis3D.Z:
            default:
                return CenterZ + HalfSizeZ;
        }
    }

    [XmlIgnore]
    public new double HalfSizeY
    {
        get { return m_sizeY / 2; }
        set
        {
            if (HalfSizeY != value)
            {
                SizeY = 2 * value;
                Notify();
            }
        }
    }

    [XmlIgnore]
    public new double HalfSizeZ
    {
        get { return m_sizeZ / 2; }
        set
        {
            if (HalfSizeZ != value)
            {
                SizeZ = 2 * value;
                Notify();
            }
        }
    }

    public double SizeX
    {
        get { return m_sizeX; }
        set
        {
            if (m_sizeX != value)
            {
                m_sizeX = value;
                HalfSizeX = m_sizeX / 2;
                NotifyMethod("SizeX");
            }
        }
    }

    public double SizeY
    {
        get { return m_sizeY; }
        set
        {
            if (m_sizeY != value)
            {
                m_sizeY = value;
                HalfSizeY = m_sizeY / 2;
                NotifyMethod("SizeY");
            }
        }
    }

    public double SizeZ
    {
        get { return m_sizeZ; }
        set
        {
            if (m_sizeZ != value)
            {
                m_sizeZ = value;
                HalfSizeZ = m_sizeZ / 2;
                NotifyMethod("SizeZ");

            }
        }
    }

    public double GetMaxHalfSize()
    {
        if (HalfSizeX > HalfSizeY)
        {
            return HalfSizeX > HalfSizeZ ? HalfSizeX : HalfSizeZ;
        }
        return HalfSizeY > HalfSizeZ ? HalfSizeY : HalfSizeZ;
    }

    public double GetMaxSize()
    {
        return 2 * GetMaxHalfSize();
    }

    public UnityEngine.Bounds GetBounds()
    {
        return new UnityEngine.Bounds(GetCenter().Vector3, GetSize().Vector3);
    }

    private double RoundIt(float value, bool roundIt)
    {
        return roundIt ? ((double)value).RoundToSignificantDigits(4) : (double)value;
    }

    public void SetFromGraphicBounds(UnityEngine.Bounds bounds, bool rountIt)
    {
        Vector3 computationSpaceCenter = Coord.Transform(bounds.center);
        CenterX = RoundIt(computationSpaceCenter.x, rountIt);
        CenterY = RoundIt(computationSpaceCenter.y, rountIt);
        CenterZ = RoundIt(computationSpaceCenter.z, rountIt);
        SizeX = RoundIt(bounds.extents.x * 2, rountIt);
        SizeY = RoundIt(bounds.extents.y * 2, rountIt);
        SizeZ = RoundIt(bounds.extents.z * 2, rountIt);
    }

    private double m_sizeX;
    private double m_sizeY;
    private double m_sizeZ;
}
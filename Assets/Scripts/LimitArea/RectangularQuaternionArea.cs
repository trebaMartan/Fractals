﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartMathLibrary;
using System.Xml.Serialization;

public class RectangularQuaternionArea : QuaternionAreaBase
{
    public RectangularQuaternionArea()
    {
        Center = new Quaternion();
        m_size = new Quaternion();
        Size.X0 = 4;
        Size.I = 4;
        Size.J = 4;
        Size.K = 4;
    }
    public RectangularQuaternionArea(Quaternion center, Quaternion halfsize)
    {
        Center = center;
        HalfSize = halfsize;
    }

    public override string ToString()
    {
        string res = base.ToString();
        res += m_Center.ToString() + ",";
        res += HalfSize.ToString();
        return res;
    }

    public override bool IsOut(Quaternion q, int iteration)
    {
        if (Math.Abs(q.X0 - Center.X0) > HalfSize.X0)
            return true;
        if (Math.Abs(q.I - Center.I) > HalfSize.I)
            return true;
        if (Math.Abs(q.J - Center.J) > HalfSize.J)
            return true;
        if (Math.Abs(q.K - Center.K) > HalfSize.K)
            return true;
        return false;
    }
    public override QuaternionAreaBase Clone()
    {
        return new RectangularQuaternionArea(Center, HalfSize);
    }
    private Quaternion m_Center;
    public Quaternion Center
    {
        get { return m_Center; }
        set { m_Center = value; }
    }

    [XmlIgnore]
    public Quaternion HalfSize
    {
        get { return m_size * 0.5; }
        set
        {
            if (HalfSize != value)
            {
                Size = value * 2;
            }
        }
    }

    public Quaternion Size
    {
        get { return m_size; }
        set
        {
            if (m_size != value)
            {
                m_size = value;
                HalfSize = new Quaternion(m_size * 0.5);
            }
        }
    }

    public override double GetMin(AxisQuat axis)
    {
        switch (axis)
        {
            case AxisQuat.X0:
            default:
                return Center.X0 - HalfSize.X0;
            case AxisQuat.I:
                return Center.I - HalfSize.I;
            case AxisQuat.J:
                return Center.J - HalfSize.J;
            case AxisQuat.K:
                return Center.K - HalfSize.K;
        }
    }

    public override double GetMax(AxisQuat axis)
    {
        switch (axis)
        {
            case AxisQuat.X0:
            default:
                return Center.X0 + HalfSize.X0;
            case AxisQuat.I:
                return Center.I + HalfSize.I;
            case AxisQuat.J:
                return Center.J + HalfSize.J;
            case AxisQuat.K:
                return Center.K + HalfSize.K;
        }
    }

    public override void RoundDoubles()
    {
        Center.RoundDoubles();
        Size.RoundDoubles();
    }

    private Quaternion m_size;
}

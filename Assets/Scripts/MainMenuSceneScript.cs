﻿using UnityEngine;


public class MainMenuSceneScript : MonoBehaviour
{

    public void OnClicked2DFractals()
    {
        MySceneManager.GoToScene("2D Fractals");
    }

    public void OnClicked3DFractals()
    {
        MySceneManager.GoToScene("3D Fractals");
    }

    public void OnClickedRandomFractal()
    {
        Persistent.Instance.ComputeImmediatelyRandom2D = true;
        MySceneManager.GoToScene("Complex2D");
    }

    public void OnClickedGallery()
    {
        MySceneManager.GoToScene("FractalSelection");
    }

    public void OnButtonHelp()
    {
        string helpPath = Application.persistentDataPath + "/Help/help.html";
        string URL = "file://localhost/" + helpPath;
        Application.OpenURL(URL);
    }

    public void OnButtonAbout()
    {
        MySceneManager.GoToScene("About", UnityEngine.SceneManagement.LoadSceneMode.Additive);
    }

    public void OnClickedSettings()
    {
        MySceneManager.GoToScene("Settings");
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Math
{
    public class Expression
    {
        delegate double DoubleFunction(double value);

        Expression(string source)
        {

        }

        bool Parse()
        {

            return true;
        }

        double Evaluate(double value)
        {
            double res = value;
            foreach(DoubleFunction function in m_functions)
            {
                res = function(value);
            }
            return res;
        }

        List<DoubleFunction> m_functions;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Math
{
    public static class Extensions
    {
        public static double RoundToSignificantDigits(this double d, int digits)
        {
            if (d == 0)
            {
                return 0;
            }
            double scale = System.Math.Pow(10, System.Math.Floor(System.Math.Log10(System.Math.Abs(d))) + 1);
            return scale * System.Math.Round(d / scale, digits);
        }
    }
}

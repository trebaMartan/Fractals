﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Math
{
    static public class Functions
    {
        static public float Min(float a, float b)
        {
            return a < b ? a : b;
        }
        static public float Max(float a, float b)
        {
            return a > b ? a : b;
        }

    }
}

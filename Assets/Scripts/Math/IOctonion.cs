namespace SmartMathLibrary
{
    /// <summary>
    /// Interface to implement an Octonion class.
    /// </summary>
    public interface IOctonion
    {
        /// <summary>
        /// Gets or sets the real number part of the octonion.
        /// </summary>
        /// <value>The real number part of the octonion.</value>
        double RealPart { get; set; }

        /// <summary>
        /// Gets or sets the i part of the octonion.
        /// </summary>
        /// <value>The i part of the octonion.</value>
        double I { get; set; }

        /// <summary>
        /// Gets or sets the j part of the octonion.
        /// </summary>
        /// <value>The j part of the octonion.</value>
        double J { get; set; }

        /// <summary>
        /// Gets or sets the k part of the octonion.
        /// </summary>
        /// <value>The k part of the octonion.</value>
        double K { get; set; }

        /// <summary>
        /// Gets or sets the l part of the octonion.
        /// </summary>
        /// <value>The l part of the octonion.</value>
        double L { get; set; }

        /// <summary>
        /// Gets or sets the il part of the octonion.
        /// </summary>
        /// <value>The il part of the octonion.</value>
        double IL { get; set; }

        /// <summary>
        /// Gets or sets the jl part of the octonion.
        /// </summary>
        /// <value>The jl part of the octonion.</value>
        double JL { get; set; }

        /// <summary>
        /// Gets or sets the kl part of the octonion.
        /// </summary>
        /// <value>The kl part of the octonion.</value>
        double KL { get; set; }

        /// <summary>
        /// Copies the current instance of an octonion.
        /// </summary>
        /// <returns>The copy of the current instance.</returns>
        Octonion Copy();

        /// <summary>
        /// Compares the current instance of an octonion to another instance.
        /// </summary>
        /// <param name="compare">The octonion to compare.</param>
        /// <returns>True if both octonions are even otherwise, false.</returns>
        bool CompareTo(Octonion compare);

        /// <summary>
        /// Powers the current sedenion raised to the specified power.
        /// </summary>
        /// <param name="value">The value to raised with.</param>
        /// <returns>The new sedenion raised to the specified power.</returns>
        Octonion Pow(double value);
    }
}
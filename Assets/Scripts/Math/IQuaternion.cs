namespace SmartMathLibrary
{
    /// <summary>
    /// Interface to implement an Quaternion class.
    /// </summary>
    public interface IQuaternion
    {
        /// <summary>
        /// Gets or sets the real number part of the quaternion.
        /// </summary>
        /// <value>The real number part of the quaternion.</value>
        double X0 { get; set; }

        /// <summary>
        /// Gets or sets the imaginary number part i of the quaternion.
        /// </summary>
        /// <value>The imaginary number part i of the quaternion.</value>
        double I { get; set; }

        /// <summary>
        /// Gets or sets the imaginary number part j of the quaternion.
        /// </summary>
        /// <value>The imaginary number part j of the quaternion.</value>
        double J { get; set; }

        /// <summary>
        /// Gets or sets the imaginary number part k of the quaternion.
        /// </summary>
        /// <value>The imaginary number part k of the quaternion.</value>
        double K { get; set; }

        /// <summary>
        /// Gets the conjugation of the quaternion.
        /// </summary>
        /// <returns>The conjugation of the quaternion.</returns>
        Quaternion Conjugation { get; }

        /// <summary>
        /// Normalizes the current instance of the quaternion.
        /// </summary>
        /// <returns>The normalized instance of the quaternion.</returns>
        Quaternion Normalize();

        /// <summary>
        /// Gets the multiplicative inverse of the current quaternion.
        /// </summary>
        /// <returns>The multiplicative inverse of the current quaternion.</returns>
        Quaternion Inverse { get; }

        /// <summary>
        /// Gets the length of the quaternion.
        /// </summary>
        /// <returns>The length of the quaternion.</returns>
        double Length { get; }

        /// <summary>
        /// Compares the current quaternioninstance to another quaternion.
        /// </summary>
        /// <param name="compare">The quaternion to compare.</param>
        /// <returns>True if the current quaternioninstance is even to the specified quaternion otherwise, false.</returns>
        bool CompareTo(Quaternion compare);

        /// <summary>
        /// Copies this current instance.
        /// </summary>
        /// <returns>A copy of the current instance.</returns>
        Quaternion Copy();

        /// <summary>
        /// Determines whether the real number part is zero.
        /// </summary>
        /// <returns>
        /// True if real number part is zero otherwise, false.
        /// </returns>
        bool IsRealNumberPartZero { get; }

        /// <summary>
        /// Determines whether the imaginary number parts i, j and k are zero.
        /// </summary>
        /// <returns>
        /// True if imaginary number parts i, j and k are zero otherwise, false.
        /// </returns>
        bool IsImaginaryNumberPartZero { get; }
    }
}
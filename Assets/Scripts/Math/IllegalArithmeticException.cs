﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SmartMathLibrary
{
    /// <summary>
    /// This class represents an IllegalAlarithmeticException, which will be thrown the 
    /// current mathematic operation is not allowed.
    /// </summary>
    [Serializable]
    public class IllegalArithmeticException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IllegalArithmeticException"/> class.
        /// </summary>
        public IllegalArithmeticException()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IllegalArithmeticException"/> class.
        /// </summary>
        /// <param name="message">The error message which the exception includes.</param>
        public IllegalArithmeticException(String message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IllegalArithmeticException"/> class.
        /// </summary>
        /// <param name="message">The message of the exception.</param>
        /// <param name="exception">The inner exception.</param>
        public IllegalArithmeticException(String message, Exception exception)
            : base(message, exception)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IllegalArithmeticException"/> class.
        /// </summary>
        /// <param name="serializationInfo">The internal serializationinfo.</param>
        /// <param name="streamingContext">The internal streamingcontext.</param>
        protected IllegalArithmeticException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {
        }
    }
}
﻿using Assets.Scripts.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartMathLibrary
{
    /// <summary>
    /// This class provides methods to handle mathematical quaternions.
    /// </summary>
    [Serializable]
    public class Quaternion : System.IComparable<Quaternion>
    {
        /// <summary>
        /// The real number path of the quaternion.
        /// </summary>
        private double x0;

        /// <summary>
        /// The imaginary number path i of the quaternion.
        /// </summary>
        private double i;

        /// <summary>
        /// The imaginary number path j of the quaternion.
        /// </summary>
        private double j;

        /// <summary>
        /// The imaginary number path k of the quaternion.
        /// </summary>
        private double k;

        /// <summary>
        /// Initializes a new instance of the <see cref="Quaternion"/> class.
        /// </summary>
        public Quaternion()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Quaternion"/> class.
        /// </summary>
        /// <param name="x0">The real number part x0 of the quaternion.</param>
        public Quaternion(double x0)
        {
            this.x0 = x0;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Quaternion"/> class.
        /// </summary>
        /// <param name="quaternion">Another quaternion that should be cloned.</param>
        public Quaternion(Quaternion quaternion)
        {
            if (quaternion == (Quaternion)null)
            {
                throw new ArgumentNullException("quaternion");
            }

            this.x0 = quaternion.X0;
            this.i = quaternion.I;
            this.j = quaternion.J;
            this.k = quaternion.K;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Quaternion"/> class.
        /// </summary>
        /// <param name="x0">The real number part x0 of the quaternion.</param>
        /// <param name="x1">The imaginary number path i of the quaternion.</param>
        /// <param name="x2">The imaginary number path j of the quaternion.</param>
        /// <param name="x3">The imaginary number path k of the quaternion.</param>
        public Quaternion(double x0, double x1, double x2, double x3)
        {
            this.x0 = x0;
            this.i = x1;
            this.j = x2;
            this.k = x3;
        }

        public Quaternion(List<double> vector)
        {
            UnityEngine.Debug.Assert(vector.Count == 4);
            X0 = vector[0];
            I = vector[1];
            J = vector[2];
            K = vector[3];
        }

        public double GetCoordinate(int i)
        {
            UnityEngine.Debug.Assert(i >= 0 && i < 4);
            switch(i)
            {
            case 0:
                return X0;
            case 1:
                return I;
            case 2:
                return J;
            case 3:
                return K;
            }
            return 0;
        }

        public void CopyFrom(Quaternion old)
        {
            x0 = old.x0;
            i = old.i;
            j = old.j;
            k = old.k;
        }

        /// <summary>
        /// Gets or sets the real number part of the quaternion.
        /// </summary>
        /// <value>The real number part of the quaternion.</value>
        public double X0
        {
            get { return x0; }
            set { x0 = value; }
        }

        /// <summary>
        /// Gets or sets the imaginary number part i of the quaternion.
        /// </summary>
        /// <value>The imaginary number part i of the quaternion.</value>
        public double I
        {
            get { return i; }
            set { i = value; }
        }

        /// <summary>
        /// Gets or sets the imaginary number part j of the quaternion.
        /// </summary>
        /// <value>The imaginary number part j of the quaternion.</value>
        public double J
        {
            get { return j; }
            set { j = value; }
        }

        /// <summary>
        /// Gets or sets the imaginary number part k of the quaternion.
        /// </summary>
        /// <value>The imaginary number part k of the quaternion.</value>
        public double K
        {
            get { return k; }
            set { k = value; }
        }

        /// <summary>
        /// Implements the operator +.
        /// </summary>
        /// <param name="a">The first quaternion.</param>
        /// <param name="b">The second quaternion.</param>
        /// <returns>The result of the operator.</returns>
        public static Quaternion operator +(Quaternion a, Quaternion b)
        {
            if (a == (Quaternion)null)
            {
                throw new ArgumentNullException("a");
            }

            if (b == (Quaternion)null)
            {
                throw new ArgumentNullException("b");
            }

            Quaternion result = new Quaternion();

            result.X0 = a.X0 + b.X0;
            result.I = a.I + b.I;
            result.J = a.J + b.J;
            result.K = a.K + b.K;

            return result;
        }

        public double Max()
        {
            return Math.Max(X0, Math.Max(I, Math.Max(J, K)));
        }

        /// <summary>
        /// Implements the operator -.
        /// </summary>
        /// <param name="a">The first quaternion.</param>
        /// <param name="b">The second quaternion.</param>
        /// <returns>The result of the operator.</returns>
        public static Quaternion operator -(Quaternion a, Quaternion b)
        {
            if (a == (Quaternion)null)
            {
                throw new ArgumentNullException("a");
            }

            if (b == (Quaternion)null)
            {
                throw new ArgumentNullException("b");
            }

            Quaternion result = new Quaternion();

            result.X0 = a.X0 - b.X0;
            result.I = a.I - b.I;
            result.J = a.J - b.J;
            result.K = a.K - b.K;

            return result;
        }

        /// <summary>
        /// Implements the operator *.
        /// </summary>
        /// <param name="a">The first quaternion.</param>
        /// <param name="b">The second quaternion.</param>
        /// <returns>The result of the operator.</returns>
        public static Quaternion operator *(Quaternion a, Quaternion b)
        {
            if (a == (Quaternion)null)
            {
                throw new ArgumentNullException("a");
            }

            if (b == (Quaternion)null)
            {
                throw new ArgumentNullException("b");
            }

            Quaternion result = new Quaternion();

            result.X0 = a.X0 * b.X0 - a.I * b.I - a.J * b.J - a.K * b.K;
            result.I = a.X0 * b.I + a.I * b.X0 + a.J * b.K - a.K * b.J;
            result.J = a.X0 * b.J - a.I * b.K + a.J * b.X0 + a.K * b.I;
            result.K = a.X0 * b.K + a.I * b.J - a.J * b.I + a.K * b.X0;

            return result;
        }

        public static Quaternion operator *(Quaternion a, double b)
        {
            Quaternion result = new Quaternion();
            result.I = a.I * b;
            result.J = a.J * b;
            result.K = a.K * b;
            result.X0 = a.X0 * b;

            return result;
        }

        /// <summary>
        /// Implements the operator /.
        /// </summary>
        /// <param name="a">The first quaternion.</param>
        /// <param name="b">The second quaternion.</param>
        /// <returns>The result of the operator.</returns>
        public static Quaternion operator /(Quaternion a, Quaternion b)
        {
            return a * b.Inverse;
        }

        /// <summary>
        /// Gets the conjugation of the quaternion.
        /// </summary>
        /// <returns>The conjugation of the quaternion.</returns>
        public Quaternion Conjugation
        {
            get
            {
                Quaternion result = new Quaternion(this);

                result.I = -result.I;
                result.J = -result.J;
                result.K = -result.K;

                return result;
            }
        }

        /// <summary>
        /// Normalizes the current instance of the quaternion.
        /// </summary>
        /// <returns>The normalized instance of the quaternion.</returns>
        public Quaternion Normalize()
        {
            if (this.Length == 0)
            {
                throw new IllegalArithmeticException("Division by zero not allowed.");
            }

            Quaternion q = new Quaternion();
            double length = this.Length;

            q.X0 = this.x0 / length;
            q.I = this.i / length;
            q.J = this.j / length;
            q.K = this.k / length;

            return q;
        }

        /// <summary>
        /// Gets the multiplicative inverse of the current quaternion.
        /// </summary>
        /// <returns>The multiplicative inverse of the current quaternion.</returns>
        public Quaternion Inverse
        {
            get
            {
                if (this.Length == 0)
                {
                    throw new IllegalArithmeticException("Division by zero not allowed.");
                }

                Quaternion q = new Quaternion(this);
                double length = this.Length;
                double lenght2 = length * length;

                q.x0 /= lenght2;
                q.i = -q.i / lenght2;
                q.j = -q.j / lenght2;
                q.k = -q.k / lenght2;

                return q;
            }
        }

        /// <summary>
        /// Gets the length of the quaternion.
        /// </summary>
        /// <returns>The length of the quaternion.</returns>
        public double Length
        {
            get { return Math.Sqrt(x0 * x0 + i * i + j * j + k * k); }//Math.Pow(this.x0, 2) + Math.Pow(this.i, 2) + Math.Pow(this.j, 2) + Math.Pow(this.k, 2)); }
        }

        public double SquareLength
        {
            get { return x0 * x0 + i * i + j * j + k * k; }//return Math.Pow(this.x0, 2) + Math.Pow(this.i, 2) + Math.Pow(this.j, 2) + Math.Pow(this.k, 2); }
        }

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="a">The first quaternion.</param>
        /// <param name="b">The second quaternion.</param>
        /// <returns>The result of the operator.</returns>
        public static bool operator ==(Quaternion a, Quaternion b)
        {
            if (System.Object.ReferenceEquals(a, b))
            {
                return true;
            }

            if (((object)a == null) || ((object)b == null))
            {
                return false;
            }

            return ((a.X0 == b.X0) && (a.I == b.I) && (a.J == b.J) && (a.K == b.K));
        }

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        /// <param name="a">The first quaternion.</param>
        /// <param name="b">The second quaternion.</param>
        /// <returns>The result of the operator.</returns>
        public static bool operator !=(Quaternion a, Quaternion b)
        {
            return !(a == b);
        }

        /// <summary>
        /// Compares the current quaternioninstance to another quaternion.
        /// </summary>
        /// <param name="compare">The quaternion to compare.</param>
        /// <returns>True if the current quaternioninstance is even to the specified quaternion otherwise, false.</returns>
        public int CompareTo(Quaternion compare)
        {
            return(this == compare)? 0 : 1;
        }

        /// <summary>
        /// Copies this current instance.
        /// </summary>
        /// <returns>A copy of the current instance.</returns>
        public Quaternion Copy()
        {
            return new Quaternion(this.x0, this.i, this.j, this.k);
        }

        /// <summary>
        /// Determines whether the real number part is zero.
        /// </summary>
        /// <returns>
        /// True if real number part is zero otherwise, false.
        /// </returns>
        public bool IsRealNumberPartZero
        {
            get { return this.x0 == 0; }
        }

        /// <summary>
        /// Determines whether the imaginary number parts i, j and k are zero.
        /// </summary>
        /// <returns>
        /// True if imaginary number parts i, j and k are zero otherwise, false.
        /// </returns>
        public bool IsImaginaryNumberPartZero
        {
            get { return ((this.i == 0) && (this.j == 0) && (this.k == 0)); }
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <param name="obj">The <see cref="T:System.Object"/> to compare with the current <see cref="T:System.Object"/>.</param>
        /// <returns>
        /// true if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, false.
        /// </returns>
        /// <exception cref="T:System.NullReferenceException">The <paramref name="obj"/> parameter is null.</exception>
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public double ScalarProd(Quaternion other)
        {
            return x0 * other.X0 + I * other.I + J * other.J + K * other.K;
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        {
            return this.x0.ToString() + " + " + this.i.ToString() + "i + " + this.j.ToString() + "j + " +
                   this.k.ToString() + "k";
        }

        public void Clear()
        {
            x0 = i = j = k = 0;
        }
        public bool IsZero()
        {
            return x0 == 0 && i == 0 && j == 0 && k == 0;
        }

        private static Quaternion helpQuat = new Quaternion();
        public static Quaternion operator ^(Quaternion c1, int pow)
        {
            if ( pow == 0 )
            {
                return new Quaternion(1);
            }
            helpQuat.x0 = c1.x0;
            helpQuat.i = c1.i;
            helpQuat.j = c1.j;
            helpQuat.k = c1.k;

            for ( int i=1; i< pow; i++ )
            {
                helpQuat *= c1;
            }

            //c1.x0 = helpQuat.x0;
            //c1.i = helpQuat.i;
            //c1.j = helpQuat.j;
            //c1.k = helpQuat.k;
            return helpQuat;
        }

        public static Quaternion operator^(Quaternion c1, double power)
        {
            double m_norm = c1.Length;
            //if (m_norm == 0)
            //{
            //    m_theta = 0;
            //    m_unitVectorI = 0;
            //    m_unitVectorJ = 0;
            //    m_unitVectorK = 0;
            //    return;
            //}
            double vectorNorm = Math.Sqrt(c1.i * c1.i + c1.j * c1.j + c1.k * c1.k); // norm of vector part of quaternion
            double m_theta = Math.Asin(vectorNorm / m_norm);
            // unit vector n = (i,j,k) / vectorNorm
            //Quaternion unitVector = Quaternion(0, i / vectorNorm, j / vectorNorm, k / vectorNorm);
            double m_unitVectorI = c1.i / vectorNorm;
            double m_unitVectorJ = c1.j / vectorNorm;
            double m_unitVectorK = c1.k / vectorNorm;

            double normPowered = Math.Pow(m_norm, power);
            double normPoweredTimesSinPowerTheta = normPowered * Math.Sin(power * m_theta);
            Quaternion res = new Quaternion(normPowered * Math.Cos(power * m_theta),
                normPoweredTimesSinPowerTheta * m_unitVectorI,
                normPoweredTimesSinPowerTheta * m_unitVectorJ,
                normPoweredTimesSinPowerTheta * m_unitVectorK);
            return res;
        }

        public void SetCoordinate(AxisQuat axis, double value)
        {
            switch (axis)
            {
                case AxisQuat.X0:
                    X0 = value;
                    break;
                case AxisQuat.I:
                    I = value;
                    break;
                case AxisQuat.J:
                    J = value;
                    break;
                case AxisQuat.K:
                    K = value;
                    break;
            }
        }

        public void RoundDoubles()
        {
            X0 = Rounding.Round(X0);
            I = Rounding.Round(I);
            J = Rounding.Round(J);
            K = Rounding.Round(K);
        }

        static public Quaternion Zero
        {
            get { return new Quaternion(0, 0, 0, 0); }
        }
        static public Quaternion One
        {
            get { return new Quaternion(1, 0, 0, 0); }
        }
    }
}
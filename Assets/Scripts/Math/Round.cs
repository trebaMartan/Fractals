﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Math
{
    public class Rounding
    {
        public static double Round(double d)
        {
            return Convert.ToDouble(Convert.ToDecimal(d));
        }
    }
}

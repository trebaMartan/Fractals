﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Math
{
    public static class Vec3
    {
        public static Vector3 Create(double x, double y, double z)
        {
            return new Vector3((float)x, (float)y, (float)z);
        }
    }
}

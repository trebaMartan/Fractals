﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartMathLibrary;
using Assets.Scripts.Math;
using LinearAlgebra.Matricies;
using System.Diagnostics;

public class Vector2QuaternionMapping
{
    public Vector2QuaternionMapping()
    {
        m_vqMapping = new double[4, 3];
        m_vqMapping[0, 0] = 1;
        m_vqMapping[1, 1] = 1;
        m_vqMapping[2, 2] = 1;
        CountDependentValues();
    }
    public Vector2QuaternionMapping(double[,] mapping)
    {
        SetVQMapping(mapping);
    }
    public Vector2QuaternionMapping(Vector2QuaternionMapping old)
    {
        m_vqMapping = new double[4, 3];
        for (int row = 0; row < 4; row++)
        {
            for (int column = 0; column < 3; column++)
            {
                m_vqMapping[row, column] = old.m_vqMapping[row, column];
            }
        }
        CountDependentValues();
    }
    public override string ToString()
    {
        string str = base.ToString();
        for (int row = 0; row < GetRowCount(); row++ )
        {
            for (int column = 0; column < GetColumnCount(); column++)
            {
                str += m_vqMapping[row, column].ToString() + ",";
            }
        }
        return str;
    }
    public int GetRowCount() { return 4; }
    public int GetColumnCount() { return 3; }
    public double GetCoefficient(int row, int column)
    {
        return m_vqMapping[row, column];
    }
    public void SetCoefficient(int row, int column, double value, bool countDependentValues =true)
    {
        m_vqMapping[row, column] = value;
        if (countDependentValues)
        {
            CountDependentValues();
        }
    }
    public Quaternion GetMappingQuaternion(int column)
    {
        return new Quaternion(GetCoefficient(0, column), GetCoefficient(1, column), GetCoefficient(2, column), GetCoefficient(3, column));
    }
    public void SetMappingQuaternion(int column, Quaternion quaternion)
    {
        SetCoefficient(0, column, quaternion.X0);
        SetCoefficient(1, column, quaternion.I);
        SetCoefficient(2, column, quaternion.J);
        SetCoefficient(3, column, quaternion.K);
    }

    public Vector2QuaternionMapping Clone()
    {
        return new Vector2QuaternionMapping(this);
    }

    void SetVQMapping(double[,] mapping)
    {
        UnityEngine.Debug.Assert(mapping.GetLength(0) == 4 && mapping.GetLength(1) == 3, "wrong mapping matrix size - should be 3*4");
        m_vqMapping = mapping;

        CountDependentValues();
    }

    public void CountDependentValues()
    {
        CountOneDimensionalVqMapping();
        CountInverseMapping();
    }

    void CountOneDimensionalVqMapping()
    {
        m_oneDimensionalVQMapping = new double[4 * 3];
        ToFromOneDimensionalVQMapping(true);
    }
    
    void CountVQMappingFromOneDimensional()
    {
        m_vqMapping = new double[4, 3];
        ToFromOneDimensionalVQMapping(false);
    }

    private void ToFromOneDimensionalVQMapping(bool toOneDimensional)
    {
        int i = 0;
        for (int row = 0; row < 4; row++)
        {
            for (int column = 0; column < 3; column++, i++)
            {
                if (toOneDimensional)
                {
                    m_oneDimensionalVQMapping[i] = m_vqMapping[row, column];
                }
                else
                {
                    m_vqMapping[row, column] = m_oneDimensionalVQMapping[i];
                }
            }
        }
    }

    public string Validate()
    {
        string result = "";

        if (!AreBaseQuaternionsIndependent())
        {
            return "Your base quaternions are linearly dependent. You have to change them.";
        }
        return result;
    }

    bool AreBaseQuaternionsIndependent()
    {
        // check if are base quaternions linearlz independent

        // copy of three quaternions into extended matrix
        DoubleMatrix quaternionMatrix = new DoubleMatrix(4, 4);
        for (int row = 0; row < 4; row++)
        {
            for (int column = 0; column < 3; column++)
            {
                quaternionMatrix[column, row] = m_vqMapping[row, column];
            }
        }

        bool inverseFound = false;
        // 4 attempts to generate inverse matrix
        for (int setRow = 0; setRow < 4; setRow++)
        {
            for (int r = 0; r < 4; r++)
            {
                quaternionMatrix[3, r] = r == setRow ? 1 : 0;
            }
            DoubleMatrix inverseMatrix = quaternionMatrix.Inverse;
            if (double.IsNaN(inverseMatrix[0, 0]))
            {
                continue;
            }
            inverseFound = true;
            break;
        }
        return inverseFound;
    }

    void CountInverseMapping()
    {
        // computing of inverse mapping matrix
        //zxc TODO: finish this for non zero row (make it universal)
        m_qvMapping = new double[3, 4];

        if (!AreBaseQuaternionsIndependent())
        {
            // inverse doesn't exist
            return;
        }

        bool inverseFound = false;
        // computation of fourth base quaternion
        DoubleMatrix m33 = new DoubleMatrix(3, 3);
        for (int omittedRow = 0; omittedRow < 4; omittedRow++)
        {
            // 4. attempts to set selected quaternion coordinate as 1 and recount next 3 coordinates
            // 4-th quaternion should be perpendicular to the others - scalar multiplication Q1.Q4 = 0, Q2.Q4 = 0, Q3.Q4 = 0
            // lets X04=1 and try to computed quations
            // X01*1 + I1*I4 + J1*j4+ K1 * K4 = 0 
            // X02*1 + I2*I4 + J2*j4+ K2 * K4 = 0
            // X03*1 + I3*I4 + J3*j4+ K3 * K4 = 0 
            // we get matrix equation
            //  | I1 J1 K1 || I4 |    |-X01|  
            //  | I2 J2 K2 || J4 | =  |-X02|  
            //  | I3 J3 K3 || K4 |    |-X03|
            //
            // with solution
            // | I4 |    -1     |-X01|
            // | J4 | = M    *  |-X02|
            // | K4 |           |-X03|
            // if it don't give result now we will try set quaul to 1 another coordinate (it is possible that coordinate of fourth quaternion can be 0 and thus we don't get result when we set it as 1...)
            // omittedRow = 0 - we try to set X04 = 1. omittedRow = 1 - we try to set I4 = 1, ...

            for (int row = 0; row < 4; row++)
            {
                for (int column = 0; column < 3; column++)
                {
                    if (row != omittedRow)
                    {
                        m33[column, row > omittedRow? row - 1: row] = m_vqMapping[row, column];
                    }
                }
            }

            // computation of inverse
            DoubleMatrix inverse = m33.Inverse;
            if (double.IsNaN(inverse[0, 0]))
            {
                continue;
            }
            inverseFound = true;

            DoubleMatrix rightColumn = new DoubleMatrix(1, 3);
            for (int row = 0; row < 3; row++)
            {
                rightColumn[0, row] = -m_vqMapping[omittedRow, row];
            }
            DoubleMatrix computedQuaternionColumn = inverse * rightColumn;

            // computation of quaternion
            List<double> coefficients = new List<double>();
            for (int row = 0; row < 4; row++)
            {
                double coef = row == omittedRow ? 1 : computedQuaternionColumn[0, row > omittedRow ? row - 1 : row];
                coefficients.Add(coef);
            }
            Quaternion fourth = new Quaternion(coefficients);
            fourth = fourth.Normalize();

            // and now create transformation matrix 4*4 and its inverse
            DoubleMatrix extendedMatrix = new DoubleMatrix(4, 4);
            for (int row = 0; row < 4; row++)
            {
                for (int column = 0; column < 3; column++)
                {
                    extendedMatrix[column, row] = m_vqMapping[row, column];
                }
                extendedMatrix[3, row] = fourth.GetCoordinate(row);
            }

            DoubleMatrix extendedInverse = extendedMatrix.Inverse;

            for (int row = 0; row < 3; row++)
            {
                for (int column = 0; column < 4; column++)
                {
                    m_qvMapping[row, column] = extendedInverse[column, row];
                }
            }
            break;
        }

        Debug.Assert(inverseFound, "Inverse transformation (quaternion to vector) not found.");
    }

    int GetZeroRowFromVQ()
    {
        for (int row = 0; row < 4; row++)
        {
            bool zeroRow = true;
            for(int column = 0; column <3; column++)
            {
                if (m_vqMapping[row, column] != 0)
                {
                    zeroRow = false;
                    break;
                }
            }
            if (zeroRow)
            {
                return row;
            }
        }
        return -1;
    }

    public Quaternion toQuaternion(Vector3D vector)
    {
        s_resultQuaternion.X0 = m_vqMapping[0, 0] * vector.x + m_vqMapping[0, 1] * vector.y + m_vqMapping[0, 2] * vector.z;
        s_resultQuaternion.I = m_vqMapping[1, 0] * vector.x + m_vqMapping[1, 1] * vector.y + m_vqMapping[1, 2] * vector.z;
        s_resultQuaternion.J = m_vqMapping[2, 0] * vector.x + m_vqMapping[2, 1] * vector.y + m_vqMapping[2, 2] * vector.z;
        s_resultQuaternion.K = m_vqMapping[3, 0] * vector.x + m_vqMapping[3, 1] * vector.y + m_vqMapping[3, 2] * vector.z;
        return s_resultQuaternion;
    }

    private double[,] m_vqMapping;
    private double[] m_oneDimensionalVQMapping;
    public double[] VQMapping ///< one dimensional mapping for serialization (workaround for serialization of two dimensional arrays)
    {
        get { return m_oneDimensionalVQMapping; }
        set
        {
            m_oneDimensionalVQMapping = value;
            CountVQMappingFromOneDimensional();
            CountInverseMapping();
        }
    }
    static Quaternion s_resultQuaternion = new Quaternion();

    // mapping from quaternion to vector
    public Vector3D toVector(Quaternion quat)
    {
        s_resultVector.x = m_qvMapping[0, 0] * quat.X0 + m_qvMapping[0, 1] * quat.I + m_qvMapping[0, 2] * quat.J + m_qvMapping[0, 3] * quat.K;
        s_resultVector.y = m_qvMapping[1, 0] * quat.X0 + m_qvMapping[1, 1] * quat.I + m_qvMapping[1, 2] * quat.J + m_qvMapping[1, 3] * quat.K;
        s_resultVector.z = m_qvMapping[2, 0] * quat.X0 + m_qvMapping[2, 1] * quat.I + m_qvMapping[2, 2] * quat.J + m_qvMapping[2, 3] * quat.K;
        return s_resultVector;
    }

    Quaternion QuatToVectorCoefficient
    {
        get { return m_quatToVectorCoefficient; }
        set
        {
            m_quatToVectorCoefficient = value;
            m_VectorToQuatCoefficient = m_quatToVectorCoefficient.Inverse;
        }
    }

    public Vector3D toVectorByQuat(Quaternion quat)
    {
        // transformation by quaternion multiplication
        Quaternion mult = m_quatToVectorCoefficient * quat;
        s_resultVector.x = mult.I;
        s_resultVector.y = mult.J;
        s_resultVector.z = mult.K;
        return s_resultVector;
    }
    public Quaternion toQuaternionByQuat(Vector3D vec)
    {
        Quaternion mapped = new Quaternion(0, vec.x, vec.y, vec.z);
        return m_VectorToQuatCoefficient * mapped;
    }
    private double[,] m_qvMapping;
    static Vector3D s_resultVector = new Vector3D();

    private Quaternion m_quatToVectorCoefficient;   // multiplying coefficient for quaternion transformation
    private Quaternion m_VectorToQuatCoefficient;   // multiplying coefficient for quaternion transformation
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Math
{
    public static class Vector3Extension
    {
        public static Vector3 Mult(this Vector3 vec, double multiplier)
        {
            return new Vector3((float)(vec.x * multiplier), (float)(vec.y * multiplier), (float)(vec.z * multiplier));
        }
    }
    public struct Vector3D
    {
        //
        // Summary:
        //     ///
        //     X component of the vector.
        //     ///
        public double x;
        //
        // Summary:
        //     ///
        //     Y component of the vector.
        //     ///
        public double y;
        //
        // Summary:
        //     ///
        //     Z component of the vector.
        //     ///
        public double z;

        public Vector3D(double x, double y, double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Vector3 Vector3
        {
            get { return Vec3.Create(x, y, y); }
        }

        public void Normalize()
        {
            double length = Length();
            x /= length;
            y /= length;
            z /= length;
        }

        public double magnitude { get { return Length(); } }
        public double Length()
        {
            return System.Math.Sqrt(x * x + y * y + z * z);
        }

        public static double Dot(Vector3D lhs, Vector3D rhs)
        {
            return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z;
        }

        public static Vector3D Cross(Vector3D u, Vector3D v)
        {
            Vector3D res = new Vector3D();
            res.x = u.y * v.z - u.z * v.y;
            res.y = u.z * v.x - u.x * v.z;
            res.z = u.x * v.y - u.y * v.x;
            return res;
        }

        public static Vector3D operator -(Vector3D vec)
        {
            return new Vector3D(-vec.x, -vec.y, -vec.z);
        }

        public static Vector3D operator *(double d, Vector3D a)
        {
            Vector3D res = new Vector3D();
            res.x = a.x * d;
            res.y = a.y * d;
            res.z = a.z * d;
            return res;
        }
        public static Vector3D operator *(Vector3D a, double d)
        {
            Vector3D res = new Vector3D();
            res.x = a.x * d;
            res.y = a.y * d;
            res.z = a.z * d;
            return res;
        }

        public static Vector3D operator +(Vector3D a, Vector3D b)
        {
            Vector3D res = new Vector3D();
            res.x = a.x + b.x;
            res.y = a.y + b.y;
            res.z = a.z + b.z;
            return res;
        }

        public static bool operator ==(Vector3D a, Vector3D b)
        {
            return a.x == b.x && a.y == b.y && a.z == b.z;
        }
        public static bool operator !=(Vector3D a, Vector3D b)
        {
            return a.x != b.x || a.y != b.y || a.z != b.z;
        }

    }
}

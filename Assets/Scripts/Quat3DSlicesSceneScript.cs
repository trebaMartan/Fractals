﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Assets.Scripts.Math;
using Assets.Scripts.Utils;

public class Quat3DSlicesSceneScript : MonoBehaviour
{
    enum ECutPlane
    {
        xy,
        yz,
        zx,
        definedByNormal
    }
    // Use this for initialization
    void Start()
    {
        m_quatFractal = new QuaternionFractal3D();

        if (Persistent.Instance.QuaternionFractal3D != null)
        {
            m_quatFractal = Persistent.Instance.QuaternionFractal3D;
        }
        else
        {
            QuaternionFractal3D.Load(Persistent.Instance.Seleced3DFractalFile, out m_quatFractal, false);
        }
        Text filenameText = GameObject.Find("Text Filename").GetComponent<Text>();
        string fractalPath = m_quatFractal.FilePath;
        if (fractalPath.Length > 0 && filenameText.text != fractalPath)
        {
            filenameText.text = fractalPath;
        }

        int colorCount = m_quatFractal.MaxComputationStepCount;
        m_quatFractal.SinglePalette = new SinglePalette(m_quatFractal.FractalBackgroundColor, m_quatFractal.FractalColor);
        //int colorCount = 15;
        //m_quatFractal.SinglePalette = new SinglePalette(new Color(1, 0, 0, 1), new Color(0, 0, 1, 1));
        m_quatFractal.SliceColoringAlgorithm.ColorCount = colorCount;
        //m_quatFractal.FractalColor = Color.blue;
        //m_quatFractal.C = new System.Collections.Generic.List<SmartMathLibrary.Quaternion>();
        //m_quatFractal.C.Add(new SmartMathLibrary.Quaternion(-0.8, -0.5, 0.1, 0.1));
        //m_quatFractal.C.Add(SmartMathLibrary.Quaternion.Zero);
        //m_quatFractal.C.Add(new SmartMathLibrary.Quaternion(1, 0.1, -0.1, 0.1));
        m_quatFractal.MaximizeComputationArea();

        m_cutImage = GameObject.Find("RawImage Cut").GetComponent<RawImage>();

        m_editElevation = GameObject.Find("InputField Elevation").GetComponent<InputField>();
        m_editElevation.text = "0";

        OnDrawingPlaneChanged();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnButtonCompute()
    {
        double elevation = 0;
        try
        {
            elevation = double.Parse(m_editElevation.text);
        }
        catch
        { }

        RecomputeCutPlaneVectors();

        Vector3D elevationVector = Vector3D.Cross(m_planeXVector, m_planeYVector);
        elevationVector *= elevation;

        int textureSizeX = Mathf.FloorToInt(m_cutImage.rectTransform.rect.width);
        int textureSizeY = Mathf.FloorToInt(m_cutImage.rectTransform.rect.height);
        float currentTime = UnityEngine.Time.realtimeSinceStartup;
        //Texture2D texture = GetSliceTexture(m_quatFractal, textureSizeX, textureSizeY, m_planeXVector, m_planeYVector, elevationVector);
        float t1 = UnityEngine.Time.realtimeSinceStartup - currentTime;
        currentTime = UnityEngine.Time.realtimeSinceStartup;
        Texture2D textureCpp = m_quatFractal.GetSliceTextureCpp(textureSizeX, textureSizeY, m_planeXVector, m_planeYVector, elevationVector);
        float t2 = UnityEngine.Time.realtimeSinceStartup - currentTime;
        m_cutImage.texture = textureCpp;
    }

    private void RecomputeCutPlaneVectors()
    {
        if (m_cutPlane == ECutPlane.definedByNormal)
        {
            // defining of x and y vector in this plane
            if (Persistent.Instance.PlaneNormalVector.magnitude == 0)
            {
                m_planeXVector = new Vector3D(1, 0, 0);
                m_planeYVector = new Vector3D(0, 1, 0);
            }
            else
            {
                // determining from normal vector
                m_planeYVector = Vector3D.Cross(Persistent.Instance.PlaneNormalVector, new Vector3D(1, 0, 0));
                if (m_planeYVector.magnitude > 0)
                {
                    m_planeXVector = Vector3D.Cross(m_planeYVector, Persistent.Instance.PlaneNormalVector);
                }
                else
                {
                    m_planeXVector = Vector3D.Cross(new Vector3D(0, 1, 0), Persistent.Instance.PlaneNormalVector);
                    m_planeYVector = Vector3D.Cross(Persistent.Instance.PlaneNormalVector, m_planeXVector);
                }
                m_planeXVector.Normalize();
                m_planeYVector.Normalize();
            }
        }
    }

    public void OnDrawingPlaneChanged()
    {
        Dropdown dropdown = GameObject.Find("Dropdown Plane Selection").GetComponent<Dropdown>();
        m_cutPlane = (ECutPlane)dropdown.value;
        Vector3D normalVector = new Vector3D();
        switch (dropdown.value)
        {
        case 0: // xy
            {
                m_planeXVector = new Vector3D(1, 0, 0);
                m_planeYVector = new Vector3D(0, 1, 0);
                normalVector = Vector3D.Cross(m_planeXVector, m_planeYVector);
                ShowDisabledNormalVector(normalVector);
                break;
            }
        case 1: // yz
            {
                m_planeXVector = new Vector3D(0, 1, 0);
                m_planeYVector = new Vector3D(0, 0, 1);
                normalVector = Vector3D.Cross(m_planeXVector, m_planeYVector);
                ShowDisabledNormalVector(normalVector);
                break;
            }
        case 2: // zx
            {
                m_planeXVector = new Vector3D(0, 0, 1);
                m_planeYVector = new Vector3D(1, 0, 0);
                normalVector = Vector3D.Cross(m_planeXVector, m_planeYVector);
                ShowDisabledNormalVector(normalVector);
                break;
            }
        case 3:
            // plane defined by normal vector
            {
                new DoubleWidget(new Ref<double>(
                    () => Persistent.Instance.PlaneNormalVector.x,
                    v => { Persistent.Instance.SetNormalX(v); }),
                    GameObject.Find("InputField Normal X").GetComponent<InputField>());
                new DoubleWidget(new Ref<double>(
                    () => Persistent.Instance.PlaneNormalVector.y,
                    v => { Persistent.Instance.SetNormalY(v); }),
                    GameObject.Find("InputField Normal Y").GetComponent<InputField>());
                new DoubleWidget(new Ref<double>(
                    () => Persistent.Instance.PlaneNormalVector.z,
                    v => { Persistent.Instance.SetNormalZ(v); }),
                    GameObject.Find("InputField Normal Z").GetComponent<InputField>());

                SetEnabledNormalVector(true);
                break;
            }
        }
    }

    void ShowDisabledNormalVector(Vector3D vector)
    {
        new DoubleWidget(new Ref<double>(
                    () => vector.x,
                    v => { }),
                    GameObject.Find("InputField Normal X").GetComponent<InputField>());
        new DoubleWidget(new Ref<double>(
            () => vector.y,
            v => { }),
            GameObject.Find("InputField Normal Y").GetComponent<InputField>());
        new DoubleWidget(new Ref<double>(
            () => vector.z,
            v => { }),
            GameObject.Find("InputField Normal Z").GetComponent<InputField>());
        SetEnabledNormalVector(false);
    }

    void SetEnabledNormalVector(bool enabled)
    {
        GameObject.Find("InputField Normal X").GetComponent<InputField>().interactable = enabled;
        GameObject.Find("InputField Normal Y").GetComponent<InputField>().interactable = enabled;
        GameObject.Find("InputField Normal Z").GetComponent<InputField>().interactable = enabled;
    }

    public void OnButtonMainMenu()
    {
        MySceneManager.GoToScene("MainMenu");
    }

    public void OnButtonBack()
    {
        MySceneManager.GoToPreviousScene();
    }

    Texture2D GetSliceTexture(QuaternionFractal3D fractal, int width, int height, Vector3D axisX, Vector3D axisY, Vector3D pointZeroOnPlane)
    {
        axisX.Normalize();
        axisY.Normalize();
        Debug.Assert(Vector3D.Dot(axisX, axisY) == 0);
        Debug.Assert(axisX.magnitude == 1);
        Debug.Assert(axisY.magnitude == 1);

        Texture2D texture = new Texture2D(width, height);

        int paletteCount = fractal.SliceColoringAlgorithm.ColorCount;
        int maxIterations = paletteCount - 1;

        // clearing
        Color colorTransparent = new Color(0, 0, 0, 0);
        TextureUtils.FillWithColor(texture, colorTransparent);

        int minTextureSize = width < height ? width : height;
        double maxRadius = fractal.ComputationArea.GetMaxSize() / 2;

        double step = 2 * maxRadius / minTextureSize;
        int columnOffset = width < height ? 0 : (width - height) / 2;
        int rowOffset = width < height ? (height - width) / 2 : 0;
        int columnEnd = width - columnOffset;
        int rowEnd = height - rowOffset;
        double xStart = -maxRadius;
        double yStart = -maxRadius;
        for (int column = columnOffset; column < columnEnd; column++)
        {
            double x = xStart + (column - columnOffset) * step;
            for (int row = rowOffset; row < rowEnd; row++) 
            {
                double y = yStart + (row - rowOffset) * step;
                // recounting of plane coordinates into 3D coordinates
                Vector3D point = pointZeroOnPlane + axisX * x + axisY * y;
                int iterations = fractal.ComputeIterations(point.x, point.y, point.z, 0, maxIterations);
                texture.SetPixel(column, row, fractal.Palette.GetColor(iterations));
            }
        }

        texture.Apply();
        return texture;
    }

    QuaternionFractal3D m_quatFractal;
    InputField m_editElevation;
    Vector3D m_planeXVector;
    Vector3D m_planeYVector;
    ECutPlane m_cutPlane;
    RawImage m_cutImage;
}
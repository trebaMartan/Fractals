﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Collections.Generic;
using Exocortex.DSP;
using SmartMathLibrary;
using Assets.Scripts.Math;
using System.IO;
using Assets.Scripts.Utils;
using System.Linq;
using Assets.Scripts.Graphic;
using System.Xml.Serialization;
using System.Runtime.InteropServices;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System;
using OxOD;
using UnityEngine.Events;
using Assets.Scripts.Computation;
using System.Diagnostics;

public class Quaternion3DSceneScript : MonoBehaviour
{

    [Header("OxOD Reference")]
    public FileDialog fileDialog;

    enum EMappingType
    {
        mtDirect,
        mtSideDiagonal,
        mtAntisymetrical,
        mtMainAntisymetrical,
    };

    enum EMenuPanelType
    {
        Main,
        Parameters,
        HiddenParameters,
        AllParameters,
        CopyFromFractalSelection,
        ExportPNG,
        ExportSTL
    };

    public enum ParametersPanelType
    {
        CoefficientsC,
        CoefficientsC0,
        LimitArea,
        Computation,
        Mapping,
        Other,
        Undefined,
    };

    void Awake()
    {
        m_questionPanel = ModalPanel.Instance();
        m_questionPanel.gameObject.SetActive(false);
    }

    // Use this for initialization
    void Start()
    {
        FileUtils.CreateOrClearDirectory(Persistent.Get3DFractalsTempGraphicsDir());

        lastMousePos = initMousePos;

        m_camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        m_camera.farClipPlane = Constants.FarClippingPlane;
        m_camera.nearClipPlane = Constants.NearClippingPlane;

        m_sceneBaseObject = GameObject.Find(Fractal3DGraphic.BaseSceneObjectName);

        m_directionalLight = GameObject.Find("Directional Light").GetComponent<Light>();
        ResetLight();

        m_toggleShowAxis = GameObject.Find("Toggle Show Axes").GetComponent<Toggle>();
        m_toggleShowComputationArea = GameObject.Find("Toggle Show Limit Area").GetComponent<Toggle>();

        m_menuPanels[EMenuPanelType.Main] = m_panelMain = GameObject.Find("Panel Main");
        m_menuPanels[EMenuPanelType.Parameters] = GameObject.Find("Panel Parameters");
        m_menuPanels[EMenuPanelType.HiddenParameters] = GameObject.Find("Panel Hidden Parameters");
        m_menuPanels[EMenuPanelType.AllParameters] = GameObject.Find("Panel All Parameters");
        m_menuPanels[EMenuPanelType.CopyFromFractalSelection] = GameObject.Find("Panel Copy Parameters");
        m_menuPanels[EMenuPanelType.ExportPNG] = GameObject.Find("Panel Export Png");
        m_menuPanels[EMenuPanelType.ExportSTL] = GameObject.Find("Panel Export STL");


        m_propertyOnPanel["SinglePalette"] = ParametersPanelType.Other;
        m_propertyOnPanel["CompositePalette"] = ParametersPanelType.Other;
        m_propertyOnPanel["Color"] = ParametersPanelType.Other;
        m_propertyOnPanel["BackgroundColor"] = ParametersPanelType.Other;
        m_propertyOnPanel["C"] = ParametersPanelType.CoefficientsC;
        m_propertyOnPanel["CA"] = ParametersPanelType.CoefficientsC;
        m_propertyOnPanel["PowerCindex"] = ParametersPanelType.CoefficientsC;
        m_propertyOnPanel["C0"] = ParametersPanelType.CoefficientsC0;
        m_propertyOnPanel["C0A"] = ParametersPanelType.CoefficientsC0;
        m_propertyOnPanel["PowerC0index"] = ParametersPanelType.CoefficientsC0;
        m_propertyOnPanel["Name"] = ParametersPanelType.Other;
        m_propertyOnPanel["ColorCount"] = ParametersPanelType.Other;
        m_propertyOnPanel["MaxComputationStepCount"] = ParametersPanelType.Computation;
        m_propertyOnPanel["MinCubeSize"] = ParametersPanelType.Computation;
        m_propertyOnPanel["CoefficientBeforePower"] = ParametersPanelType.CoefficientsC;
        m_propertyOnPanel["CoefficientAfterPower"] = ParametersPanelType.CoefficientsC;
        m_propertyOnPanel["CoefficientC0BeforePower"] = ParametersPanelType.CoefficientsC0;
        m_propertyOnPanel["CoefficientC0AfterPower"] = ParametersPanelType.CoefficientsC0;
        m_propertyOnPanel["CubeFineness"] = ParametersPanelType.Computation;
        m_propertyOnPanel["LimitArea"] = ParametersPanelType.LimitArea;
        m_propertyOnPanel["LimitAreaType"] = ParametersPanelType.LimitArea;
        m_propertyOnPanel["ComputationArea"] = ParametersPanelType.Computation;
        m_propertyOnPanel["Vec2QuatMapping"] = ParametersPanelType.Mapping;
        m_propertyOnPanel["ComputeDelegateType"] = ParametersPanelType.Computation;
        m_propertyOnPanel["GraphicsCompute"] = ParametersPanelType.Computation;


        m_textAllParameters = GameObject.Find("Text All Parameters");
        m_imageFractalColor = GameObject.Find("Image Fractal Color").GetComponent<RawImage>();
        m_imageFractalBackgroundColor = GameObject.Find("Image Fractal Background Color").GetComponent<RawImage>();

        m_filenameText = GameObject.Find("Text Filename").GetComponent<Text>();

        m_buttonCopy = GameObject.Find("Button Copy");
        HandleButtonCopyVisibility();

        m_buttonUndo = GameObject.Find("Button Undo").GetComponent<Button>();
        m_buttonRedo = GameObject.Find("Button Redo").GetComponent<Button>();

        m_copyFromFractalDropwown = GameObject.Find("Dropdown Fractal Numbers").GetComponent<Dropdown>();
        m_dropdownC = GameObject.Find("Dropdown C").GetComponent<Dropdown>();

        m_loadPath = Persistent.Get3DFractalsDir();

        InitParametersToggles();

        InitFractal();

        if (GetActiveFractal().ComputeDelegateType == QuaternionDelegateType.Julia &&
            Persistent.Instance.LastSelectedQuat3DParametersType == ParametersPanelType.CoefficientsC0)
        {
            Persistent.Instance.LastSelectedQuat3DParametersType = ParametersPanelType.Computation;
            SetParameterToggle(Persistent.Instance.LastSelectedQuat3DParametersType, true);
        }
        else
        {
            SetParameterToggle(Persistent.Instance.LastSelectedQuat3DParametersType, true);
        }

        ActivateMenuPanel(EMenuPanelType.Main);

        new BoolWidget(new Ref<bool>(() => ShowAxes, v => { ShowAxes = v; }), UIUtils.FindComponentInChildren<Toggle>(m_panelMain, "Toggle Show Axes"));
        new BoolWidget(new Ref<bool>(() => ShowComputationBoundingBox, v => { ShowComputationBoundingBox = v; }), UIUtils.FindComponentInChildren<Toggle>(m_panelMain, "Toggle Show Limit Area"));
        new BoolWidget(new Ref<bool>(() => ShowOnlyOneFractal, v => { ShowOnlyOneFractal = v; }), UIUtils.FindComponentInChildren<Toggle>(m_panelMain, "Toggle One Displayed Fractal"));
        OnButtonResetView();

        HandleFractalDisplayColors();

        m_connectionsAreComputed = false;
        m_computingConnections = false;
    }

    void OnApplicationFocus(bool hasFocus)
    {
        //isPaused = !hasFocus;
        lastMousePos = initMousePos;
    }

    public static Vector3 GetPredefinedCameraPosition(int position)
    {
        if (position < 0 || position > 3)
        {
            return Vector3.zero;
        }
        switch (position)
        {
        case 0:
        default:
            return new Vector3(0, 0, -3);
        case 1:
            if (Persistent.Instance.AxisSystem == Persistent.AxisSystemType.Left)
            {
                return new Vector3(-3, 0, 0);
            }
            return new Vector3(3, 0, 0);
        case 2:
            if (Persistent.Instance.AxisSystem == Persistent.AxisSystemType.Left)
            {
                return new Vector3(0, -3, 0);
            }
            return new Vector3(0, 3, 0);
        case 3:
            if (Persistent.Instance.AxisSystem == Persistent.AxisSystemType.Left)
            {
                return new Vector3(3, -3, -3);
            }
            return new Vector3(-3, 3, -3);
        }
    }

    public static void SetPredefinedCameraPosition(Transform transform, int position)
    {
        if (position < 0 || position > 3)
        {
            return;
        }
        transform.position = GetPredefinedCameraPosition(position);
        transform.LookAt(new Vector3(0, 0, 0));
        switch (position)
        {
        case 0:
            break;
        case 1:
            transform.Rotate(new Vector3(0, 0, 1), 90);
            break;
        case 2:
            transform.Rotate(new Vector3(0, 0, 1), -90);
            break;
        case 3:
            break;
        }
    }

    // Update is called once per frame
    void Update()
    {

        bool isStringInputFieldFocused = UIUtils.IsStringInputFieldFocused();
        bool isInputFieldFocused = UIUtils.IsInputFieldFocused();

        GameObject textProgress = GameObject.Find("Text Progress");
        if (textProgress)
        {
            if (Persistent.Instance.Progress == 100 || Persistent.Instance.Progress == 0)
            {
                textProgress.GetComponent<Text>().text = "";
            }
            else
            {
                textProgress.GetComponent<Text>().text = Persistent.Instance.Progress.ToString();
            }
        }

        if (m_computingConnections)
        {
            Text statusTaxt = GameObject.Find("Text Status").GetComponent<Text>();
            switch (m_computeConnectionsPhase)
            {
            case ComputeConnectionsPhase.GettingBoxes:
                {
                    statusTaxt.text = "Getting fractal shape";
                    m_computeConnectionsPhase = ComputeConnectionsPhase.GettingBoxes2;
                }
                break;
            case ComputeConnectionsPhase.GettingBoxes2:
                {
                    Fractal3DGraphic graphic = GetActiveFractal().GetGraphic();
                    m_intBoxes = graphic.GetGraphicIntBoxes();
                    m_fractalBounds = graphic.GetBounds();
 
 
                    m_computeConnectionsPhase = ComputeConnectionsPhase.SortingBoxes;
                }
                break;
            case ComputeConnectionsPhase.SortingBoxes:
                {
                    statusTaxt.text = "Sorting boxes";
                    m_computeConnectionsPhase = ComputeConnectionsPhase.SortingBoxes2;
                }
                break;
            case ComputeConnectionsPhase.SortingBoxes2:
                {
                    // setrideni boxu
                    m_intBoxes.Sort(m_comparer);
                    m_computeConnectionsPhase = ComputeConnectionsPhase.ComputingConnections;
                }
                break;
            case ComputeConnectionsPhase.ComputingConnections:
                {
                    statusTaxt.text = "Computing connections";
                    m_zConnections = new List<Vector3Int>();
                    m_xConnections = new List<Vector3Int>();
                    m_yConnections = new List<Vector3Int>();
                    m_connections111 = new List<Vector3Int>();
                    m_connections11m1 = new List<Vector3Int>();
                    m_connections1m11 = new List<Vector3Int>();
                    m_connections1m1m1 = new List<Vector3Int>();
                    sw.Reset();
                    sw.Start();
                    m_finishedConnectingThreads = 0;
                    int j = 0;
                    for (int i = 0; i < CONNECTING_THREAD_COUNT; i++)
                    {
                        new System.Threading.Thread(() =>
                        {
                            MakeDiagonalConnections(j++);
                        }).Start();
                    }
                    m_computeConnectionsPhase = ComputeConnectionsPhase.ComputingConnections2;
                }
                break;
            case ComputeConnectionsPhase.ComputingConnections2:
                {
                    statusTaxt.text = String.Format("Computing connections: {0:0.00}%", 100 * m_computedConnectionsProgressRowCount / (float)m_totalConnectionsProgressRowCount);
                    if (m_finishedConnectingThreads == CONNECTING_THREAD_COUNT)
                    {
                        // all threads are finished
                        m_computeConnectionsPhase = ComputeConnectionsPhase.SortingConnections;
                        sw.Stop();
                    }
                }
                break;
            case ComputeConnectionsPhase.SortingConnections:
                {
                    statusTaxt.text = "Sorting connections";
                    m_computeConnectionsPhase = ComputeConnectionsPhase.SortingConnections2;
                }
                break;
            case ComputeConnectionsPhase.SortingConnections2:
                {
                    SortConnections();
                    m_computeConnectionsPhase = ComputeConnectionsPhase.CreatingConnections;
                }
                break;
            case ComputeConnectionsPhase.CreatingConnections:
                {
                    statusTaxt.text = "Creating connections";
                    m_computeConnectionsPhase = ComputeConnectionsPhase.CreatingConnections2;
                }
                break;
            case ComputeConnectionsPhase.CreatingConnections2:
                {
                    ConnectCubes();
                    Fractal3DGraphic graphic = GetActiveFractal().GetGraphic();
                    graphic.AddZSideConnections(m_zConnectionCubes);
                    graphic.AddXSideConnections(m_xConnectionCubes);
                    graphic.AddYSideConnections(m_yConnectionCubes);

                    graphic.AddCorner111Connections(m_connections111);
                    graphic.AddCorner11m1Connections(m_connections11m1);
                    graphic.AddCorner1m11Connections(m_connections1m11);
                    graphic.AddCorner1m1m1Connections(m_connections1m1m1);
                    statusTaxt.text = "";
                    statusTaxt.text = String.Format("Elapsed={0}", sw.Elapsed);
                    m_computeConnectionsPhase = ComputeConnectionsPhase.SaveSTL;
                }
                break;
            case ComputeConnectionsPhase.SaveSTL:
                {
                    SaveSTL();
                    m_computingConnections = false;
                }
                break;
            }
        }

        if (m_computationStruct != null)
        {
            if (m_computationStruct.IsComputationFinished)
            {
                m_computationStruct.IsComputationFinished = false;
                m_computationStruct.PerformAfterComputation();
            }
        }


        if (!isStringInputFieldFocused && !m_isPanelHidden && Input.GetKeyDown(KeyCode.H))
        {
            HideUI();
            return;
        }

        if (m_isPanelHidden && Input.anyKeyDown && UnityEngine.Time.realtimeSinceStartup > m_hideTime + MinHideDelay)
        {
            UnhideUI();
            return;
        }

        if (Input.GetKeyDown(KeyCode.Z) && (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)))
        {
            // undo
            OnUndo();
            return;
        }
        if (Input.GetKeyDown(KeyCode.Y) && (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)))
        {
            // redo
            OnRedo();
            return;
        }

        if (!isStringInputFieldFocused)
        {
            if (Input.GetKeyDown(KeyCode.C))
            {
                if (GetActivatedMenuPanel() == EMenuPanelType.Main)
                {
                    OnButtonCompute();
                }
                else if (GetActivatedMenuPanel() == EMenuPanelType.Parameters)
                {
                    OnButtonOkAndCompute();
                }
            }

            if (Input.GetKeyDown(KeyCode.Q))
            {
                // plane xy
                SetPredefinedCameraPosition(m_camera.transform, 0);
                UpdateAxesCenterAndBoundingBox();
                AdjustZoom();
            }

            if (Input.GetKeyDown(KeyCode.W))
            {
                SetPredefinedCameraPosition(m_camera.transform, 1);
                UpdateAxesCenterAndBoundingBox();
                AdjustZoom();
            }
            if (Input.GetKeyDown(KeyCode.E))
            {
                SetPredefinedCameraPosition(m_camera.transform, 2);
                UpdateAxesCenterAndBoundingBox();
                AdjustZoom();
            }
            if (Input.GetKeyDown(KeyCode.R))
            {
                SetPredefinedCameraPosition(m_camera.transform, 3);
                UpdateAxesCenterAndBoundingBox();
                AdjustZoom();
            }

            if (Input.GetKeyDown(KeyCode.A))
            {
                m_toggleShowAxis.isOn = !m_toggleShowAxis.isOn;
            }

            if (Input.GetKeyDown(KeyCode.L))
            {
                m_toggleShowComputationArea.isOn = !m_toggleShowComputationArea.isOn;
            }
        }

        bool smallZoomSensitivity = false;
        if (Input.GetKey(KeyCode.LeftShift))
        {
            smallZoomSensitivity = true;
        }
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            OnZoomIn(smallZoomSensitivity);
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            OnZoomOut(smallZoomSensitivity);
        }

        int nbTouches = Input.touchCount;

        if (nbTouches > 0)
        {
            for (int i = 0; i < nbTouches; i++)
            {
                Touch touch = Input.GetTouch(i);

                TouchPhase phase = touch.phase;

                switch (phase)
                {
                case TouchPhase.Began:
                    print("New touch detected at position " + touch.position + " , index " + touch.fingerId);
                    break;
                case TouchPhase.Moved:
                    print("Touch index " + touch.fingerId + " has moved by " + touch.deltaPosition);
                    break;
                case TouchPhase.Stationary:
                    print("Touch index " + touch.fingerId + " is stationary at position " + touch.position);
                    break;
                case TouchPhase.Ended:
                    print("Touch index " + touch.fingerId + " ended at position " + touch.position);
                    break;
                case TouchPhase.Canceled:
                    print("Touch index " + touch.fingerId + " cancelled");
                    break;
                }
            }
        }

        Vector3 currentMousePos = Input.mousePosition;
        Vector2 difMousePos = currentMousePos - lastMousePos;
        Vector2 mouseChange = lastMousePos == initMousePos ? Vector2.zero : new Vector2(difMousePos.x, difMousePos.y);
        lastMousePos = currentMousePos;

        if (mouseChange == Vector2.zero
            && !(Input.GetMouseButton(1) || Input.GetMouseButton(0) || Input.GetMouseButton(2)))
        {
            lastMousePos = initMousePos;
        }
        if (mouseChange != Vector2.zero)
        {
            if (Input.GetMouseButton(1) && Input.GetMouseButton(0))
            {
                RotateLight(mouseChange.x, mouseChange.y);
                return;
            }

            if (Input.GetMouseButton(0))
            {
                float borderLimit = 0.2f;
                float borderX = Screen.width * borderLimit;
                float borderY = Screen.height * borderLimit;
                bool dxIsBigger = Mathf.Abs(mouseChange.x) > Mathf.Abs(mouseChange.y);
                if (dxIsBigger && currentMousePos.y < borderY)
                {
                    RotateAroundScreenAxe(-mouseChange.x);
                    return;
                }
                if (dxIsBigger && currentMousePos.y > Screen.height - borderY)
                {
                    RotateAroundScreenAxe(mouseChange.x);
                    return;
                }
                if (!dxIsBigger && currentMousePos.x < borderX)
                {
                    RotateAroundScreenAxe(mouseChange.y);
                    return;
                }
                if (!dxIsBigger && currentMousePos.x > Screen.width - borderX)
                {
                    RotateAroundScreenAxe(-mouseChange.y);
                    return;
                }

                // rotate object or camera
                Rotate(mouseChange.x, mouseChange.y);
                return;
            }

            // panning
            if (Input.GetMouseButton(2))
            {
                Panning(mouseChange.x, mouseChange.y);
                return;
            }

            if (Input.GetMouseButton(1))
            {
                // looking around mode
                if (!m_lookingAround)
                {
                    m_startLookingAroundRotation = m_camera.transform.rotation;
                }
                m_lookingAround = true;
                RotateCameraAround(mouseChange.x, mouseChange.y);
                return;
            }
        }

        if (m_lookingAround && !Input.GetMouseButton(1))
        {
            m_camera.transform.rotation = m_startLookingAroundRotation;
            m_lookingAround = false;
        }

        if (m_menuPanels.ContainsKey(EMenuPanelType.HiddenParameters) && IsActiveMenu(EMenuPanelType.HiddenParameters))
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                ActivateMenuPanel(EMenuPanelType.Parameters);
                return;
            }
        }

        if (IsActiveMenu(EMenuPanelType.HiddenParameters) || IsActiveMenu(EMenuPanelType.Main)
            || IsActiveMenu(EMenuPanelType.ExportPNG) || IsActiveMenu(EMenuPanelType.ExportSTL))
        {
            if (Input.GetKey(KeyCode.LeftAlt) && Input.GetKeyDown(KeyCode.UpArrow))
            {
                OnZoomIn(smallZoomSensitivity);
                return;
            }
            if (Input.GetKey(KeyCode.LeftAlt) && Input.GetKeyDown(KeyCode.DownArrow))
            {
                OnZoomOut(smallZoomSensitivity);
                return;
            }
            float panningKoef = 0.1f;
            if (Input.GetKey(KeyCode.LeftControl))
            {
                panningKoef *= 0.1f;
            }
            if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.UpArrow))
            {
                Panning(0, Screen.height * panningKoef);
                return;
            }
            if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.DownArrow))
            {
                Panning(0, -Screen.height * panningKoef);
                return;
            }
            if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.LeftArrow))
            {
                Panning(-Screen.width * panningKoef, 0);
                return;
            }
            if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.RightArrow))
            {
                Panning(Screen.width * panningKoef, 0);
                return;
            }

            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.DownArrow))
            {
                RotateLightVertical(-10);
                return;
            }
            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.UpArrow))
            {
                RotateLightVertical(10);
                return;
            }
            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.LeftArrow))
            {
                RotateLightHorizontal(10);
                return;
            }
            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.RightArrow))
            {
                RotateLightHorizontal(-10);
                return;
            }

            if (!isInputFieldFocused)
            {
                if (Input.GetKeyDown(KeyCode.DownArrow))
                {
                    OnButtonRotateDown();
                }
                if (Input.GetKeyDown(KeyCode.UpArrow))
                {
                    OnButtonRotateUp();
                }
                if (Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    OnButtonRotateLeft();
                }
                if (Input.GetKeyDown(KeyCode.RightArrow))
                {
                    OnButtonRotateRight();
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (IsActiveMenu(EMenuPanelType.Main))
            {
                OnButtonBack();
                return;
            }
            if (IsActiveMenu(EMenuPanelType.Parameters))
            {
                OnButtonOk();
                return;
            }
            if (IsActiveMenu(EMenuPanelType.HiddenParameters))
            {
                ActivateMenuPanel(EMenuPanelType.Parameters);
                return;
            }
            if (IsActiveMenu(EMenuPanelType.ExportPNG))
            {
                OnButtonExportBack();
                return;
            }
            if (IsActiveMenu(EMenuPanelType.ExportSTL))
            {
                OnButtonExportSTLBack();
                return;
            }
        }
        int pressedNumber;
        if (IsActiveMenu(EMenuPanelType.Main) &&
            IsDownNumberKey(out pressedNumber))
        {
            ActivateFractal(pressedNumber - 1);
        }

        //if (Input.GetKeyDown(KeyCode.O) && Input.GetKey(KeyCode.LeftControl))
        //{
        //    // open 
        //    OnButtonLoad();
        //    return;
        //}

        //if (Input.GetKeyDown(KeyCode.P))
        //{
        //    Ray ray = m_camera.ScreenPointToRay(Input.mousePosition);
        //    RaycastHit hit;

        //    if (Physics.Raycast(ray, out hit))
        //    {
        //        Transform objectHit = hit.transform;

        //        // Do something with the object that was hit by the raycast.
        //        //Text text = GameObject.Find("Text position").GetComponent<Text>();
        //        Vector3 pos = objectHit.position;
        //        //Vector3 posTr = pos;
        //        //posTr = GetBaseObject().transform.InverseTransformPoint(pos);//GetActiveFractal().GetBaseObject().transform.InverseTransformPoint(pos);
        //        //text.text = string.Format("X:{0:0.000}, Y:{1:0.000}, Z:{2:0.000}", posTr.x, posTr.y, posTr.z);

        //        // transformation to original fractal coordinates
        //        UnityEngine.Quaternion fractRotation = GetActiveFractal().GetGraphic().GetBaseObject().transform.localRotation;
        //        UnityEngine.Quaternion fractRotationInverse = UnityEngine.Quaternion.Inverse(fractRotation);
        //        Vector3 posOriginal = fractRotationInverse * pos;

        //        Text textOriginal = GameObject.Find("Text position original").GetComponent<Text>();
        //        textOriginal.text = string.Format("Orig: X:{0:0.000}, Y:{1:0.000}, Z:{2:0.000}", posOriginal.x, posOriginal.y, posOriginal.z);
        //    }
        //}
    }

    // public properties
    bool ShowAxes
    {
        get { return m_showAxes; }
        set
        {
            m_showAxes = value;
            if (m_showAxes)
            {
                CreateAxisArrowsAccordingToComputingArea();
            }
            else
            {
                if (m_axes != null && m_axes.GetBaseObject() != null)
                {
                    m_axes.GetBaseObject().SetActive(false);
                    Destroy(m_axes.GetBaseObject());
                }
            }
        }
    }

    bool ShowComputationBoundingBox
    {
        get { return m_showComputationBoundingBox; }
        set
        {
            m_showComputationBoundingBox = value;
            if (m_showComputationBoundingBox)
            {
                CreateComputationAreaObjectAccordingToComputingArea();
                CreateLightPointers();
            }
            else
            {
                if (m_computationBoundingBox != null && m_computationBoundingBox.GetBaseObject() != null)
                {
                    m_computationBoundingBox.GetBaseObject().SetActive(false);
                    Destroy(m_computationBoundingBox.GetBaseObject());
                }
                if (m_lightPointers != null && m_lightPointers.GetBaseObject() != null)
                {
                    m_lightPointers.GetBaseObject().SetActive(false);
                    Destroy(m_lightPointers.GetBaseObject());
                }
            }
        }
    }

    bool ShowOnlyOneFractal
    {
        get { return m_showOnlyOneFractal; }
        set
        {
            m_showOnlyOneFractal = value;
            if (m_showOnlyOneFractal)
            {
                DisplayOnlyOneFractal();
            }
        }
    }


    ////////////// public functions ///////////////////
    public void OnButtonSave()
    {
        if (GetActiveFractal().FilePath.Length == 0)
        {
            OnButtonSaveAs();
            return;
        }
        // eventual replace of appendix
        string path = (string)GetActiveFractal().FilePath.Clone();
        string oldExtension = Persistent.GetExtension(Persistent.SaveExtension.fractal3DDefinitionExtension);
        string newExtension = Persistent.GetExtension(Persistent.SaveExtension.fractal3DZipExtension);
        GetActiveFractal().FilePath = path.Replace(oldExtension, newExtension);
        GetActiveFractal().Save(GetActiveFractal().FilePath);
        Persistent.Instance.Seleced3DFractalFile = GetActiveFractal().FilePath;

        MakeScreenshots();
    }

    public void OnButtonSaveAs()
    {
        string saveDir = Persistent.Get3DFractalsDir();
        string extension = Persistent.GetExtensionWithDot(Persistent.SaveExtension.fractal3DZipExtension);

        fileDialog.onResultDelegate = (string path) =>
        {
            UnityAction writeAction = () =>
            {
                GetActiveFractal().Save(path);
                Persistent.Instance.Seleced3DFractalFile = GetActiveFractal().FilePath;
                MakeScreenshots();
            };
            if (File.Exists(path) && path != GetActiveFractal().FilePath)
            {
                OverwriteConfirmation(writeAction);
            }
            else
            {
                writeAction();
            }
        };
        string saveName = GetActiveFractal().FilePath.Length > 0 ? Path.GetFileName(GetActiveFractal().FilePath) : GetNewSaveName(saveDir);
        StartCoroutine(fileDialog.Save(Path.Combine(saveDir, saveName), extension, "Save fractal"));
    }

    public void OverwriteConfirmation(UnityAction overwriteAction)
    {
        m_questionPanel.SetButtonText(1, "Yes");
        m_questionPanel.SetButtonText(2, "No");
        m_questionPanel.Choice("Do you want to overwrite existing file?", overwriteAction);
        m_questionPanel.gameObject.SetActive(true);
    }

    public void OnButtonSaveNew()
    {
        string saveDir = Persistent.Get3DFractalsDir();
        string extension = Persistent.GetExtensionWithDot(Persistent.SaveExtension.fractal3DZipExtension);

        fileDialog.onResultDelegate = (string path) =>
        {
            GetActiveFractal().Save(path);
            Persistent.Instance.Seleced3DFractalFile = GetActiveFractal().FilePath;
            MakeScreenshots();
        };
        string saveName = GetNewSaveName(saveDir);
        StartCoroutine(fileDialog.Save(Path.Combine(saveDir, saveName), extension, "Save fractal"));
    }

    public void OnButtonLoad()
    {
        string extensions = Persistent.GetExtensionWithDot(Persistent.SaveExtension.fractal3DZipExtension);

        fileDialog.onResultDelegate = (string path) =>
        {
            m_loadPath = path;
            QuaternionFractal3D fractal;
            ResetCameraAndSceneBaseObject();
            if (QuaternionFractal3D.Load(path, out fractal, true))
            {
                ClearScene();
                SetFractal(m_activeFractalId, fractal);
                Persistent.Instance.Seleced3DFractalFile = path;
            }
            OnButtonResetView();
            if (EventSystem.current.currentSelectedGameObject == null)
            {
                UnityEngine.Debug.Log("Reselecting first input");
                EventSystem.current.SetSelectedGameObject(EventSystem.current.firstSelectedGameObject);
            }
            HandleFractalDisplayColors();
        };
        StartCoroutine(fileDialog.Open(m_loadPath, extensions, "Load 3D Fractal"));
    }

    public void OnButtonConvert()
    {
        GetActiveFractal().GetGraphic().Clear(true, false);
        bool loadGraphic = true;
        bool recursive = true;
        OtherUtils.Convert3DSaves(Persistent.Get3DFractalsDir(), recursive, loadGraphic);
    }

    public void OnButtonCompute()
    {
        ResetCameraAndSceneBaseObject();
        if (GetActiveFractal() == null)
        {
            return;
        }

        m_computationStruct = new _3DComputationStruct(GetActiveFractal());

        // starting of computation
        m_computationStruct.IsJustComputed = true;
        m_computationStruct.IsComputationFinished = false;
        m_computationStruct.PerformAfterComputation = () =>
        {
            // creation of graphic from boxes
            QuaternionFractal3D fractal = m_computationStruct.Fractal as QuaternionFractal3D;
            fractal.GetGraphic().InitFromList(m_computationStruct.BoxesCpp, fractal.MinCubeSize);
            fractal.GetGraphic().ComputationHash = fractal.GetComputationParametersHash();
            fractal.GetGraphic().GraphicsHash = fractal.GetGraphicParametersHash();
            fractal.GetGraphic().SavePath = m_computationStruct.GraphicSaveFile;

            // some statistic
            float VBoxes = fractal.GetVolume(m_computationStruct.BoxesCpp);
            float boxVolume = (float)Math.Pow(fractal.MinCubeSize, 3);
            fractal.GetGraphic().CubesCount = (int)(VBoxes / boxVolume);
            fractal.GetGraphic().LinesCount = m_computationStruct.BoxesCpp.Count;

            // update of interface
            //if (GameObject.Find("Text cubes"))
            //{
            //    Text text = GameObject.Find("Text cubes").GetComponent<Text>();
            //    text.text = string.Format("boxes:{0} lines:{1}", GetActiveFractal().GetGraphic().CubesCount,
            //        //GetActiveFractal().InsideCubesCount, GetActiveFractal().CubesCount - GetActiveFractal().InsideCubesCount,
            //        GetActiveFractal().GetGraphic().LinesCount);
            //}

            UpdateAxesCenterAndBoundingBox();
            SetEnabledComputation(true);
            m_computationStruct.IsJustComputed = false;
        };

        SetEnabledComputation(false);
        m_computationStruct.ComputeAsynchronous();

        //new System.Threading.Thread(() =>
        //{
        //    GetActiveFractal().ComputeFractal(true);

        //    if (GameObject.Find("Text cubes"))
        //    {
        //        Text text = GameObject.Find("Text cubes").GetComponent<Text>();
        //        text.text = string.Format("boxes:{0} lines:{1}", GetActiveFractal().GetGraphic().CubesCount,
        //            //GetActiveFractal().InsideCubesCount, GetActiveFractal().CubesCount - GetActiveFractal().InsideCubesCount,
        //            GetActiveFractal().GetGraphic().LinesCount);
        //    }

        //    UpdateAxesCenterAndBoundingBox();
        //}).Start();

        //GetActiveFractal().ComputeFractal(true);

        //if (GameObject.Find("Text cubes"))
        //{
        //    Text text = GameObject.Find("Text cubes").GetComponent<Text>();
        //    text.text = string.Format("boxes:{0} lines:{1}", GetActiveFractal().GetGraphic().CubesCount,
        //        //GetActiveFractal().InsideCubesCount, GetActiveFractal().CubesCount - GetActiveFractal().InsideCubesCount,
        //        GetActiveFractal().GetGraphic().LinesCount);
        //}

        //UpdateAxesCenterAndBoundingBox();
    }

    private void SetEnabledComputation(bool enabled)
    {
        GameObject buttonCompute = GameObject.Find("Button Compute");
        if (buttonCompute != null)
        {
            buttonCompute.GetComponent<Button>().interactable = enabled;
        }
    }

    public void OnButtonGoToExportPng()
    {
        ActivateMenuPanel(EMenuPanelType.ExportPNG);

        InitPredefinedImageResolutions();
    }

    public void OnButtonGoToExportSTL()
    {
        ActivateMenuPanel(EMenuPanelType.ExportSTL);
        CountBiggestConnectedPart();
    }

    private void CountBiggestConnectedPart()
    {
        //zxc
    }

    public void OnButtonExportSTLBack()
    {
        GetActiveFractal().GetGraphic().RemoveDiagonalConnections();
        m_computingConnections = false;
        m_connectionsAreComputed = false;
        Text statusTaxt = GameObject.Find("Text Status").GetComponent<Text>();
        statusTaxt.text = "";
        ActivateMenuPanel(EMenuPanelType.Main);
    }

    public void OnButtonExportSTL()
    {
        if (m_connectionsAreComputed)
        {
            SaveSTL();
            return;
        }
        if (GetActiveFractal() != null && GetActiveFractal().GetGraphic() != null)
        {
            GameObject baseObject = GetActiveFractal().GetGraphic().GetBaseObject();
            float minCubeSize = GetActiveFractal().GetGraphic().MinCubeSize;

            m_computingConnections = true;
            m_computeConnectionsPhase = ComputeConnectionsPhase.GettingBoxes;
            EnableExportButton(false);
        }
    }

    private void EnableExportButton(bool enable)
    {
        GameObject.Find("Button Export STL").GetComponent<Button>().interactable = enable;
    }

    void SaveSTL()
    {
        string saveDir = Persistent.Instance.Save3DSTLDirectory;
        string extension = ".stl";
        fileDialog.onResultDelegate = (string path) =>
        {
            UnityAction writeAction = () =>
            {
                GameObject baseObject = GetActiveFractal().GetGraphic().GetBaseObject();
                bool asASCII = false;
                bool success = STL.Export(baseObject, path, asASCII);
                if (success)
                {
                    UnityEngine.Debug.Log("Fractal was exported to binary based STL file." + System.Environment.NewLine + path);
                }

                // record saving path                
                Persistent.Instance.Save3DSTLDirectory = Path.GetDirectoryName(path);
            };
            if (File.Exists(path))
            {
                OverwriteConfirmation(writeAction);
            }
            else
            {
                writeAction();
            }
        };
        string saveName = "";
        if (GetActiveFractal().FilePath.Length > 0)
        {
            saveName = Path.GetFileNameWithoutExtension(GetActiveFractal().FilePath)
                + String.Format("_{0}_{1}", Persistent.Instance.Exported3dScreenshotWidth, Persistent.Instance.Exported3dScreenshotHeight)
                + extension;
        }
        else
        {
            saveName = extension;
        }
        StartCoroutine(fileDialog.Save(Path.Combine(saveDir, saveName), extension, "Save fractal"));
        EnableExportButton(true);
    }

    private void MakeDiagonalConnections(int threadNumber)
    {
        // this function will add connection between diagonally connected cubes so STL model will be connected on side diagonal of cubes

        Fractal3DGraphic graphic = GetActiveFractal().GetGraphic();

        float minCubeSize = graphic.MinCubeSize;
        // transofmation of bounds into center of cubes
        m_fractalBounds.Expand(-minCubeSize / 2);
        Vector3 minCenter = m_fractalBounds.min;
        Vector3 maxCenter = m_fractalBounds.max;
        Vector3Int minIntPosition = new Vector3Int();
        Vector3Int maxIntPosition = new Vector3Int();

        graphic.TransformToIntCoordinates(minCenter, ref minIntPosition);
        graphic.TransformToIntCoordinates(maxCenter, ref maxIntPosition);
        if (minIntPosition.x > maxIntPosition.x)
        {
            int i = minIntPosition.x;
            minIntPosition.x = maxIntPosition.x;
            maxIntPosition.x = i;
        }
        if (minIntPosition.y > maxIntPosition.y)
        {
            int i = minIntPosition.y;
            minIntPosition.y = maxIntPosition.y;
            maxIntPosition.y = i;
        }
        if (minIntPosition.z > maxIntPosition.z)
        {
            int i = minIntPosition.z;
            minIntPosition.z = maxIntPosition.z;
            maxIntPosition.z = i;
        }

        m_totalConnectionsProgressRowCount = maxIntPosition.x - minIntPosition.x + 1;
        m_computedConnectionsProgressRowCount = 0;
        if (CONNECTING_THREAD_COUNT == 1)
        {
            for (int x = minIntPosition.x; x <= maxIntPosition.x && m_computingConnections; x++)
            {
                for (int y = minIntPosition.y; y < maxIntPosition.y; y++)
                {
                    for (int z = minIntPosition.z; z < maxIntPosition.z; z++)
                    {
                        bool xyz = IsCubeInFractal(x, y, z, m_intBoxes);
                        bool x1yz = IsCubeInFractal(x + 1, y, z, m_intBoxes);
                        bool xy1z = IsCubeInFractal(x, y + 1, z, m_intBoxes);
                        bool x1y1z = IsCubeInFractal(x + 1, y + 1, z, m_intBoxes);
                        if ((xyz && x1y1z || x1yz && xy1z) && CountCubes(xyz, x1yz, xy1z, x1y1z) < 4)
                        {
                            m_zConnections.Add(new Vector3Int(x, y, z));
                        }
                        bool xyz1 = IsCubeInFractal(x, y, z + 1, m_intBoxes);
                        bool x1yz1 = IsCubeInFractal(x + 1, y, z + 1, m_intBoxes);
                        if ((xyz && x1yz1 || x1yz && xyz1) && CountCubes(xyz, x1yz, xyz1, x1yz1) < 4)
                        {
                            m_yConnections.Add(new Vector3Int(x, y, z));
                        }
                        bool xy1z1 = IsCubeInFractal(x, y + 1, z + 1, m_intBoxes);
                        if ((xyz && xy1z1 || xy1z && xyz1) && CountCubes(xyz, xy1z, xyz1, xy1z1) < 4)
                        {
                            m_xConnections.Add(new Vector3Int(x, y, z));
                        }
                        if (xyz)
                        {
                            // test of corner diagonal connection
                            bool x1y1z1 = IsCubeInFractal(x + 1, y + 1, z + 1, m_intBoxes);
                            if (x1y1z1 && !xy1z && !xyz1 && !xy1z1 && !x1yz && !x1y1z && !x1yz1)
                            {
                                m_connections111.Add(new Vector3Int(x, y, z));
                            }
                            bool x1ym1z1 = IsCubeInFractal(x + 1, y - 1, z + 1, m_intBoxes);
                            bool xym1z = IsCubeInFractal(x, y - 1, z, m_intBoxes);
                            bool x1ym1z = IsCubeInFractal(x + 1, y - 1, z, m_intBoxes);
                            if (x1ym1z1 && !xyz1 && !x1yz1 && !x1yz && !xym1z && !x1ym1z && !IsCubeInFractal(x, y - 1, z + 1, m_intBoxes))
                            {
                                m_connections1m11.Add(new Vector3Int(x, y - 1, z));
                            }
                            bool x1y1zm1 = IsCubeInFractal(x + 1, y + 1, z - 1, m_intBoxes);
                            bool xyzm1 = IsCubeInFractal(x, y, z - 1, m_intBoxes);
                            bool x1yzm1 = IsCubeInFractal(x + 1, y, z - 1, m_intBoxes);
                            if (x1y1zm1 && !xy1z && !xyzm1 && !IsCubeInFractal(x, y + 1, z - 1, m_intBoxes) && !x1yz && !x1y1z && !x1yzm1)
                            {
                                m_connections11m1.Add(new Vector3Int(x, y, z - 1));
                            }
                            bool x1ym1zm1 = IsCubeInFractal(x + 1, y - 1, z - 1, m_intBoxes);
                            bool xym1zm1 = IsCubeInFractal(x, y - 1, z - 1, m_intBoxes);
                            if (x1ym1zm1 && !xym1z && !xyzm1 && !xym1zm1 && !x1yz && !x1ym1z && !x1yzm1)
                            {
                                m_connections1m1m1.Add(new Vector3Int(x, y - 1, z - 1));
                            }
                        }
                    }
                }
                m_computedConnectionsProgressRowCount++;
            }
        }
        else
        for (int x = minIntPosition.x; x <= maxIntPosition.x && m_computingConnections; x++)
        {
            if (Math.Abs(x) % CONNECTING_THREAD_COUNT != threadNumber)
            {
                continue;
            }
            for (int y = minIntPosition.y; y < maxIntPosition.y; y++)
            {
                for (int z = minIntPosition.z; z < maxIntPosition.z; z++)
                {
                    bool xyz = IsCubeInFractal(x, y, z, m_intBoxes);
                    bool x1yz = IsCubeInFractal(x + 1, y, z, m_intBoxes);
                    bool xy1z = IsCubeInFractal(x, y + 1, z, m_intBoxes);
                    bool x1y1z = IsCubeInFractal(x + 1, y + 1, z, m_intBoxes);
                    bool addZConnection = false;
                    bool addXConnection = false;
                    bool addYConnection = false;
                    bool addConnections111 = false;
                    bool addConnections1m11 = false;
                    bool addConnections11m1 = false;
                    bool addConnections1m1m1 = false;
                    if ((xyz && x1y1z || x1yz && xy1z) && CountCubes(xyz, x1yz, xy1z, x1y1z) < 4)
                    {
                        addZConnection = true;
                    }
                    bool xyz1 = IsCubeInFractal(x, y, z + 1, m_intBoxes);
                    bool x1yz1 = IsCubeInFractal(x + 1, y, z + 1, m_intBoxes);
                    if ((xyz && x1yz1 || x1yz && xyz1) && CountCubes(xyz, x1yz, xyz1, x1yz1) < 4)
                    {
                        addYConnection = true;
                    }
                    bool xy1z1 = IsCubeInFractal(x, y + 1, z + 1, m_intBoxes);
                    if ((xyz && xy1z1 || xy1z && xyz1) && CountCubes(xyz, xy1z, xyz1, xy1z1) < 4)
                    {
                        addXConnection = true;
                    }
                    if (xyz)
                    {
                        // test of corner diagonal connection
                        bool x1y1z1 = IsCubeInFractal(x + 1, y + 1, z + 1, m_intBoxes);
                        if (x1y1z1 && !xy1z && !xyz1 && !xy1z1 && !x1yz && !x1y1z && !x1yz1)
                        {
                            addConnections111 = true;
                        }
                        bool x1ym1z1 = IsCubeInFractal(x + 1, y - 1, z + 1, m_intBoxes);
                        bool xym1z = IsCubeInFractal(x, y - 1, z, m_intBoxes);
                        bool x1ym1z = IsCubeInFractal(x + 1, y - 1, z, m_intBoxes);
                        if (x1ym1z1 && !xyz1 && !x1yz1 && !x1yz && !xym1z && !x1ym1z && !IsCubeInFractal(x, y - 1, z + 1, m_intBoxes))
                        {
                            addConnections1m11 = true;
                        }
                        bool x1y1zm1 = IsCubeInFractal(x + 1, y + 1, z - 1, m_intBoxes);
                        bool xyzm1 = IsCubeInFractal(x, y, z - 1, m_intBoxes);
                        bool x1yzm1 = IsCubeInFractal(x + 1, y, z - 1, m_intBoxes);
                        if (x1y1zm1 && !xy1z && !xyzm1 && !IsCubeInFractal(x, y + 1, z - 1, m_intBoxes) && !x1yz && !x1y1z && !x1yzm1)
                        {
                            addConnections11m1 = true;
                        }
                        bool x1ym1zm1 = IsCubeInFractal(x + 1, y - 1, z - 1, m_intBoxes);
                        bool xym1zm1 = IsCubeInFractal(x, y - 1, z - 1, m_intBoxes);
                        if (x1ym1zm1 && !xym1z && !xyzm1 && !xym1zm1 && !x1yz && !x1ym1z && !x1yzm1)
                        {
                            addConnections1m1m1 = true;
                        }
                    }
                    if (addZConnection || addXConnection || addYConnection || addConnections111 || addConnections1m11 || addConnections11m1 || addConnections1m1m1)
                    {
                        lock (m_listLock)
                        {
                            if (addZConnection)
                            {
                                m_zConnections.Add(new Vector3Int(x, y, z));
                            }
                            if (addXConnection)
                            {
                                m_xConnections.Add(new Vector3Int(x, y, z));
                            }
                            if (addYConnection)
                            {
                                m_yConnections.Add(new Vector3Int(x, y, z));
                            }
                            if (addConnections111)
                            {
                                m_connections111.Add(new Vector3Int(x, y, z));
                            }
                            if (addConnections1m11)
                            {
                                m_connections1m11.Add(new Vector3Int(x, y - 1, z));
                            }
                            if (addConnections11m1)
                            {
                                m_connections11m1.Add(new Vector3Int(x, y, z - 1));
                            }
                            if (addConnections1m1m1)
                            {
                                m_connections1m1m1.Add(new Vector3Int(x, y - 1, z - 1));
                            }
                        }
                    }  
                }
            }
            m_computedConnectionsProgressRowCount++;
        }
        m_finishedConnectingThreads++;
    }

    void ConnectCubes()
    {
        // connecting of cubes
        m_zConnectionCubes = new List<B>();
        B activeBox = new B();
        int activeX = int.MaxValue;
        int activeY = int.MaxValue;
        int activeZ = int.MaxValue;

        foreach (Vector3Int vector in m_zConnections)
        {
            if (activeX == int.MaxValue)
            {
                activeX = vector.x;
                activeY = vector.y;
                activeZ = vector.z;
                activeBox.x = activeX;
                activeBox.y = activeY;
                activeBox.z = activeZ;
                activeBox.c = 1;
                continue;
            }
            if (vector.x == activeX && vector.y == activeY && vector.z == activeZ + 1)
            {
                // expanding of box
                activeBox.c++;
                activeZ = vector.z;
            }
            else
            {
                m_zConnectionCubes.Add(activeBox);

                activeBox = new B();
                activeX = vector.x;
                activeY = vector.y;
                activeZ = vector.z;
                activeBox.x = activeX;
                activeBox.y = activeY;
                activeBox.z = activeZ;
                activeBox.c = 1;
            }
        }
        // last box
        if (m_zConnections.Count > 0)
        {
            m_zConnectionCubes.Add(activeBox);
        }
        m_zConnections.Clear();

        m_xConnectionCubes = new List<B>();
        activeBox = new B();
        activeX = int.MaxValue;
        activeY = int.MaxValue;
        activeZ = int.MaxValue;

        foreach (Vector3Int vector in m_xConnections)
        {
            if (activeX == int.MaxValue)
            {
                activeX = vector.x;
                activeY = vector.y;
                activeZ = vector.z;
                activeBox.x = activeX;
                activeBox.y = activeY;
                activeBox.z = activeZ;
                activeBox.c = 1;
                continue;
            }
            if (vector.x == activeX + 1 && vector.y == activeY && vector.z == activeZ)
            {
                // expanding of box
                activeBox.c++;
                activeX = vector.x;
            }
            else
            {
                m_xConnectionCubes.Add(activeBox);

                activeBox = new B();
                activeX = vector.x;
                activeY = vector.y;
                activeZ = vector.z;
                activeBox.x = activeX;
                activeBox.y = activeY;
                activeBox.z = activeZ;
                activeBox.c = 1;
            }
        }
        // last box
        if (m_xConnections.Count > 0)
        {
            m_xConnectionCubes.Add(activeBox);
        }
        m_xConnections.Clear();

        m_yConnectionCubes = new List<B>();
        activeBox = new B();
        activeX = int.MaxValue;
        activeY = int.MaxValue;
        activeZ = int.MaxValue;

        foreach (Vector3Int vector in m_yConnections)
        {
            if (activeX == int.MaxValue)
            {
                activeX = vector.x;
                activeY = vector.y;
                activeZ = vector.z;
                activeBox.x = activeX;
                activeBox.y = activeY;
                activeBox.z = activeZ;
                activeBox.c = 1;
                continue;
            }
            if (vector.x == activeX && vector.y == activeY + 1 && vector.z == activeZ)
            {
                // expanding of box
                activeBox.c++;
                activeY = vector.y;
            }
            else
            {
                m_yConnectionCubes.Add(activeBox);

                activeBox = new B();
                activeX = vector.x;
                activeY = vector.y;
                activeZ = vector.z;
                activeBox.x = activeX;
                activeBox.y = activeY;
                activeBox.z = activeZ;
                activeBox.c = 1;
            }
        }
        // last box
        if (m_yConnections.Count > 0)
        {
            m_yConnectionCubes.Add(activeBox);
        }
        m_yConnections.Clear();
        m_connectionsAreComputed = true;
    }

    private void SortConnections()
    {
        // sort x connections according to y,z and x
        m_xConnections.Sort(new Fractal3DGraphic.VectorXComparer());
        // sort y connections according to x,z and y
        m_yConnections.Sort(new Fractal3DGraphic.VectorYComparer());
        // sort z connections according to x,y and z
        m_zConnections.Sort(new Fractal3DGraphic.VectorZComparer());
    }

    int CountCubes(bool one, bool two, bool three, bool four)
    {
        return (one ? 1 : 0) + (two ? 1 : 0) + (three ? 1 : 0) + (four ? 1 : 0);
    }

    int CountCubes(bool c1, bool c2, bool c3, bool c4, bool c5, bool c6, bool c7, bool c8)
    {
        return (c1 ? 1 : 0) + (c2 ? 1 : 0) + (c3 ? 1 : 0) + (c4 ? 1 : 0) + (c5 ? 1 : 0) + (c6 ? 1 : 0) + (c7 ? 1 : 0) + (c8 ? 1 : 0);
    }

    public class BoxComparer : IComparer<B>
    {
        public int Compare(B x, B y)
        {
            if (x.x < y.x)
            {
                return -1;
            }
            else if (x.x > y.x)
            {
                return 1;
            }
            if (x.y < y.y)
            {
                return -1;
            }
            else if (x.y > y.y)
            {
                return 1;
            }
            //if (x.c != 1)
            //{
            //    int i = 0;
            //}
            //if (y.c != 1)
            //{
            //    int i = 0;
            //}
            if (x.z < y.z)
            {
                return -1;
            }
            if (x.z >= y.z && x.z < y.z + y.c)
            {
                return 0;
            }
            return 1;
        }
    }
    IComparer<B> m_comparer = new BoxComparer();

    private bool IsCubeInFractalSlow(int positionX, int positionY, int positionZ, List<B> intBoxes)
    {
        //foreach (var i in m_cubeCache)
        //{
        //    if (i.first.x == positionX && i.first.y == positionY && i.first.z == positionZ)
        //    {
        //        return i.second;
        //    }
        //}
        //Vector3Int vec = new Vector3Int(positionX, positionY, positionZ);
        //if (m_cubeCache.Count > 12)
        //{
        //    m_cubeCache.RemoveAt(0);
        //}

        foreach (B box in intBoxes)
        {
            if (positionX == box.x && positionY == box.y)
            {
                if (positionZ >= box.z && positionZ < box.z + box.c)
                {
                    //m_cubeCache.Add(new Pair<Vector3Int, bool>(vec, true));
                    return true;
                }
            }
        }
        //m_cubeCache.Add(new Pair<Vector3Int, bool>(vec, false));
        return false;
    }

    private bool IsCubeInFractal(int positionX, int positionY, int positionZ, List<B> intBoxes)
    {
        Vector3Int vec = new Vector3Int(positionX, positionY, positionZ);
        B b = new B();
        b.x = positionX;
        b.y = positionY;
        b.z = positionZ;
        int index = intBoxes.BinarySearch(b, m_comparer);
        if (index < 0)
        {
            return false;
        }
        return true;
    }

    public void OnButtonSavePng()
    {
        if (Persistent.Instance.Exported3dScreenshotWidth <= 0 || Persistent.Instance.Exported3dScreenshotHeight <= 0)
        {
            MessagePanel.Message("Image size has to be positive like you :-)");
            return;
        }

        string saveDir = Persistent.Instance.Save3DPicturesDirectory;
        string extension = ".png";
        fileDialog.onResultDelegate = (string path) =>
        {
            UnityAction writeAction = () =>
            {
                bool previousShowAxes = ShowAxes;
                bool previousShowComputationBoundingBox = ShowComputationBoundingBox;
                ShowAxes = false;
                ShowComputationBoundingBox = false;

                GameObject.Find("Main Camera").GetComponent<FractalScreenshotRecorder>().MakeScreenshot(GetActiveFractal(), path, Persistent.Instance.Exported3dScreenshotWidth, Persistent.Instance.Exported3dScreenshotHeight, m_camera.transform);

                ShowAxes = previousShowAxes;
                ShowComputationBoundingBox = previousShowComputationBoundingBox;

                // record saving path                
                Persistent.Instance.Save3DPicturesDirectory = Path.GetDirectoryName(path);
            };
            if (File.Exists(path))
            {
                OverwriteConfirmation(writeAction);
            }
            else
            {
                writeAction();
            }
        };
        string saveName = "";
        if (GetActiveFractal().FilePath.Length > 0)
        {
            saveName = Path.GetFileNameWithoutExtension(GetActiveFractal().FilePath)
                + String.Format("_{0}_{1}", Persistent.Instance.Exported3dScreenshotWidth, Persistent.Instance.Exported3dScreenshotHeight)
                + extension;
        }
        else
        {
            saveName = extension;
        }
        StartCoroutine(fileDialog.Save(Path.Combine(saveDir, saveName), extension, "Save fractal"));
    }

    public void OnButtonExportZoomIn()
    {
        OnZoomIn();
    }

    public void OnButtonExportZoomOut()
    {
        OnZoomOut();
    }

    void InitPredefinedImageResolutions()
    {
        UIUtils.FillInputField("InputField Predefined 3d Size 1", "1366,768");
        UIUtils.FillInputField("InputField Predefined 3d Size 2", "1920,1080");
    }

    public void OnButtonSetPredefinedSize1()
    {
        SavePredefinedExportImageSizes();
        string value = UIUtils.GetInputFieldValue("InputField Predefined 3d Size 1");
        if (!UIUtils.ValidateInputTwoInts(value))
        {
            MessagePanel.Message("Expected input should be: width,height - like 1000,800");
        }
        string[] values = value.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        Persistent.Instance.Exported3dScreenshotWidth = int.Parse(values[0]);
        Persistent.Instance.Exported3dScreenshotHeight = int.Parse(values[1]);
        InitExportPanel();
    }
    public void OnButtonSetPredefinedSize2()
    {
        SavePredefinedExportImageSizes();
        string value = UIUtils.GetInputFieldValue("InputField Predefined 3d Size 2");
        if (!UIUtils.ValidateInputTwoInts(value))
        {
            MessagePanel.Message("Expected input should be: width,height - like 1000,800");
        }
        string[] values = value.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        Persistent.Instance.Exported3dScreenshotWidth = int.Parse(values[0]);
        Persistent.Instance.Exported3dScreenshotHeight = int.Parse(values[1]);
        InitExportPanel();
    }

    void SavePredefinedExportImageSizes()
    {
        // save of predefined sizes
        UIUtils.SaveInputField("InputField Predefined 3d Size 1");
        UIUtils.SaveInputField("InputField Predefined 3d Size 2");
    }

    public void OnButtonExportBack()
    {
        SavePredefinedExportImageSizes();

        ActivateMenuPanel(EMenuPanelType.Main);
    }

    public void OnButtonColor()
    {
        Persistent.Instance.SelColorDelegate = (Color color) =>
        {
            GetActiveFractal().SetFractalColor(color);
            GetActiveFractal().GetUndoRedoList().AddNewValue();
            // update of image
            ActivateMenuPanel(EMenuPanelType.Parameters);
        };
        Persistent.Instance.SelectedColor = GetActiveFractal().FractalColor;
        MySceneManager.GoToScene("ColorPicking", LoadSceneMode.Additive);
    }

    public void OnButtonBackgroundColor()
    {
        Persistent.Instance.SelColorDelegate = (Color color) =>
        {
            GetActiveFractal().FractalBackgroundColor = color;
            GetActiveFractal().GetUndoRedoList().AddNewValue();
            // update of image
            ActivateMenuPanel(EMenuPanelType.Parameters);
        };
        Persistent.Instance.SelectedColor = GetActiveFractal().FractalBackgroundColor;
        MySceneManager.GoToScene("ColorPicking", LoadSceneMode.Additive);
    }

    public void OnButtonSetDefaultColors()
    {
        GetActiveFractal().FractalBackgroundColor = Persistent.Instance.DefaultBackgroundColor;
        GetActiveFractal().Color = Persistent.Instance.DefaultForegroundColor;
        GetActiveFractal().GetUndoRedoList().AddNewValue();
        // update of image
        ActivateMenuPanel(EMenuPanelType.Parameters);
    }

    public void OnButtonRotateDown()
    {
        RotateVertically(-10);
    }
    public void OnButtonRotateUp()
    {
        RotateVertically(10);
    }
    public void OnButtonRotateLeft()
    {
        RotateHorizontally(10);
    }

    public void OnButtonRotateRight()
    {
        RotateHorizontally(-10);
    }

    void ResetCameraAndSceneBaseObject()
    {
        SetPredefinedCameraPosition(m_camera.transform, 0);
        //m_camera.transform.position = new Vector3(0, 0, -3);
        //m_camera.transform.rotation = new UnityEngine.Quaternion(0, 0, 0, 0);
        m_camera.transform.localScale = new Vector3(0, 0, 0);

        m_sceneBaseObject.transform.position = new Vector3(0, 0, 0);
        m_sceneBaseObject.transform.rotation = new UnityEngine.Quaternion(0, 0, 0, 0);
        m_sceneBaseObject.transform.localScale = new Vector3(1, 1, 1);
    }

    public void OnButtonResetView()
    {
        ResetCameraAndSceneBaseObject();

        ResetLight();
        UpdateAxesCenterAndBoundingBox();
        AdjustZoom();
    }

    void AdjustZoom()
    {
        if (GetActiveFractal() != null && GetActiveFractal().GetGraphic() != null)
        {
            Bounds bounds = GetActiveFractal().GetGraphic().GetBounds(1.0f, new Bounds(Vector3.zero, Vector3.one.Mult(GetActiveFractal().ComputationArea.GetMaxSize())));
            List<Vector3> limitPoints = bounds.GetLimitPoints();

            // conversion to world coordinates
            //foreach(GameObject go in m_testingPoints)
            //{
            //    GameObject.DestroyObject(go);
            //}
            //m_testingPoints.Clear();
            for (int i = 0; i < limitPoints.Count; i++)
            {
                limitPoints[i] = GetSceneBaseObject().transform.TransformPoint(limitPoints[i]);
            }

            float max = ScreenUtils.GetMaxRelativeScreenDistance(m_camera, limitPoints);
            for (int i = 0; i < 100; i++)
            {
                if (max > 0.5f)
                {
                    break;
                }
                OnZoomIn();
                max = ScreenUtils.GetMaxRelativeScreenDistance(m_camera, limitPoints);
            }
            for (int i = 0; i < 100; i++)
            {
                if (max < 0.5f)
                {
                    break;
                }
                OnZoomOut();
                max = ScreenUtils.GetMaxRelativeScreenDistance(m_camera, limitPoints);
            }
            for (int i = 0; i < 100; i++)
            {
                if (max > 0.5f)
                {
                    break;
                }
                OnZoomIn();
                max = ScreenUtils.GetMaxRelativeScreenDistance(m_camera, limitPoints);
            }
            for (int i = 0; i < 200; i++)
            {
                if (max < 0.5f)
                {
                    break;
                }
                OnZoomOutFine();
                max = ScreenUtils.GetMaxRelativeScreenDistance(m_camera, limitPoints);
            }
        }
    }

    GameObject NewTestingObject(Vector3 position)
    {
        GameObject go = GameObject.CreatePrimitive(PrimitiveType.Cube);
        go.transform.parent = GetSceneBaseObject().transform;
        go.transform.localPosition = position;
        go.transform.localScale = Vector3.one * 0.1f;
        return go;
    }

    private void Panning(float dx, float dy)
    {
        float koef = 1.0f / Screen.width;
        Vector3 forward = m_camera.transform.forward;
        Vector3 cameraPos = m_camera.transform.position;
        float distanceFromZeroInSightDirection = Mathf.Abs(Vector3.Dot(forward, Vector3.Normalize(cameraPos))) * cameraPos.magnitude;
        Vector3 change = (-dx * m_camera.transform.right + -dy * m_camera.transform.up) * distanceFromZeroInSightDirection * koef;
        change *= 1.5f;
        cameraPos += change;
        m_camera.transform.position = cameraPos;
    }

    private void RotateHorizontally(float deltaAngle)
    {
        if (m_rotateCamera)
        {
            m_camera.transform.RotateAround(new Vector3(0, 0, 0), m_camera.transform.up, -deltaAngle);
        }
        else
        {
            GetSceneBaseObject().transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), deltaAngle);
        }
    }

    private void RotateVertically(float deltaAngle)
    {
        if (m_rotateCamera)
        {
            m_camera.transform.RotateAround(new Vector3(0, 0, 0), -m_camera.transform.right, deltaAngle);
        }
        else
        {
            GetSceneBaseObject().transform.RotateAround(new Vector3(0, 0, 0), new Vector3(1, 0, 0), deltaAngle);
        }
    }

    private void Rotate(float dx, float dy)
    {
        float d = Mathf.Sqrt(dx * dx + dy * dy);
        d *= 0.5f;
        if (m_rotateCamera)
        {
            m_camera.transform.RotateAround(new Vector3(0, 0, 0), -dy * m_camera.transform.right + dx * m_camera.transform.up, d);
        }
        else
        {
            GetSceneBaseObject().transform.RotateAround(new Vector3(0, 0, 0), new Vector3(dy, -dx, 0), d);
        }
    }

    private void RotateCameraAround(float dx, float dy)
    {
        float d = Mathf.Sqrt(dx * dx + dy * dy);
        m_camera.transform.RotateAround(m_camera.transform.position, -dy * m_camera.transform.right + dx * m_camera.transform.up, d);
    }

    private void RotateAroundScreenAxe(float d)
    {
        if (m_rotateCamera)
        {
            m_camera.transform.RotateAround(new Vector3(0, 0, 0), m_camera.transform.forward, d);
        }
        else
        {
            GetSceneBaseObject().transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 0, 1), -d);
        }
    }

    private void RotateLightHorizontal(float deltaAngle)
    {
        if (m_rotateCamera)
        {
            m_directionalLight.transform.RotateAround(new Vector3(0, 0, 0), m_camera.transform.up, deltaAngle);
        }
        else
        {
            m_directionalLight.transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), deltaAngle);
        }
        CreateLightPointers();
    }

    private void RotateLightVertical(float deltaAngle)
    {
        if (m_rotateCamera)
        {
            m_directionalLight.transform.RotateAround(new Vector3(0, 0, 0), m_camera.transform.right, deltaAngle);
        }
        else
        {
            m_directionalLight.transform.RotateAround(new Vector3(0, 0, 0), new Vector3(1, 0, 0), deltaAngle);
        }
        CreateLightPointers();
    }

    private void RotateLight(float dx, float dy)
    {
        float d = Mathf.Sqrt(dx * dx + dy * dy);
        if (m_rotateCamera)
        {
            m_directionalLight.transform.RotateAround(new Vector3(0, 0, 0), dy * m_camera.transform.right - dx * m_camera.transform.up, d);
        }
        else
        {
            m_directionalLight.transform.RotateAround(new Vector3(0, 0, 0), new Vector3(dy, -dx, 0), d);
        }
        CreateLightPointers();
    }

    public void OnButtonNormalizeQx()
    {
        // normalize mapping quaternion Qx - that maps axix x to quaternion space
        int column = 0;
        SmartMathLibrary.Quaternion q = GetActiveFractal().Vec2QuatMapping.GetMappingQuaternion(column);
        q = q.Normalize();
        GetActiveFractal().Vec2QuatMapping.SetMappingQuaternion(column, q);
        //update ui
        InitMapping();
    }

    public void OnButtonNormalizeQy()
    {
        // normalize mapping quaternion Qy - that maps axix y to quaternion space
        int column = 1;
        SmartMathLibrary.Quaternion q = GetActiveFractal().Vec2QuatMapping.GetMappingQuaternion(column);
        q = q.Normalize();
        GetActiveFractal().Vec2QuatMapping.SetMappingQuaternion(column, q);
        //update ui
        InitMapping();
    }

    public void OnButtonNormalizeQz()
    {
        // normalize mapping quaternion Qz - that maps axix z to quaternion space
        int column = 2;
        SmartMathLibrary.Quaternion q = GetActiveFractal().Vec2QuatMapping.GetMappingQuaternion(column);
        q = q.Normalize();
        GetActiveFractal().Vec2QuatMapping.SetMappingQuaternion(column, q);
        //update ui
        InitMapping();
    }

    public void OnButtonParameters()
    {
        // creation of copy of fractal
        SetLimitAreaType();
        ActivateMenuPanel(EMenuPanelType.Parameters);
    }

    void ActivateMenuPanel(EMenuPanelType menuPanelType)
    {
        foreach (var panel in m_menuPanels)
        {
            if (panel.Key == menuPanelType)
            {
                panel.Value.SetActive(true);
                m_activePanel = panel.Value;
            }
            else
            {
                panel.Value.SetActive(false);
            }
        }
        switch (menuPanelType)
        {
        case EMenuPanelType.Parameters:
            SwitchToActiveParametersPanel();
            break;
        case EMenuPanelType.CopyFromFractalSelection:
            {
                // setting of value to dropwown
                m_copyFromFractalDropwown.ClearOptions();
                List<string> newOptions = new List<string>();
                for (int i = 0; i < m_fractals.Count; i++)
                {
                    if (i != m_activeFractalId)
                    {
                        newOptions.Add(ConvertFractalIndexToNumber(i).ToString());
                    }
                }
                m_copyFromFractalDropwown.AddOptions(newOptions);
                //m_copyFromFractalDropwow
            }
            break;
        case EMenuPanelType.ExportPNG:
            {
                InitExportPanel();
            }
            break;
        case EMenuPanelType.ExportSTL:
            {
                new EnumWidget(new Ref<int>(() => Persistent.Instance.ExportBiggestPart ? 0 : 1, v => { Persistent.Instance.ExportBiggestPart = (v == 0); }), UIUtils.FindGameObjectInChildren(m_menuPanels[GetActivatedMenuPanel()], "Dropdown Export Part"));//, this.OnCoefficientCPowerChanged).OnChangeDelegate += UpdateUndoList;
            }
            break;
        }
    }

    private int ConvertFractalIndexToNumber(int index)
    {
        return index + 1;
    }
    private int ConvertFractalNumberToIndex(int number)
    {
        return number - 1;
    }

    private bool IsDownNumberKey(out int number)
    {
        for (int i = 0; i < 10; i++)
        {
            if (Input.GetKeyDown(i.ToString()))
            {
                number = i;
                return true;
            }
        }
        number = -1;
        return false;
    }

    public void OnButtonOk()
    {
        // changes are applied
        if (!ApplyEditedChanges())
        {
            return;
        }
        ActivateMenuPanel(EMenuPanelType.Main);
        HandleFractalDisplayColors();
    }

    public void OnButtonOkAndCompute()
    {
        if (!ApplyEditedChanges())
        {
            return;
        }

        // get parameters are received from ui (for case that we were in some inputfield)
        UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(null);

        OnButtonCompute();
        ActivateMenuPanel(EMenuPanelType.HiddenParameters);
    }

    public void OnButtonAllParameters()
    {
        m_textAllParameters.GetComponent<Text>().text = GetActiveFractal().GetAllParameters();

        ActivateMenuPanel(EMenuPanelType.AllParameters);
    }

    public void OnButtonOkAllParameters()
    {
        ActivateMenuPanel(EMenuPanelType.Parameters);
    }

    public void OnButtonOkCopyParameters()
    {
        int pos = m_copyFromFractalDropwown.value;
        List<Dropdown.OptionData> list = m_copyFromFractalDropwown.options;
        Dropdown.OptionData selectedItem = list[pos];
        string selectedNumber = selectedItem.text;// as string;// m_copyFromFractalDropwown.options[pos];

        int selectedNumberInt = -1;
        Int32.TryParse(selectedNumber, out selectedNumberInt);
        if (selectedNumberInt != -1)
        {
            int selectedFracatlIndex = ConvertFractalNumberToIndex(selectedNumberInt);
            GetActiveFractal().GetGraphic().Clear(true, false);
            // 
            SetFractal(m_activeFractalId, GetFractalByIndex(selectedFracatlIndex).Clone(cloneGraphic: true) as QuaternionFractal3D);
            //if (!GetActiveFractal().GetComputationParameters().Equals(computationParameters))
            //{
            //    GetActiveFractal().GraphicIsComputed = false;
            //}
            GetActiveFractal().FractalColor = GetFractalByIndex(selectedFracatlIndex).FractalColor;
        }
        ActivateMenuPanel(EMenuPanelType.Main);
        HandleFractalDisplayColors();
    }

    public void OnButtonCancelCopyParameters()
    {
        ActivateMenuPanel(EMenuPanelType.Main);
    }

    public void OnButtonBack()
    {
        Persistent.Instance.QuaternionFractal3D = null;
        if (MySceneManager.GoToPreviousScene() == SceneManager.GetActiveScene())
        {
            // no previous scene
            MySceneManager.GoToScene("3D Fractals");
        }
    }

    public void OnButtonSlices()
    {
        Persistent.Instance.QuaternionFractal3D = GetActiveFractal();
        MySceneManager.GoToScene("Quat3DSlices", LoadSceneMode.Additive);
    }

    public void OnZoomIn(bool smallZoomSensitivity = false)
    {
        Vector3 position = m_camera.transform.position;
        position *= smallZoomSensitivity ? 0.99f : 0.9f;
        m_camera.transform.position = position;
    }

    public void OnZoomOut(bool smallZoomSensitivity = false)
    {
        Vector3 position = m_camera.transform.position;
        position /= smallZoomSensitivity ? 0.99f : 0.9f;
        m_camera.transform.position = position;
    }
    void OnZoomOutFine()
    {
        Vector3 position = m_camera.transform.position;
        position /= 0.99f;
        m_camera.transform.position = position;
    }

    public void OnButtonMaximizeComputationArea()
    {
        GetActiveFractal().MaximizeComputationArea();
        InitComputationArea();
    }

    public void OnButtonShowParameters()
    {
        ActivateMenuPanel(EMenuPanelType.Parameters);
    }

    public void OnButtonGoShowParameters()
    {
        ActivateMenuPanel(EMenuPanelType.Parameters);
    }

    public void OnButtonAddFractal()
    {
        if (GetFractalsCount() >= GetFractalsMaxCount())
            return;

        GameObject mainPanel = GameObject.Find("Panel Main");

        int newButtonDistanceY = 40;

        // previous object
        int oldFractalsCount = GetFractalsCount();
        string name = string.Format(ToggleShowFractalNumber, oldFractalsCount - 1);
        GameObject prevToggle = GameObject.Find(name);
        Vector3 transform = prevToggle.GetComponent<Transform>().position;
        transform.y -= newButtonDistanceY;

        // adding of new fractal toggle
        GameObject newToggle = GameObject.Instantiate(prevToggle);
        transform = prevToggle.GetComponent<Transform>().position;
        transform.y -= newButtonDistanceY;
        newToggle.GetComponent<Transform>().position = transform;
        newToggle.transform.SetParent(mainPanel.transform);
        newToggle.GetComponent<Toggle>().GetComponentInChildren<Text>().text = string.Format("{0}", oldFractalsCount + 1);
        newToggle.name = string.Format(ToggleShowFractalNumber, oldFractalsCount);

        // adding of new selection toggle
        GameObject prevSelectionToggle = GameObject.Find(string.Format(ToggleSelectFractalNumber, oldFractalsCount - 1));
        GameObject newSelectionToggle = GameObject.Instantiate(prevSelectionToggle);
        transform = prevSelectionToggle.GetComponent<Transform>().position;
        transform.y -= newButtonDistanceY;
        newSelectionToggle.GetComponent<Transform>().position = transform;
        newSelectionToggle.transform.SetParent(mainPanel.transform);
        newSelectionToggle.name = string.Format(ToggleSelectFractalNumber, oldFractalsCount);
        newSelectionToggle.GetComponent<Toggle>().group = prevSelectionToggle.GetComponent<Toggle>().group;
        newSelectionToggle.GetComponent<Toggle>().isOn = false;

        // moving of plus button
        GameObject plusButton = GameObject.Find("Button Fractal +");
        transform = plusButton.GetComponent<Transform>().position;
        transform.y -= newButtonDistanceY;
        plusButton.GetComponent<Transform>().position = transform;

        // adding of new fractal
        QuaternionFractal3D fractal = new QuaternionFractal3D();
        int newFractalIndex = GetFractalsCount();
        SetFractal(newFractalIndex, fractal);

        if (ShowOnlyOneFractal)
        {
            DisplayOnlyOneFractal();
        }

        HandleButtonCopyVisibility();

        // switch active fractal to new added fractal
        ActivateFractal(newFractalIndex);
        HandleFractalDisplayColors();
    }

    public void OnButtonSelectActiveFractal()
    {
        int fractalIndex = GetSelectedFractalIndex(ToggleSelectFractal);
        if (fractalIndex == -1)
        {
            return;
        }
        ActivateFractal(fractalIndex);

        SetFractalVisible(fractalIndex, true);
        SetToggleFractalVisible(fractalIndex, true);
    }

    public void OnButtonCopy()
    {
        ActivateMenuPanel(EMenuPanelType.CopyFromFractalSelection);
    }

    bool IsFractalDisplayed(int fractalIndex)
    {
        if (!IsFractalsIndexValid(fractalIndex))
        {
            return false;
        }
        string toggleName = string.Format(ToggleShowFractalNumber, fractalIndex);
        GameObject toggleObject = GameObject.Find(toggleName);
        if (toggleObject != null)
        {
            return toggleObject.GetComponent<Toggle>().isOn;
        }
        return false;
    }

    void SetFractalVisible(int fractalIndex, bool visible)
    {
        QuaternionFractal3D fractal = GetFractalByIndex(fractalIndex);
        if (fractal == null
            || fractal.GetGraphic() == null)
        //|| fractal.GetGraphic().GetBaseObject() == null)
        {
            return;
        }
        fractal.GetGraphic().GetBaseObject().SetActive(visible);

        if (m_showOnlyOneFractal)
        {
            if (visible)
            {
                // switchnig off other toggles
                foreach (var fractalPair in m_fractals)
                {
                    bool isEnabled = fractalPair.Key == fractalIndex;
                    fractalPair.Value.GetGraphic().GetBaseObject().SetActive(isEnabled);
                    string toggleName1 = string.Format(ToggleShowFractalNumber, fractalPair.Key);
                    GameObject toggleObject1 = GameObject.Find(toggleName1);
                    if (toggleObject1 != null)
                    {
                        if (toggleObject1.GetComponent<Toggle>().isOn != isEnabled)
                        {
                            toggleObject1.GetComponent<Toggle>().isOn = isEnabled;
                        }
                    }
                }
            }
        }
    }

    void SetToggleFractalVisible(int fractalIndex, bool visible)
    {
        string toggleName = string.Format(ToggleShowFractalNumber, fractalIndex);
        GameObject toggleObject = GameObject.Find(toggleName);
        if (toggleObject == null)
        {
            return;
        }
        bool isVisible = toggleObject.GetComponent<Toggle>().isOn;
        if (isVisible != visible)
        {
            toggleObject.GetComponent<Toggle>().isOn = visible;
        }
    }

    public void OnButtonSwitchFractalVisibility()
    {
        int fractalIndex = GetSelectedFractalIndex(ToggleShowFractal);
        QuaternionFractal3D fractal = GetFractalByIndex(fractalIndex);
        if (fractal == null)
        {
            return;
        }
        string toggleName = string.Format(ToggleShowFractalNumber, fractalIndex);
        GameObject toggleObject = GameObject.Find(toggleName);
        if (toggleObject == null)
        {
            return;
        }
        bool visible = toggleObject.GetComponent<Toggle>().isOn;

        SetFractalVisible(fractalIndex, visible);
        if (visible && m_showOnlyOneFractal)
        {
            ActivateFractal(fractalIndex);
        }
        HandleFractalDisplayColors();
    }

    private void DisplayOnlyOneFractal()
    {
        foreach (var fractalPair in m_fractals)
        {
            int fractalId = fractalPair.Key;
            string toggleName = string.Format(ToggleShowFractalNumber, fractalPair.Key);
            GameObject toggleObject = GameObject.Find(toggleName);
            if (toggleObject == null)
            {
                continue;
            }
            bool enabled = toggleObject.GetComponent<Toggle>().isOn;
            bool shouldBeEnabled = m_activeFractalId == fractalId;
            if (enabled != shouldBeEnabled)
            {
                toggleObject.GetComponent<Toggle>().isOn = shouldBeEnabled;
                SetFractalVisible(fractalId, shouldBeEnabled);
            }
        }
    }

    public void OnButtonAdjustArea()
    {
        // adjusting of area according to computed boxes
        // counting of area occupied by fractal
        if (GetActiveFractal() != null && GetActiveFractal().GetGraphic() != null)
        {
            Bounds graphicBounds = GetActiveFractal().GetGraphic().GetBounds(1.005f, new Bounds(Vector3.zero, Vector3.one * 4));
            GetActiveFractal().ComputationArea.SetFromGraphicBounds(graphicBounds, true);
        }
    }

    public void OnButtonAdjustArea10percent()
    {
        // adjusting of area according to computed boxes + 10 percent more
        // counting of area occupied by fractal
        if (GetActiveFractal() != null && GetActiveFractal().GetGraphic() != null)
        {
            Bounds graphicBounds = GetActiveFractal().GetGraphic().GetBounds(1.1f, new Bounds(Vector3.zero, Vector3.one * 4));
            GetActiveFractal().ComputationArea.SetFromGraphicBounds(graphicBounds, true);
        }
    }

    public void OnButtonAdjustArea20percent()
    {
        // adjusting of area according to computed boxes + 20 percent more
        // counting of area occupied by fractal
        if (GetActiveFractal() != null && GetActiveFractal().GetGraphic() != null)
        {
            Bounds graphicBounds = GetActiveFractal().GetGraphic().GetBounds(1.2f, new Bounds(Vector3.zero, Vector3.one * 4));
            GetActiveFractal().ComputationArea.SetFromGraphicBounds(graphicBounds, true);
        }
    }

    public void SwitchToActiveParametersPanel()
    {
        //if (IsParameterPanelVisible(Persistent.Instance.LastSelectedQuat3DParametersType))
        //{
        //    ActivateParameterPanel(Persistent.Instance.LastSelectedQuat3DParametersType);
        //}
        //else
        //{
        //    ActivateParameterPanel(Quaternion3DSceneScript.ParametersPanelType.Computation);
        //}
        foreach (var item in m_parametersToggles)
        {
            if (item.Value.GetComponent<Toggle>().isOn)
            {
                ActivateParameterPanel(m_mapParametersTogglePanel[item.Key]);
                break;
            }
        }
        HandleToggleVisibility();
    }

    public void OnCoefficientCPowerChanged(int row)
    {
        row = m_dropdownC.value;
        GameObject panelC = GameObject.Find("Panel Coefficient C");

        if (row >= GetActiveFractal().C.Count)
        {
            GetActiveFractal().SetC(row, SmartMathLibrary.Quaternion.Zero);
        }
        SmartMathLibrary.Quaternion q = GetActiveFractal().GetC(row);

        QuaternionWidget widget =
        new QuaternionWidget(new Ref<SmartMathLibrary.Quaternion>(() => q, v => { q = v; }),
            UIUtils.FindInputFieldInChildren(panelC, "InputField X0"),
            UIUtils.FindInputFieldInChildren(panelC, "InputField I"),
            UIUtils.FindInputFieldInChildren(panelC, "InputField J"),
            UIUtils.FindInputFieldInChildren(panelC, "InputField K"));

        widget.OnChangeDelegate += UpdateUndoList;
        widget.OnChangeDelegate += () =>
        {
            InputField usedPowers = UIUtils.FindInputFieldInChildren(panelC, "InputField Used Powers");
            usedPowers.text = GetActiveFractal().GetUsedCPowers();
        };

        if (row >= GetActiveFractal().CA.Count)
        {
            GetActiveFractal().SetCA(row, SmartMathLibrary.Quaternion.Zero);
        }
        SmartMathLibrary.Quaternion qAfter = GetActiveFractal().GetCA(row);

        QuaternionWidget widgetAfter =
        new QuaternionWidget(new Ref<SmartMathLibrary.Quaternion>(() => qAfter, v => { qAfter = v; }),
            UIUtils.FindInputFieldInChildren(panelC, "InputField X0 After"),
            UIUtils.FindInputFieldInChildren(panelC, "InputField I After"),
            UIUtils.FindInputFieldInChildren(panelC, "InputField J After"),
            UIUtils.FindInputFieldInChildren(panelC, "InputField K After"));

        widgetAfter.OnChangeDelegate += UpdateUndoList;
        widgetAfter.OnChangeDelegate += () =>
        {
            InputField usedPowers = UIUtils.FindInputFieldInChildren(panelC, "InputField Used Powers After");
            usedPowers.text = GetActiveFractal().GetUsedCAPowers();
        };

        InputField usedPowers2 = UIUtils.FindInputFieldInChildren(panelC, "InputField Used Powers");
        usedPowers2.text = GetActiveFractal().GetUsedCPowers();
        usedPowers2.enabled = false;
        InputField usedPowers3 = UIUtils.FindInputFieldInChildren(panelC, "InputField Used Powers After");
        usedPowers3.text = GetActiveFractal().GetUsedCAPowers();
        usedPowers3.enabled = false;
    }

    public void OnCoefficientC0PowerChanged(int row)
    {
        GameObject dropdown = GameObject.Find("Dropdown C0");
        row = dropdown.GetComponent<Dropdown>().value;
        GameObject panelC = GameObject.Find("Panel Coefficient C0");

        if (row >= GetActiveFractal().C0.Count)
        {
            GetActiveFractal().SetC0(row, SmartMathLibrary.Quaternion.Zero);
        }
        SmartMathLibrary.Quaternion q = GetActiveFractal().GetC0(row);

        QuaternionWidget widget =
        new QuaternionWidget(new Ref<SmartMathLibrary.Quaternion>(() => q, v => { q = v; }),
            UIUtils.FindInputFieldInChildren(panelC, "InputField X0"),
            UIUtils.FindInputFieldInChildren(panelC, "InputField I"),
            UIUtils.FindInputFieldInChildren(panelC, "InputField J"),
            UIUtils.FindInputFieldInChildren(panelC, "InputField K"));

        if (row >= GetActiveFractal().C0A.Count)
        {
            GetActiveFractal().SetC0A(row, SmartMathLibrary.Quaternion.Zero);
        }
        SmartMathLibrary.Quaternion qAfter = GetActiveFractal().GetC0A(row);

        QuaternionWidget widgetAfter =
        new QuaternionWidget(new Ref<SmartMathLibrary.Quaternion>(() => qAfter, v => { qAfter = v; }),
            UIUtils.FindInputFieldInChildren(panelC, "InputField X0 After"),
            UIUtils.FindInputFieldInChildren(panelC, "InputField I After"),
            UIUtils.FindInputFieldInChildren(panelC, "InputField J After"),
            UIUtils.FindInputFieldInChildren(panelC, "InputField K After"));

        widget.OnChangeDelegate += UpdateUndoList;
        widget.OnChangeDelegate += () =>
        {
            InputField usedPowers = UIUtils.FindInputFieldInChildren(panelC, "InputField Used Powers");
            usedPowers.text = GetActiveFractal().GetUsedC0Powers();
        };

        widgetAfter.OnChangeDelegate += UpdateUndoList;
        widgetAfter.OnChangeDelegate += () =>
        {
            InputField usedPowers = UIUtils.FindInputFieldInChildren(panelC, "InputField Used Powers After");
            usedPowers.text = GetActiveFractal().GetUsedC0APowers();
        };

        InputField usedPowers2 = UIUtils.FindInputFieldInChildren(panelC, "InputField Used Powers");
        usedPowers2.text = GetActiveFractal().GetUsedC0Powers();
        usedPowers2.enabled = false;
        InputField usedPowers3 = UIUtils.FindInputFieldInChildren(panelC, "InputField Used Powers After");
        usedPowers3.text = GetActiveFractal().GetUsedC0APowers();
        usedPowers3.enabled = false;
    }

    public void OnUndo()
    {
        string changedProperty = GetActiveFractal().GetUndoRedoList().Undo();
        // setting of UI
        SetPanelAccordingToChangedProperty(changedProperty);
        UpdateUndoRedoButtons();
    }

    public void OnRedo()
    {
        string changedProperty = GetActiveFractal().GetUndoRedoList().Redo();
        // setting of UI
        SetPanelAccordingToChangedProperty(changedProperty);
        UpdateUndoRedoButtons();
    }

    public void OnAreaTypeChanged(int row)
    {
        // setting of right panel for type of area
        //GameObject dropdown = GameObject.Find("Dropdown Area Type");
        //row = dropdown.GetComponent<Dropdown>().value;

        string areaPanelName;
        GameObject areaPanel;
        switch (row)
        {
        case 0: // circular area zxc improve constant
            {
                areaPanelName = "Panel Limit Area Circular";
                ActivateAreaPanel(areaPanelName);
                areaPanel = m_limitAreaPanels[areaPanelName];

                if (!(GetActiveFractal().LimitArea is CircularQuaternionArea))
                {
                    // change to circular area
                    GetActiveFractal().LimitArea = new CircularQuaternionArea();
                }

                CircularQuaternionArea area = GetActiveFractal().LimitArea as CircularQuaternionArea;
                new QuaternionWidget(new Ref<SmartMathLibrary.Quaternion>(() => area.Center, v => { area.Center = v; }),
                    UIUtils.FindInputFieldInChildren(areaPanel, "InputField X0"),
                    UIUtils.FindInputFieldInChildren(areaPanel, "InputField I"),
                    UIUtils.FindInputFieldInChildren(areaPanel, "InputField J"),
                    UIUtils.FindInputFieldInChildren(areaPanel, "InputField K")).OnChangeDelegate += UpdateUndoList;
                new DoubleWidget(new Ref<double>(() => area.Radius, v => { area.Radius = v; }), UIUtils.FindInputFieldInChildren(areaPanel, "InputField Radius")).OnChangeDelegate += UpdateUndoList;
            }
            break;
        case 1:
            {
                // rectangular area
                areaPanelName = "Panel Limit Area Rectangular";
                ActivateAreaPanel(areaPanelName);
                areaPanel = m_limitAreaPanels[areaPanelName];

                if (!(GetActiveFractal().LimitArea is RectangularQuaternionArea))
                {
                    // change to circular area
                    GetActiveFractal().LimitArea = new RectangularQuaternionArea();
                }

                RectangularQuaternionArea area = GetActiveFractal().LimitArea as RectangularQuaternionArea;
                new QuaternionWidget(new Ref<SmartMathLibrary.Quaternion>(() => area.Center, v => { area.Center = v; }),// area.Center,
                    UIUtils.FindInputFieldInChildren(areaPanel, "InputField X0"),
                    UIUtils.FindInputFieldInChildren(areaPanel, "InputField I"),
                    UIUtils.FindInputFieldInChildren(areaPanel, "InputField J"),
                    UIUtils.FindInputFieldInChildren(areaPanel, "InputField K")).OnChangeDelegate += UpdateUndoList;

                new QuaternionWidget(new Ref<SmartMathLibrary.Quaternion>(() => area.Size, v => { area.Size = v; }), //area.HalfSize,
                    UIUtils.FindInputFieldInChildren(areaPanel, "InputField Size X0"),
                    UIUtils.FindInputFieldInChildren(areaPanel, "InputField Size I"),
                    UIUtils.FindInputFieldInChildren(areaPanel, "InputField Size J"),
                    UIUtils.FindInputFieldInChildren(areaPanel, "InputField Size K")).OnChangeDelegate += UpdateUndoList;
            }
            break;
        }
    }

    //////////////// private functions ////////////////////

    void HideUI()
    {
        m_isPanelHidden = true;
        m_activePanel.SetActive(false);
        m_hideTime = UnityEngine.Time.realtimeSinceStartup;
    }

    //System.Collections.IEnumerator UnhideUI()
    void UnhideUI()
    {
        m_activePanel.SetActive(true);
        m_isPanelHidden = false;
        //yield return new WaitForSeconds(0.0f);
    }


    void ClearScene()
    {
        if (GetActiveFractal() != null)
        {
            GetActiveFractal().GetGraphic().Clear(true, false);
            //SetActiveFractal(null);
            SetFractal(m_activeFractalId, null);
        }
    }

    private bool IsActiveMenu(EMenuPanelType menuType)
    {
        return m_menuPanels[menuType].activeInHierarchy;
    }
    private EMenuPanelType GetActivatedMenuPanel()
    {
        foreach (var item in m_menuPanels)
        {
            if (item.Value.activeInHierarchy)
            {
                return item.Key;
            }
        }
        return EMenuPanelType.Main;
    }

    private string GetNewSaveName(string saveDir)
    {
        string nameMask = "Quat_{0}." + Persistent.GetExtension(Persistent.SaveExtension.fractal3DZipExtension);
        string name;
        for (int i = 0; true; i++)
        {
            name = string.Format(nameMask, i);
            if (!File.Exists(saveDir + name))
            {
                break;
            }
        }
        return name;
    }

    bool IsParameterPanelVisible(ParametersPanelType panelType)
    {
        if (panelType == ParametersPanelType.CoefficientsC0 && GetActiveFractal().ComputeDelegateType == QuaternionDelegateType.Julia)
        {
            return false;
        }
        return true;
    }

    bool IsParameterPanelVisible(string panelName)
    {
        return IsParameterPanelVisible(m_panelName2Type[panelName]);
    }

    void ShowParameterToggle(string toggleName, bool active)
    {
        m_parametersToggles[toggleName].SetActive(active);
    }

    void SetParameterToggle(ParametersPanelType panelType, bool on)
    {
        if (!m_parametersTypeToToggles[panelType].isOn == on)
        {
            m_parametersTypeToToggles[panelType].isOn = on;
        }
    }

    void HandleToggleVisibility()
    {
        bool c0isVisible = GetActiveFractal().ComputeDelegateType == QuaternionDelegateType.MixedMJ;
        ShowParameterToggle("Toggle Coefficient C0", c0isVisible);
        if (!c0isVisible)
        {
            SetParameterToggle(ParametersPanelType.CoefficientsC0, false);
        }
    }

    void ActivateParameterPanel(ParametersPanelType panelType)
    {
        ActivateParameterPanel(m_panelType2Name[panelType]);
    }
    void ActivateParameterPanel(string panelName)
    {
        ParametersPanelType panelType = m_panelName2Type[panelName];
        Persistent.Instance.LastSelectedQuat3DParametersType = panelType;
        foreach (var item in m_parametersPanels)
        {
            item.Value.SetActive(item.Key == panelName);
        }
        // some initialization
        GameObject panel = m_parametersPanels[panelName];

        switch (panelType)
        {
        case ParametersPanelType.CoefficientsC:
            {
                new EnumWidget(new Ref<int>(() => GetActiveFractal().PowerCindex, v => { GetActiveFractal().PowerCindex = v; }), UIUtils.FindGameObjectInChildren(panel, "Dropdown C"), this.OnCoefficientCPowerChanged).OnChangeDelegate += UpdateUndoList;
                OnCoefficientCPowerChanged(GetActiveFractal().PowerCindex);
                new BoolWidget(new Ref<bool>(() => GetActiveFractal().CoefficientBeforePower, v => { GetActiveFractal().CoefficientBeforePower = v; }), UIUtils.FindComponentInChildren<Toggle>(panel, "Toggle Coefficient Before Power")).OnChangeDelegate += UpdateUndoList;
                new BoolWidget(new Ref<bool>(() => GetActiveFractal().CoefficientAfterPower, v => { GetActiveFractal().CoefficientAfterPower = v; }), UIUtils.FindComponentInChildren<Toggle>(panel, "Toggle Coefficient After Power")).OnChangeDelegate += UpdateUndoList;
            }
            break;
        case ParametersPanelType.CoefficientsC0:
            {
                new EnumWidget(new Ref<int>(() => GetActiveFractal().PowerC0index, v => { GetActiveFractal().PowerC0index = v; }), UIUtils.FindGameObjectInChildren(panel, "Dropdown C0"), this.OnCoefficientC0PowerChanged).OnChangeDelegate += UpdateUndoList;
                OnCoefficientC0PowerChanged(GetActiveFractal().PowerC0index);
                new BoolWidget(new Ref<bool>(() => GetActiveFractal().CoefficientC0BeforePower, v => { GetActiveFractal().CoefficientC0BeforePower = v; }), UIUtils.FindComponentInChildren<Toggle>(panel, "Toggle Coefficient Before Power")).OnChangeDelegate += UpdateUndoList;
                new BoolWidget(new Ref<bool>(() => GetActiveFractal().CoefficientC0AfterPower, v => { GetActiveFractal().CoefficientC0AfterPower = v; }), UIUtils.FindComponentInChildren<Toggle>(panel, "Toggle Coefficient After Power")).OnChangeDelegate += UpdateUndoList;
            }
            break;
        case ParametersPanelType.LimitArea:
            {
                new EnumWidget(new Ref<int>(() => GetActiveFractal().LimitAreaType, v => { GetActiveFractal().LimitAreaType = v; }), UIUtils.FindGameObjectInChildren(panel, "Dropdown Area Type"), this.OnAreaTypeChanged).OnChangeDelegate += UpdateUndoList;
                OnAreaTypeChanged(GetActiveFractal().LimitAreaType);
            }
            break;
        case ParametersPanelType.Computation:
            {
                new FloatWidget(new Ref<float>(() => GetActiveFractal().MinCubeSize, v => { GetActiveFractal().MinCubeSize = v; }), UIUtils.FindInputFieldInChildren(panel, "InputField Min Cube Size")).OnChangeDelegate += UpdateUndoList;
                new IntWidget(new Ref<int>(() => GetActiveFractal().MaxComputationStepCount, v => { GetActiveFractal().MaxComputationStepCount = v; }), UIUtils.FindInputFieldInChildren(panel, "InputField Max Computation Step")).OnChangeDelegate += UpdateUndoList;
                new EnumWidget(new Ref<int>(() => (int)GetActiveFractal().ComputeDelegateType, v => { GetActiveFractal().ComputeDelegateType = (QuaternionDelegateType)v; this.ComputationTypeChanged((int)GetActiveFractal().ComputeDelegateType); }), UIUtils.FindGameObjectInChildren(panel, "Dropdown Algorithm Type"), null).OnChangeDelegate += UpdateUndoList;
                new EnumWidget(new Ref<int>(() => (int)GetActiveFractal().CubeFineness, v => { GetActiveFractal().CubeFineness = (QuaternionFractal3D.Fineness)v; }), UIUtils.FindGameObjectInChildren(panel, "Dropdown Fineness Type"), this.FinenessTypeChanged).OnChangeDelegate += UpdateUndoList;
                ComputationTypeChanged((int)GetActiveFractal().ComputeDelegateType);
                InitComputationArea();
            }
            break;
        case ParametersPanelType.Mapping:
            {
                InitMapping(panel);
            }
            break;
        case ParametersPanelType.Other:
            {
                new StringWidget(new Ref<string>(() => GetActiveFractal().Name, v => { GetActiveFractal().Name = v; }), UIUtils.FindInputFieldInChildren(panel, "InputField Name")).OnChangeDelegate += UpdateUndoList;
                int textureSize = 2;
                Texture2D t = new Texture2D(textureSize, textureSize);
                TextureUtils.FillWithColor(t, GetActiveFractal().FractalColor);
                m_imageFractalColor.texture = t;

                t = new Texture2D(textureSize, textureSize);
                TextureUtils.FillWithColor(t, GetActiveFractal().FractalBackgroundColor);
                m_imageFractalBackgroundColor.texture = t;

                m_filenameText.text = GetActiveFractal().FilePath;
            }
            break;
        }
        ActivateToggle(panelName);

        UpdateUndoRedoButtons();
        Persistent.Instance.LastSelectedQuat3DParametersType = panelType;
    }

    void SetPanelAccordingToChangedProperty(string propertyName)
    {
        if (propertyName == "")
        {
            return;
        }

        if (!m_propertyOnPanel.ContainsKey(propertyName))
        {
            UnityEngine.Debug.Assert(false, string.Format("not handled property name {0}", propertyName));
            return;
        }

        ActivateParameterPanel(m_propertyOnPanel[propertyName]);
    }

    void ActivateToggle(string panelName)
    {
        SetParameterToggle(m_panelName2Type[panelName], true);
    }

    void ComputationTypeChanged(int newType)
    {
        GetActiveFractal().ComputeDelegateType = (QuaternionDelegateType)newType;
        HandleToggleVisibility();
    }

    bool ApplyEditedChanges()
    {
        string error = ValidateEditedParameters();
        if (error.Length > 0)
        {
            MessagePanel.Message(error);
            return false;
        }

        string computationParameters = GetActiveFractal().GetComputationParameters();
        UpdateAxesCenterAndBoundingBox();
        // background
        m_camera.backgroundColor = GetActiveFractal().FractalBackgroundColor;
        return true;
    }

    string ValidateEditedParameters()
    {
        if (GetActiveFractal().ComputationArea.SizeX < 0)
        {
            ActivateParameterPanel(ParametersPanelType.Computation);
            return "Size X - Computation Area size has to be positive";
        }
        if (GetActiveFractal().ComputationArea.SizeY < 0)
        {
            ActivateParameterPanel(ParametersPanelType.Computation);
            return "Size Y - Computation Area size has to be positive";
        }
        if (GetActiveFractal().ComputationArea.SizeZ < 0)
        {
            ActivateParameterPanel(ParametersPanelType.Computation);
            return "Size Z - Computation Area size has to be positive";
        }
        if (GetActiveFractal().LimitArea is CircularQuaternionArea &&
            (GetActiveFractal().LimitArea as CircularQuaternionArea).Radius <= 0)
        {
            ActivateParameterPanel(ParametersPanelType.LimitArea);
            return "Radius - Limit Area radius has to be positive";
        }
        if (GetActiveFractal().LimitArea is RectangularQuaternionArea &&
            (GetActiveFractal().LimitArea as RectangularQuaternionArea).Size.X0 <= 0)
        {
            ActivateParameterPanel(ParametersPanelType.LimitArea);
            return "X0 - Limit Area size has to be positive";
        }
        if (GetActiveFractal().LimitArea is RectangularQuaternionArea &&
            (GetActiveFractal().LimitArea as RectangularQuaternionArea).Size.I <= 0)
        {
            ActivateParameterPanel(ParametersPanelType.LimitArea);
            return "I - Limit Area size has to be positive";
        }
        if (GetActiveFractal().LimitArea is RectangularQuaternionArea &&
            (GetActiveFractal().LimitArea as RectangularQuaternionArea).Size.J <= 0)
        {
            ActivateParameterPanel(ParametersPanelType.LimitArea);
            return "J - Limit Area size has to be positive";
        }
        if (GetActiveFractal().LimitArea is RectangularQuaternionArea &&
            (GetActiveFractal().LimitArea as RectangularQuaternionArea).Size.K <= 0)
        {
            ActivateParameterPanel(ParametersPanelType.LimitArea);
            return "K - Limit Area size has to be positive";
        }
        if (GetActiveFractal().ComputeDelegateType == QuaternionDelegateType.Julia
            && !GetActiveFractal().CoefficientBeforePower
            && !GetActiveFractal().CoefficientAfterPower)
        {
            ActivateParameterPanel(ParametersPanelType.CoefficientsC);
            return "Check Use coefficients before power or Use coefficients after power or both";
        }
        if (GetActiveFractal().ComputeDelegateType == QuaternionDelegateType.MixedMJ
            && !GetActiveFractal().CoefficientBeforePower
            && !GetActiveFractal().CoefficientAfterPower
            && !GetActiveFractal().CoefficientC0BeforePower
            && !GetActiveFractal().CoefficientC0AfterPower)
        {
            ActivateParameterPanel(ParametersPanelType.CoefficientsC);
            return "At least one of Check Use coefficients before power or Use coefficients after power on Coefficient C page or Coefficient C0 page should be checked";
        }

        string mappingValidation = GetActiveFractal().Vec2QuatMapping.Validate();
        if (mappingValidation.Length > 0)
        {
            ActivateParameterPanel(ParametersPanelType.Mapping);
            return mappingValidation;
        }

        // no error
        return "";
    }

    void InitFractal()
    {
        // loading of last fractal
        if (Persistent.Instance.Seleced3DFractalFile.Length > 0 && File.Exists(Persistent.Instance.Seleced3DFractalFile))
        {
            QuaternionFractal3D fractal;
            QuaternionFractal3D.Load(Persistent.Instance.Seleced3DFractalFile, out fractal, true);
            SetFractal(m_activeFractalId, fractal);

            if (Persistent.Instance.CreateFractal)
            {
                // clearing of path
                fractal.FilePath = "";
            }
        }

        if (GetActiveFractal() == null)
        {
            InitDefaultFractal();
        }

        GetActiveFractal().GetUndoRedoList().AddNewValue();

        //m_quatFractal.GetGraphic().Load("C:/joy/graphic.sav");
        //m_quatFractal.GetGraphic().Save("C:/joy/graphic1.sav");
        //m_quatFractal.GetGraphic().Load("C:/joy/graphic1.sav");
        //m_quatFractal.Save("C:/joy/tempFractal." + Saver.GetExtension(Saver.SaveExtension.fractal3DZipExtension));
    }

    void SetLimitAreaType()
    {
        if (GetActiveFractal().LimitArea is CircularQuaternionArea)
        {
            GetActiveFractal().LimitAreaType = 0;
        }
        else
        {
            GetActiveFractal().LimitAreaType = 1;
        }
    }

    void InitDefaultFractal()
    {
        int colorCount = 25;
        m_activeFractalId = 0;
        QuaternionFractal3D fractal = new QuaternionFractal3D();
        SetFractal(m_activeFractalId, fractal);

        GetActiveFractal().SinglePalette = new SinglePalette(new Color(0, 0, 1, 1), new Color(1, 0, 0, 1));
        GetActiveFractal().FractalColor = Color.blue;
        GetActiveFractal().SliceColoringAlgorithm.ColorCount = colorCount;
        GetActiveFractal().C = new System.Collections.Generic.List<SmartMathLibrary.Quaternion>();
        GetActiveFractal().SetC(0, new SmartMathLibrary.Quaternion(-0.4, -0.5, 0.2, -0.1));
        GetActiveFractal().SetC(1, SmartMathLibrary.Quaternion.Zero);
        GetActiveFractal().SetC(2, new SmartMathLibrary.Quaternion(1, 0, 0, 0));

        GetActiveFractal().ComputeDelegateType = QuaternionDelegateType.Julia;
        GetActiveFractal().CubeFineness = QuaternionFractal3D.Fineness.Normal;
        GetActiveFractal().ComputationArea = new RectangularArea3D(0, 0, 0, 2, 2, 2);

        double[,] mapping = new double[4, 3];
        EMappingType mappingType = EMappingType.mtDirect;
        double k = 1;
        switch (mappingType)
        {
        case EMappingType.mtDirect:
            {
                mapping[0, 0] = k;
                mapping[1, 1] = k;
                mapping[2, 2] = k;
            }
            break;
        case EMappingType.mtSideDiagonal:
            {
                mapping[1, 0] = k;
                mapping[2, 1] = k;
                mapping[3, 2] = k;
            }
            break;
        case EMappingType.mtAntisymetrical:
            {
                mapping[2, 0] = k;
                mapping[1, 1] = -k;
                mapping[3, 0] = -k;
                mapping[1, 2] = k;
                mapping[3, 1] = k;
                mapping[2, 2] = -k;
            }
            break;
        case EMappingType.mtMainAntisymetrical:
            {
                mapping[1, 0] = k;
                mapping[0, 1] = -k;
                mapping[2, 0] = -k;
                mapping[0, 2] = k;
                mapping[2, 1] = k;
                mapping[1, 2] = -k;
            }
            break;
        }
        GetActiveFractal().Vec2QuatMapping = new Vector2QuaternionMapping(mapping);
        //Vector3D vector = new Vector3D(1,2,3);

        GetActiveFractal().MaxComputationStepCount = colorCount;
        GetActiveFractal().ComputeMinCubeSize(QuaternionFractal3D.Fineness.Low);
        GetActiveFractal().ComputeFractal(true);
    }
    void UpdateMinCubeSize()
    {
        InputField minCubeSizeInput = GameObject.Find("InputField Min Cube Size").GetComponent<InputField>();
        switch (GetActiveFractal().CubeFineness)
        {
        case QuaternionFractal3D.Fineness.UserDefined:
            {
                // enable editation of cubesize
                minCubeSizeInput.interactable = true;
            }
            break;
        default:
            //case QuaternionFractal3D.Fineness.High:
            //case QuaternionFractal3D.Fineness.ExtraHigh:
            //case QuaternionFractal3D.Fineness.UltraHigh:
            //case QuaternionFractal3D.Fineness.Low:
            //case QuaternionFractal3D.Fineness.Normal:
            {
                GetActiveFractal().RecomuteMinCubeSize();
                // disable editation of cubesize
                minCubeSizeInput.interactable = false;
            }
            break;
        }
        float step = GetActiveFractal().MinCubeSize;
        try
        {
            minCubeSizeInput.text = Convert.ToDecimal(step).ToString();
        }
        catch (Exception e)
        {
            //int i = 0;
        }
    }

    void FinenessTypeChanged(int newType)
    {
        QuaternionFractal3D.Fineness fineness = (QuaternionFractal3D.Fineness)newType;
        GetActiveFractal().CubeFineness = fineness;
        UpdateMinCubeSize();
    }

    void InitComputationArea()
    {
        string panelName = m_panelType2Name[ParametersPanelType.Computation];
        GameObject panel = m_parametersPanels[panelName];
        DoubleWidget widget = new DoubleWidget(new Ref<double>(() => GetActiveFractal().ComputationArea.CenterX, v => { GetActiveFractal().ComputationArea.CenterX = v; }), UIUtils.FindInputFieldInChildren(panel, "InputField Center X"));
        GetActiveFractal().ComputationArea.AddMethodObserver(widget, "CenterX");
        GetActiveFractal().ComputationArea.AddObserver(widget);
        widget.OnChangeDelegate += UpdateUndoList;
        widget = new DoubleWidget(new Ref<double>(() => GetActiveFractal().ComputationArea.CenterY, v => { GetActiveFractal().ComputationArea.CenterY = v; }), UIUtils.FindInputFieldInChildren(panel, "InputField Center Y"));
        GetActiveFractal().ComputationArea.AddMethodObserver(widget, "CenterY");
        widget.OnChangeDelegate += UpdateUndoList;
        GetActiveFractal().ComputationArea.AddObserver(widget);
        widget = new DoubleWidget(new Ref<double>(() => GetActiveFractal().ComputationArea.CenterZ, v => { GetActiveFractal().ComputationArea.CenterZ = v; }), UIUtils.FindInputFieldInChildren(panel, "InputField Center Z"));
        GetActiveFractal().ComputationArea.AddMethodObserver(widget, "CenterZ");
        widget.OnChangeDelegate += UpdateUndoList;
        GetActiveFractal().ComputationArea.AddObserver(widget);
        m_computationAreaX = new DoubleWidget(new Ref<double>(() => (GetActiveFractal().ComputationArea as RectangularArea3D).SizeX, v => { (GetActiveFractal().ComputationArea as RectangularArea3D).SizeX = v; }), UIUtils.FindInputFieldInChildren(panel, "InputField Size X"));
        GetActiveFractal().ComputationArea.AddMethodObserver(m_computationAreaX, "SizeX");
        m_computationAreaX.OnChangeDelegate += UpdateUndoList;
        //GetActiveFractal().ComputationArea.AddMethodObserver(m_computationAreaX, "set_HalfSizeX");
        GetActiveFractal().ComputationArea.AddObserver(m_computationAreaX);
        m_computationAreaY = new DoubleWidget(new Ref<double>(() => (GetActiveFractal().ComputationArea as RectangularArea3D).SizeY, v => { (GetActiveFractal().ComputationArea as RectangularArea3D).SizeY = v; }), UIUtils.FindInputFieldInChildren(panel, "InputField Size Y"));
        GetActiveFractal().ComputationArea.AddMethodObserver(m_computationAreaY, "SizeY");
        m_computationAreaY.OnChangeDelegate += UpdateUndoList;
        GetActiveFractal().ComputationArea.AddObserver(m_computationAreaY);
        m_computationAreaZ = new DoubleWidget(new Ref<double>(() => (GetActiveFractal().ComputationArea as RectangularArea3D).SizeZ, v => { (GetActiveFractal().ComputationArea as RectangularArea3D).SizeZ = v; }), UIUtils.FindInputFieldInChildren(panel, "InputField Size Z"));
        GetActiveFractal().ComputationArea.AddMethodObserver(m_computationAreaZ, "SizeZ");
        m_computationAreaZ.OnChangeDelegate += UpdateUndoList;
        GetActiveFractal().ComputationArea.AddObserver(m_computationAreaZ);
        m_computationAreaX.OnChangeDelegate += UpdateMinCubeSize;
        m_computationAreaY.OnChangeDelegate += UpdateMinCubeSize;
        m_computationAreaZ.OnChangeDelegate += UpdateMinCubeSize;
        UpdateMinCubeSize();
    }

    void InitExportPanel()
    {
        GameObject panel = m_menuPanels[EMenuPanelType.ExportPNG];
        new IntWidget(new Ref<int>(() => Persistent.Instance.Exported3dScreenshotWidth, v => { Persistent.Instance.Exported3dScreenshotWidth = v; }), UIUtils.FindInputFieldInChildren(panel, "InputField Image Width"));
        new IntWidget(new Ref<int>(() => Persistent.Instance.Exported3dScreenshotHeight, v => { Persistent.Instance.Exported3dScreenshotHeight = v; }), UIUtils.FindInputFieldInChildren(panel, "InputField Image Height"));
    }

    void UpdateUndoList()
    {
        GetActiveFractal().GetUndoRedoList().AddNewValue();
        UpdateUndoRedoButtons();
    }

    void InitMapping()
    {
        GameObject panel = m_parametersPanels[m_panelType2Name[ParametersPanelType.Mapping]];
        InitMapping(panel);
    }

    void InitMapping(GameObject panel)
    {
        for (int row = 0; row < GetActiveFractal().Vec2QuatMapping.GetRowCount(); row++)
        {
            for (int column = 0; column < GetActiveFractal().Vec2QuatMapping.GetColumnCount(); column++)
            {
                int r = row;
                int c = column;
                string inputFieldName = string.Format("InputField M{0}{1}", row, column);
                new DoubleWidget(new Ref<double>(
                    () => GetActiveFractal().Vec2QuatMapping.GetCoefficient(r, c),
                    v => { GetActiveFractal().Vec2QuatMapping.SetCoefficient(r, c, v); }),
                    UIUtils.FindInputFieldInChildren(panel, inputFieldName)).OnChangeDelegate += UpdateUndoList;
            }
        }
    }

    void ActivateAreaPanel(string panelName)
    {
        foreach (var item in m_limitAreaPanels)
        {
            item.Value.SetActive(item.Key == panelName);
        }
    }

    void InitParametersToggles()
    {
        m_panelName2Type["Panel Coefficient C"] = ParametersPanelType.CoefficientsC;
        m_panelName2Type["Panel Coefficient C0"] = ParametersPanelType.CoefficientsC0;
        m_panelName2Type["Panel Limit Area"] = ParametersPanelType.LimitArea;
        m_panelName2Type["Panel Computation"] = ParametersPanelType.Computation;
        m_panelName2Type["Panel Mapping"] = ParametersPanelType.Mapping;
        m_panelName2Type["Panel Other"] = ParametersPanelType.Other;

        foreach (var pair in m_panelName2Type)
        {
            // opposite direction 
            m_panelType2Name[pair.Value] = pair.Key;
        }

        m_mapParametersTogglePanel.Add("Toggle Coefficient C", m_panelType2Name[ParametersPanelType.CoefficientsC]);
        m_mapParametersTogglePanel.Add("Toggle Coefficient C0", m_panelType2Name[ParametersPanelType.CoefficientsC0]);
        m_mapParametersTogglePanel.Add("Toggle Limit Area", m_panelType2Name[ParametersPanelType.LimitArea]);
        m_mapParametersTogglePanel.Add("Toggle Computation", m_panelType2Name[ParametersPanelType.Computation]);
        m_mapParametersTogglePanel.Add("Toggle Mapping", m_panelType2Name[ParametersPanelType.Mapping]);
        m_mapParametersTogglePanel.Add("Toggle Other", m_panelType2Name[ParametersPanelType.Other]);

        foreach (var item in m_mapParametersTogglePanel)
        {
            string toggleName = item.Key;
            GameObject toggleObject = GameObject.Find(toggleName);
            UnityEngine.Debug.Assert(toggleObject, "missing toggle");
            m_parametersToggles.Add(toggleName, toggleObject);

            ParametersPanelType panelType = m_panelName2Type[item.Value];
            m_parametersTypeToToggles.Add(panelType, toggleObject.GetComponent<Toggle>());

            UIUtils.AddPanelToDictionary(m_parametersPanels, item.Value);
        }

        UIUtils.AddPanelToDictionary(m_limitAreaPanels, "Panel Limit Area Circular");
        UIUtils.AddPanelToDictionary(m_limitAreaPanels, "Panel Limit Area Rectangular");
    }

    void CenterSceneOnActiveFractalCenter()
    {
        // movimg to the center of computed area or center of graphic bounds
        if (GetSceneBaseObject() != null && GetActiveFractal() != null)
        {
            UnityEngine.Quaternion rotation = GetSceneBaseObject().transform.rotation;

            if (GetActiveFractal().GetGraphic() != null)
            {
                Bounds bounds = GetActiveFractal().GetGraphic().GetBounds(1.0f, new Bounds(Vector3.zero, Vector3.one.Mult(GetActiveFractal().ComputationArea.GetMaxSize())));
                GetSceneBaseObject().transform.position = rotation * (-bounds.center);
            }
            else
            {
                GetSceneBaseObject().transform.position = rotation * (-GetActiveFractal().ComputationArea.GetCenter()).Vector3;
            }
        }
    }

    void UpdateAxesCenterAndBoundingBox()
    {
        CenterSceneOnActiveFractalCenter();

        ShowAxes = m_showAxes;
        ShowComputationBoundingBox = m_showComputationBoundingBox;
    }


    void ActivateFractal(int fractalIndex)
    {
        if (!m_fractals.ContainsKey(fractalIndex))
        {
            return;
        }
        m_activeFractalId = fractalIndex;
        if (m_showComputationBoundingBox)
        {
            CreateComputationAreaObjectAccordingToComputingArea();
            CreateLightPointers();
        }
        if (m_showOnlyOneFractal)
        {
            DisplayOnlyOneFractal();
        }

        // setting of toggle
        GameObject selectionToggle = GameObject.Find(string.Format(ToggleSelectFractalNumber, m_activeFractalId));
        if (selectionToggle != null)
        {
            selectionToggle.GetComponent<Toggle>().isOn = true;
        }

        SetFractalVisible(fractalIndex, true);
        SetToggleFractalVisible(fractalIndex, true);

        HandleFractalDisplayColors();
    }

    void CreateAxisArrowsAccordingToComputingArea()
    {
        if (m_axes != null)
        {
            GameObject.DestroyObject(m_axes.GetBaseObject());
        }
        m_axes = new AxisArrows(Vector3.zero, 2);
        m_axes.GetBaseObject().transform.parent = GetSceneBaseObject().transform;
        m_axes.GetBaseObject().transform.localPosition = Vector3.zero;
        m_axes.GetBaseObject().transform.localRotation = UnityEngine.Quaternion.Euler(0, 0, 0);
    }

    void CreateComputationAreaObjectAccordingToComputingArea()
    {
        if (m_computationBoundingBox != null)
        {
            DestroyObject(m_computationBoundingBox.GetBaseObject());
        }
        m_computationBoundingBox = new BoundingBoxObject(GetSceneBaseObject(), GetActiveFractal().ComputationArea.GetCenter().Vector3, GetActiveFractal().ComputationArea);
    }

    void CreateLightPointers()
    {
        if (m_lightPointers != null)
        {
            DestroyObject(m_lightPointers.GetBaseObject());
        }
        m_lightPointers = new LightPointersObject(GetSceneBaseObject(), GetActiveFractal().ComputationArea.GetCenter().Vector3, GetActiveFractal().ComputationArea);
    }

    void ResetLight()
    {
        m_directionalLight.transform.rotation = GetResetLightRotation();
    }

    public static UnityEngine.Quaternion GetResetLightRotation()
    {
        UnityEngine.Quaternion result;
        if (Persistent.Instance.AxisSystem == Persistent.AxisSystemType.Left)
        {
            result = UnityEngine.Quaternion.FromToRotation(new Vector3(0, 0, 1), new Vector3(1, 1, 1));
        }
        else
        {
            result = UnityEngine.Quaternion.FromToRotation(new Vector3(0, 0, 1), new Vector3(-1, -1, 1));
        }
        return result;
    }

    int GetSelectedFractalIndex(string selectedObjectFilter)
    {
        if (EventSystem.current.currentSelectedGameObject == null)
        {
            return -1;
        }
        string selectedItem = EventSystem.current.currentSelectedGameObject.name;
        if (!selectedItem.Contains(selectedObjectFilter))
        {
            return -1;
        }
        selectedItem = selectedItem.Replace(selectedObjectFilter, "");
        selectedItem = selectedItem.TrimStart(' ');

        int selectedItemNumber = -1;
        try
        {
            selectedItemNumber = int.Parse(selectedItem);
        }
        catch { };
        return selectedItemNumber;
    }

    QuaternionFractal3D GetActiveFractal()
    {
        if (m_activeFractalId < 0 || m_activeFractalId >= m_fractals.Count)
        {
            return null;
        }
        return m_fractals[m_activeFractalId];
    }
    void SetFractal(int id, QuaternionFractal3D fractal)
    {
        m_fractals[id] = fractal;
    }
    string GetFractalExtractedDirectory(QuaternionFractal3D fractal, int fractalNumber)
    {
        return GetBasic3DFractalDirectory() + string.Format("{0}", fractalNumber);
    }
    string GetBasic3DFractalDirectory()
    {
        return "D:/fractals/3D/";
    }

    int GetFractalsCount()
    {
        return m_fractals.Count;
    }
    int GetFractalsMaxCount()
    {
        return 3;
    }
    QuaternionFractal3D GetFractalByIndex(int index)
    {
        if (m_fractals.ContainsKey(index))
        {
            return m_fractals[index];
        }
        return null;
    }

    void MakeScreenshots()
    {
        bool previousShowAxes = ShowAxes;
        bool previousShowComputationBoundingBox = ShowComputationBoundingBox;
        ShowAxes = false;
        ShowComputationBoundingBox = false;

        MakeScreenshots(GetActiveFractal());

        ShowAxes = previousShowAxes;
        ShowComputationBoundingBox = previousShowComputationBoundingBox;
    }

    public static void MakeScreenshots(QuaternionFractal3D fractal)
    {
        if (!fractal.GraphicIsComputed())
        {
            // delete of screenshots that are not valid already
            Persistent.DeleteQuaternionFractalScreenshots(fractal.FilePath);
            return;
        }
        GameObject.Find("Main Camera").GetComponent<FractalScreenshotRecorder>().MakeScreenshots(fractal);
    }

    void HandleButtonCopyVisibility()
    {
        m_buttonCopy.SetActive(m_fractals.Count > 1);
    }

    Color GetActualBackgroundColor()
    {
        return GetActiveFractal().FractalBackgroundColor;
    }

    List<Color> GetReplacementColors()
    {
        List<Color> colors = new List<Color>();
        colors.Add(new Color(1, 0, 1));
        colors.Add(new Color(0, 1, 1));
        colors.Add(new Color(1, 1, 0));
        colors.Add(new Color(0.1f, 0.9f, 0.8f));
        colors.Add(new Color(0.5f, 0.5f, 0.8f));
        colors.Add(new Color(0.9f, 0.1f, 0.8f));
        colors.Add(new Color(0.9f, 0.9f, 0.3f));
        colors.Add(new Color(0.3f, 0.1f, 0.5f));

        Color backgroundColor = GetActualBackgroundColor();
        for (int i = 0; i < colors.Count;)
        {
            if (OtherUtils.AreColorsClose(backgroundColor, colors[i]))
            {
                colors.Remove(colors[i]);
            }
            else
            {
                i++;
            }
        }

        return colors;
    }

    void HandleFractalDisplayColors()
    {
        // background color
        Color backgroundColor = GetActiveFractal().FractalBackgroundColor;
        m_camera.backgroundColor = backgroundColor;

        List<QuaternionFractal3D> displayedFractals = GetDisplayedFractals();
        if (displayedFractals.Count < 2)
        {
            // only one or zero displayed
            SetAllShowFractalTogglesWhite();
            foreach (var fractal in displayedFractals)
            {
                int fractalIndex = GetFractalIndex(fractal);
                SetFractalDisplayColor(fractalIndex, fractal.FractalColor, Color.white);
            }
            return;
        }

        List<Color> replacementColors = GetReplacementColors();
        int replacementColorIndex = 0;
        Color activeFractalColor = GetActiveFractal().FractalColor;
        foreach (var fractal in displayedFractals)
        {
            int fractalIndex = GetFractalIndex(fractal);
            if (fractal == GetActiveFractal())
            {
                SetFractalDisplayColor(fractalIndex, fractal.FractalColor, Color.white);
            }
            else
            {
                Color replacementColor = replacementColors[replacementColorIndex];
                while (OtherUtils.AreColorsClose(replacementColor, activeFractalColor))
                {
                    replacementColorIndex++;
                    replacementColor = replacementColors[replacementColorIndex];
                }
                replacementColorIndex++;
                SetFractalDisplayColor(fractalIndex, replacementColor, replacementColor);
            }
        }
    }

    void SetAllShowFractalTogglesWhite()
    {
        foreach (var fractalPair in m_fractals)
        {
            SetShowFractalToggleColor(fractalPair.Key, Color.white);
        }
    }

    int GetFractalIndex(QuaternionFractal3D fractal)
    {
        foreach (var fractalPair in m_fractals)
        {
            if (fractalPair.Value == fractal)
            {
                return fractalPair.Key;
            }
        }
        return -1;
    }

    List<QuaternionFractal3D> GetDisplayedFractals()
    {
        List<QuaternionFractal3D> fractalList = new List<QuaternionFractal3D>();
        foreach (var fractalPair in m_fractals)
        {
            int fractalIndex = fractalPair.Key;
            if (IsFractalDisplayed(fractalIndex))
            {
                fractalList.Add(fractalPair.Value);
            }
        }
        return fractalList;
    }

    int NumberOfDisplayedFractals()
    {
        int displayedNumber = 0;
        foreach (var fractalPair in m_fractals)
        {
            int fractalIndex = fractalPair.Key;
            if (IsFractalDisplayed(fractalIndex))
            {
                displayedNumber++;
            }
        }
        return displayedNumber;
    }

    void SetFractalDisplayColor(int fractalIndex, Color color, Color toggleColor)
    {
        // sets color of fractal and its adjacent toggle
        if (!IsFractalsIndexValid(fractalIndex))
        {
            return;
        }
        GetFractalByIndex(fractalIndex).SetGraphicColor(color);
        SetShowFractalToggleColor(fractalIndex, toggleColor);
    }

    void SetShowFractalToggleColor(int fractalIndex, Color color)
    {
        string toggleName = string.Format(ToggleShowFractalNumber, fractalIndex);
        GameObject toggleObject = GameObject.Find(toggleName);
        if (toggleObject == null)
        {
            return;
        }
        Toggle toggle = toggleObject.GetComponent<Toggle>();
        ColorBlock colorBlock = toggle.colors;
        colorBlock.normalColor = color;
        colorBlock.highlightedColor = color;
        toggle.colors = colorBlock;
    }

    bool IsFractalsIndexValid(int fractalIndex)
    {
        return fractalIndex >= 0 && fractalIndex < m_fractals.Count;
    }

    void UpdateUndoRedoButtons()
    {
        m_buttonUndo.interactable = GetActiveFractal().GetUndoRedoList().IsUndoAvailable();
        m_buttonRedo.interactable = GetActiveFractal().GetUndoRedoList().IsRedoAvailable();
    }

    GameObject GetSceneBaseObject() { return m_sceneBaseObject; }

    private enum ComputeConnectionsPhase
    {
        GettingBoxes, // update of ui
        GettingBoxes2,// real getting (in main thread)
        SortingBoxes,
        SortingBoxes2,
        ComputingConnections,
        ComputingConnections2,
        SortingConnections, // update of ui
        SortingConnections2,// sorting
        CreatingConnections, // update of interface
        CreatingConnections2,// real creating (int main thread)
        SaveSTL,
    };

    //////////////////// class variables ////////////////////
    const string ToggleShowFractal = "Toggle Show Fractal";
    const string ToggleShowFractalNumber = ToggleShowFractal + " {0}";
    const string ToggleSelectFractal = "Toggle Fractal Selection";
    const string ToggleSelectFractalNumber = ToggleSelectFractal + " {0}";
    Dictionary<int, QuaternionFractal3D> m_fractals = new Dictionary<int, QuaternionFractal3D>();
    int m_activeFractalId = 0;
    Dictionary<EMenuPanelType, GameObject> m_menuPanels = new Dictionary<EMenuPanelType, GameObject>();
    GameObject m_panelMain;
    GameObject m_textAllParameters;
    GameObject m_buttonCopy;
    Dropdown m_copyFromFractalDropwown;
    Dropdown m_dropdownC;
    RawImage m_imageFractalColor;
    RawImage m_imageFractalBackgroundColor;
    Text m_filenameText;
    GameObject m_sceneBaseObject;
    AxisArrows m_axes;
    BoundingBoxObject m_computationBoundingBox;
    LightPointersObject m_lightPointers;
    Light m_directionalLight;
    Button m_buttonUndo;
    Button m_buttonRedo;
    Toggle m_toggleShowAxis;
    Toggle m_toggleShowComputationArea;

    Camera m_camera;
    bool m_rotateCamera = true;
    bool m_lookingAround = false;
    UnityEngine.Quaternion m_startLookingAroundRotation;

    Dictionary<string, string> m_mapParametersTogglePanel = new Dictionary<string, string>();    // map of name of active toggle to name of panel that shold be activated
    Dictionary<string, GameObject> m_parametersToggles = new Dictionary<string, GameObject>();
    Dictionary<ParametersPanelType, Toggle> m_parametersTypeToToggles = new Dictionary<ParametersPanelType, Toggle>();
    Dictionary<string, GameObject> m_parametersPanels = new Dictionary<string, GameObject>();
    Dictionary<string, GameObject> m_limitAreaPanels = new Dictionary<string, GameObject>();

    Dictionary<string, ParametersPanelType> m_panelName2Type = new Dictionary<string, ParametersPanelType>();
    Dictionary<ParametersPanelType, string> m_panelType2Name = new Dictionary<ParametersPanelType, string>();
    bool m_showAxes = false;
    bool m_showComputationBoundingBox = false;
    bool m_showOnlyOneFractal = false;
    DoubleWidget m_computationAreaX = null;
    DoubleWidget m_computationAreaY = null;
    DoubleWidget m_computationAreaZ = null;
    string m_loadPath;
    Vector3 initMousePos = new Vector3(-1, -1, -1);
    Vector3 lastMousePos;
    List<GameObject> m_testingPoints = new List<GameObject>();
    private ModalPanel m_questionPanel;
    Dictionary<string, ParametersPanelType> m_propertyOnPanel = new Dictionary<string, ParametersPanelType>();
    bool m_isPanelHidden;
    float m_hideTime = 0;
    GameObject m_activePanel;
    const float MinHideDelay = 0.1f;
    _3DComputationStruct m_computationStruct;
    bool m_connectionsAreComputed; // connections are computed and visual objects are added
    bool m_computingConnections;    // connection are comuted just now
    ComputeConnectionsPhase m_computeConnectionsPhase;
    int m_totalConnectionsProgressRowCount;  // number of x coordinates that have to be computed
    int m_computedConnectionsProgressRowCount;
    List<B> m_intBoxes; // boxes with graphis for computing of connections
    Bounds m_fractalBounds;
    List<B> m_zConnectionCubes;
    List<B> m_xConnectionCubes;
    List<B> m_yConnectionCubes;
    List<Vector3Int> m_zConnections;
    List<Vector3Int> m_xConnections;
    List<Vector3Int> m_yConnections;
    List<Vector3Int> m_connections111;
    List<Vector3Int> m_connections11m1;
    List<Vector3Int> m_connections1m11;
    List<Vector3Int> m_connections1m1m1;
    const int CONNECTING_THREAD_COUNT = 32;
    int m_finishedConnectingThreads;
    private System.Object m_listLock = new System.Object();
    Stopwatch sw = new Stopwatch();
}

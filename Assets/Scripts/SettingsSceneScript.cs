﻿using Assets.Scripts.Utils;
using UnityEngine;
using UnityEngine.UI;

public class SettingsSceneScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
        InitColors();
        
        GameObject panel = GameObject.Find("Panel");

        EnumWidget axisSystemTypeWidget = new EnumWidget(new Ref<int>(() => (int)Persistent.Instance.AxisSystem,
        v => {
            Persistent.Instance.AxisSystem = (Persistent.AxisSystemType)v;
        }),
        UIUtils.FindComponentInChildren<Dropdown>(panel, "Dropdown Axis System"));

        new EnumWidget(new Ref<int>(() => (int)Persistent.Instance.FormOfComplexNumbers,
        v => {
            Persistent.Instance.FormOfComplexNumbers = (Persistent.ComplexNumberForm)v;
        }),
        UIUtils.FindComponentInChildren<Dropdown>(panel, "Dropdown Complex Form"));

        new FloatWidget(new Ref<float>(() => Persistent.Instance.MaxSliderValue, v => { Persistent.Instance.MaxSliderValue = v; }), UIUtils.FindInputFieldInChildren(panel, "InputField Slider Limit"));
    }

    void InitColors()
    {
        int textureSize = 30;
        Texture2D t1 = new Texture2D(textureSize, textureSize);
        TextureUtils.FillWithColor(t1, Persistent.Instance.DefaultBackgroundColor);
        GameObject.Find("Image Background Color").GetComponent<RawImage>().texture = t1;

        Texture2D t2 = new Texture2D(textureSize, textureSize);
        TextureUtils.FillWithColor(t2, Persistent.Instance.DefaultForegroundColor);
        GameObject.Find("Image Foreground Color").GetComponent<RawImage>().texture = t2;
    }

    // Update is called once per frame
    void Update () {
	
	}

    public void OnButtonBackgroundColor()
    {
        Persistent.Instance.SelColorDelegate = (Color color) =>
        {
            Persistent.Instance.DefaultBackgroundColor = color;
            InitColors();
        };
        Persistent.Instance.SelectedColor = Persistent.Instance.DefaultBackgroundColor;
        MySceneManager.GoToScene("ColorPicking");
    }

    public void OnButtonForegroundColor()
    {
        Persistent.Instance.SelColorDelegate = (Color color) =>
        {
            Persistent.Instance.DefaultForegroundColor = color;
            InitColors();
        };
        Persistent.Instance.SelectedColor = Persistent.Instance.DefaultForegroundColor;
        MySceneManager.GoToScene("ColorPicking");
    }

    public void OnOkClicked()
    {
        if (!CheckParameters())
        {
            return;
        }

        MySceneManager.Instance.PopScene();
    }

    bool CheckParameters()
    {
        string error = ValidateEditedParameters();
        if (error.Length > 0)
        {
            MessagePanel.Message(error);
            return false;
        }
        return true;
    }

    string ValidateEditedParameters()
    {
        if (Persistent.Instance.MaxSliderValue <= 0)
        {
            return "Slider Limit has to be positive float number";
        }

        // no error
        return "";
    }



}
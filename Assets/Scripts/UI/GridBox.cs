﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class GridBox
    {
        public const int NoSelection = -1;
        const int MaxClickedItems = 4;
        const float HoldTimeLimit = 1.0f;   // action OnHolded is called after that time of holding of some item
        const float MaxDoubleClickTime = 0.5f;

        public GridBox(GameObject scrollView, int itemWidth, int itemSpacing, bool c_vertScrollV = false, GUISkin c_customSkin = null)// nefungule double click detekce ani hold detekce, ale zase nepouziva gui.Button funkce 
        {
            Scrollbar bar = scrollView.GetComponent<Scrollbar>();
            m_scrollView = scrollView;
            m_scrollbar = bar;

            m_whiteBackground = GameObject.Find("Image White Background");
            float cellSize = itemWidth + 2 * itemSpacing;
            m_whiteBackground.GetComponent<RectTransform>().sizeDelta = new Vector2(cellSize, cellSize);

            m_scrollBarWidth = GetScrollbarWidth();

            RectTransform transform = bar.GetComponent<RectTransform>();

            Rect area = transform.rect;
            area.x = transform.rect.xMin;//transform.position.x - transform.pivot.x * area.width;
            area.y = transform.rect.yMax;

            Rect visibleRect = new Rect(0, 0, transform.rect.width, transform.rect.height);

            Init(area, itemWidth, itemSpacing, visibleRect, c_vertScrollV, c_customSkin);

            m_rowLength = GetMaxHorizontalCount();
        }

        // ======================= properties ======================
        public ListItemDoubleclicked onDoubleClickEvent = new ListItemDoubleclicked();
        public ListSelectionChanged onSelectionChangedEvent = new ListSelectionChanged();

        public int Count
        {
            get { return m_listItems.Count; }
        }

        public int SelectedIndex
        {
            get { return m_SelectedIndex; }
            set
            {
                if (value == NoSelection)
                {
                    m_SelectedIndex = NoSelection;
                    ClearSelection();
                    return;
                }
                if (value < 0 || value >= m_listItems.Count)
                    return;
                if (m_SelectedIndex != value)
                {
                    m_SelectedIndex = value;
                    //MakeWhiteBackground(m_SelectedIndex);
                    onSelectionChangedEvent.Invoke(value);
                }

                // visual selection
                if (!SetSelectedItem(value))
                {
                    m_SelectedIndex = NoSelection;
                    ClearSelection();
                }
                else
                {
                    MakeWhiteBackground(m_SelectedIndex);
                }
            }
        }

        // ========================== public =======================
        public int GetMaxHorizontalCount()
        {
            int sizeFormItems = (int)(m_drawRect.width + 0.5) - GetScrollbarWidth();
            return sizeFormItems / GetTotalItemWidth();
        }

        public int GetMaxVerticalCount()
        {
            return (m_listItems.Count + GetMaxHorizontalCount() - 1) / GetMaxHorizontalCount();
        }

        int GetTotalItemWidth()
        {
            return m_itemWidth + 2 * m_itemSpacing;
        }

        int GetTotalItemHeigth()
        {
            return m_itemHeight + 2 * m_itemSpacing;
        }

        public void Update()
        {
            if (m_needRecomputePositions)
            {
                RecountButtonTransformsAndResizeArea(GetTotalItemHeigth());
                m_needRecomputePositions = false;
            }
        }

        public int GetCurSel()
        {
            return SelectedIndex;
        }

        public System.Object GetItemData(int itemIndex)
        {
            if (!IsIndexValid(itemIndex))
                return null;
            return m_listItems[itemIndex].ItemData;
        }

        public bool SetItemData(int itemIndex, System.Object itemData)
        {
            if (!IsIndexValid(itemIndex))
            {
                return false;
            }
            m_listItems[itemIndex].ItemData = itemData;
            return true;
        }

        public void Clear()
        {
            m_listItems.Clear();
            if (m_scrollView != null)
            {
                // clearing of buttons
                foreach (Button button in m_scrollView.GetComponentsInChildren(typeof(Button)))
                {
                    UnityEngine.Object.Destroy(button.gameObject);
                }
                RectTransform contentTransform = GetContentTransform();
                contentTransform.sizeDelta = new Vector2(contentTransform.sizeDelta.x, 0);    // resizing of listbox content area
            }
            SelectedIndex = NoSelection;
        }

        public int AddItem(Texture2D c_ItemIcon, GUIStyle style = null)
        {
            int itemIndex = m_listItems.Count;
            GridListItem newListItem = new GridListItem(this, new Rect(0, 0, GetTotalItemWidth(), GetTotalItemHeigth()), style);
            m_listItems.Add(newListItem);

            if (m_scrollView != null)
            {
                // creating of a button

                var buttonObject = new GameObject("Button");
                Button button = buttonObject.AddComponent<Button>();

                var image = buttonObject.AddComponent<RawImage>();
                image.rectTransform.sizeDelta = new Vector2(GetTotalItemWidth(), GetTotalItemHeigth());
                image.rectTransform.pivot = new Vector2(0.0f, 1.0f);
                image.texture = c_ItemIcon;

                button.targetGraphic = image;
                button.onClick.AddListener(() => Debug.Log(Time.time));
                button.onClick.AddListener(delegate { OnClickedItem(itemIndex); });
                button.GetComponent<RectTransform>().pivot = new Vector2(0.0f, 1.0f);

                RectTransform contentTransform = GetContentTransform();
                button.transform.SetParent(contentTransform, false);
                ColorBlock cb = button.colors;
                cb.highlightedColor = new Color(224 / 255.0f, 224 / 255.0f, 224 / 255.0f);//new Color(224/255.0f, 227/255.0f, 27/255.0f);
                if (style != null)
                {
                    cb.highlightedColor = style.hover.textColor;
                    cb.pressedColor = style.active.textColor;
                }
                button.colors = cb;
                newListItem.Button = button;

                m_needRecomputePositions = true;
            }

            return itemIndex;
        }

        // ========================= private =======================
        private void Init(Rect c_drawRect, int itemWidth, int itemSpacing, Rect c_visibleRect, bool c_vertScrollV = false, GUISkin c_custom_skin = null)
        {
            m_drawRect = c_drawRect;
            m_visibleRect = c_visibleRect;

            m_itemWidth =  itemWidth;
            m_itemHeight = itemWidth;
            m_itemSpacing = itemSpacing;

            m_horScrollVisible = false;
            m_vertScrollVisible = c_vertScrollV;

            if (c_custom_skin)
            {
                m_custom_skin = c_custom_skin;
                m_customSkinFlag = true;
            }
        }

        void OnClickedItem(int itemId)
        {
            SelectedIndex = itemId;
            SClickedListItem lastClicked = GetLastClickedItem();
            AddNewClickedItem(itemId);
            if (lastClicked.time != 0 && lastClicked.itemIndex == itemId)
            {
                // detection of double click
                if (Time.time - lastClicked.time < MaxDoubleClickTime)
                {
                    OnDoubleClick(itemId);
                }
            }
        }

        private SClickedListItem GetLastClickedItem()
        {
            if (m_clickedIndexes.Count == 0)
            {
                return new SClickedListItem();
            }
            return m_clickedIndexes[m_clickedIndexes.Count - 1];
        }

        private void AddNewClickedItem(int selectedIndex)
        {
            SClickedListItem newClickedItem = new SClickedListItem();
            newClickedItem.time = Time.time;
            newClickedItem.itemIndex = selectedIndex;

            while (m_clickedIndexes.Count >= MaxClickedItems)
            {
                // remove first records (oldest)
                m_clickedIndexes.RemoveAt(0);
            }

            m_clickedIndexes.Add(newClickedItem);
        }

        private void RemoveLastClickedItem()
        {
            if (m_clickedIndexes.Count == 0)
            {
                return;
            }

            m_clickedIndexes.RemoveAt(m_clickedIndexes.Count - 1);
        }

        private void OnDoubleClick(int itemIndex)
        {
            onDoubleClickEvent.Invoke(itemIndex);
        }

        private bool SetSelectedItem(int itemIndex)
        {
            ClearSelection();
            if (itemIndex == NoSelection)
            {
                return true;
            }

            if (!IsIndexValid(itemIndex))
            {
                // index isn't valid
                return false;
            }

            // index is valid
            m_listItems[itemIndex].Selected = true;
            return true;
        }

        private void ClearSelection()
        {
            foreach (GridListItem nextListItem in m_listItems)
            {
                nextListItem.Selected = false;
            }
            ClearWhiteBackground();
        }

        Vector2 GetItemPosition(int itemIndex)
        {
            return new Vector2((itemIndex % GetMaxHorizontalCount()) * GetTotalItemWidth(), -(itemIndex/ GetMaxHorizontalCount()) * GetTotalItemHeigth());
        }

        void MakeWhiteBackground(int itemIndex)
        {
            if (!IsIndexValid(itemIndex))
            {
                ClearWhiteBackground();
                return;
            }
            m_whiteBackground.SetActive(true);
            Vector2 vec = GetItemPosition(itemIndex);
            m_whiteBackground.transform.localPosition = new Vector3(vec.x, vec.y, 0);
        }

        void ClearWhiteBackground()
        {
            m_whiteBackground.SetActive(false);
        }

        void RecountButtonTransformsAndResizeArea(int itemHeight)
        {
            RectTransform contentTransform = GetContentTransform();
            contentTransform.sizeDelta = new Vector2(GetMaxHorizontalCount() * GetTotalItemWidth(), GetMaxVerticalCount() * itemHeight);    // resizing of listbox content area

            GameObject scroll = GameObject.Find("Scrollbar Vertical");
            Scrollbar scrollbar = scroll.GetComponent<Scrollbar>();
            float scrollbarValue = scrollbar.value;
            scrollbar.value = 1;
            ScrollRect scrollrect = scroll.GetComponent<ScrollRect>();
            int itemIndex = 0;
            foreach (Button button in m_scrollView.GetComponentsInChildren(typeof(Button)))
            {
                if (!button.enabled)
                    // don't count old deleted buttons
                    continue;
                button.transform.SetParent(contentTransform, true);
                Vector3 currentPosition = button.transform.position;
                Vector3 shouldBePosition = m_scrollView.transform.position;
                Vector2 itemPosition = GetItemPosition(itemIndex);
                button.transform.localPosition = new Vector3(itemPosition.x,
                                                        itemPosition.y, 0);
                itemIndex++;
            }
            scrollbar.value = scrollbarValue;
        }

        int GetScrollbarWidth()
        {
            int width = 16;
            List<RectTransform> transforms = new List<RectTransform>();
            m_scrollView.GetComponentsInChildren<RectTransform>(transforms);

            foreach (RectTransform transform in transforms)
            {
                if (transform.name == "Scrollbar Vertical")
                {
                    width = (int)transform.sizeDelta.x;
                    break;
                }
            }
            return width;
        }

        RectTransform GetContentTransform()
        {
            List<RectTransform> transforms = new List<RectTransform>();
            m_scrollView.GetComponentsInChildren<RectTransform>(transforms);

            RectTransform contentTransform = null;
            foreach (RectTransform transform in transforms)
            {
                if (transform.name == "Content")
                {
                    contentTransform = transform;
                    break;
                }
            }
            return contentTransform;
        }

        private bool IsIndexValid(int itemIndex)
        {
            return (itemIndex >= 0 && itemIndex < m_listItems.Count);
        }

        // fields =================================================
        private Rect m_drawRect;
        private Rect m_visibleRect;

        private GameObject m_scrollView;
        private Scrollbar m_scrollbar;
        private Vector2 m_scrollPos;

        private bool m_horScrollVisible;
        private bool m_vertScrollVisible;

        private List<GridListItem> m_listItems = new List<GridListItem>();
        protected int m_SelectedIndex = NoSelection;

        protected GUISkin m_custom_skin;
        protected bool m_customSkinFlag = false;

        private int m_itemWidth = 0;
        private int m_itemSpacing = 0;
        private int m_itemHeight = 0;
        private int m_rowLength; // nomber of items in one row

        int m_scrollBarWidth = 16;  // width of scroll bar strip

        List<SClickedListItem> m_clickedIndexes = new List<SClickedListItem>();    // list of selected indexes

        float m_processedHoldTime = 0;
        float m_processedDblClickTime = 0;
        bool m_needRecomputePositions;
        GameObject m_whiteBackground;
    }

    public class GridListItem
    {
        public GridListItem(GridBox parent, Rect c_drawRect, GUIStyle style = null)
        {
            this.m_parent = parent;
            m_drawRect = c_drawRect;
            m_style = style;
        }

        public bool Selected
        {
            get { return m_isSelected; }
            set
            {
                m_isSelected = value;
                if (m_button != null)
                {
                    if (m_isSelected)
                    {
                        m_button.Select();
                    }
                }
            }
        }

        public Button Button
        {
            get { return m_button; }
            set { m_button = value; }
        }

        public System.Object ItemData
        {
            get { return m_itemData; }
            set { m_itemData = value; }
        }

        System.Object m_itemData;
        private Rect m_drawRect;
        private GUIStyle m_style = null;
        private GridBox m_parent;
        private bool m_isSelected;
        Button m_button;
    }
}

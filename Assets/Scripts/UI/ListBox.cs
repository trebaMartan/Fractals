﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.Events;
using UnityEngine.UI;
using Assets.Scripts.UI;

public class ListItemDoubleclicked : UnityEvent<int>
{
}

public class ListSelectionChanged : UnityEvent<int>
{
}

public class SClickedListItem
{
    public SClickedListItem() { itemIndex = ListBox.NoSelection; time = 0; endTime = 0; }
    public int itemIndex;  // index of selected item or -1 if nothing is clicked
    public float time; // time of selection
    public float endTime;
}

//****LISTBOX CLASS****
public class ListBox
{
    public const int NoSelection = -1;
    const int MaxClickedItems = 4;
    const float HoldTimeLimit = 1.0f;   // action OnHolded is called after that time of holding of some item
    const float MaxDoubleClickTime = 0.5f;

    public delegate float Del(string message = null, float lastTime = -1);
    public Del m_recordDelegate;
    //float RecordTime(string message, float lastTime)

    //======
    // Constructors
    public ListBox(Rect c_drawRect, Rect c_visibleRect, bool c_horScrollV = false, bool c_vertScrollV = false, GUISkin c_custom_skin = null)
    {
        Init(c_drawRect, c_visibleRect, c_horScrollV, c_vertScrollV, c_custom_skin);
    }

    //Skins added
    public ListBox(Rect c_drawRect, Rect c_visibleRect, GUISkin c_custom_skin)
    {
        Init(c_drawRect, c_visibleRect, false, false, c_custom_skin);
    }

    public ListBox(GameObject scrollView, bool c_horScrollV = false, bool c_vertScrollV = false, GUISkin c_customSkin = null)// nefungule double click detekce ani hold detekce, ale zase nepouziva gui.Button funkce 
    {
        Scrollbar bar = scrollView.GetComponent<Scrollbar>();
        m_scrollView = scrollView;
        m_scrollbar = bar;

        m_scrollBarWidth = GetScrollbarWidth();

        RectTransform transform = bar.GetComponent<RectTransform>();

        Rect area = transform.rect;
        area.x = transform.rect.xMin;//transform.position.x - transform.pivot.x * area.width;
        area.y = transform.rect.yMax;

        Rect visibleRect = new Rect(0, 0, transform.rect.width, transform.rect.height);

        Init(area, visibleRect, c_horScrollV, c_vertScrollV, c_customSkin);
    }


    //======
    // Properties
    public int Count
    {
        get { return m_listItems.Count; }
    }

    public int SelectedIndex
    {
        get { return m_SelectedIndex; }
        set
        {
            if (value < 0 || value >= m_listItems.Count)
                return;
            if (m_SelectedIndex != value)
            {
                m_SelectedIndex = value;
                onSelectionChangedEvent.Invoke(value);
            }          

            // visual selection
            if (!SetSelectedItem(value))
            {
                m_SelectedIndex = NoSelection;
            }
        }
    }

    public int TextOffset
    {
        get { return m_textOffset; }
        set { m_textOffset = value; }
    }

    public int TextRightLimit
    {
        get { return m_textRightLimit; }
        set { m_textRightLimit = value; }
    }

    public ListItemDoubleclicked onDoubleClickEvent = new ListItemDoubleclicked();
    public ListSelectionChanged onSelectionChangedEvent = new ListSelectionChanged();

    //=============
    // Methods

    public int GetCurSel()
    {
        return SelectedIndex;
    }

    public System.Object GetSelectedItemData()
    {
        return GetItemData(SelectedIndex);
    }

    public System.Object GetItemData(int itemIndex)
    {
        if (!IsIndexValid(itemIndex))
            return null;
        return m_listItems[itemIndex].ItemData;
    }

    public bool SetItemData(int itemIndex, System.Object itemData)
    {
        if (!IsIndexValid(itemIndex))
        {
            return false;
        }
        m_listItems[itemIndex].ItemData = itemData;
        return true;
    }

    public string GetItemText(int itemIndex)
    {
        ListItem item = GetItem(itemIndex);
        if (item == null)
        {
            return default(string);
        }
        return item.Text;
    }

    public void Update()
    {
        if (m_needRecomputePositions)
        {
            RecountButtonTransformsAndResizeArea(m_itemHeight);
            m_needRecomputePositions = false;
        }
    }

    //---ITEMS---
    //Text items
    public int AddItem(string c_ItemLabel, GUIStyle style = null)
    {
        int itemIndex = m_listItems.Count;
        m_listItems.Add(new ListItem(this, new Rect(0, 0, (int)m_itemWidth, 20), c_ItemLabel, style));
        return itemIndex;
    }
    public int AddItem(string c_ItemLabel, int c_ItemH, GUIStyle style = null)
    {
        int itemIndex = m_listItems.Count;
        m_listItems.Add(new ListItem(this, new Rect(0, 0, (int)m_itemWidth, c_ItemH), c_ItemLabel, style));
        return itemIndex;
    }

    public int InsertItem(int insertPos, string c_ItemLabel)
    {
        m_listItems.Insert(insertPos, new ListItem(this, new Rect(0, 0, (int)m_itemWidth, 20), c_ItemLabel));
        return insertPos;
    }
    public int InsertItem(int insertPos, string c_ItemLabel, int c_ItemH)
    {
        m_listItems.Insert(insertPos, new ListItem(this, new Rect(0, 0, (int)m_itemWidth, c_ItemH), c_ItemLabel));
        return insertPos;
    }

    //---------
    //Icon items
    public int AddItem(Texture2D c_ItemIcon, int c_ItemH = 20)
    {
        int itemIndex = m_listItems.Count;
        m_listItems.Add(new ListItem(this, new Rect(0, 0, (int)m_itemWidth, c_ItemH), c_ItemIcon));
        return itemIndex;
    }

    public int AddItem(Texture2D c_ItemIcon, string textPar, GUIStyle style, int itemHeight = 20)
    {
        //m_recordDelegate("AddItem -zac");
        int itemIndex = m_listItems.Count;
        ListItem newListItem = new ListItem(this, new Rect(0, 0, (int)m_itemWidth, itemHeight), c_ItemIcon, textPar, style);
        m_listItems.Add(newListItem);
        
        if (m_scrollView != null)
        {
            // creating of a button

            var buttonObject = new GameObject("Button");
            Button button = buttonObject.AddComponent<Button>();

            var image = buttonObject.AddComponent<RawImage>();
            image.rectTransform.sizeDelta = new Vector2(m_itemWidth, itemHeight);
            image.rectTransform.pivot = new Vector2(0.0f,1.0f);
            image.texture = c_ItemIcon;

            button.targetGraphic = image;
            button.onClick.AddListener(() => Debug.Log(Time.time));
            button.onClick.AddListener(delegate { OnClickedItem(itemIndex); });
            button.GetComponent<RectTransform>().pivot = new Vector2(0.0f, 1.0f);

            RectTransform contentTransform = GetContentTransform();
            button.transform.SetParent(contentTransform, false);
            ColorBlock cb = button.colors;
            cb.highlightedColor = new Color(224 / 255.0f, 224 / 255.0f, 224 / 255.0f);//new Color(224/255.0f, 227/255.0f, 27/255.0f);
            if (style != null)
            {
                cb.highlightedColor = style.hover.textColor;
                cb.pressedColor = style.active.textColor;
            }
            button.colors = cb;
            newListItem.Button = button;

            m_itemHeight = itemHeight;

            var textObject = new GameObject("Text");
            textObject.transform.parent = buttonObject.transform;
            var text = textObject.AddComponent<Text>();
            text.rectTransform.sizeDelta = Vector2.zero;
            text.rectTransform.anchorMin = Vector2.zero;
            text.rectTransform.anchorMax = Vector2.one;
            text.rectTransform.anchoredPosition = new Vector2(0, .5f);
            string txt = String.Format("{0}. {1}", itemIndex + 1, textPar);
            text.text = txt;
            text.font = Resources.FindObjectsOfTypeAll<Font>()[0];
            text.fontSize = 20;
            text.color = Color.yellow;
            text.alignment = TextAnchor.MiddleLeft;
            text.transform.Translate(new Vector3(m_textOffset, 0, 0));

            m_needRecomputePositions = true;
        }
        
        return itemIndex;
    }

    //-----------
    public int InsertItem(int insertPos, Texture2D c_ItemIcon, int c_ItemH = 20, string text = "", GUIStyle style = null)
    {
        if (!IsInsertPosValid(insertPos))
        {
            return -1;
        }
        m_listItems.Insert(insertPos, new ListItem(this, new Rect(0, 0, (int)m_itemWidth, c_ItemH), c_ItemIcon, text, style));
        return insertPos;
    }

    //-----------
    public bool RemoveItem(int iIndex)
    {
        int i = 0;
        foreach (ListItem nextListItem in m_listItems)
        {
            if (i == iIndex)
            {
                m_listItems.Remove(nextListItem);
                if ( iIndex == SelectedIndex )
                {
                    // selected item was deleted
                    SelectedIndex = NoSelection;
                }
                return true;
            }
            i++;
        }
        return false;
    }

    public void MoveItemUp(int itemIndex)
    {
        int otherIndex = itemIndex - 1;
        if (!Swa.AreIndexesOK(m_listItems, itemIndex, otherIndex))
        {
            return;
        }
        Swa.Swap(m_listItems, itemIndex, otherIndex);
        if (SelectedIndex == itemIndex)
        {
            SelectedIndex = otherIndex;
        }
    }

    public void MoveItemDown(int itemIndex)
    {
        int otherIndex = itemIndex + 1;
        if (!Swa.AreIndexesOK(m_listItems, itemIndex, otherIndex))
        {
            return;
        }
        Swa.Swap(m_listItems, itemIndex, otherIndex);
        if (SelectedIndex == itemIndex)
        {
            SelectedIndex = otherIndex;
        }
    }

    public void Clear()
    {
        m_listItems.Clear();
        if (m_scrollView!=null)
        {
            // clearing of buttons
            foreach (Button button in m_scrollView.GetComponentsInChildren(typeof(Button)))
            {
                UnityEngine.Object.Destroy(button.gameObject);
            }
            RectTransform contentTransform = GetContentTransform();
            contentTransform.sizeDelta = new Vector2(contentTransform.sizeDelta.x, 0);    // resizing of listbox content area
        }
        m_SelectedIndex = NoSelection;
    }

    public bool ReDraw()
    {
        if (m_scrollbar != null)
            return false;
        // following code is active only for old gui style
        bool clicked_button = false;

        GUISkin backup_skin = GUI.skin;
        if (m_customSkinFlag)
        {
            GUI.skin = m_custom_skin;
        }

        GUI.Box(m_drawRect, "");
        m_visibleRect.xMax = m_drawRect.width - m_scrollBarWidth;

        m_scrollPos = GUI.BeginScrollView(m_drawRect, m_scrollPos, m_visibleRect, m_horScrollVisible, m_vertScrollVisible);

        int but_y_pos = 0;
        int i = 0;
        int selectedIndex = NoSelection;
        foreach (ListItem nextListItem in m_listItems)
        {
            nextListItem.SetY(but_y_pos); //Put list item in right position
            if (nextListItem.DrawItem())  //Draw it, and check for click on it
            {
                ClearSelection();
                nextListItem.Selected = true;
                clicked_button = true;
                SelectedIndex = i;
                selectedIndex = i;
            }
            i++;
            but_y_pos += nextListItem.GetHeight(); //Set Y position of next list item
        }

        // ================== testing of doubleclick and holding od some item ======================
        SClickedListItem lastClickedItem = GetLastClickedItem();
        if (selectedIndex == lastClickedItem.itemIndex)
        {
            lastClickedItem.endTime = Time.time;
        }
        float miniMalPauseDuration = 0.05f;
        if (selectedIndex != lastClickedItem.itemIndex)
        {
            lastClickedItem.endTime = Time.time;
            float lastDuration = lastClickedItem.endTime - lastClickedItem.time;
            // checking if state "not clicked" lasted enough time
            if (lastClickedItem.itemIndex == NoSelection && lastDuration < miniMalPauseDuration)
            {
                // too short pause - pause is caused by repeat button (not by user)
                RemoveLastClickedItem();
            }
        }
        lastClickedItem = GetLastClickedItem();

        // another check of last selection
        if (selectedIndex == lastClickedItem.itemIndex)
        {
            lastClickedItem.endTime = Time.time;

            // testing of holding of an item =================
            if (selectedIndex != NoSelection)
            {
                // is holding long enough?
                if (Time.time - lastClickedItem.time > HoldTimeLimit)
                {
                    if (lastClickedItem.time != m_processedHoldTime)
                    {
                        // process hold time event only once for one holding
                        m_processedHoldTime = lastClickedItem.time;
                        OnHolded(selectedIndex);
                    }
                }
            }
            // ===============================================
        }

        if (selectedIndex != lastClickedItem.itemIndex)
        {
            // some new item is clicked (or clicking is released)
            AddNewClickedItem(selectedIndex);

            // handling of doubleclick
            if (selectedIndex == NoSelection)
            {
                int lastSelectionIndex = MaxClickedItems - 2;
                int prevSelectionIndex = MaxClickedItems - 4;
                if (m_clickedIndexes.Count >= 4
                    && (m_clickedIndexes[lastSelectionIndex].time - m_clickedIndexes[prevSelectionIndex].time) < MaxDoubleClickTime
                    && (m_clickedIndexes[lastSelectionIndex].itemIndex == m_clickedIndexes[prevSelectionIndex].itemIndex)// last and previous clicked items are same
                    && (m_clickedIndexes[MaxClickedItems - 3].itemIndex == NoSelection))// between them is no clicked item 
                {
                    if (m_processedDblClickTime != m_clickedIndexes[lastSelectionIndex].time)
                    {
                        // process only one event for one dbl click
                        m_processedDblClickTime = m_clickedIndexes[lastSelectionIndex].time;
                        OnDoubleClick(m_clickedIndexes[lastSelectionIndex].itemIndex);
                    }
                }
            }
        }
        // ============================================================

        //Set height of visible zone of ListBox
        if (but_y_pos >= m_drawRect.height)
        {
            m_visibleRect.height = but_y_pos;
        }
        else
        {
            m_visibleRect.height = m_drawRect.height;
        }
        //---

        GUI.EndScrollView();

        if (m_customSkinFlag)
        {
            GUI.skin = backup_skin;
        }

        return clicked_button;
    }

    // ================================= private ================================
    //-------------

    private void Init(Rect c_drawRect, Rect c_visibleRect, bool c_horScrollV = false, bool c_vertScrollV = false, GUISkin c_custom_skin = null)
    {
        m_drawRect = c_drawRect;
        m_visibleRect = c_visibleRect;

        m_itemBorder = m_scrollBarWidth;
        m_itemWidth = c_visibleRect.size.x - m_itemBorder;

        m_horScrollVisible = c_horScrollV;
        m_vertScrollVisible = c_vertScrollV;

        if (c_custom_skin)
        {
            m_custom_skin = c_custom_skin;
            m_customSkinFlag = true;
        }

        m_textOffset = 10;
    }

    private bool IsInsertPosValid(int insertPos)
    {
        if (insertPos < 0 || insertPos > m_listItems.Count)
            return false;
        return true;
    }

    RectTransform GetContentTransform()
    {
        List<RectTransform> transforms = new List<RectTransform>();
        m_scrollView.GetComponentsInChildren<RectTransform>(transforms);

        RectTransform contentTransform = null;
        foreach (RectTransform transform in transforms)
        {
            if (transform.name == "Content")
            {
                contentTransform = transform;
                break;
            }
        }
        return contentTransform;
    }

    int GetScrollbarWidth()
    {
        int width = 16;
        List<RectTransform> transforms = new List<RectTransform>();
        m_scrollView.GetComponentsInChildren<RectTransform>(transforms);

        foreach (RectTransform transform in transforms)
        {
            if (transform.name == "Scrollbar Vertical")
            {
                width = (int)transform.sizeDelta.x;
                break;
            }
        }
        return width;
    }

    void RecountButtonTransformsAndResizeArea(int itemHeight)
    {
        RectTransform contentTransform = GetContentTransform();
        contentTransform.sizeDelta = new Vector2((m_scrollView.transform as RectTransform).sizeDelta.x, m_listItems.Count * itemHeight + itemHeight);    // resizing of listbox content area

        GameObject scroll = GameObject.Find("Scrollbar Vertical");
        Scrollbar scrollbar = scroll.GetComponent<Scrollbar>();
        float scrollbarValue = scrollbar.value;
        scrollbar.value = 1;
        ScrollRect scrollrect = scroll.GetComponent<ScrollRect>();
        int itemIndex = 0;
        foreach (Button button in m_scrollView.GetComponentsInChildren(typeof(Button)))
        {
            if (!button.enabled)
                // don't count old deleted buttons
                continue;
            button.transform.SetParent(contentTransform, true);
            Vector3 currentPosition = button.transform.position;
            Vector3 shouldBePosition = m_scrollView.transform.position;
            button.transform.position = new Vector3(button.transform.position.x + shouldBePosition.x - currentPosition.x,
                                                    button.transform.position.y + shouldBePosition.y - currentPosition.y - itemHeight * itemIndex, 0);
            itemIndex++;
        }
        scrollbar.value = scrollbarValue;
    }

    void OnClickedItem(int itemId)
    {
        SelectedIndex = itemId;
        SClickedListItem lastClicked = GetLastClickedItem();
        AddNewClickedItem(itemId);
        if (lastClicked.time != 0 && lastClicked.itemIndex == itemId)
        {
            // detection of double click
            if (Time.time - lastClicked.time < MaxDoubleClickTime)
            {
                OnDoubleClick(itemId);
            }
        }
    }

    private SClickedListItem GetLastClickedItem()
    {
        if (m_clickedIndexes.Count == 0)
        {
            return new SClickedListItem();
        }
        return m_clickedIndexes[m_clickedIndexes.Count - 1];
    }

    private void AddNewClickedItem(int selectedIndex)
    {
        SClickedListItem newClickedItem = new SClickedListItem();
        newClickedItem.time = Time.time;
        newClickedItem.itemIndex = selectedIndex;

        while (m_clickedIndexes.Count >= MaxClickedItems)
        {
            // remove first records (oldest)
            m_clickedIndexes.RemoveAt(0);
        }

        m_clickedIndexes.Add(newClickedItem);
    }

    private void RemoveLastClickedItem()
    {
        if (m_clickedIndexes.Count == 0)
        {
            return;
        }

        m_clickedIndexes.RemoveAt(m_clickedIndexes.Count - 1);
    }

    private void ClearSelection()
    {
        foreach (ListItem nextListItem in m_listItems)
        {
            nextListItem.Selected = false;
        }
    }

    private bool SetSelectedItem(int itemIndex)
    {
        ClearSelection();
        if (itemIndex == NoSelection)
        {
            return true;
        }

        if (!IsIndexValid(itemIndex))
        {
            // index isn't valid
            return false;
        }

        // index is valid
        m_listItems[itemIndex].Selected = true;
        return true;
    }

    private bool IsIndexValid(int itemIndex)
    {
        if (itemIndex < 0 || itemIndex >= m_listItems.Count)
        {
            return false;
        }
        return true;
    }

    private ListItem GetItem(int itemIndex)
    {
        if (!IsIndexValid(itemIndex))
        {
            return null;
        }
        return m_listItems[itemIndex];
    }

    private void OnDoubleClick(int itemIndex)
    {
        onDoubleClickEvent.Invoke(itemIndex);
    }

    private void OnHolded(int itemIndex)
    {
        OnDoubleClick(itemIndex);
    }
    //=============

    //Fields

    private Rect m_drawRect;
    private Rect m_visibleRect;

    private GameObject m_scrollView;
    private Scrollbar m_scrollbar;
    private Vector2 m_scrollPos;

    private bool m_horScrollVisible;
    private bool m_vertScrollVisible;

    private List<ListItem> m_listItems = new List<ListItem>();
    protected int m_SelectedIndex = NoSelection;

    protected GUISkin m_custom_skin;
    protected bool m_customSkinFlag = false;

    private float m_itemWidth = 0;
    private int m_itemHeight = 0;
    private int m_itemBorder;
    private int m_textOffset;       // offset of text from left margin of item
    private int m_textRightLimit;   // right limit of text rectangle (pixels from left margin of item)

    int m_scrollBarWidth = 16;  // width of scroll bar strip

    List<SClickedListItem> m_clickedIndexes = new List<SClickedListItem>();    // list of selected indexes

    float m_processedHoldTime = 0;
    float m_processedDblClickTime = 0;
    bool m_needRecomputePositions;
}

// ===========================================================================================================================================

static public class Swa
{
    public static bool AreIndexesOK<T>(this IList<T> list, int indexA, int indexB)
    {
        if (indexA < 0 || indexA >= list.Count)
        {
            return false;
        }
        if (indexB < 0 || indexB >= list.Count)
        {
            return false;
        }
        return true;
    }
    public static IList<T> Swap<T>(this IList<T> list, int indexA, int indexB)
    {
        if (indexA < 0 || indexA >= list.Count)
        {
            return list;
        }
        if (indexB < 0 || indexB >= list.Count)
        {
            return list;
        }
        T tmp = list[indexA];
        list[indexA] = list[indexB];
        list[indexB] = tmp;
        return list;
    }
}
//****************************************************************************************************************************************************

//****LISTITEM CLASS****
class ListItem
{
    //Constructors
    public ListItem(ListBox parent, Rect c_drawRect, string c_ItemLabel, GUIStyle style = null)
    {
        this.m_parent = parent;

        m_drawRect = c_drawRect;
        m_itemLabel = c_ItemLabel;
        m_style = style;

        m_isIconButton = false;
    }

    public ListItem(ListBox parent, Rect c_drawRect, Texture2D c_ItemIcon, string text = "", GUIStyle style = null)
    {
        this.m_parent = parent;

        m_drawRect = c_drawRect;
        //m_itemIcon = c_ItemIcon;
        this.m_style = style;
        m_itemLabel = text;

        m_isIconButton = true;
    }

    // properties
    System.Object m_itemData;
    public System.Object ItemData
    {
        get { return m_itemData; }
        set { m_itemData = value; }
    }

    public string Text
    {
        get { return m_itemLabel; }
    }

    public Button Button
    {
    get { return m_button; }
    set { m_button = value; }
    }

    public bool Selected {
        get { return m_isSelected; }
        set { m_isSelected = value;
            if (m_button != null)
            {
                if (m_isSelected)
                {
                    m_button.Select();
                }
            }
        }
    }

    // functions
    int GetTextOffset() { if (m_parent != null) return m_parent.TextOffset; else return 0; }
    int GetTextRightLimit() { if (m_parent != null) return m_parent.TextRightLimit; else return (int)m_drawRect.xMax; }

    //--------------

    public bool DrawItem()
    {
        bool is_clicked;

        GUIStyle style;
        if (m_style == null)
        {
            style = new GUIStyle();
        }
        else
        {
            style = m_style;
        }

        if (m_isIconButton)
        {
            GUIContent content = new GUIContent();

            //if (is_clicked = GUI.Toggle(drawRect, m_isSelected, content, style))
            //if (is_clicked = GUI.Button(drawRect, content, style))
            if (is_clicked = GUI.RepeatButton(m_drawRect, content, style))
            {
                is_clicked = true;
                m_isSelected = true;
            }

            if (m_isSelected)
            {
                // for selection after is click released
                GUI.DrawTexture(m_drawRect, m_style.active.background);
            }

            Rect textRect = m_drawRect;
            textRect.xMax = GetTextRightLimit();
            textRect.xMin = GetTextOffset();    // limitation of text area

            if (m_itemLabel.Length > 0)
            {
                GUI.contentColor = Color.black;
                GUI.Label(textRect, m_itemLabel);
            }
        }
        else
        {
            if (m_isSelected && style.active.background != null)
            {
                GUI.DrawTexture(m_drawRect, style.active.background);
            }
            is_clicked = GUI.Button(m_drawRect, m_itemLabel, style);
        }

        return is_clicked;
    }

    public void SetY(int set_value)
    {
        m_drawRect.y = set_value;
    }

    public int GetHeight()
    {
        return (int)m_drawRect.height;
    }

    // fields -------------------
    private Rect m_drawRect;
    private string m_itemLabel;
    //private Texture2D m_itemIcon;
    private bool m_isIconButton;
    private GUIStyle m_style = null;
    private bool m_isSelected;
    private ListBox m_parent;

    Button m_button;
}
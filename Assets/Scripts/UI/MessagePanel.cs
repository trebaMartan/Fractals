﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MessagePanel : MonoBehaviour {

    public Text message;
    public Button buttonOk;
    public GameObject messagePanelObject;

    private static MessagePanel messagePanel;

    static public void Message(string message, UnityAction okEvent)
    {
        Instance().ShowMessage(message, okEvent);
    }
    static public void Message(string message)
    {
        Instance().ShowMessage(message, null);
    }

    public static MessagePanel Instance()
    {
        if (messagePanel == null)
        {
            messagePanel = FindObjectOfType(typeof(MessagePanel)) as MessagePanel;
            if (!messagePanel)
            {
                Debug.LogError("modal panel isn't find");
            }
        }
        return messagePanel;
    }

    public static MessagePanel GetLastInstance()
    {
        object[] instances = Object.FindObjectsOfType(typeof(MessagePanel));
        if (instances.Length == 0)
        {
            return null;
        }
        return instances[instances.Length - 1] as MessagePanel;
    }

    public void ShowMessage(string message, UnityAction okEvent = null)
    {
        this.message.text = message;

        messagePanelObject.SetActive(true);
        buttonOk.onClick.RemoveAllListeners();
        if (okEvent != null)
        {
            buttonOk.onClick.AddListener(okEvent);
        }
        buttonOk.onClick.AddListener(ClosePanel);
        buttonOk.gameObject.SetActive(true);

        transform.SetAsLastSibling();
    }

    void ClosePanel()
    {
        messagePanelObject.SetActive(false);
    }

    public void Start()
    {
        MessagePanel panel = Instance();
        ClosePanel();
    }
}

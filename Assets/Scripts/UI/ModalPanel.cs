﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class ModalPanel : MonoBehaviour {

    public Text question;
    public Button button1;
    public Button button2;
    public Button button3;
    public GameObject modalPanelObject;

    private static ModalPanel modalPanel;

    public static ModalPanel Instance ()
    {
        //if (modalPanel == null)
        {
            modalPanel = FindObjectOfType(typeof(ModalPanel)) as ModalPanel;
            if ( !modalPanel)
            {
                Debug.LogError("modal panel isn't find");
            }
        }
        return modalPanel;
    }

    public void Choice(string question, UnityAction firstEvent, UnityAction secondEvent = null)
    {
        this.question.text = question;

        modalPanelObject.SetActive(true);
        button1.onClick.RemoveAllListeners();
        button1.onClick.AddListener(firstEvent);
        button1.onClick.AddListener(ClosePanel);
        button1.gameObject.SetActive(true);

        button2.onClick.RemoveAllListeners();
        if (secondEvent != null)
        {
            button2.onClick.AddListener(secondEvent);
        }
        button2.onClick.AddListener(ClosePanel);
        button2.gameObject.SetActive(true);

        button3.gameObject.SetActive(false);

        transform.SetAsLastSibling();
    }

    void ClosePanel()
    {
        modalPanelObject.SetActive(false);
    }

    public void Choice(string question, UnityAction firstEvent, UnityAction secondEvent, UnityAction thirdEvent)
    {
        this.question.text = question;

        modalPanelObject.SetActive(true);
        button1.onClick.RemoveAllListeners();
        button1.onClick.AddListener(firstEvent);
        button1.onClick.AddListener(ClosePanel);
        button1.gameObject.SetActive(true);

        button2.onClick.RemoveAllListeners();
        button2.onClick.AddListener(secondEvent);
        button2.onClick.AddListener(ClosePanel);
        button2.gameObject.SetActive(true);

        button3.onClick.RemoveAllListeners();
        button3.onClick.AddListener(thirdEvent);
        button3.onClick.AddListener(ClosePanel);
        button3.gameObject.SetActive(true);

        transform.SetAsLastSibling();
    }

    public void SetButtonText(int buttonNumber, string text)
    {
        // indexes are in range 1 .. 3
        if (buttonNumber < 1 || buttonNumber > 3)
            return;
        switch(buttonNumber)
        {
            case 1:
                button1.GetComponentInChildren<Text>().text = text;
                break;
            case 2:
                button2.GetComponentInChildren<Text>().text = text;
                break;
            case 3:
                button3.GetComponentInChildren<Text>().text = text;
                break;
        }
    }

    // Use this for initialization
    void Start () {
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
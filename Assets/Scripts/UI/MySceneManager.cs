﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;


public struct SceneInfo
{
    public string sceneName;
}

public class MySceneManager
{
    public static MySceneManager Instance
    {
        get
        {
            if (s_instance == null)
            {
                s_instance = new MySceneManager();
                s_instance.Init();
            }
            return s_instance;
        }
    }

    public static Scene GoToScene(string sceneName, LoadSceneMode mode = LoadSceneMode.Single)
    {
        return MySceneManager.Instance.SwitchToScene(sceneName, mode);
    }

    public static Scene GoToPreviousScene()
    {
        return MySceneManager.Instance.PopScene();
    }

    public Scene SwitchToScene(string sceneName, LoadSceneMode mode = LoadSceneMode.Single, bool recordToHistory = true, bool loadScene = true)
    {
        bool multipleScenesLoaded = SceneManager.sceneCount > 1;
        if (m_activeSceneName.Length == 0)
        {
            m_activeSceneName = SceneManager.GetActiveScene().name;
        }
        if (mode == LoadSceneMode.Additive)
        {
            // hiding of previous scene
            ChangeStateOfObjects(m_activeSceneName, false);
        }
        if (loadScene || !SceneManager.GetSceneByName(sceneName).isLoaded)
        {
            SceneManager.LoadScene(sceneName, mode);
        }
        Scene newScene = SceneManager.GetSceneByName(sceneName);
        bool ok;

        if (newScene.IsValid())
        {
            // record into switching history
            if (recordToHistory)
            {
                if (m_sceneInfos.Count == 0)
                {
                    // record of current scene
                    AddSceneRecord(SceneManager.GetActiveScene());
                }
                // record of new scene
                AddSceneRecord(newScene);
            }
            ok = SceneManager.SetActiveScene(newScene);
        }
        if (mode == LoadSceneMode.Single && m_activeSceneName.Length > 0 
            && SceneManager.GetSceneByName(m_activeSceneName).isLoaded 
            && multipleScenesLoaded)
        {
            Scene unloadedScene = SceneManager.GetSceneByName(m_activeSceneName);
            ChangeStateOfObjects(unloadedScene, false);
            SceneManager.UnloadSceneAsync(m_activeSceneName);
        }
        m_activeSceneName = newScene.name;
        return newScene;
    }

    private void AddSceneRecord(Scene scene)
    {
        SceneInfo sceneInfo = new SceneInfo();
        sceneInfo.sceneName = scene.name;
        m_sceneInfos.Add(sceneInfo);
    }

    public static void ChangeStateOfObjects(Scene scene, bool activate)
    {
        GameObject[] rootGameObjects = scene.GetRootGameObjects();
        foreach (GameObject gameObject in rootGameObjects)
        {
            if (gameObject.name != "Main Camera")
            {
                gameObject.SetActive(activate);
            }
        }
    }
    public static void ChangeStateOfObjects(string sceneName, bool activate)
    {
        Scene scene = SceneManager.GetSceneByName(sceneName);
        if (scene.IsValid())
        {
            ChangeStateOfObjects(scene, activate);
        }
    }

    public Scene PopScene()
    {
        if (m_sceneInfos.Count == 0)
        {
            return SceneManager.GetActiveScene();
        }
        // remove last scene
        m_sceneInfos.RemoveAt(m_sceneInfos.Count - 1);

        if (m_sceneInfos.Count == 0)
        {
            return SceneManager.GetActiveScene();
        }

        // get previous
        string prevScene = m_sceneInfos[m_sceneInfos.Count - 1].sceneName;

        Scene scene = SwitchToScene(prevScene, LoadSceneMode.Single, false, false);
        if (scene.isLoaded)
        {
            // activate game object
            ChangeStateOfObjects(scene, true);
        }
        
        return scene;
    }

    // Use this for initialization
    void Start()
    {
        if (s_instance == null)
        {
            s_instance = new MySceneManager();
            s_instance.Init();
        }
        SceneManager.sceneLoaded += SceneLoaded;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SceneLoaded(Scene scene, LoadSceneMode mode)
    {
        SceneManager.SetActiveScene(scene);
    }

    void Init()
    {
        m_sceneInfos = new List<SceneInfo>(10);
    }

    private List<SceneInfo> m_sceneInfos;
    private static MySceneManager s_instance = null;
    private string m_activeSceneName = string.Format("");
}
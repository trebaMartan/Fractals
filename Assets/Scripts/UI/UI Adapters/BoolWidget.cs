﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.UI;

namespace Assets.Scripts.Utils
{
    // class for transfering of bool value to/from a toggle
    public class BoolWidget : UIWidgetBase<bool>
    {
        public BoolWidget(Ref<bool> valueReference, Toggle input) : base(valueReference)
        {
            m_inputField = input;

            // clearing of end edit function
            m_inputField.onValueChanged.RemoveAllListeners();

            // setting of value
            m_inputField.isOn = ValueReference.Value;

            // new listenter
            m_inputField.onValueChanged.AddListener(delegate (bool v) 
            {
                SetValue(m_inputField.isOn); 
            });
        }
                                      
        Toggle m_inputField;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Exocortex.DSP;
using UnityEngine.UI;

namespace Assets.Scripts.Utils.UI_Adapters
{
    public class ComplexWidget : UIWidgetBase<Complex>
    {
        public ComplexWidget(Ref<Complex> valueReference, InputField reInput, InputField imInput) : base(valueReference)
        {
            m_inputFieldRe = reInput;
            m_inputFieldIm = imInput;

            // clearing of end edit functions
            m_inputFieldRe.onEndEdit.RemoveAllListeners();
            m_inputFieldIm.onEndEdit.RemoveAllListeners();

            // setting of values
            if (Persistent.Instance.FormOfComplexNumbers == Persistent.ComplexNumberForm.RealImaginary)
            {
                m_inputFieldRe.text = Convert.ToDecimal(ValueReference.Value.Re).ToString();
                m_inputFieldIm.text = Convert.ToDecimal(ValueReference.Value.Im).ToString();

                m_complex = new Ref<Complex>(() => ValueReference.Value, v =>
                {
                    ValueReference.Value = v;
                }, NotifyChange);
                //m_re = new Ref<double>(() => ValueReference.Value.Re, v => { ValueReference.Value.Re = v; }, NotifyChange);
                //m_im = new Ref<double>(() => ValueReference.Value.Im, v => { ValueReference.Value.Im = v; }, NotifyChange);


                // new listenters
                m_inputFieldRe.onEndEdit.AddListener(delegate (string text)
                {
                    m_complex.Value = new Complex(Convert.ToDouble(text), Convert.ToDouble(m_inputFieldIm.text));
                });
                m_inputFieldIm.onEndEdit.AddListener(delegate (string text)
                {
                    m_complex.Value = new Complex(Convert.ToDouble(m_inputFieldRe.text), Convert.ToDouble(text));
                });
            }
            else
            {
                // polar coordinates
                double radius = ValueReference.Value.GetModulus();
                double argument = ValueReference.Value.GetArgument();
                m_inputFieldRe.text = Convert.ToDecimal(radius).ToString();
                m_inputFieldIm.text = Convert.ToDecimal(argument).ToString();

                m_complex = new Ref<Complex>(() => ValueReference.Value, v =>
                {
                    ValueReference.Value = v;
                }, NotifyChange);

                // new listenters
                m_inputFieldRe.onEndEdit.AddListener(delegate (string text)
                {
                    m_complex.Value = new Complex(Complex.FromModulusArgument(Convert.ToDouble(text), Convert.ToDouble(m_inputFieldIm.text)));
                });
                m_inputFieldIm.onEndEdit.AddListener(delegate (string text)
                {
                    m_complex.Value = new Complex(Complex.FromModulusArgument(Convert.ToDouble(text), Convert.ToDouble(m_inputFieldIm.text)));
                });
            }
        }

        InputField m_inputFieldRe;
        InputField m_inputFieldIm;

        Ref<Complex> m_complex;
        //Ref<double> m_re;
        //Ref<double> m_im;
    }
}


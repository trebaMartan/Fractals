﻿using Assets.Scripts.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.UI;

namespace Assets.Scripts.Utils
{
    // class for transfering of value of double to/from edit box
    public class DoubleWidget : UIWidgetBase<double>, IObserver
    {
        public DoubleWidget(Ref<double> valueReference, InputField input) : base(valueReference)
        {
            m_inputField = input;

            valueReference.NotifyChangeDelegate += SetTextFromValue;

            // clearing of end edit function
            m_inputField.onEndEdit.RemoveAllListeners();

            // setting of value
            SetTextFromValue();

            // new listenters
            m_inputField.onEndEdit.AddListener(delegate (string text)
            {
                if (text.Length == 0)
                {
                    SetValue(0);
                    SetTextFromValue();
                }
                else
                {
                    SetValue(Rounding.Round(Convert.ToDouble(text)));
                }
            });
        }

        void SetTextFromValue()
        {
            try
            {
                m_inputField.text = Convert.ToDecimal(ValueReference.Value).ToString();
            }
            catch (Exception e)
            {
                // int i = 0;
            }
        }

        public void OnChange(Object observable)
        {
            // source object is changed - get data from it
            SetTextFromValue();
        }

        InputField m_inputField;
    }
}

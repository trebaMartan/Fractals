﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.UI;
using UnityEngine;

namespace System
{
    public delegate void FuncInt(int i);
}
namespace Assets.Scripts.Utils
{
    public class EnumWidget : UIWidgetBase<int>
    {
        public EnumWidget(Ref<int> valueReference, Dropdown dropdown, FuncInt callback = null) : base(valueReference)
        {
            m_input = dropdown;
            Init(valueReference, callback);
        }

        void Init(Ref<int> valueReference, FuncInt callback)
        {
            m_callback = callback;

            // clearing of end edit function
            m_input.onValueChanged.RemoveAllListeners();

            // setting of value
            m_input.value = ValueReference.Value;
            // refresh of interface

            // new listenters
            m_input.onValueChanged.AddListener(delegate (int v) { OnValueChanged(v); });
        }
        public EnumWidget(Ref<int> valueReference, GameObject dropdown, FuncInt callback = null) : base(valueReference)
        {
            m_input = dropdown.GetComponent<Dropdown>();
            Init(valueReference, callback);
        }

        public void SetOptions(List<string> options)
        {
            m_input.ClearOptions();
            m_input.AddOptions(options);
        }

        void OnValueChanged(int something)
        {
            int value = m_input.value;
            SetValue(value);
            if (m_callback != null)
            {
                m_callback(value);
            }
        }

        Dropdown m_input;
        FuncInt m_callback;
    }
}

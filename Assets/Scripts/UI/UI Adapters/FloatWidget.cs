﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.UI;

namespace Assets.Scripts.Utils
{
    // class for transfering of float value to/from InputField
    public class FloatWidget : UIWidgetBase<float>, IObserver
    {
        public FloatWidget(Ref<float> valueReference, InputField input) : base(valueReference)
        {
            m_inputField = input;

            // clearing of end edit function
            m_inputField.onEndEdit.RemoveAllListeners();

            // setting of value
            SetTextFromValue();

            // new listenter
            m_inputField.onEndEdit.AddListener(delegate (string text) 
            {
                if (text.Length == 0)
                {
                    SetValue(0);
                    SetTextFromValue();
                }
                else
                {
                    SetValue(Convert.ToSingle(text));
                }
            });
        }

        void SetTextFromValue()
        {
            try
            {
                m_inputField.text = Convert.ToDecimal(ValueReference.Value).ToString();
            }
            catch (Exception e)
            {
               // int i = 0;
            }
        }

        public void OnChange(Object observable)
        {
            // source object is changed - get data from it
            SetTextFromValue();
        }

        InputField m_inputField;
    }
}
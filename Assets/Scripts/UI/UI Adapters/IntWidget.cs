﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.UI;

namespace Assets.Scripts.Utils
{
    // class for transfering of value of int to/from InputField
    public class IntWidget : UIWidgetBase<int>
    {
        public IntWidget(Ref<int> valueReference, InputField input) : base(valueReference)
        {                       
            m_inputField = input;

            // clearing of end edit function
            m_inputField.onEndEdit.RemoveAllListeners();

            // setting of value
            SetTextFromValue();

            // new listenters
            m_inputField.onEndEdit.AddListener(delegate (string text) 
            {
                if (text.Length == 0)
                {
                    SetValue(0);
                    SetTextFromValue();
                }
                else
                {
                    SetValue(Convert.ToInt32(text));
                }
            });
        }

        void SetTextFromValue()
        {
            try
            {
                m_inputField.text = ValueReference.Value.ToString();
            }
            catch (Exception e)
            {
                // int i = 0;
            }
        }



        InputField m_inputField;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.UI;
using SmartMathLibrary;
using System.Globalization;
using Assets.Scripts.Math;

namespace Assets.Scripts.Utils
{
    // class for transfering of value of quaternion to/from InputFields
    public class QuaternionWidget : UIWidgetBase<Quaternion>
    {
        public QuaternionWidget(Ref<Quaternion> valueReference, InputField X0, InputField I, InputField J, InputField K) : base(valueReference)
        {
            m_inputFieldX0 = X0;
            m_inputFieldI = I;
            m_inputFieldJ = J;
            m_inputFieldK = K;

            // clearing of end edit functions
            m_inputFieldX0.onEndEdit.RemoveAllListeners();
            m_inputFieldI.onEndEdit.RemoveAllListeners();
            m_inputFieldJ.onEndEdit.RemoveAllListeners();
            m_inputFieldK.onEndEdit.RemoveAllListeners();

            // setting of values
            m_inputFieldX0.text = Convert.ToDecimal(ValueReference.Value.X0).ToString();
            m_inputFieldI.text = Convert.ToDecimal(ValueReference.Value.I).ToString();
            m_inputFieldJ.text = Convert.ToDecimal(ValueReference.Value.J).ToString();
            m_inputFieldK.text = Convert.ToDecimal(ValueReference.Value.K).ToString();

            m_X0 = new Ref<double>(() => ValueReference.Value.X0, v => { ValueReference.Value.X0 = v; }, NotifyChange);
            m_I = new Ref<double>(() => ValueReference.Value.I, v => { ValueReference.Value.I = v; }, NotifyChange);
            m_J = new Ref<double>(() => ValueReference.Value.J, v => { ValueReference.Value.J = v; }, NotifyChange);
            m_K = new Ref<double>(() => ValueReference.Value.K, v => { ValueReference.Value.K = v; }, NotifyChange);

            // new listenters
            m_inputFieldX0.onEndEdit.AddListener(delegate (string text) { m_X0.Value = Rounding.Round(Convert.ToDouble(text)); });
            m_inputFieldI.onEndEdit.AddListener(delegate (string text) { m_I.Value = Rounding.Round(Convert.ToDouble(text)); });
            m_inputFieldJ.onEndEdit.AddListener(delegate (string text) { m_J.Value = Rounding.Round(Convert.ToDouble(text)); });
            m_inputFieldK.onEndEdit.AddListener(delegate (string text) { m_K.Value = Rounding.Round(Convert.ToDouble(text)); });
        }

        InputField m_inputFieldX0;
        InputField m_inputFieldI;
        InputField m_inputFieldJ;
        InputField m_inputFieldK;

        Ref<double> m_X0;
        Ref<double> m_I;
        Ref<double> m_J;
        Ref<double> m_K;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Utils.UI_Adapters
{
    public class SliderWidget
    {
        public SliderWidget(string sliderName, DoubleWidget doubleWidget, float min, float max)
        {
            m_slider = GameObject.Find(sliderName).GetComponent<Slider>();
            m_doubleWidget = doubleWidget;
            m_min = min;
            m_max = max;
            m_slider.minValue = min;
            m_slider.maxValue = max;
            InitCallbacks();
            ComputeMinStep();
            OnWidgetValueChanged();
        }

        public SliderWidget(Slider slider, DoubleWidget doubleWidget, float min, float max)
        {
            m_slider = slider;
            m_min = min;
            m_max = max;
            m_doubleWidget = doubleWidget;
            m_slider.minValue = min;
            m_slider.maxValue = max;
            InitCallbacks();
            ComputeMinStep();
            OnWidgetValueChanged();
        }

        void ComputeMinStep()
        {
            int steps = (int)m_slider.gameObject.GetComponent<RectTransform>().rect.width - 20;
            m_minSliderStep = (m_max - m_min) / steps;
        }

        public void RemoveListeners()
        {
            m_slider.onValueChanged.RemoveAllListeners();
            m_doubleWidget.OnChangeDelegate -= OnWidgetValueChanged;
        }

        void InitCallbacks()
        {
            m_doubleWidget.OnChangeDelegate += OnWidgetValueChanged;
            m_slider.onValueChanged.RemoveAllListeners();
            m_slider.onValueChanged.AddListener(OnSliderChanged);
        }

        void OnWidgetValueChanged()
        {
            float doubleWidgetValue = (float)m_doubleWidget.ValueReference.Value;
            if (doubleWidgetValue <= m_min)
            {
                m_slider.value = m_min;
            }
            else if (doubleWidgetValue >= m_max)
            {
                m_slider.value = (float)m_max;
            }
            m_slider.value = doubleWidgetValue;
        }

        float Abs(float v)
        {
            return v < 0 ? -v : v;
        }

        void OnSliderChanged(float value)
        {
            // value is in range 0 - 1
            // step is 1/140; lenght is 160
            // step is 1/180; lenght is 200
            bool isSliderOnLimitValue = value == m_min || value == m_max;
            float doubleWidgetValue = (float)m_doubleWidget.ValueReference.Value;
            if (Abs(value - doubleWidgetValue) < m_minSliderStep ||
                ((doubleWidgetValue < m_min || doubleWidgetValue > m_max) && isSliderOnLimitValue))
            {
                // nothing to do
                return;
            }
            // setting of double widget
            m_doubleWidget.ValueReference.Value = (double)value;
        }

        private float m_min;
        private float m_max;
        private float m_minSliderStep;
        private Slider m_slider; 
        private DoubleWidget m_doubleWidget;
    }
}

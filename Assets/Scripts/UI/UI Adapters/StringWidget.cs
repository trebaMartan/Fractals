﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.UI;

namespace Assets.Scripts.Utils
{
    // class for transfering of string value to/from InputField
    public class StringWidget : UIWidgetBase<string>
    {
        public StringWidget(Ref<string> valueReference, InputField input) : base(valueReference)
        {
            m_inputField = input;

            // clearing of end edit function
            m_inputField.onEndEdit.RemoveAllListeners();

            // setting of value
            m_inputField.text = ValueReference.Value;

            // new listenters
            m_inputField.onEndEdit.AddListener(delegate (string text) 
            {
                SetValue(text);
            });
        }

        InputField m_inputField;
    }
}

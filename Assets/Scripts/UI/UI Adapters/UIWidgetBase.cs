﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Utils
{
    public delegate void OnNotifyChangeDelegate();

    public abstract class UIWidgetBase <T> where T : System.IComparable<T>
    {
        protected UIWidgetBase(Ref<T> valueReference)
        {
            m_valueReference = valueReference;
        }
        public void SetValue(T value)
        {              
            T oldValue = m_valueReference.Value;
            m_valueReference.Value = value;
            if (oldValue.CompareTo(value) != 0)
            {
                NotifyChange();
            }
        }
        public Ref<T> ValueReference { 
            get { return m_valueReference; }
        }

        public void NotifyChange()
        {
            if (OnChangeDelegate != null)
            {
                OnChangeDelegate();
            }
        }

        public OnNotifyChangeDelegate OnChangeDelegate {
            get { return m_onChange; }
            set { m_onChange = value; }
        }

        OnNotifyChangeDelegate m_onChange = null;
        Ref<T> m_valueReference;
    }
}

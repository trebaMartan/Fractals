﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.Utils
{
    public class UIUtils
    {
        public static void AddPanelToDictionary(Dictionary<string, GameObject> dictionary, string panelName)
        {
            GameObject panel = GameObject.Find(panelName);
            UnityEngine.Debug.Assert(panel, "missing panel");
            dictionary.Add(panelName, panel);
        }

        public static InputField FindInputFieldInChildren(GameObject gameObject, string inputFieldName)
        {
            if (!gameObject)
            {
                return null;
            }
            InputField[] fields = gameObject.GetComponentsInChildren<InputField>(true);
            foreach (InputField field in fields)
            {
                if (field.name == inputFieldName)
                {
                    return field;
                }
            }
            return null;
        }

        public static T FindComponentInChildren<T>(GameObject gameObject, string componentName) where T : Component
        {
            T res = new Component() as T;
            if (!gameObject)
            {
                return res;
            }
            T[] components = gameObject.GetComponentsInChildren<T>(true);
            foreach (T component in components)
            {
                if (component.name == componentName)
                {
                    return component;
                }
            }
            return res;
        }

        static public void FillInputField(string inputFieldName, string defaultValue)
        {
            FillInputField(inputFieldName, inputFieldName, defaultValue);
        }

        public static bool ValidateInputTwoInts(string twoIntValues)
        {
            return Regex.IsMatch(twoIntValues, "^[0-9]+[ ]*,[ ]*[0-9]+[ ]*$");
        }

        public static string GetInputFieldValue(string inputFieldName)
        {
            InputField input = GetInputField(inputFieldName);
            if (input == null)
            {
                return "";
            }
            return input.text;
        }

        static InputField GetInputField(string inputFieldName)
        {
            GameObject go = GameObject.Find(inputFieldName);
            if (go == null)
            {
                return null;
            }
            InputField input = go.GetComponent<InputField>();
            return input;
        }
        static void FillInputField(string inputFieldName, string persistentPropertyName, string defaultValue)
        {
            InputField input = GetInputField(inputFieldName);
            if (input == null)
            {
                return;
            }
            input.text = Persistent.Instance.GetStringProperty(persistentPropertyName, defaultValue);
        }

        public static void SaveInputField(string inputFieldName)
        {
            SaveInputField(inputFieldName, inputFieldName);
        }
        static void SaveInputField(string inputFieldName, string persistentPropertyName)
        {
            InputField input = GetInputField(inputFieldName);
            if (input == null)
            {
                return;
            }
            Persistent.Instance.SetStringProperty(persistentPropertyName, input.text);
        }

        public static GameObject FindGameObjectInChildren(GameObject gameObject, string objectName)
        {
            if (!gameObject)
            {
                return null;
            }
            foreach (Transform transform in gameObject.transform)
            {
                if (transform.gameObject.name == objectName)
                {
                    return transform.gameObject;
                }
            }
            return null;
        }

        public static bool IsInputFieldFocused()
        {
            GameObject obj = EventSystem.current.currentSelectedGameObject;
            return obj != null && obj.GetComponent<InputField>() != null;
        }

        public static bool IsStringInputFieldFocused()
        {
            GameObject obj = EventSystem.current.currentSelectedGameObject;
            return obj != null && obj.GetComponent<InputField>() != null && obj.GetComponent<InputField>().contentType == InputField.ContentType.Standard;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Utils
{
    public class BoxContainer
    {
        public BoxContainer(float minCubeSize)
        {
            m_minCubeSize = 0;
            m_moreThanCubeSize = 1.5f * minCubeSize;
            m_xFirst = m_yFirst = m_zFirst = m_zLast = 1000000000;
            m_lastBoxIsFinished = true;
            m_boxes = new List<Box>();
        }

        public void SetMinCubeSize(float minCubeSize)
        {
            m_minCubeSize = minCubeSize;
            m_moreThanCubeSize = 1.5f * minCubeSize;
        }

        public void AddCube(float xCube, float yCube, float zCube)
        {
            if (yCube != m_yFirst || xCube != m_xFirst)
            {
                // finishing of previous line
                if (!m_lastBoxIsFinished)
                {
                    FinishLastBox();
                }
                // start of a new line
                m_xFirst = xCube;
                m_yFirst = yCube;
                m_zLast = m_zFirst = zCube;
                m_lastBoxIsFinished = false;
                return;
            }
            if (zCube > m_zLast + m_moreThanCubeSize)
            {
                // line is broken - finish last line and start new one
                if (!m_lastBoxIsFinished)
                {
                    FinishLastBox();
                }
                // x and y remains the same
                // starting new line
                m_zLast = m_zFirst = zCube;
                m_lastBoxIsFinished = false;
            }
            else
            {
                m_zLast = zCube;
            }
        }

        public List<Box> GetBoxes()
        {
            if (!m_lastBoxIsFinished)
            {
                FinishLastBox();
            }
            return m_boxes;
        }

        private void FinishLastBox()
        {
            //ASSERT(m_boxes.size() > 0);
            Box box;
            box.position.x = m_xFirst;
            box.position.y = m_yFirst;
            box.position.z = (m_zFirst + m_zLast) / 2;
            box.size.y = box.size.x = m_minCubeSize;
            box.size.z = m_zLast - m_zFirst + m_minCubeSize;
            m_boxes.Add(box);
            m_lastBoxIsFinished = true;
        }

        private bool m_lastBoxIsFinished;
        private float m_minCubeSize;
        private List<Box> m_boxes;
        private float m_xFirst;
        private float m_yFirst;
        private float m_zFirst;
        private float m_zLast;
        private float m_moreThanCubeSize;
    }
}

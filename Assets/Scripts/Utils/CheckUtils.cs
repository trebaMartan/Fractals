﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    public class CheckUtils
    {
        static public void CheckCubeCoordinates(List<UnityEngine.Vector3> cubeCoordinates,
        List<UnityEngine.Vector3> cubeCoordinates1)
        {
            // comparing of counted cubes
            if (cubeCoordinates.Count == cubeCoordinates1.Count)
            {
                for (int i = 0; i < cubeCoordinates.Count; i++)
                {
                    if (cubeCoordinates[i] != cubeCoordinates1[i])
                    {
                        Debug.Assert(false, "cube coordinates are different");
                    }
                }
            }
            else
            {
                Debug.Assert(false, "different cubes size");
            }
        }

        public static void CheckBoxes(List<Box> boxes, List<Box> boxes1)
        {
            if (boxes.Count != boxes1.Count)
            {
                Debug.Assert(false, "different boxes count");
                return;
            }
            for (int i = 0; i < boxes.Count; i++)
            {
                if (boxes[i] != boxes1[i])
                {
                    Debug.Assert(false, "Different boxes");
                    return;
                }
            }
        }

        public static void CheckGraphic(Fractal3DGraphic graphic, Fractal3DGraphic graphic1, List<Box> boxes)
        {
            if (graphic.GetBaseObject().transform.childCount != graphic1.GetBaseObject().transform.childCount)
            {
                Debug.Assert(false, "not equal children size");
                return;
            }
            for (int i = 0; i < graphic.GetBaseObject().transform.childCount; i++)
            {
                UnityEngine.Transform box = graphic.GetBaseObject().transform.GetChild(i);
                UnityEngine.Transform box1 = graphic1.GetBaseObject().transform.GetChild(i);
                //Box boxOrginal = boxes[i];
                if (box.position != box1.position || box.localScale != box1.localScale)
                {
                    Debug.Assert(false, "not equal boxes");
                    return;
                }
            }
        }

    }
}

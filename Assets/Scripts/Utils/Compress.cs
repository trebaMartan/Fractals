﻿using System.Collections.Generic;
using Ionic.Zip;

namespace Assets.Scripts.Utils
{
    public class Compress
    {
        public delegate void ProgressDelegate(string sMessage);

        public static void Zip(string[] filePaths, string zipPath)
        {
            using (ZipFile zip = new ZipFile())
            {
                foreach (string filePath in filePaths)
                {
                    // add this file into the zip archive (directory tree isn't saved)
                    zip.AddFile(filePath, "");
                }
                zip.Save(zipPath);
            }
        }
        public static void Unzip(string zipPath, string extractDir)
        {
            using (ZipFile zip = ZipFile.Read(zipPath))
            {
                zip.ExtractAll(extractDir);
            }
        }
    }
}
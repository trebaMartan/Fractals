﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Utils
{
    public class Constants
    {
        public const float NearClippingPlane = 0.002f;
        public const float FarClippingPlane = 8.0f;
    }
}

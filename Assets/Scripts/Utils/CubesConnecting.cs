﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    public struct Box
    {
        //public int i;
        public Vector3 position;   // position of center//minimal corner
        public Vector3 size;       // size of this box
        public static bool operator ==(Box box1, Box box2)
        {
            return box1.position == box2.position && box1.size == box2.size;
        }
        public static bool operator !=(Box box1, Box box2)
        {
            return !(box1.position == box2.position && box1.size == box2.size);
        }
        public float GetVolume() { return size.x * size.y * size.z; }
        public List<Vector3> GetCubes()
        {
            List<Vector3> cubes = new List<Vector3>();
            int cubesCount = (int)(size.z / MinCubeSize + 0.5f);
            Vector3 cube;
            cube.x = position.x;
            cube.y = position.y;
            float zOffset = -(cubesCount-1) * MinCubeSize / 2;// 1 offset 0, 2 offset MinCubeSize/2, 3 offset MinCubeSize 
            for (int i = 0; i < cubesCount; i++)
            {
                cube.z = position.z + MinCubeSize * i + zOffset;
                cubes.Add(cube);
            }
            return cubes;
        }
        public static int CompareBoxes(Box box1, Box box2)
        {
            //if (box1 == null || box2 == null)
            //{
            //    return 0;
            //}
            double sgn = System.Math.Sign(box1.position.x - box2.position.x);
            if (sgn != 0)
            {
                return (int)sgn;
            }
            sgn = System.Math.Sign(box1.position.y - box2.position.y);
            if (sgn != 0)
            {
                return (int)sgn;
            }
            return (int)System.Math.Sign(box1.position.z - box2.position.z);
        }
        static public float MinCubeSize = 0;
    }
    public class CubesConnecting
    {
        static public void MakeZLines(List<Vector3> cubePositions, float cubeSize, List<Box> boxes)
        {
            // creates lines in z direction - x and y coordinates are often not changing and z is changing
            if (cubePositions.Count == 0)
            {
                return;
            }

            float xLast = cubePositions[0].x;
            float yLast = cubePositions[0].y;
            float zFirst = cubePositions[0].z;
            float zLast = zFirst;

            float oneAndHelfCubeSize = cubeSize * 1.5f;
            Box box = new Box();
            box.size.x = cubeSize;
            box.size.y = cubeSize;
            box.position.x = xLast;
            box.position.y = yLast;
            box.position.z = zFirst;

            foreach (Vector3 cubePosition in cubePositions)
            {
                float xCube = cubePosition.x;
                float yCube = cubePosition.y;
                float zCube = cubePosition.z;
                if (xCube != xLast || yCube != yLast || zCube > zLast + oneAndHelfCubeSize)
                {
                    // new cube x, y position => finish previous line
                    box.size.z = zLast - zFirst + cubeSize;
                    box.position.z = (zFirst + zLast) / 2;
                    //box.i = boxes.Count;
                    boxes.Add(box);

                    // and starting of a new box
                    xLast = xCube;
                    yLast = yCube;
                    zFirst = cubePosition.z;
                    zLast = zFirst; 

                    box.position.x = xLast;
                    box.position.y = yLast;
                }
                else
                {
                    zLast = cubePosition.z;
                }
            }
            // adding of last line
            box.position.z = (zFirst + zLast) / 2;
            box.size.z = zLast - zFirst + cubeSize;
            //box.i = boxes.Count;
            boxes.Add(box);
        }

        public void Connect2BiggerObjects(List<Vector3> cubePositions, float cubeSize, List<Box> boxes)
        {
            // firts of all connect 2 tuples, than 2 quadruples, than 2 bigger cubes, ...

        }
    }
}

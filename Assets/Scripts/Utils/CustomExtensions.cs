﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    public static class TransformEx
    {
        public static Transform Clear(this Transform transform)
        {
            foreach (Transform child in transform)
            {
                GameObject.Destroy(child.gameObject);
            }
            return transform;
        }
    }

    public static class BoundsExtensions
    {
        public static List<Vector3> GetLimitPoints(this Bounds bounds)
        {
            List<Vector3> result = new List<Vector3>();
            Vector3 min = bounds.min;
            Vector3 max = bounds.max;
            result.Add(new Vector3(max.x, min.y, min.z));
            result.Add(new Vector3(min.x, min.y, min.z));
            result.Add(new Vector3(min.x, min.y, max.z));
            result.Add(new Vector3(min.x, max.y, min.z));
            result.Add(new Vector3(min.x, max.y, max.z));
            result.Add(new Vector3(max.x, min.y, max.z));
            result.Add(new Vector3(max.x, max.y, min.z));
            result.Add(new Vector3(max.x, max.y, max.z));
            return result;
        }

    }
}

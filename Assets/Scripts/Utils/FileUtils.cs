﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Assets.Scripts.Utils
{
    public class FileUtils
    {
        static public string GetMaxNumberFilename(string directory, string mask, string fractalExtension)
        {
            var files = Directory.GetFiles(directory, mask, SearchOption.TopDirectoryOnly);
            string filename;
            int maxNumber = 0;
            foreach (string file in files)
            {
                // part before last .
                filename = Path.GetFileNameWithoutExtension(file);
                try
                {
                    int number = int.Parse(filename);
                    if (number > maxNumber)
                    {
                        maxNumber = number;
                    }
                }
                catch(Exception)
                { }
            }
            string firstPartOfMask = mask.Replace("*", "");
            firstPartOfMask = Path.GetFileNameWithoutExtension(firstPartOfMask);
            filename = String.Format("{0}{1}.{2}", firstPartOfMask, (maxNumber + 1).ToString("D4")
                , fractalExtension);
            return Path.Combine(directory, filename);
        }

        public static void CreateOrClearDirectory(string dir)
        {
            try
            {
                if (Directory.Exists(dir))
                {
                    // clearing of packing dir
                    Directory.Delete(dir, true);
                }
                // creation
                Directory.CreateDirectory(dir);
            }
            catch(Exception)
            {
            }
        }

        static public void CreateDirectoryIfNotExist(string directory)
        {
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
        }

        public static string GetFileWithMask(string directory, string mask)
        {
            string res = "";
            string[] files = Directory.GetFiles(directory);

            foreach (string path in files)
            {
                if (path.Contains(mask))
                {
                    return path;
                }
            }
            return res;
        }

        public static void MoveIfNotExists(string sourcePath, string destinationPath)
        {
            if (!File.Exists(destinationPath) && File.Exists(sourcePath))
            {
                File.Move(sourcePath, destinationPath);
            }
        }

        public static void CopyIfNotExists(string sourcePath, string destinationPath)
        {
            if (!File.Exists(destinationPath) && File.Exists(sourcePath))
            {
                File.Copy(sourcePath, destinationPath);
            }
        }

        public static void DeleteIfExists(string path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }

    }
}

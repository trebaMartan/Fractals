﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.IO;
using Assets.Scripts.Math;

namespace Assets.Scripts.Utils
{
    class FractalScreenshotRecorder : MonoBehaviour
    {
        public GameObject Camera;
        public GameObject BaseObject;
        public GameObject DirectionalLight;

        void InitComponents()
        {
            m_directionalLight = DirectionalLight.GetComponent<Light>();
            m_camera = Camera.GetComponent<Camera>();
        }

        public void MakeScreenshot(QuaternionFractal3D fractal, string path, int width, int height, Transform cameraTransform)
        {
            if (fractal == null)
            {
                return;
            }
            InitComponents();
            m_fractal = fractal;
            m_camera.transform.position = transform.position;
            m_camera.transform.rotation = transform.rotation;

            // making of screenshots
            ScreenRecorder screenRecorder = m_camera.GetComponent<ScreenRecorder>();
            screenRecorder.captureWidth = width;
            screenRecorder.captureHeight = height;
            screenRecorder.screenshotPath = path;
            screenRecorder.CaptureScreenshotImmediate();
        }

        public void MakeScreenshots(QuaternionFractal3D fractal)
        {
            if (fractal == null)
            {
                return;
            }
            InitComponents();
            m_fractal = fractal;
            string fractalSavePath = m_fractal.FilePath;
            if (fractalSavePath.Length == 0)
            {
                return;
            }

            int frontScreenshotSize = FractalSelectionSceneScript.SmallTextureSize;
            string frontScreenshotPath = MakeFrontScreenshot(fractal, frontScreenshotSize, frontScreenshotSize);
            string fourScreenshotPath = Make4Screenshot(fractal, 600, 400);
            string path = Persistent.GetSmallTexturePathFromFractalPath(fractalSavePath);
            File.Delete(path);
            File.Copy(frontScreenshotPath, path);
            string path2 = Persistent.GetBigTexturePathFromFractalPath(fractalSavePath);//string.Format("{0}Four.png", fractalSavePathWithoutSufix);
            File.Delete(path2);
            File.Copy(fourScreenshotPath, path2);
        }

        public string MakeFrontScreenshot(QuaternionFractal3D fractal, int width, int height)
        {
            InitComponents();
            m_fractal = fractal;
            string screenshotPath = string.Format("{0}ScreenshotFront.png", Persistent.GetTempDir());
            File.Delete(screenshotPath);

            // saving of current values that will be changed
            SaveCurrentView();
            m_camera.clearFlags = CameraClearFlags.SolidColor;
            m_camera.backgroundColor = m_fractal.FractalBackgroundColor;

            ResetLight();
            //ShowAxes = false;
            //ShowComputationBoundingBox = false;

            // setting of camera
            ResetView();

            // making of screenshots
            ScreenRecorder screenRecorder = m_camera.GetComponent<ScreenRecorder>();
            screenRecorder.captureWidth = width;
            screenRecorder.captureHeight = height;
            screenRecorder.screenshotPath = screenshotPath;
            screenRecorder.CaptureScreenshotImmediate();

            // restoring of old values
            RestoreCurrentView();
            return screenshotPath;
        }

        void ResetView()
        {
            m_camera.transform.position = Quaternion3DSceneScript.GetPredefinedCameraPosition(0);
            m_camera.transform.rotation = new UnityEngine.Quaternion(0, 0, 0, 0);
            m_camera.transform.localScale = new Vector3(0, 0, 0);

            GameObject sceneBaseObject = GameObject.Find(Fractal3DGraphic.BaseSceneObjectName);
            sceneBaseObject.transform.position = new Vector3(0, 0, 0);
            sceneBaseObject.transform.rotation = new UnityEngine.Quaternion(0, 0, 0, 0);
            sceneBaseObject.transform.localScale = new Vector3(1, 1, 1);

            UpdateCenterOfFractal();
            AdjustZoom();
        }

        public string Make4Screenshot(QuaternionFractal3D fractal, int width, int height)
        {
            InitComponents();
            m_fractal = fractal;
            Debug.Assert(width % 2 == 0 && height % 2 == 0);
            string fourScreenshotPath = string.Format("{0}ScreenshotComplete.png", Persistent.GetTempDir());
            File.Delete(fourScreenshotPath);

            List<string> screenshotPaths = new List<string>();
            int partScreenshotWidth = width / 2;
            int partScreenshotHeight = height / 2;
            for (int i = 0; i < 4; i++)
            {
                screenshotPaths.Add(string.Format("{0}Screenshot4{1}.png", Persistent.GetTempDir(), i));
            }

            // making of screenshots
            ScreenRecorder screenRecorder = m_camera.GetComponent<ScreenRecorder>();
            screenRecorder.captureWidth = partScreenshotWidth;
            screenRecorder.captureHeight = partScreenshotHeight;

            // saving of current values that will be changed
            SaveCurrentView();
            m_camera.clearFlags = CameraClearFlags.SolidColor;
            m_camera.backgroundColor = m_fractal.FractalBackgroundColor;

            ResetLight();

            for (int i = 0; i < 4; i++)
            {
                Quaternion3DSceneScript.SetPredefinedCameraPosition(m_camera.transform, i);
                UpdateCenterOfFractal();
                AdjustZoom();
                screenRecorder.screenshotPath = screenshotPaths[i];
                screenRecorder.CaptureScreenshotImmediate();
            }

            // make summary screenshot from particular screenshots
            Texture2D texture = null;
            byte[] fileData;

            List<Texture2D> textures = new List<Texture2D>();
            foreach (string filePath in screenshotPaths)
            {
                if (File.Exists(filePath))
                {
                    fileData = File.ReadAllBytes(filePath);
                    texture = new Texture2D(2, 2);
                    textures.Add(texture);
                    texture.LoadImage(fileData); //..this will auto-resize the texture dimensions.
                }
            }

            Texture2D completeTexture = new Texture2D(width, height, TextureFormat.RGB24, false);
            for (int i = 0; i < 4; i++)
            {
                Rect rect = new Rect();
                Vector2 position = new Vector2();
                position.x = i % 2 == 0 ? 0 : partScreenshotWidth;
                position.y = i < 2 ? partScreenshotHeight : 0;
                rect.position = position;
                rect.size = new Vector2(partScreenshotWidth, partScreenshotHeight);
                TextureUtils.WriteTextureIntoTexture(completeTexture, textures[i], rect);
            }

            // writing of complete texture into a file
            fileData = completeTexture.EncodeToPNG();
            // create file and write optional header with image bytes
            var f = System.IO.File.Create(fourScreenshotPath);
            f.Write(fileData, 0, fileData.Length);
            f.Close();
 
            // restoring of old values
            RestoreCurrentView();
            return fourScreenshotPath;
        }

        void UpdateCenterOfFractal()
        {
            // movimg to the center of computed area or center of graphic bounds
            if (GetBaseObject() != null && m_fractal != null)
            {
                UnityEngine.Quaternion rotation = GetBaseObject().transform.rotation;
                GetBaseObject().transform.position = rotation * (-m_fractal.ComputationArea.GetCenter()).Vector3;

                if (m_fractal.GetGraphic() != null)
                {
                    Bounds bounds = m_fractal.GetGraphic().GetBounds(1.0f, new Bounds(Vector3.zero, Vector3.one.Mult ( m_fractal.ComputationArea.GetMaxSize())));
                    GetBaseObject().transform.position = rotation * (-bounds.center);
                }
            }
        }

        void SaveCurrentView()
        {
            m_previousLightRotation = m_directionalLight.transform.rotation;
            m_previousCameraRotation = m_camera.transform.rotation;
            m_previousCameraPosition = m_camera.transform.position;
            m_previousClearFlags = m_camera.clearFlags;
            m_previousColor = m_camera.backgroundColor;
        }

        void RestoreCurrentView()
        {
            m_directionalLight.transform.rotation = m_previousLightRotation;
            m_camera.transform.rotation = m_previousCameraRotation;
            m_camera.transform.position = m_previousCameraPosition;
            m_camera.clearFlags = m_previousClearFlags;
            m_camera.backgroundColor = m_previousColor;
        }

        void ResetLight()
        {
            m_directionalLight.transform.rotation = Quaternion3DSceneScript.GetResetLightRotation();
            //m_directionalLight.transform.rotation = UnityEngine.Quaternion.FromToRotation(new Vector3(0, 0, 1), new Vector3(-1, -1, -1));
        }

        public void OnZoomIn()
        {
            Vector3 position = m_camera.transform.position;
            position *= 0.9f;
            m_camera.transform.position = position;
        }

        public void OnZoomOut()
        {
            Vector3 position = m_camera.transform.position;
            position /= 0.9f;
            m_camera.transform.position = position;
        }
        void OnZoomOutFine()
        {
            Vector3 position = m_camera.transform.position;
            position /= 0.99f;
            m_camera.transform.position = position;
        }

        void AdjustZoom()
        {
            if (m_fractal != null && m_fractal.GetGraphic() != null)
            {
                Bounds bounds = m_fractal.GetGraphic().GetBounds(1.0f, new Bounds(Vector3.zero, Vector3.one.Mult( m_fractal.ComputationArea.GetMaxSize())));
                List<Vector3> limitPoints = bounds.GetLimitPoints();

                // conversion to world coordinates
                //foreach(GameObject go in m_testingPoints)
                //{
                //    GameObject.DestroyObject(go);
                //}
                //m_testingPoints.Clear();
                for (int i = 0; i < limitPoints.Count; i++)
                {
                    //m_testingPoints.Add(NewTestingObject(limitPoints[i]));
                    limitPoints[i] = GetBaseObject().transform.TransformPoint(limitPoints[i]);
                }

                float max = ScreenUtils.GetMaxRelativeScreenDistance(m_camera, limitPoints);
                for (int i = 0; i < 100; i++)
                {
                    if (max > 0.5f)
                    {
                        break;
                    }
                    OnZoomIn();
                    max = ScreenUtils.GetMaxRelativeScreenDistance(m_camera, limitPoints);
                }
                for (int i = 0; i < 100; i++)
                {
                    if (max < 0.5f)
                    {
                        break;
                    }
                    OnZoomOut();
                    max = ScreenUtils.GetMaxRelativeScreenDistance(m_camera, limitPoints);
                }
                for (int i = 0; i < 100; i++)
                {
                    if (max > 0.5f)
                    {
                        break;
                    }
                    OnZoomIn();
                    max = ScreenUtils.GetMaxRelativeScreenDistance(m_camera, limitPoints);
                }
                for (int i = 0; i < 100; i++)
                {
                    if (max < 0.5f)
                    {
                        break;
                    }
                    OnZoomOutFine();
                    max = ScreenUtils.GetMaxRelativeScreenDistance(m_camera, limitPoints);
                }
            }
        }

        GameObject GetBaseObject()
        {
            return BaseObject;
        }

        QuaternionFractal3D m_fractal;
        Camera m_camera;
        Light m_directionalLight;
        UnityEngine.Quaternion m_previousLightRotation;
        UnityEngine.Quaternion m_previousCameraRotation;
        Vector3 m_previousCameraPosition;
        CameraClearFlags m_previousClearFlags;
        Color m_previousColor;
    }
}

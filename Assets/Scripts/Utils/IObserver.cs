﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Utils
{
    public class Observable
    {
        public struct MethodObserver
        {
            public MethodObserver(IObserver observer, string methodId)
            {
                this.observer = observer;
                this.methodId = methodId;
            }
            public IObserver observer;
            public string methodId;
        }
        public void AddMethodObserver(IObserver observer, string methodId)
        {
            m_methodObservers.Add(new MethodObserver(observer, methodId));
        }
        public void AddObserver(IObserver observer)
        {
            m_observers.Add(observer);
        }
        public void RemoveObserver(IObserver observer)
        {
            m_observers.Remove(observer);
        }
        public void Notify()
        {
            foreach(IObserver observer in m_observers)
            {
                observer.OnChange(this);
            }
        }
        public void NotifyMethod(string methodId)
        {
            foreach (MethodObserver methodObserver in m_methodObservers)
            {
                if (methodId == methodObserver.methodId)
                {
                    methodObserver.observer.OnChange(this);
                }
            }
        }
        private List<IObserver> m_observers = new List<IObserver>();
        private List<MethodObserver>m_methodObservers = new List<MethodObserver>();
    }
    public interface IObserver
    {
        void OnChange(Object observable);
    }
}

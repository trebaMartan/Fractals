﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using SmartMathLibrary;
using UnityEngine;
using System.Reflection;

namespace Assets.Scripts.Utils
{
    public class OtherUtils
    {
        public static object GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }

        public static bool SetPropValue(object src, string propName, object value)
        {
            PropertyInfo propertyInfo = src.GetType().GetProperty(propName);
            if (propertyInfo == null)
            {
                return false;
            }
            propertyInfo.SetValue(src, value, null);
            if (GetPropValue(src, propName) == value)
            {
                return true;
            }
            return false;
        }

        public static void Convert2DSaves(string directory)
        {
            string fractalMask = "*." + Persistent.GetExtension(Persistent.SaveExtension.fractal2DDefinitionExtension);
            var files = Directory.GetFiles(directory, fractalMask, SearchOption.TopDirectoryOnly);
            foreach (string path in files)
            {
                ComplexFractal2D fractal;
                if (ComplexFractal2D.Load(path, out fractal))
                {
                    //fractal.ColoringAlgorithm.ColorCount = fractal.ColorCount;
                    //fractal.ColoringAlgorithm.SinglePalette = fractal.SinglePalette.Clone() as SinglePalette;
                    //fractal.ColoringAlgorithm.RepeatType = fractal.RepeatType;
                    //fractal.ColoringAlgorithm.BackgroundColor = fractal.BackgroundColor;
                    fractal.Save(path);
                }
            }
        }

        public static void Convert3DSaves(string directory, bool recursive, bool loadGraphic)
        {
            string fractalMask = "*." + Persistent.GetExtension(Persistent.SaveExtension.fractal3DZipExtension);
            var files = Directory.GetFiles(directory, fractalMask, recursive? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);
            foreach(string path in files)
            {
                QuaternionFractal3D fractal;

                //string fractalSavePathWithoutSufix = Path.Combine(Path.GetDirectoryName(path), Path.GetFileNameWithoutExtension(path));
                //string screenshotPath = string.Format("{0}_38.png", fractalSavePathWithoutSufix);
                //string path2 = string.Format("{0}Four.png", fractalSavePathWithoutSufix);
                //if (File.Exists(screenshotPath) && File.Exists(path2))
                //{
                //    continue;
                //}

                loadGraphic = false;
                if (QuaternionFractal3D.Load(path, out fractal, loadGraphic))
                {
                    //if (Conversion1(fractal))
                    //if (Conversion3(fractal))
                    if (Conversion2(fractal))
                    {
                        string graphicPath = Persistent.GetExtractDir() + Persistent.GetGraphicFilename();

                        fractal.Save(path, graphicPath);
                    }
                    fractal.GetGraphic().Clear(true, true);
                    fractal.RemoveObserver(fractal);
                }
                else
                {
                    // save some log or something
                    using (StreamWriter outputFile = new StreamWriter("C:/joy/ConverionFailLoad.txt", true))
                    {
                        outputFile.WriteLine(String.Format("path: {0}", path));
                    };
                }
            }
        }

        static bool Conversion1(QuaternionFractal3D fractal)
        {
            if (!fractal.CoefficientBeforePower &&
                !fractal.CoefficientAfterPower &&
                (fractal.ComputeDelegateType == QuaternionDelegateType.Julia || fractal.ComputeDelegateType == QuaternionDelegateType.MixedMJ))
            {
                // changes
                fractal.CoefficientAfterPower = true;
                //copy of C
                for (int i = 0; i < fractal.C.Count; i++)
                {
                    fractal.SetCA(i, fractal.C[i]);
                    fractal.C[i] = SmartMathLibrary.Quaternion.Zero;
                }
                return true;
            }
            return false;
        }

        static bool Conversion2(QuaternionFractal3D fractal)
        {
            //if (fractal.ComputeDelegateType == QuaternionDelegateType.Mandelbrot)
            //{
            //    // changes
            //    fractal.CoefficientBeforePower = true;
            //    fractal.CoefficientAfterPower = false;
            //    fractal.CoefficientC0BeforePower = true;
            //    fractal.CoefficientC0AfterPower = false;
            //    fractal.C.Clear();
            //    fractal.SetC(2, Quaternion.One);
            //    fractal.C0.Clear();
            //    fractal.SetC0(1, Quaternion.One);
            //    fractal.ComputeDelegateType = QuaternionDelegateType.MixedMJ;
            //    return true;
            //}
            //string fractalSavePath = fractal.FilePath;
            //string fractalSavePathWithoutSufix = Path.Combine(Path.GetDirectoryName(fractalSavePath), Path.GetFileNameWithoutExtension(fractalSavePath));
            //string path = string.Format("{0}_38.png", fractalSavePathWithoutSufix);
            //if (!File.Exists(path))
            //{
            //GameObject.Find("Main Camera").GetComponent<FractalScreenshotRecorder>().MakeScreenshots(fractal);
            //}
            //if (fractal.LimitArea is RectangularQuaternionArea)
            //{
            //    (fractal.LimitArea as RectangularQuaternionArea).HalfSize = (fractal.LimitArea as RectangularQuaternionArea).HalfSize;
                return true;
            //}
            //if (fractal.ComputationArea is RectangularArea3D)
            //{
            //    //(fractal.ComputationArea as RectangularArea3D) = (fractal.ComputationArea as RectangularArea3D).HalfSize;
            //    return true;
            //}

            //return false;// true;
        }

        static bool Conversion3(QuaternionFractal3D fractal)
        {
            // save will produce screenshots
            return true;
        }

        static bool Conversion4(QuaternionFractal3D fractal)
        {
            // recomputing of mixed fractals
            if (fractal.ComputeDelegateType == QuaternionDelegateType.MixedMJ)
            {
                fractal.ComputeFractal(true);
                //GameObject.Find("Main Camera").GetComponent<FractalScreenshotRecorder>().MakeScreenshots(fractal);
                return true;
            }
            return false;
        }

        static float ColorDifference(Color color1, Color color2)
        {
            float dr = color1.r - color2.r;
            float dg = color1.g - color2.g;
            float db = color1.b - color2.b;
            float res = (float)System.Math.Sqrt(dr * dr + dg * dg + db * db);
            return res;
        }

        static public bool AreColorsClose(Color color1, Color color2)
        {
            return ColorDifference(color1, color2) < 0.1f;
        }
    }
}

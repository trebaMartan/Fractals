﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using System.Linq.Expressions;
using System;
using System.Reflection;
using Assets.Scripts.Utils;
using UnityEngine.Events;
using Assets.Scripts.Graphic;
using Assets.Scripts.Math;

// class for storing and restoring of persistent objects - settings, selected fractals, ...
public class Persistent
{
    public enum SaveExtension
    {
        fractal3DDefinitionExtension,
        fractal3DGraphicExtension,
        fractal3DZipExtension,
        fractal3DPreviewExtension,
        fractal2DDefinitionExtension,
        fractal2DPreviewExtension,
        configSaveExtension,
    };

    public enum EEditPaletteMode
    {
        CreatePalette,
        EditPaletteList,
        EditPalette,
    } 

    public enum AxisSystemType
    {
        Right,
        Left
    }

    public enum CoefficientsDependencyType
    {
        Independent,
        Transposed,
        Inverse
    }

    // constructor
    private Persistent()
    {
        FileUtils.CreateDirectoryIfNotExist(GetPersistentDir());
        FileUtils.CreateDirectoryIfNotExist(GetExportDir());
        FileUtils.CreateDirectoryIfNotExist(GetFractalsDir());
        FileUtils.CreateDirectoryIfNotExist(GetTempDir());
        FileUtils.CreateDirectoryIfNotExist(Get3DFractalsDir());
        FileUtils.CreateDirectoryIfNotExist(GetRandom3DFractalDir());
        FileUtils.CreateDirectoryIfNotExist(GetRandom2DFractalDir());
        FileUtils.CreateOrClearDirectory(Get3DFractalsTempGraphicsDir());

        // setting of saved axis system
        AxisSystem = AxisSystem;
    }

    static Persistent()
    {
        SaveExtensions[SaveExtension.fractal3DDefinitionExtension] = "xml";
        SaveExtensions[SaveExtension.fractal3DGraphicExtension] = "xml";
        SaveExtensions[SaveExtension.fractal3DZipExtension] = "f3z";
        SaveExtensions[SaveExtension.fractal3DPreviewExtension] = "png";
        SaveExtensions[SaveExtension.fractal2DDefinitionExtension] = "fr";
        SaveExtensions[SaveExtension.fractal2DPreviewExtension] = "png";
        SaveExtensions[SaveExtension.configSaveExtension] = "sav";
    }

    // singleton instance
    private static Persistent s_instance = null;
    public static Persistent Instance
    {
        get
        {
            if (s_instance == null)
            {
                s_instance = new Persistent();
                s_instance.Init();
            }
            return s_instance;
        }
    }

    public const string ComplexFractalFilenamePrefix = "Complex_";
    public const string ComplexFractalFilenameAnnex = ".fr";

    // properties
    private SinglePalette m_singlePalette;
    public SinglePalette SinglePalette
    {
        get { return m_singlePalette; }
        set
        {
            m_singlePalette = value;
            Save(() => SinglePalette);
        }
    }

    public int SelectedSinglePaletteIndex
    {
        get { return PlayerPrefs.GetInt("SelectedSinglePaletteIndex"); }
        set
        {
            PlayerPrefs.SetInt("SelectedSinglePaletteIndex", value);
            PlayerPrefs.Save();
        }
    }

    public int SelectedCompositePaletteIndex
    {
        get { return PlayerPrefs.GetInt("SelectedCompositePaletteIndex"); }
        set
        {
            PlayerPrefs.SetInt("SelectedCompositePaletteIndex", value);
            PlayerPrefs.Save();
        }
    }

    public int ExportedPictureWidth
    {
        get { return PlayerPrefs.GetInt("ExportedPictureWidth", 1366); }
        set
        {
            PlayerPrefs.SetInt("ExportedPictureWidth", value);
            PlayerPrefs.Save();
        }
    }

    public int ExportedPictureHeight
    {
        get { return PlayerPrefs.GetInt("ExportedPictureHeight", 768); }
        set
        {
            PlayerPrefs.SetInt("ExportedPictureHeight", value);
            PlayerPrefs.Save();
        }
    }

    public int Exported3dScreenshotWidth
    {
        get { return PlayerPrefs.GetInt("Exported3dScreenshotWidth", 1366); }
        set
        {
            PlayerPrefs.SetInt("Exported3dScreenshotWidth", value);
            PlayerPrefs.Save();
        }
    }

    public int Exported3dScreenshotHeight
    {
        get { return PlayerPrefs.GetInt("Exported3dScreenshotHeight", 768); }
        set
        {
            PlayerPrefs.SetInt("Exported3dScreenshotHeight", value);
            PlayerPrefs.Save();
        }
    }

    public Complex2DSceneScript.ExportSizeType ExportSizeType
    {
        get { return (Complex2DSceneScript.ExportSizeType)PlayerPrefs.GetInt("ExportSizeType", (int)Complex2DSceneScript.ExportSizeType.UserDefined); }
        set {
            PlayerPrefs.SetInt("ExportSizeType", (int)value);
            PlayerPrefs.Save();
        }
    }

    public bool ExportedPictureSetWidth
    {
        get { return Convert.ToBoolean(PlayerPrefs.GetInt("ExportedPictureSetWidth", Convert.ToInt32(true))); }
        set
        {
            PlayerPrefs.SetInt("ExportedPictureSetWidth", Convert.ToInt32(value));
            PlayerPrefs.Save();
        }
    }

    public string SavePicturesDirectory
    {
        get { return PlayerPrefs.GetString("SavePicturesDirectory", GetExportDir()); }
        set { PlayerPrefs.SetString("SavePicturesDirectory", value);
            PlayerPrefs.Save();
        }
    }

    public string Save3DPicturesDirectory
    {
        get { return PlayerPrefs.GetString("Save3DPicturesDirectory", GetExportDir()); }
        set
        {
            PlayerPrefs.SetString("Save3DPicturesDirectory", value);
            PlayerPrefs.Save();
        }
    }

    public string Save3DSTLDirectory
    {
        get { return PlayerPrefs.GetString("Save3DSTLDirectory", GetExportDir()); }
        set
        {
            PlayerPrefs.SetString("Save3DSTLDirectory", value);
            PlayerPrefs.Save();
        }
    }

    public bool ExportBiggestPart
    {
        get { return PlayerPrefs.GetInt("ExportBiggestPart", 1) > 0; }
        set
        {
            PlayerPrefs.SetInt("ExportBiggestPart", value ? 1 : 0);
            PlayerPrefs.Save();
        }
    }

    public CoefficientsDependencyType RandomCoefficientsDependenceType
    {
        get { return (CoefficientsDependencyType)PlayerPrefs.GetInt("RandomCoefficientsDependenceType", (int)CoefficientsDependencyType.Independent); }
        set {
            PlayerPrefs.SetInt("RandomCoefficientsDependenceType", (int)value);
            PlayerPrefs.Save();
            }
    }

    public Fractals3DSceneScript.QuaternionMappingType RandomQuaternionMappingType
    {
        get { return (Fractals3DSceneScript.QuaternionMappingType)PlayerPrefs.GetInt("RandomQuaternionMappingType", (int)Fractals3DSceneScript.QuaternionMappingType.X0_I_J); }
        set
        {
            PlayerPrefs.SetInt("RandomQuaternionMappingType", (int)value);
            PlayerPrefs.Save();
        }
    }

    public Quaternion3DSceneScript.ParametersPanelType LastSelectedQuat3DParametersType
    {
        get { return (Quaternion3DSceneScript.ParametersPanelType)PlayerPrefs.GetInt("LastSelectedQuat3DParametersType", (int)Quaternion3DSceneScript.ParametersPanelType.Computation); }
        set
        {
            PlayerPrefs.SetInt("LastSelectedQuat3DParametersType", (int)value);
            PlayerPrefs.Save();
        }
    }

    public PaletteSelectionSceneScript.PaletteType SelectedPaletteType
    {
        get { return (PaletteSelectionSceneScript.PaletteType)PlayerPrefs.GetInt("SelectedPaletteType", (int)PaletteSelectionSceneScript.PaletteType.Single); }
        set
        {
            PlayerPrefs.SetInt("SelectedPaletteType", (int)value);
            PlayerPrefs.Save();
        }
    }

    private IColorPalette m_selectedPalette;
    public IColorPalette SelectedPalette // not saved
    {
        get
        {
            if (m_selectedPalette == null)
            {
                m_selectedPalette = new SinglePalette(Color.red, Color.yellow); 
            }
            return m_selectedPalette;
        }
        set { m_selectedPalette = value;
            if (SelectedPalette is SinglePalette)
            {
                string propertyName = "SelectedPalette";
                SinglePalette palette = SelectedPalette as SinglePalette;
                string fileName = GetFileName(propertyName);
                Saver.Save(palette, fileName);
            }
        }
    }

    public int ProcessorCount
    {
        get { return m_processorCount; }
    }

    private bool m_random2DSettingsWithoutGenerate = false;
    public bool Random2DSettingsWithoutGenerate
    {
        get { return m_random2DSettingsWithoutGenerate; }
        set { m_random2DSettingsWithoutGenerate = value; }
    }

    public string SelectedComplex2DFile
    {
        get { return PlayerPrefs.GetString("SelectedComplex2DFile"); }
        set
        {
            PlayerPrefs.SetString("SelectedComplex2DFile", value);
            PlayerPrefs.Save();
        }
    }

    public string Seleced3DFractalFile
    {
        get { return PlayerPrefs.GetString("Seleced3DFractalFile"); }
        set
        {
            PlayerPrefs.SetString("Seleced3DFractalFile", value);
            PlayerPrefs.Save();
        }
    }

    Color m_selectedColor;
    public Color SelectedColor
    {
        get { return m_selectedColor; }
        set
        {
            m_selectedColor = value;
            Save(() => SelectedColor);
        }
    }

    private Color m_defaultBackgroundColor;
    public Color DefaultBackgroundColor
    {
        get { return m_defaultBackgroundColor; }
        set
        {
            m_defaultBackgroundColor = value;
            Save(() => DefaultBackgroundColor);
        }
    }

    private Color m_defaultForegroundColor;
    public Color DefaultForegroundColor
    {
        get { return m_defaultForegroundColor; }
        set
        {
            m_defaultForegroundColor = value;
            Save(() => DefaultForegroundColor);
        }
    }

    public bool CreateFractal;    // not saved - just flag for creating/editing of fractal

    QuaternionFractal3D m_quaternionFractal3D;
    public QuaternionFractal3D QuaternionFractal3D
    {
        get { return m_quaternionFractal3D; }
        set { m_quaternionFractal3D = value; }
    }

    // delegate for result of color picking
    public delegate void SetResultColor(Color color);
    private SetResultColor m_selColorDelegate = null;
    public SetResultColor SelColorDelegate // not saved - result delegate for ColorPickingScript
    {
        get { return m_selColorDelegate; }
        set { m_selColorDelegate = value; }
    }

    public delegate void SetPaletteDelegate(IColorPalette palette);
    private SetPaletteDelegate m_setPaletteDelegate = null;
    public SetPaletteDelegate SelectedPaletteDelegate // not saved - result delegate for Palette Selection script
    {
        get { return m_setPaletteDelegate; }
        set { m_setPaletteDelegate = value; }
    }

    public AxisSystemType AxisSystem
    {
        get { return (AxisSystemType)PlayerPrefs.GetInt("AxisSystemType", (int)AxisSystemType.Right); } // default is right
        set { 
            PlayerPrefs.SetInt("AxisSystemType", (int)value);
            PlayerPrefs.Save();
            Coord.SetRightHanded(AxisSystem == AxisSystemType.Right);
        }
    }

    /// ///////////////////////
    public EEditPaletteMode EditPaletteMode; // not saved - flag for CreateEditPaletteScript with mode of editing/creation
    public bool ShowOnlyRandomFractals = false; // not saved
    SetPaletteDelegate m_editedPaletteDelegate;
    public SetPaletteDelegate EditedPaletteDelegate // not saved - result delegate for CreateEditSinglePaletteScript
    {
        get { return m_editedPaletteDelegate; }
        set { m_editedPaletteDelegate = value; }
    }

    public UnityAction EditPaletteDelegate; // not saved - result delegate for CreateEditSinglePaletteScript
    /// ///////////////////////////////

    public FractalSelectionSceneScript.FractalType SelectedFractalType
    {
        get { return (FractalSelectionSceneScript.FractalType)PlayerPrefs.GetInt("SelectedFractalType", (int)FractalSelectionSceneScript.FractalType.Complex2D); }
        set
        {
            PlayerPrefs.SetInt("SelectedFractalType", (int)value);
            PlayerPrefs.Save();
        }
    }

    public Fractals3DSceneScript.UsedPowersType RandomComputationUsedPowersType
    {
        get { return (Fractals3DSceneScript.UsedPowersType)PlayerPrefs.GetInt("RandomComputationUsedPowersType", (int)Fractals3DSceneScript.UsedPowersType.MaxIsMaximum); }
        set
        {
            PlayerPrefs.SetInt("RandomComputationUsedPowersType", (int)value);
            PlayerPrefs.Save();
        }
    }

    public Fractals2DRandomSceneScript.UsedPowersType RandomComputationUsed2DPowersType
    {
        get { return (Fractals2DRandomSceneScript.UsedPowersType)PlayerPrefs.GetInt("RandomComputationUsed2DPowersType", (int)Fractals2DRandomSceneScript.UsedPowersType.MaxIsMaximum); }
        set
        {
            PlayerPrefs.SetInt("RandomComputationUsed2DPowersType", (int)value);
            PlayerPrefs.Save();
        }
    }

    private Vector3D m_planeNormalVector = new Vector3D(1,1,1);
    public Vector3D PlaneNormalVector
    {
        get { return m_planeNormalVector; }
        set { m_planeNormalVector = value; }
    }
    public void SetNormalX(double x)
    {
        m_planeNormalVector.x = x;
    }
    public void SetNormalY(double y)
    {
        m_planeNormalVector.y = y;
    }
    public void SetNormalZ(double z)
    {
        m_planeNormalVector.z = z;
    }

    public Fractals3DSceneScript.UsedAlgorithmType RandomComputationUsedAlgorithmType
    {
        get { return (Fractals3DSceneScript.UsedAlgorithmType)PlayerPrefs.GetInt("RandomComputationUsedAlgorithmType", (int)Fractals3DSceneScript.UsedAlgorithmType.Random); }
        set
        {
            PlayerPrefs.SetInt("RandomComputationUsedAlgorithmType", (int)value);
            PlayerPrefs.Save();
        }
    }

    public Fractals2DRandomSceneScript.UsedAlgorithmType RandomComputation2DUsedAlgorithmType
    {
        get { return (Fractals2DRandomSceneScript.UsedAlgorithmType)PlayerPrefs.GetInt("RandomComputation2DUsedAlgorithmType", (int)Fractals2DRandomSceneScript.UsedAlgorithmType.Random); }
        set
        {
            PlayerPrefs.SetInt("RandomComputation2DUsedAlgorithmType", (int)value);
            PlayerPrefs.Save();
        }
    }

    public Fractals2DRandomSceneScript.UsedPaletteType RandomComputation2DUsedPaletteType
    {
        get { return (Fractals2DRandomSceneScript.UsedPaletteType)PlayerPrefs.GetInt("RandomComputation2DUsedPaletteType", (int)Fractals2DRandomSceneScript.UsedPaletteType.Random); }
        set
        {
            PlayerPrefs.SetInt("RandomComputation2DUsedPaletteType", (int)value);
            PlayerPrefs.Save();
        }
    }


    public Fractals3DSceneScript.UsedQuaternionPartsType RandomComputationUsedQuaternionPartsType
    {
        get { return (Fractals3DSceneScript.UsedQuaternionPartsType)PlayerPrefs.GetInt("RandomComputationUsedQuaternionPartsType", (int)Fractals3DSceneScript.UsedQuaternionPartsType.Random); }
        set
        {
            PlayerPrefs.SetInt("RandomComputationUsedQuaternionPartsType", (int)value);
            PlayerPrefs.Save();
        }
    }

    public Fractals2DRandomSceneScript.UsedComplexPartsType RandomComputationUsedComplexPartsType
    {
        get { return (Fractals2DRandomSceneScript.UsedComplexPartsType)PlayerPrefs.GetInt("RandomComputationUsedComplexPartsType", (int)Fractals2DRandomSceneScript.UsedComplexPartsType.Random); }
        set
        {
            PlayerPrefs.SetInt("RandomComputationUsedComplexPartsType", (int)value);
            PlayerPrefs.Save();
        }
    }

    public QuaternionFractal3D.Fineness RandomComputationFineness
    {
        get { return (QuaternionFractal3D.Fineness)PlayerPrefs.GetInt("RandomComputationFineness", (int)QuaternionFractal3D.Fineness.High); }
        set
        {
            PlayerPrefs.SetInt("RandomComputationFineness", (int)value);
            PlayerPrefs.Save();
        }
    }

    public string SelectedFractalFile
    {
        get
        {
            switch (SelectedFractalType)
            {
                case FractalSelectionSceneScript.FractalType.Complex2D:
                    return SelectedComplex2DFile;
                case FractalSelectionSceneScript.FractalType.Quaternion3D:
                    return Seleced3DFractalFile;
                default:
                    Debug.Assert(false);
                    return "";
            }
        }
        set
        {
            switch (SelectedFractalType)
            {
                case FractalSelectionSceneScript.FractalType.Complex2D:
                    SelectedComplex2DFile = value;
                    break;
                case FractalSelectionSceneScript.FractalType.Quaternion3D:
                    Seleced3DFractalFile = value;
                    break;
                default:
                    Debug.Assert(false);
                    break;
            }
        }
    }

    public int RandomComputation3DCount
    {
        get { return PlayerPrefs.GetInt("RandomComputation3DCount", 5); }
        set {
            PlayerPrefs.SetInt("RandomComputation3DCount", value);
            PlayerPrefs.Save();
        }
    }

    public int RandomComputation2DCount
    {
        get { return PlayerPrefs.GetInt("RandomComputation2DCount", 5); }
        set
        {
            PlayerPrefs.SetInt("RandomComputation2DCount", value);
            PlayerPrefs.Save();
        }
    }

    public int RandomComputation3DMaxPower
    {
        get { return PlayerPrefs.GetInt("RandomComputation3DMaxPower", 4); }
        set
        {
            PlayerPrefs.SetInt("RandomComputation3DMaxPower", value);
            PlayerPrefs.Save();
        }
    }

    public int RandomComputation2DMaxPower
    {
        get { return PlayerPrefs.GetInt("RandomComputation2DMaxPower", 4); }
        set
        {
            PlayerPrefs.SetInt("RandomComputation2DMaxPower", value);
            PlayerPrefs.Save();
        }
    }

    public int RandomComputationMinParts
    {
        get { return PlayerPrefs.GetInt("RandomComputationMinParts", 1); }
        set
        {
            PlayerPrefs.SetInt("RandomComputationMinParts", value);
            PlayerPrefs.Save();
        }
    }

    public int RandomComputationMaxParts
    {
        get { return PlayerPrefs.GetInt("RandomComputationMaxParts", 4); }
        set
        {
            PlayerPrefs.SetInt("RandomComputationMaxParts", value);
            PlayerPrefs.Save();
        }
    }

    public string RandomComputationUsedPowers
    {
        get { return PlayerPrefs.GetString("RandomComputationUsedPowers", "2,0"); }
        set {
            PlayerPrefs.SetString("RandomComputationUsedPowers", value);
            PlayerPrefs.Save();
        }
    }

    public string RandomComputationUsed2DPowers
    {
        get { return PlayerPrefs.GetString("RandomComputationUsed2DPowers", "2,0"); }
        set
        {
            PlayerPrefs.SetString("RandomComputationUsed2DPowers", value);
            PlayerPrefs.Save();
        }
    }

    public float MaxSliderValue
    {
        get { return PlayerPrefs.GetFloat("MaxSliderValue", 1.2f); }
        set
        {
            PlayerPrefs.SetFloat("MaxSliderValue", value);
            PlayerPrefs.Save();
        }
    }

    public enum ComplexNumberForm
    {
        RealImaginary,
        Polar,
    };
    public ComplexNumberForm FormOfComplexNumbers
    {
        get { return (ComplexNumberForm)PlayerPrefs.GetInt("FormOfComplexNumbers", 0); }
        set
        {
            PlayerPrefs.SetInt("FormOfComplexNumbers", (int)value);
            PlayerPrefs.Save();
        }
    }

    public bool ComputeImmediatelyRandom2D = false;

    public int Progress = 0;

    // ==================== this is derived from properties =========================
    public int GetSelectedPaletteIndex(PaletteSelectionSceneScript.PaletteType paletteType)
    {
        switch (paletteType)
        {
        case PaletteSelectionSceneScript.PaletteType.Single:
            return SelectedSinglePaletteIndex;
        //case PaletteSelectionSceneScript.PaletteType.Composite:
        //    return SelectedCompositePaletteIndex;
        default:
            Debug.Assert(false);
            return 0;
        }
    }
    public void SetSelectedPaletteIndex(PaletteSelectionSceneScript.PaletteType paletteType, int value)
    {
        switch (paletteType)
        {
        case PaletteSelectionSceneScript.PaletteType.Single:
            SelectedSinglePaletteIndex = value;
            break;
        //case PaletteSelectionSceneScript.PaletteType.Composite:
        //    SelectedCompositePaletteIndex = value;
        //    break;
        default:
            Debug.Assert(false);
            break;
        }
    }

    public void SetStringProperty(string variableName, string value)
    {
        PlayerPrefs.SetString(variableName, value);
        PlayerPrefs.Save();
    }

    public string GetStringProperty(string variableName, string defaultValue)
    {
        return PlayerPrefs.GetString(variableName, defaultValue);
    }

    public string GetPersistentDir()
    {
        return Application.persistentDataPath + "/Persistent/";
    }

    public string GetExportDir()
    {
        return Application.persistentDataPath + "/Export/";
    }

    public static string GetFractalsDir()
    {
        return Application.persistentDataPath + "/Fractals/";
    }

    public static string GetTempDir()
    {
        return Application.persistentDataPath + "/Temp/";
    }

    public static string GetPackingDir()
    {
        return Application.persistentDataPath + "/Zip/";
    }
    public static string GetExtractDir()
    {
        return Application.persistentDataPath + "/Extract/";
    }
    public static string Get3DFractalsDir()
    {
        return Application.persistentDataPath + "/Fractals/3D/";
    }
    public static string Get3DFractalsTempGraphicsDir()
    {
        return Get3DFractalsDir() + "TempGraphics/";
    }
    public static string GetRandom3DFractalDir()
    {
        return Get3DFractalsDir() + "Random/";
    }
    public static string GetRandom2DFractalDir()
    {
        return GetFractalsDir() + "Random/";
    }

    public static string GetFractalDefinitionFilename()
    {
        return "definition." + Persistent.GetExtension(Persistent.SaveExtension.fractal3DDefinitionExtension);
    }
    public static string GetGraphicFilename()
    {
        return "graphic." + Persistent.GetExtension(Persistent.SaveExtension.fractal3DGraphicExtension);
    }

    public static string GetSinglePaletteSaveFile()
    {
        return Application.persistentDataPath + "/" + "SinglePalettes.sav";
    }

    public static string GetCompositePaletteSaveFile()
    {
        return Application.persistentDataPath + "/" + "CompositePalettes.sav";
    }

    public static string GetNotComputedTextureSmallPath()
    {
        int textureSize = FractalSelectionSceneScript.SmallTextureSize;
        return GetFractalsDir() + String.Format("NotComputed_{0}.png", textureSize);
    }
    public static string GetNotComputedTextureBigPath()
    {
        return GetFractalsDir() + "NotComputed.png";
    }

    public static string GetSmallTexturePathFromFractalPath(string path)
    {
        string fractalSavePathWithoutSufix = Path.Combine(Path.GetDirectoryName(path), Path.GetFileNameWithoutExtension(path));
        int textureSize = FractalSelectionSceneScript.SmallTextureSize;
        string result = string.Format("{0}_{1}.png", fractalSavePathWithoutSufix, textureSize);
        return result;
    }

    public static string GetBigTexturePathFromFractalPath(string path)
    {
        string fractalSavePathWithoutSufix = Path.Combine(Path.GetDirectoryName(path), Path.GetFileNameWithoutExtension(path));
        string path2 = string.Format("{0}_Four.png", fractalSavePathWithoutSufix);
        return path2;
    }

    public static void DeleteQuaternionFractalScreenshots(string fractalPath)
    {
        File.Delete(Persistent.GetSmallTexturePathFromFractalPath(fractalPath));
        File.Delete(Persistent.GetBigTexturePathFromFractalPath(fractalPath));
    }

    public static string GetExtension(SaveExtension extension)
    {
        return SaveExtensions[extension];
    }

    public static string GetExtensionWithDot(SaveExtension extension)
    {
        return "." + SaveExtensions[extension];
    }

    // private functions
    void Init()
    {
        s_instance = this;

        LoadSavedObjects();

        m_processorCount = UnityEngine.SystemInfo.processorCount;
        //SelectedPalette = new SinglePalette(SinglePalette);
    }

    // ========================= functions for saving and loading ============================

    void LoadSavedObjects()
    {
        Load(() => SinglePalette, new SinglePalette(Color.yellow, Color.red)); 
        Load(() => SelectedColor, Color.cyan);
        Load(() => DefaultBackgroundColor, new UnityEngine.Color(248 / 255.0f, 203 / 255.0f, 4 / 255.0f, 1));
        Load(() => DefaultForegroundColor, Color.blue);

        string propertyName = "SelectedPalette";
        SinglePalette value;
        string fileName = GetFileName(propertyName);
        bool result = Saver.Load(fileName, out value);
        SelectedPalette = value;
        // other things are stored in player prefs
    }

    void Save<T>(Expression<Func<T>> propertyLambda)
    {
        string fileName = GetFileName(GetPropertyName<T>(propertyLambda));
        var getter = propertyLambda.Compile();
        T t = getter();
        Save(t, fileName);
    }

    void Save<T>(T objectName, string filename)
    {
        XmlSerializer mySerializer = new XmlSerializer(typeof(T));
        FileStream file = File.Create(filename);
        mySerializer.Serialize(file, objectName);
        file.Close();
    }

    void Load<T>(Expression<Func<T>> propertyLambda)
    {
        string propertyName = GetPropertyName<T>(propertyLambda);
        LoadProperty<T>(propertyName);
    }
    void Load<T>(Expression<Func<T>> propertyLambda, T defaultValue)
    {
        string propertyName = GetPropertyName(propertyLambda);
        LoadProperty(propertyName, defaultValue);
    }

    bool LoadProperty<T>(string propertyName)
    {
        PropertyInfo pi = GetType().GetProperty(propertyName);
        string fileName = GetFileName(propertyName);

        T value;
        bool result = Saver.Load(fileName, out value);

        if (result)
        {
            pi.SetValue(this, value, null);
        }
        else
        {
            pi.SetValue(this, default(T), null);
        }
        return result;
    }
    bool LoadProperty<T>(string propertyName, T defaultValue)
    {
        PropertyInfo pi = GetType().GetProperty(propertyName);
        string fileName = GetFileName(propertyName);

        T value;
        bool result = Saver.Load(fileName, out value);

        if (result)
        {
            pi.SetValue(this, value, null);
        }
        else
        {
            pi.SetValue(this, defaultValue, null);
        }
        return result;
    }

    string GetFileName(string propertyName)
    {
        string filename = GetPersistentDir() + propertyName + ".sav";
        return filename;
    }

    string GetPropertyName<T>(Expression<Func<T>> propertyLambda)
    {
        var me = propertyLambda.Body as MemberExpression;

        if (me == null)
        {
            throw new ArgumentException("You must pass a lambda of the form: '() => Class.Property' or '() => object.Property'");
        }

        return me.Member.Name;
    }

    static private Dictionary<SaveExtension, string> SaveExtensions = new Dictionary<SaveExtension, string>();

    // ========================= end of functions for saving and loading ============================

    int m_processorCount;
}
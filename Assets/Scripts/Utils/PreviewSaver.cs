﻿using UnityEngine;
using System.Collections;

using System.IO;

public class PreviewSaver
{
    public static string GetPreviewFileName(string fractalFilename, int size)
    {
        int position = fractalFilename.LastIndexOf(".");
        string tillDot = fractalFilename.Substring(0, position);
        string previewFileName = tillDot + "_" + string.Format("{0}.png", size);
        return previewFileName;
    }
}
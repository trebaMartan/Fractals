﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Utils
{
    // class for referencing of value types
    sealed public class Ref<T> where T : System.IComparable<T>
    {
        private Func<T> getter;
        private Action<T> setter;
        public Ref(Func<T> getter, Action<T> setter, OnNotifyChangeDelegate notifyChangeDelegate = null)
        {
            this.getter = getter;
            this.setter = setter;
            m_notifyChangeDelegate = notifyChangeDelegate;
        }
        public T Value
        {
            get { return getter(); }
            set {
                T oldValue = Value;
                setter(value);
                if (oldValue.CompareTo(value) != 0 && m_notifyChangeDelegate != null)
                {
                    m_notifyChangeDelegate();
                }
            }
        }

        public OnNotifyChangeDelegate NotifyChangeDelegate
        {
            get { return m_notifyChangeDelegate; }
            set { m_notifyChangeDelegate = value; }
        }

        OnNotifyChangeDelegate m_notifyChangeDelegate;
    }
}
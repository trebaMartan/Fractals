﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.ComponentModel;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    // helper class for serialization of boxes coordinates
    public class B
    {
        public B() { /*a = b =*/ c = 1; }
        //public int i;
        [XmlAttribute]
        public int x { get; set; }
        [XmlAttribute]
        public int y { get; set; }
        [XmlAttribute]
        public int z { get; set; }
        //[DefaultValue(1)]
        //[XmlAttribute]
        //public int a { get; set; }
        //[DefaultValue(1)]
        //[XmlAttribute]
        //public int b { get; set; }
        [DefaultValue(1)]
        [XmlAttribute]
        public int c { get; set; }
    }

    public class Fractal3DGraphicSaver
    {
        public Fractal3DGraphicSaver()
        {

        }
        public Fractal3DGraphicSaver(float basicCubeSize, List<B> boxes)
        {
            // in originalBoxes are real coordinates => change it into int positions in basic grid
            m_basicCubeSize = basicCubeSize;
            m_boxes = boxes;
        }
        public float BasicCubeSize
        {
            get { return m_basicCubeSize; }
            set { m_basicCubeSize = value; }
        }
        public List<B> Boxes { get { return m_boxes; } }

        public void Save(string path)
        {
            Saver.Save<Fractal3DGraphicSaver>(this, path);
        }

        public static bool Load(string path, out Fractal3DGraphicSaver loadedGraphic)
        {
            bool ok = Saver.Load(path, out loadedGraphic);
            return ok;
        }

        private float m_basicCubeSize;
        private List<B> m_boxes = new List<B>();
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Assets.Scripts.Utils;
using System;

// saver (and loader) class
static public class Saver
{
    public static void SaveSinglePalettes(List<SinglePalette> singlePalettes)
    {
        string path = Persistent.GetSinglePaletteSaveFile();
        SaveList<SinglePalette>(path, singlePalettes);
    }

    public static List<SinglePalette> LoadSinglePalettes()
    {
        List<SinglePalette> singlePalettes = new List<SinglePalette>();
        string path = Persistent.GetSinglePaletteSaveFile();
        singlePalettes = LoadList<SinglePalette>(path);
        return singlePalettes;
    }

    public static void SaveCompositePalettes(List<CompositePalette> palettes)
    {
        string path = Persistent.GetCompositePaletteSaveFile();
        SaveList<CompositePalette>(path, palettes);
    }

    public static List<CompositePalette> LoadCompositePalettes()
    {
        List<CompositePalette> palettes = new List<CompositePalette>();
        string path = Persistent.GetCompositePaletteSaveFile();
        palettes = LoadList<CompositePalette>(path);
        return palettes;
    }

    public static void SaveCubeCoordinates(string path, List<Vector3> coordinates)
    {
        SaveList<Vector3>(path, coordinates);
    }

    public static List<Vector3> LoadCubeCoordinates(string path)
    {
        List<Vector3> coordinates = new List<Vector3>();       
        coordinates = LoadList<Vector3>(path);
        return coordinates;
    }

    public static void SaveBoxes(string path, List<Box> boxes)
    {
        SaveList<Box>(path, boxes);
    }

    public static List<Box> LoadBoxes(string path)
    {
        List<Box> boxes = new List<Box>();
        boxes = LoadList<Box>(path);
        return boxes;
    }

    public static void Save<T>(T @object, string path)
    {
        XmlSerializer mySerializer = new XmlSerializer(typeof(T));
        FileStream file = File.Create(path);
        mySerializer.Serialize(file, @object);
        file.Close();
    }

    public static string Serialize<T>(T serializedObject)
    {
        XmlSerializer mySerializer = new XmlSerializer(typeof(T));
        MemoryStream stream = new MemoryStream();
        mySerializer.Serialize(stream, serializedObject);
        return System.Text.Encoding.ASCII.GetString(stream.ToArray());
    }

    public static string Serialize(object serializedObject, Type objectType)
    {
        XmlSerializer mySerializer = new XmlSerializer(objectType);
        MemoryStream stream = new MemoryStream();
        mySerializer.Serialize(stream, serializedObject);
        return System.Text.Encoding.ASCII.GetString(stream.ToArray());
    }

    public static Stream ToStream(this string str)
    {
        MemoryStream stream = new MemoryStream();
        StreamWriter writer = new StreamWriter(stream);
        writer.Write(str);
        writer.Flush();
        stream.Position = 0;
        return stream;
    }

    public static T Deserialize<T>(string source)
    {
        T obj;
        Stream input = source.ToStream();
        try
        {
            XmlSerializer ser = new XmlSerializer(typeof(T));
            obj = (T)ser.Deserialize(input);
        }
        finally
        {
            //if (file != null)
            //    file.Close();
        }
        return obj;
    }

    public static object Deserialize(string source, Type objectType)
    {
        object obj;
        Stream input = source.ToStream();
        try
        {
            XmlSerializer ser = new XmlSerializer(objectType);
            obj = ser.Deserialize(input);
        }
        finally
        {
            //if (file != null)
            //    file.Close();
        }
        return obj;
    }

    public static T Load<T>(string path)
    {
        T obj;
        if (!File.Exists(path))
            return default(T);

        FileStream file = File.Open(path, FileMode.Open);
        try
        {
            XmlSerializer ser = new XmlSerializer(typeof(T));
            obj = (T)ser.Deserialize(file);
        }
        finally
        {
            if (file != null)
                file.Close();
        }
        return obj;
    }

    public static bool Load<T>(string path, out T returnValue)
    {
        T obj;
        returnValue = default(T);
        bool result = false;
        if (!File.Exists(path))
            return false;

        FileStream file = File.Open(path, FileMode.Open);
        try
        {
            XmlSerializer ser = new XmlSerializer(typeof(T));
            obj = (T)ser.Deserialize(file);
            result = true;
        }
        finally
        {
            if (file != null)
                file.Close();
        }
        returnValue = obj;
        return result;
    }

    public static string GetHashString(string str)
    {
        int hash = str.GetHashCode();
        string stringHash = "H" + hash.ToString("X8");
        return stringHash;
    }    

    static List<T> LoadList<T>(string path)
    {
        List<T> list = new List<T>();
        if (!File.Exists(path))
            return list;

        XmlSerializer ser = new XmlSerializer(typeof(List<T>));
        FileStream file = File.Open(path, FileMode.Open);

        list = (List<T>)ser.Deserialize(file);
        file.Close();
        return list;
    }

    static void SaveList<T>(string path, List<T> list)
    {
        XmlSerializer ser = new XmlSerializer(typeof(List<T>));
        FileStream file = File.Create(path);
        ser.Serialize(file, list);
        file.Close();
    }
}
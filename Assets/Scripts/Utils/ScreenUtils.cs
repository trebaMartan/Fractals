﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    public class ScreenUtils
    {
        static public float GetMaxRelativeScreenDistance(Camera camera, List<Vector3> limitPoints)
        {
            float max = 0;
            foreach (Vector3 vec in limitPoints)
            {
                max = System.Math.Max(max, GetRelativeScreenDistance(camera, vec));
            }
            return max;
        }

        static float GetRelativeScreenDistance(Camera camera, Vector3 point)
        {
            Vector3 screenPos = camera.WorldToViewportPoint(point);
            float lengthX = System.Math.Abs(screenPos.x - 0.5f);
            float lengthY = System.Math.Abs(screenPos.y - 0.5f);
            return System.Math.Max(lengthX, lengthY);
        }


    }
}

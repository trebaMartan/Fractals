﻿using UnityEngine;
using System.Collections;
using System.IO;
using Assets.Scripts.Utils;
using System.Collections.Generic;
using System;

public class TextureUtils
{

    public static void FillWithColor(Texture2D texture, Color color)
    {
        if (texture == null)
            return;

        var fillColorArray = texture.GetPixels();
        for (var i = 0; i < fillColorArray.Length; ++i)
        {
            fillColorArray[i] = color;
        }

        texture.SetPixels(fillColorArray);
        texture.Apply();
    }

    public static void WriteColorsHorizontal(Texture2D texture, float minValue, float maxValue, Func<float, Color> colorFunction)
    {
        if (texture == null)
            return;

        for (int x = 0; x < texture.width; x++)
        {
            float coef = x / (float)texture.width;
            float coefTransformed = minValue + (maxValue - minValue) * coef;
            Color color = colorFunction(coefTransformed);
            for (int y = 0; y < texture.height; y++)
            {
                texture.SetPixel(x, y, color);
            }
        }
        texture.Apply();
    }

    public static void WriteColoringAlgorithmIntoTexture(Texture2D texture, ColoringAlgorithm coloringAlgorithm, Rect rect, bool discrete = false)
    {
        if (texture == null)
            return;

        int minX = Mathf.FloorToInt(rect.xMin);
        int maxX = Mathf.FloorToInt(rect.xMax);
        int sizeX = maxX - minX;
        for (int x = minX; x < maxX; x++)
        {
            if (x < 0)
                continue;
            if (x >= texture.width)
                break;

            float relRatio = (x - rect.xMin) / (float)(sizeX - 1);
            Color color = discrete ? coloringAlgorithm.GetDiscretizedColorInBasicInterval(relRatio) : coloringAlgorithm.GetColorInBasicInterval(relRatio);

            for (int y = Mathf.FloorToInt(rect.yMin); y < rect.yMax; y++)
            {
                if (y < 0)
                    continue;
                if (y >= texture.height)
                    break;

                texture.SetPixel(x, y, color);
            }
        }
        texture.Apply();
    }

    public static void WritePaletteIntoTexture(Texture2D texture, IColorPalette palette, Rect rect)
    {
        if (texture == null || palette == null)
            return;

        int minX = Mathf.FloorToInt(rect.xMin);
        int maxX = Mathf.FloorToInt(rect.xMax);
        int sizeX = maxX - minX;
        for (int x = minX; x < maxX; x++)
        {
            if (x < 0)
                continue;
            if (x >= texture.width)
                break;

            float relRatio = (x - rect.xMin) / (float)(sizeX - 1);
            Color color = palette.GetColor(relRatio);

            for (int y = Mathf.FloorToInt(rect.yMin); y < rect.yMax; y++)
            {
                if (y < 0)
                    continue;
                if (y >= texture.height)
                    break;

                texture.SetPixel(x, y, color);
            }
        }
        texture.Apply();
    }

    public static void WriteTextureIntoTexture(Texture2D texture, Texture2D importedTexture, Rect rect)
    {
        if (texture == null || importedTexture == null)
            return;

        int minX = Mathf.FloorToInt(rect.xMin);
        int maxX = Mathf.FloorToInt(rect.xMax);
        
        int sourceMaxY = importedTexture.height;
        int sourceMaxX = importedTexture.width;
        int xSource = 0;
        for (int x = minX; x < maxX && xSource < sourceMaxX; x++, xSource++)
        {
            if (x < 0)
                continue;
            if (x >= texture.width)
                break;

            int ySource = 0;
            for (int y = Mathf.FloorToInt(rect.yMin); y < rect.yMax && ySource < sourceMaxY; y++, ySource++)
            {
                if (y < 0)
                    continue;
                if (y >= texture.height)
                    break;

                Color color = importedTexture.GetPixel(xSource, ySource);
                texture.SetPixel(x, y, color);
            }
        }
        texture.Apply();
    }

    static public void MakeTransparentBorders(Texture2D texture, int borderInPixels, float radiusOfCorners = 0)
    {
        Color transparent = new Color(0, 0, 0, 0);
        for (int x = 0; x < borderInPixels; x++)
        {
            for (int y = 0; y < texture.height; y++)
            {
                texture.SetPixel(x, y, transparent);
            }
        }
        for (int x = texture.width - borderInPixels; x < texture.width; x++)
        {
            for (int y = 0; y < texture.height; y++)
            {
                texture.SetPixel(x, y, transparent);
            }
        }

        for (int x = 0; x < texture.width; x++)
        {
            for (int y = texture.height - borderInPixels; y < texture.height; y++)
            {
                texture.SetPixel(x, y, transparent);
            }
        }
        for (int x = 0; x < texture.width; x++)
        {
            for (int y = 0; y < borderInPixels; y++)
            {
                texture.SetPixel(x, y, transparent);
            }
        }

        // corners
        int minTextureSize = Mathf.Min(texture.width, texture.height);
        radiusOfCorners = Mathf.Min(Mathf.Min((minTextureSize - 2 * borderInPixels) / 2), radiusOfCorners);
        int radiusInt = Mathf.CeilToInt(radiusOfCorners);
        float xCenter = borderInPixels + 0.0f + radiusOfCorners;
        float yCenter = borderInPixels + 0.0f + radiusOfCorners;
        radiusInt += 1;
        Vector2 centerOfCorner = new Vector2(xCenter, yCenter);
        for (int x = -radiusInt; x < radiusInt; x++)
        {
            for (int y = -radiusInt; y < radiusInt; y++)
            {
                Vector2 movement = new Vector2(x, y);
                float dist = movement.magnitude - radiusOfCorners;
                if (dist >= 0 && x <= 0 && y <= 0)
                {
                    Vector2 pixelCoord = centerOfCorner + movement;
                    float alpha = dist > 1 ? 0 : 1 - dist;
                    transparent = texture.GetPixel((int)pixelCoord.x, (int)pixelCoord.y);
                    transparent.a = alpha;
                    texture.SetPixel((int)pixelCoord.x, (int)pixelCoord.y, transparent);
                }
            }
        }
        centerOfCorner.y = texture.height - 1 - borderInPixels - radiusOfCorners;
        for (int x = -radiusInt; x < radiusInt; x++)
        {
            for (int y = -radiusInt; y < radiusInt + 1; y++)
            {
                Vector2 movement = new Vector2(x, y);
                float dist = movement.magnitude - radiusOfCorners;
                if (dist >= 0 && x <= 0 && y >= 0)
                {
                    Vector2 pixelCoord = centerOfCorner + movement;
                    float alpha = dist > 1 ? 0 : 1 - dist;
                    transparent = texture.GetPixel((int)pixelCoord.x, (int)pixelCoord.y);
                    transparent.a = alpha;
                    texture.SetPixel((int)pixelCoord.x, (int)pixelCoord.y, transparent);
                }
            }
        }

        centerOfCorner.x = texture.width - 1 - borderInPixels - radiusOfCorners;
        for (int x = -radiusInt; x < radiusInt; x++)
        {
            for (int y = -radiusInt; y < radiusInt + 1; y++)
            {
                Vector2 movement = new Vector2(x, y);
                float dist = movement.magnitude - radiusOfCorners;
                if (dist >= 0 && x >= 0 && y >= 0)
                {
                    Vector2 pixelCoord = centerOfCorner + movement;
                    float alpha = dist > 1 ? 0 : 1 - dist;
                    transparent = texture.GetPixel((int)pixelCoord.x, (int)pixelCoord.y);
                    transparent.a = alpha;
                    texture.SetPixel((int)pixelCoord.x, (int)pixelCoord.y, transparent);
                }
            }
        }
        centerOfCorner.y = borderInPixels + 0.0f + radiusOfCorners;
        for (int x = -radiusInt; x < radiusInt; x++)
        {
            for (int y = -radiusInt; y < radiusInt + 1; y++)
            {
                Vector2 movement = new Vector2(x, y);
                float dist = movement.magnitude - radiusOfCorners;
                if (dist >= 0 && x >= 0 && y <= 0)
                {
                    Vector2 pixelCoord = centerOfCorner + movement;
                    float alpha = dist > 1 ? 0 : 1 - dist;
                    transparent = texture.GetPixel((int)pixelCoord.x, (int)pixelCoord.y);
                    transparent.a = alpha;
                    texture.SetPixel((int)pixelCoord.x, (int)pixelCoord.y, transparent);
                }
            }
        }

        texture.Apply();
    }

    public static Texture2D ScaleTexture(Texture2D source, int targetWidth, int targetHeight)
    {
        Texture2D result = new Texture2D(targetWidth, targetHeight, source.format, false);
        float incX = (1.0f / (float)targetWidth);
        float incY = (1.0f / (float)targetHeight);
        for (int i = 0; i < result.height; ++i)
        {
            for (int j = 0; j < result.width; ++j)
            {
                Color newColor = source.GetPixelBilinear((float)j / (float)result.width, (float)i / (float)result.height);
                result.SetPixel(j, i, newColor);
            }
        }
        result.Apply();
        return result;
    }

    public static void MixTextureWithBackgroundColor(Texture2D texture, UnityEngine.Color unityBackgroundColor)
    {
        int imageHeight = texture.height;
        int imageWidth = texture.width;
        float alpha0 = unityBackgroundColor.a;

        if (alpha0 == 1)
        {
            float resultAlpha = 1;
            for (int i = 0; i < imageWidth; i++)
            {
                for (int j = 0; j < imageHeight; j++)
                {
                    UnityEngine.Color foregoundColor = texture.GetPixel(i, j);

                    float alpha1 = foregoundColor.a;
                    UnityEngine.Color resultColor = unityBackgroundColor * (1 - alpha1) + foregoundColor * alpha1;
                    resultColor.a = resultAlpha;
                    texture.SetPixel(i, j, resultColor);
                }
            }
        }
        else
        {
            for (int i = 0; i < imageWidth; i++)
            {
                for (int j = 0; j < imageHeight; j++)
                {
                    UnityEngine.Color foregoundColor = texture.GetPixel(i, j);

                    float alpha1 = foregoundColor.a;
                    float resultAlpha = alpha0 + alpha1 * (1 - alpha0);
                    UnityEngine.Color resultColor = foregoundColor * alpha1 + unityBackgroundColor * (1 - alpha1);
                    resultColor.a = resultAlpha;
                    texture.SetPixel(i, j, resultColor);
                }
            }
        }
    }

            public static void SaveTextureToPng(string filename, Texture2D texture)
    {
        byte[] bytes = texture.EncodeToPNG();
        File.WriteAllBytes(filename, bytes);
    }

    public static bool GetTextureFromPng(string filename, out Texture2D texture)
    {
        // read texture from png file
        texture = LoadPNG(filename);

        return texture != null;
    }

    public static Texture2D LoadPNG(string filePath)
    {
        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        }
        return tex;
    }

    public static bool HasColorCountBiggerThan(Texture2D texture, int minCount)
    {
        int width = texture.width;
        int height = texture.height;
        HashSet<Color> colors = new HashSet<Color>();
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                colors.Add(texture.GetPixel(x, y));
                if (colors.Count > minCount)
                {
                    return true;
                }
            }
        }
        return false;
    }
}

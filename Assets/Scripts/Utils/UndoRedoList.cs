﻿using Assets.Scripts.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Serialization;

namespace Assets.Scripts.Utils
{
    public class  SerializedProperty
    {
        public string PropertyName;
        public string PropertyValueString;
        public Type ObjectType;
    }

    public class UndoRedoAttribute : Attribute
    {
    }


    // class for storing of object properties - for undo and redo
    public class UndoRedoList
    {
        public int MaxUndoSize = 30;
        
        public bool IsUndoAvailable()
        {
            return m_objectPropertiesList.Count > 0 && GetCurrentListPosition() > 0;
        }

        public UndoRedoList(UndoRedoList old)
        {
            foreach (List<SerializedProperty> list in old.m_objectPropertiesList)
            {
                m_objectPropertiesList.Add(list);
            }
            m_object = old.m_object;
            m_currentListPosition = old.m_currentListPosition;
        }

        public UndoRedoList()
        {
        }

        public object Clone()
        {
            UndoRedoList newList = new UndoRedoList(this);
            return newList;
        }

        public bool IsRedoAvailable()
        {
            return m_objectPropertiesList.Count > 0 && !IsPositionLast();
        }

        public void SetObject(object @object)
        {
            if (m_object == @object)
            {
                return;
            }
            m_object = @object;
            Clear();
        }

        public string Undo()
        {
            // returns name of changed property
            if (!IsUndoAvailable())
            {
                return "";
            }
            SetCurrentListPosition(GetCurrentListPosition() - 1);
            return GetChangedPropertyAndSetProperties();
        }

        public string Redo()
        {
            // returns name of changed property
            if (!IsRedoAvailable())
            {
                return "";
            }
            SetCurrentListPosition(GetCurrentListPosition() + 1);
            return GetChangedPropertyAndSetProperties();
        }

        public void Clear()
        {
            m_objectPropertiesList.Clear();
            SetCurrentListPosition(-1);
        }

        public void AddNewValue()
        {
            object @object = GetObject();
            List<SerializedProperty> serializedProperties = GetSerializedProperties();
            if (!IsDifferentFromPosition(serializedProperties, GetCurrentListPosition()))
            {
                return;
            }
            List<SerializedProperty> differentProperties = GetDifferentProperties(serializedProperties, GetCurrentUndoRedoProperties());
            if (!IsPositionLast())
            {
                // cut positions after actual position in list
                int nextPosition = GetCurrentListPosition() + 1;
                if (IsDifferentFromPosition(serializedProperties, nextPosition))
                {
                    m_objectPropertiesList.RemoveRange(GetCurrentListPosition() + 1, m_objectPropertiesList.Count - GetCurrentListPosition() - 1);
                    Debug.Assert(m_objectPropertiesList.Count == GetCurrentListPosition());
                }
                else
                {
                    // same change as is in the list already = redo
                    SetCurrentListPosition(nextPosition);
                    return;
                }
            } 
            m_objectPropertiesList.Add(GetSerializedProperties());
            if (m_objectPropertiesList.Count > Computation.PlatformConstants.UndoRedoListSize)
            {
                m_objectPropertiesList.RemoveAt(0);
            }
            SetCurrentListPosition(m_objectPropertiesList.Count - 1);
        }

        // ========================= private functions ============================
        private bool IsIndexValid(int index)
        {
            return index >= 0 && index < m_objectPropertiesList.Count;
        }

        private bool IsDifferentFromPosition(List<SerializedProperty> serializedProperties, int index)
        {
            if (!IsIndexValid(index))
            {
                return true;
            }
            List<SerializedProperty> serializedPropertiesAtPosition = m_objectPropertiesList[index];
            List<SerializedProperty> differentProperties = GetDifferentProperties(serializedProperties, serializedPropertiesAtPosition);
            return differentProperties.Count > 0;
        }

        private List<SerializedProperty> GetSerializedProperties()
        {
            object @object = GetObject();
            List<SerializedProperty> result = new List<SerializedProperty>();
            if (@object == null)
            {
                return result;
            }

            PropertyInfo[] properties = @object.GetType().GetProperties();
            foreach (PropertyInfo property in properties)
            {
                if (Attribute.IsDefined(property, typeof(XmlIgnoreAttribute)) &&
                    !Attribute.IsDefined(property, typeof(UndoRedoAttribute)))
                {
                    continue;
                }
                SerializedProperty serializedProperty = new SerializedProperty();
                object propertyValue = OtherUtils.GetPropValue(@object, property.Name);
                if (propertyValue == null)
                {
                    continue;
                }
                Type propertyType = propertyValue.GetType();
                serializedProperty.PropertyValueString = Saver.Serialize(propertyValue, propertyType);
                serializedProperty.ObjectType = propertyType;
                serializedProperty.PropertyName = property.Name;
                result.Add(serializedProperty);
            }
            return result;
        }

        private SerializedProperty GetProperty(List<SerializedProperty> properties, string name)
        {
            if (properties == null)
            {
                return new SerializedProperty();
            }
            foreach(SerializedProperty property in properties)
            {
                if (property.PropertyName == name)
                {
                    return property;
                }
            }
            return null;
        }

        private void SetProperty(object @object, SerializedProperty property)
        {
            PropertyInfo[] properties = @object.GetType().GetProperties();
            foreach (PropertyInfo objectProperty in properties)
            {
                if (objectProperty.Name == property.PropertyName)
                {
                    string propertyValueString = property.PropertyValueString;
                    object propertyValue = Saver.Deserialize(propertyValueString, property.ObjectType);
                    OtherUtils.SetPropValue(@object, objectProperty.Name, propertyValue);
                    break;
                }
            }
        }

        private List<SerializedProperty> GetDifferentProperties(List<SerializedProperty> newProperties, List<SerializedProperty> oldProperties)
        {
            List<SerializedProperty> differentProperties = new List<SerializedProperty>();
            foreach (SerializedProperty newProperty in newProperties)
            {
                if (newProperty == null)
                {
                    continue;
                }
                SerializedProperty oldProperty = GetProperty(oldProperties, newProperty.PropertyName);
                if (oldProperty == null || oldProperty.PropertyValueString != newProperty.PropertyValueString)
                {
                    differentProperties.Add(newProperty);
                }
            }
            return differentProperties;
        }

        private void SetPropertiesFromList(object @object, List<SerializedProperty> properties)
        {
            List<SerializedProperty> differentProperties = GetDifferentProperties(properties, GetSerializedProperties());
            for (int i = 0; i < 10 && differentProperties.Count != 0; i++)
            {
                foreach (SerializedProperty differentProperty in differentProperties)
                {
                    SetProperty(@object, differentProperty);
                }
                List<SerializedProperty> objectProperties = GetSerializedProperties();
                differentProperties = GetDifferentProperties(properties, objectProperties);
            }
        }

        private string GetChangedPropertyAndSetProperties()
        {
            List<SerializedProperty> undoRedoProperties = GetCurrentUndoRedoProperties();
            List<SerializedProperty> objectProperties = GetSerializedProperties();
            List<SerializedProperty> differentProperties = GetDifferentProperties(objectProperties, undoRedoProperties);
            SetPropertiesFromList(GetObject(), undoRedoProperties);
            return differentProperties.Count > 0 ? differentProperties[0].PropertyName : "";
        }

        private int GetCurrentListPosition()
        {
            return m_currentListPosition;
        }

        private void SetCurrentListPosition(int position)
        {
            if (!IsIndexValid(position))
            {
                return;
            }
            m_currentListPosition = position;
        }

        private List<SerializedProperty> GetCurrentUndoRedoProperties()
        {
            if (!IsIndexValid(GetCurrentListPosition()))
            {
                return null;
            }
            return m_objectPropertiesList[GetCurrentListPosition()];
        }

        private bool IsPositionLast()
        {
            return GetCurrentListPosition() == m_objectPropertiesList.Count - 1;
        }

        private object GetObject()
        {
            return m_object;
        }

        List<List<SerializedProperty>> m_objectPropertiesList = new List<List<SerializedProperty>>();
        object m_object = null;
        int m_currentListPosition = -1;
    }
}
